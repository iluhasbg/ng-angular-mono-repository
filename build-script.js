const fs = require('fs-extra');
const concat = require('concat');

(async function build() {

  const files = [
    './dist/app-search/runtime-es5.js',
    './dist/app-search/polyfills-es5.js',
    './dist/app-search/main-es5.js',

  ];
  await fs.ensureDir('elements');
  await concat(files, 'elements/search.js')
  await fs.copy('./dist/app-search/assets/', 'elements/assets/')
})();
// const files = [
//   './dist/ipid-search/runtime-es5.0811dcefd377500b5b1a.js',
//   './dist/ipid-search/runtime-es2015.0811dcefd377500b5b1a.js',
//   './dist/ipid-search/polyfills-es5.c040e44e9399198e968f.js',
//   './dist/ipid-search/polyfills-es2015.81cc4ac759d47a3656c7.js',
//   './dist/ipid-search/main-es5.988faecbbc3154990dbf.js',
//   './dist/ipid-search/main-es2015.988faecbbc3154990dbf.js'
//
// ];
