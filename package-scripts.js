const { series } = require("nps-utils"); // not required, but handy!
const path = require('path');

const projPath = (name) => `./projects/${name}`;

const docsJson = (name, dir = projPath(name)) => `compodoc ${dir} -p ${dir}/.storybook/tsconfig.json -e json -d ${dir}`;
const serveStorybook = (name, dir = projPath(name)) => `start-storybook -c ${dir}/.storybook -s ${dir}/src/assets:/assets`;
const buildStorybook = (name, dir = projPath(name)) => `build-storybook -c ${dir}/.storybook -s ${dir}/src/assets:/assets`;

const serveStorybookScript = (name) => series(
  docsJson(name),
  serveStorybook(name)
);
const buildStorybookScript = (name) => series(
  docsJson(name),
  buildStorybook(name)
);

const serve = (name) => `ng serve ${name} --open`;

const forMany = (fn, projects) => {
  const configs = {};
  for (const item of projects) {
    configs[item.alias] = fn(item.project);
  }

  return configs;
}

const allProjects = [
  { project: 'app-debit', alias: 'debit' },
  { project: 'app-ipid', alias: 'ipid' },
  { project: 'app-search-ls', alias: 'search-ls' },
];


module.exports = {
  scripts: {
    storybook: forMany(serveStorybookScript, allProjects),
    'storybook:build': forMany(buildStorybookScript, allProjects),
    serve: forMany(serve, allProjects),
  }
};
