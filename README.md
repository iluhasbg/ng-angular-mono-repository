#GardiumServices MONOREPO

Состоит из нескольких проектов
- app-ipid (Приложение SPA APP IP-ID)
- app-debit (SPA приложение Дебиторская задолженность)
- app-search (Поисковая строка v1, Angular Elements)
- app-search-ls (Поисковая строка v2, Angular Elements)
- ~~gardium (Приложение SPA Gardium - старый LK gardium)~~

———————————
## Автодеплой
Для автодеплоя мы используем gitlab-ci с динамическим окружением. На каждую ветку создается поддомен для деплоя билда.

———————————

## Документация по проектам

Таблица GP/GO/Debit брендов https://cf.9958258.ru/pages/viewpage.action?pageId=87242801

## GP/GO and sub brands
- Репозиторий: https://git.9958258.ru/develop/ipid_search/
- Путь к проекту в репозитории: /projects/app-ipid
- PROD сервер: 172.16.120.100
- CI test server: msk-s1-ipid-test 172.16.164.52 - /var/www/ipid
- Все конфигурации dev билда и serve находятся в scripts блоке в package.json

#### Локальная разработка
1. Клонируем репозиторий
2. Устанавливаем зависимости: `yarn`
3. Запускаем нужный бренд: `yarn serve:ipid-pro`, `yarn serve:gardium-online`, etc...

#### Билд на прод сервере
1. Подключаемся к прод серверу `ssh ubuntu@172.16.120.100`
2. Переходим в директорию проекта `cd /var/www/ipid`
3. Устанавливаем зависимости: `yarn` (Если менялся package.json)
4. Запускаем нужный билд: 
- Все бренды: `yarn build:all --prod --output-hashing=all`,
- Определенный бренд: `yarn build ipid-pro --prod  --output-hashing=all`

#### Branches & CI builds
- dev ветка - dev
- все mr по дебиторке создавать на ветку dev-debit

обычные ветки под задачи:
- 00000
- 00000-short-description

где `00000` - номер задачи.

`short-description` - краткое описание.

В CI билд начнется на все активные бренды (неактивные бренды указаны в scripts/common.js)

Examples:

1294 ->
- http://1294.ipid-pro.lkdev.9958258.ru
- http://1294.gardium-online.lkdev.9958258.ru
- etc...

1294-short-description ->
- http://1294-short-description.ipid-pro.lkdev.9958258.ru
- http://1294-short-description.gardium-online.lkdev.9958258.ru
- etc...

dev ->
- http://dev.ipid-pro.lkdev.9958258.ru
- http://dev.gardium-online.lkdev.9958258.ru
- etc...

## Debit and sub brands (Дебиторская задолженность)
- Репозиторий: https://git.9958258.ru/develop/ipid_search/
- Путь к проекту в репозитории: /projects/app-debit
- PROD сервер: 172.16.120.100
- CI test server: msk-s1-ipid-test 172.16.164.52 - /var/www/ipid
- Все конфигурации dev билда и serve находятся в scripts блоке в package.json

#### Локальная разработка
1. Клонируем репозиторий
2. Устанавливаем зависимости: `yarn`
3. Запускаем нужный бренд: `yarn serve:debit-alfa`, `yarn serve:debit-default`

#### Билд на прод сервере
1. Подключаемся к прод серверу `ssh ubuntu@172.16.120.100`
2. Переходим в директорию проекта `cd /var/www/ipid`
3. Устанавливаем зависимости: `yarn` (Если менялся package.json)
4. Запускаем нужный билд:
- Все бренды: `yarn build:all --debit --prod --output-hashing=all`,
- Определенный бренд: `yarn build debit-alfa --prod  --output-hashing=all`

#### Branches & CI builds
- dev ветка - dev-debit
- все mr по дебиторке создавать на ветку dev-debit

обычные ветки под задачи:
- debit/задача
- 
В CI билд начнется на 2 версии (default и alfa)

По состоянию на 17.11.2022 default бренд отключен, на тесте и проде билдится только alfa версия

Examples:

debit/1294 ->
- http://debit-1294.debit-alfa.lkdev.9958258.ru
- http://debit-1294.debit-default.lkdev.9958258.ru

debit/1294-short-description ->
- http://debit-1294-short-description.debit-alfa.lkdev.9958258.ru

dev-debit
http://dev-debit.debit-alfa.lkdev.9958258.ru

———————————
## IP-ID Angular Elements Search v2

[README.md](projects/app-search-ls/README.md)

———————————

## IP-ID Angular Elements Search v1
#### Поисковая строка для встраивания в статику и другие angular приложения

##### По состоянию на 17.11.2022 данная версия поиска используется только на [legal-support](https://legal-support.ru/services/trademarks/online-proverka-tovarnogo-znaka/)

- Путь к проекту в репозитории: /projects/app-search
- Локальный билд```yarn build:elements```)
- [Документация](https://cf.9958258.ru/pages/viewpage.action?pageId=78030132) по встраиванию elements билда в статику

---
Структура после elements билда:
- assets - assets файлы из projects/app-search
- search.js - elements билд для встраивания в статику
- legal-support.html - пример встраивания билда для [legal-support](https://legal-support.ru/services/trademarks/online-proverka-tovarnogo-znaka/)
- brand-search.html - пример встраивания билда для [brand-search](https://brand-search.ru/search/)
---

##### Встраивается как elements build в:
- brand-search  - с параметром ```brandsearch="true"```
- legal-support - с параметром ```legalsupport="true"```

Параметр `designation="Yandex"` для срабатывания поиска в момент инициализации с подстановкой значения в поиск
##### Встраивается как angular компонент в:
1. GO бренды (страница поиска /search) 
```
<app-ip-id-search
  [designation]="searchTerm" // передача поискового запроса для автоматического срабатывания поиска при инициализации
  [options]="options" // описано ниже
>
</app-ip-id-search>
```
```
options = {
    style: 'appIpIdSearchPage', // измененный вид поиска по этому флагу 
    dynamicStylesForBrand: environment.SEARCH_STYLES, // передаются переменные из env для стилизирования под определенный бренд.
  };
```
1. GP/GO бренды  (подача заявки 2 шаг: mini-версия) в app-search передается объект от которого зависит вид на поиска на втором шаге:
```
<app-ip-id-search
              (eventSelected)="eventSelectedClass($event)"
              (eventDeleted)="eventDeletedClass($event)"
              [options]="optionsMktu"
>
</app-ip-id-search>
```
```
optionsMktu = {
  onlyClassSelect: true,
  search: 'mktu',
  selectedMktu: []
};
```
