const rootMain = require('../../../.storybook/main');

rootMain.stories.push(
  ...['../src/app/**/*.stories.mdx', '../src/app/**/*.stories.@(js|jsx|ts|tsx)']
);


module.exports = rootMain;
