import { EmbItemCardComponent } from './emb-item-card.component';
import { CommonModule } from '@angular/common';
import { moduleMetadata, Meta, Story } from '@storybook/angular';
import { EmbItemCardStyle } from '../../models/emb-item-card.interface';

export default {
  component: EmbItemCardComponent,
  decorators: [
    moduleMetadata({
      declarations: [EmbItemCardComponent],
      imports: [CommonModule],
    }),
  ],
  title: 'emb-item-card',
} as Meta;

const Template: Story<EmbItemCardComponent> = args => ({
  props: args
});

export const Text = Template.bind({});
Text.args = {
  item: {
    img: '',
    text: 'Товарный знак'
  }
};

export const Img = Template.bind({});
Img.args = {
  item: {
    img: 'https://avatanplus.com/files/resources/mid/573747e25e26c154aff0cc52.png',
    text: ''
  }
};

export const TextDomains = Template.bind({});
TextDomains.args = {
  item: {
    img: '',
    text: 'URL.RU'
  },
  isDomain: true
};
