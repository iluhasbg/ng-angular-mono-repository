import { Component, Input, OnInit } from '@angular/core';
import { EmbItemCardInterface, EmbItemCardStyle } from '../../models/emb-item-card.interface';
import { EmbAccordionStyle } from '../../models/emb-accordion.interface';

@Component({
  selector: 'app-ls-emb-item-card',
  templateUrl: './emb-item-card.component.html',
  styleUrls: ['./emb-item-card.component.scss']
})
export class EmbItemCardComponent implements OnInit {
  @Input() isDomain = false;
  @Input() item: EmbItemCardInterface;

  constructor() {
  }

  ngOnInit(): void {
  }

  className(bool: boolean) {
    return bool ? EmbItemCardStyle.TextDomains : '';
  }
}
