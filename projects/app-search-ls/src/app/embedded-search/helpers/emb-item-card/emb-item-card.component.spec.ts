import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmbItemCardComponent } from './emb-item-card.component';

describe('EmbItemCardComponent', () => {
  let component: EmbItemCardComponent;
  let fixture: ComponentFixture<EmbItemCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EmbItemCardComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmbItemCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
