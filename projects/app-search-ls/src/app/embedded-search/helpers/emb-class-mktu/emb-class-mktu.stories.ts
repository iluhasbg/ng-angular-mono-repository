import { componentWrapperDecorator } from '@storybook/angular';
import { Meta, Story } from '@storybook/angular';
import { EmbClassMktuComponent } from './emb-class-mktu.component';

export default {
  title: 'emb-class-mktu',
  component: EmbClassMktuComponent,
  decorators: [
    componentWrapperDecorator((story) => `<div style="margin: 3em">${story}</div>`)
  ],

} as Meta;

const Template: Story<EmbClassMktuComponent> = args => ({
  props: args
});

export const Default = Template.bind({});
Default.args = {
  MKTU: {number: 1, name: 'МКТУ 01', desc: 'Продукты химические', selected: false}
};

export const Active = Template.bind({});
Active.args = {
  MKTU: {number: 3, name: 'МКТУ 03', desc: 'Продукты нехимические', selected: true}
};





