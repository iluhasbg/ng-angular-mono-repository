import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmbClassMktuComponent } from './emb-class-mktu.component';

describe('EmbClassMktuComponent', () => {
  let component: EmbClassMktuComponent;
  let fixture: ComponentFixture<EmbClassMktuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EmbClassMktuComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmbClassMktuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
