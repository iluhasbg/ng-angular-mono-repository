import { Component, Input, OnInit } from '@angular/core';
import { ImmutableMKTUInterfaces } from '../../models/embedded-search-field-request.interface';
import { EmbClassMKTUStyle } from '../../models/emb-class-mktu.interface';

@Component({
  selector: 'app-ls-emb-class-mktu',
  templateUrl: './emb-class-mktu.component.html',
  styleUrls: ['./emb-class-mktu.component.scss']
})
export class EmbClassMktuComponent implements OnInit {

  @Input() MKTU: ImmutableMKTUInterfaces;

  constructor() {
  }

  ngOnInit(): void {
  }

  className(bool: boolean) {
    return bool ? EmbClassMKTUStyle.Active : EmbClassMKTUStyle.Default;
  }
}
