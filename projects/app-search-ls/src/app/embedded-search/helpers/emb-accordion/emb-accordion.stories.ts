import { EmbAccordionComponent } from './emb-accordion.component';
import { CommonModule } from '@angular/common';
import { moduleMetadata, Meta, Story } from '@storybook/angular';
import { EmbAccordionStyle } from '../../models/emb-accordion.interface';

export default {
  component: EmbAccordionComponent,
  decorators: [
    moduleMetadata({
      declarations: [EmbAccordionComponent],
      imports: [CommonModule],
    }),
  ],
  title: 'emb-accordion',
} as Meta;

const Template: Story<EmbAccordionComponent> = args => ({
  props: args,
});

export const Default = Template.bind({});
Default.args = {
  def: {
    style: EmbAccordionStyle.Default,
  },
  dataLength: 86
};

export const Empty = Template.bind({});
Empty.args = {
  def: {
    style: EmbAccordionStyle.Empty,
  },
  dataLength: 0
};
