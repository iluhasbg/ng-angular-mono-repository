import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DomainsDataInterface } from '../../models/embedded-search-result-domains.interface';
import { LegalNamesDataInterface } from '../../models/embedded-search-result-legalnames.interface';
import { TrademarksDataInterface } from '../../models/embedded-search-result-trademarks.interface';
import { EmbAccordionStyle } from '../../models/emb-accordion.interface';

@Component({
  selector: 'app-ls-emb-accordion',
  templateUrl: './emb-accordion.component.html',
  styleUrls: ['./emb-accordion.component.scss']
})
export class EmbAccordionComponent implements OnInit {
  isShowResult = false;
  showMoreBtn = true;

  // количество карточек, которые мы считаем показанными на кнопке Показать все
  startQuantityCount = 4;

  // количество карточек, которые мы показываем изначально (с учетом blur)
  startQuantity = 8;

  // количество карточек в ряду
  quantityPerRow = 4;

  // количество рядов в слоте (при клике на Показать еще)
  rowsPerSlots = 3;

  // max количество слотов/нажатий на Показать еще
  maxSlots: number;

  // количество рядов во всем массиве
  rowsQuantity: number;

  // остаток для неполного ряда
  remainder = 0;

  // количество сделанных кликов на Показать еще
  clickCounter = 0;

  // сколько картоек открывать за раз
  showMoreCount = 12;

  @Input() type: 'domains' | 'trademarks' | 'legalNames';
  @Input() dataLength: number;
  @Input() data: DomainsDataInterface[] | LegalNamesDataInterface[] | TrademarksDataInterface[];
  @Input() isShowTrademarks: boolean;
  @Output() slicedData = new EventEmitter();

  constructor() {
  }

  ngOnInit(): void {
    if (this.data) {
      this.toShowQuantity(this.data);
    }
  }

  openAccordion(): void {
    if (this.dataLength > 0) {
      if (this.isShowResult === true) {
        this.isShowResult = false;
      } else {
        this.clickCounter = 0;
        this.toShowQuantity(this.data);
        this.showMoreBtn = true;
        this.isShowResult = true;
      }
    }
  }

  getItemType(type: string) {
    const text: { [key: string]: string } = {
      domains: 'доменов',
      trademarks: 'товарных знаков',
      legalNames: 'юридических лиц',
    };

    return text[type];
  }

  clickShowMore() {
    if (this.clickCounter < this.maxSlots) {
      this.clickCounter++;
      this.toShowQuantity(this.data);
    } else {
      this.showMoreBtn = false;
    }
  }

  toShowQuantity(data: DomainsDataInterface[] | LegalNamesDataInterface[] | TrademarksDataInterface[]) {
    this.getMaxSlots(data);

    if (data.length <= this.startQuantity) {
      this.showMoreBtn = false;
      return this.slicedData.emit(data);
    } else {
      if (this.clickCounter === 0) {
        return this.slicedData.emit(data.slice(0, this.startQuantity));
      } else {
        this.getShowMore(data);
      }
    }
  }

  getShowMore(data: DomainsDataInterface[] | LegalNamesDataInterface[] | TrademarksDataInterface[]) {
    // определям остаток для неполного ряда, если есть
    if (data.length % this.quantityPerRow !== 0) {
      this.remainder = data.length % this.quantityPerRow;
    }

    const sliceQuantity = this.startQuantity + this.clickCounter * this.rowsPerSlots * this.quantityPerRow;

    if (this.clickCounter < this.maxSlots) {
      return this.slicedData.emit(data.slice(0, sliceQuantity));
    } else {
      this.showMoreBtn = false;
      return this.slicedData.emit(data);
    }
  }

  getMaxSlots(data: DomainsDataInterface[] | LegalNamesDataInterface[] | TrademarksDataInterface[]) {
    // определям количество рядов
    this.rowsQuantity = Math.floor(data.length / this.quantityPerRow);
    // определям max количество слотов/нажатий на Показать еще
    this.maxSlots = Math.round(this.rowsQuantity / this.rowsPerSlots);
  }

  getLengthShowMore(dataLength: number) {
    if (this.clickCounter === 0) {
      return dataLength - this.startQuantityCount;
    } else {
      return dataLength - (this.startQuantityCount + this.clickCounter * this.quantityPerRow * this.rowsPerSlots);
    }
  }

  getShowMoreCount(dataLength: number) {
    if (this.showMoreCount < (dataLength - (this.startQuantityCount + this.clickCounter * this.quantityPerRow * this.rowsPerSlots))) {
      return this.showMoreCount;
    } else {
      return dataLength - (this.startQuantityCount + this.clickCounter * this.quantityPerRow * this.rowsPerSlots);
    }
  }

  className(bool: boolean) {
    return bool ? EmbAccordionStyle.Empty : EmbAccordionStyle.Default;
  }
}
