import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmbAccordionComponent } from './emb-accordion.component';

describe('EmbAccordionComponent', () => {
  let component: EmbAccordionComponent;
  let fixture: ComponentFixture<EmbAccordionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EmbAccordionComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmbAccordionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
