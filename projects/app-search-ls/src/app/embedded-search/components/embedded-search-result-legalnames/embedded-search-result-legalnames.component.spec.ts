import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmbeddedSearchResultLegalnamesComponent } from './embedded-search-result-legalnames.component';

describe('EmbeddedSearchResultLegalnamesComponent', () => {
  let component: EmbeddedSearchResultLegalnamesComponent;
  let fixture: ComponentFixture<EmbeddedSearchResultLegalnamesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EmbeddedSearchResultLegalnamesComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmbeddedSearchResultLegalnamesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
