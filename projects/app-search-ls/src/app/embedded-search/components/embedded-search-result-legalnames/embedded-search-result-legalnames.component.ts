import { Component, Input, OnInit } from '@angular/core';
import { LegalNamesDataInterface } from '../../models/embedded-search-result-legalnames.interface';
import { EmbItemCardInterface } from '../../models/emb-item-card.interface';

@Component({
  selector: 'app-ls-embedded-search-result-legalnames',
  templateUrl: './embedded-search-result-legalnames.component.html',
  styleUrls: ['./embedded-search-result-legalnames.component.scss']
})
export class EmbeddedSearchResultLegalnamesComponent implements OnInit {
  isShowResult = false;
  selectedLegalName;
  slicedData: LegalNamesDataInterface[];

  @Input() dataLegalNames: LegalNamesDataInterface[];

  constructor() {
  }

  ngOnInit(): void {
  }

  openAccordion(): void {
    this.isShowResult = !this.isShowResult;
  }

  openModal(legalName) {
    this.selectedLegalName = legalName;
    document.body.style.overflow = 'hidden';
  }

  closeModal() {
    this.selectedLegalName = null;
    document.body.style.overflow = 'visible';
  }

  isOverlongName(legalName): boolean {
    if (legalName._source.name) {
      return legalName._source.name.length > 60 || this.getShortNameByWords(legalName._source.name).changed;
    } else {
      return legalName._source.fullName.length > 60 || this.getShortNameByWords(legalName._source.fullName).changed;
    }
  }

  getShortNameByWords(name) {
    let nameArray = name.split(' ');
    nameArray = nameArray.reverse();
    let nameIndex = 0;
    nameArray.forEach((item, index) => {
      if (item.length > 20) {
        nameIndex = index;
      }
    });
    if (nameIndex) {
      const shortNameArr = nameArray.slice(0, nameIndex + 1);
      shortNameArr[shortNameArr.length - 1] = shortNameArr[shortNameArr.length - 1].substr(0, 17);
      return {
        changed: true,
        name: shortNameArr.join(' ')
      };
    } else {
      return {
        changed: false,
        name
      };
    }
  }

  getShortNameByRows(name) {
    return name.length > 60 ? name.slice(0, 60) : name;
  }

  getSlicedData(data: LegalNamesDataInterface[]) {
    this.slicedData = data;
  }

  getPureDataForView(legalName: LegalNamesDataInterface): EmbItemCardInterface {
    let text;
    if (legalName._source?.name) {
      text = this.isOverlongName(legalName) ?
        this.getShortNameByWords(this.getShortNameByRows(legalName._source.name)).name + '...'
        : legalName._source.name;
    }

    if (!legalName._source?.name && legalName._source?.fullName) {
      text = this.isOverlongName(legalName) ?
        this.getShortNameByWords(this.getShortNameByRows(legalName._source.fullName)).name + '...'
        : legalName._source.fullName;
    }

    return {img: '', text};
  }
}
