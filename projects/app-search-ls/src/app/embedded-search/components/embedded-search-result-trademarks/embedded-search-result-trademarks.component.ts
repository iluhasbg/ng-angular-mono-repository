import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {
  TrademarksAllDataInterface,
  TrademarksDataInterface
} from '../../models/embedded-search-result-trademarks.interface';
import { MKTUInterface } from '../../models/embadded-search-request.interface';
import { EmbItemCardInterface } from '../../models/emb-item-card.interface';

@Component({
  selector: 'app-ls-embedded-search-result-trademarks',
  templateUrl: './embedded-search-result-trademarks.component.html',
  styleUrls: ['./embedded-search-result-trademarks.component.scss']
})
export class EmbeddedSearchResultTrademarksComponent implements OnInit {
  selectedTrademark;
  showModalNew = false;
  showModal = false;
  isShowResult = true;

  slicedData: TrademarksDataInterface[];

  @Input() allTrademarks: TrademarksAllDataInterface;
  @Input() dataTrademarks: TrademarksDataInterface[];
  @Input() MKTU: MKTUInterface[];
  @Input() searchText: string;

  @Output() deleteMKTUFromSearch = new EventEmitter();

  @Output() toShowMKTUClasses = new EventEmitter();

  @Output() clickMKTU = new EventEmitter();

  constructor() {
  }

  ngOnInit(): void {
  }

  openModal(tradeMark) {
    this.selectedTrademark = tradeMark;
    this.showModalNew = true;
    this.showModal = true;
    const bodyStyles = document.body.style;
    bodyStyles.setProperty('overflow', 'hidden');
  }

  closeModal() {
    this.showModal = false;
    this.showModalNew = false;
    const bodyStyles = document.body.style;
    bodyStyles.setProperty('overflow', 'visible');
  }

  clear() {
    this.deleteMKTUFromSearch.emit({text: this.searchText, mktu: []});
  }

  getSlicedData(data: TrademarksDataInterface[]) {
    this.slicedData = data;
  }

  getPureDataForView(trademark: TrademarksDataInterface): EmbItemCardInterface {
    let img;
    trademark?._source?.imageUrl && trademark?._source?.markImageFileName ? img = trademark?._source?.imageUrl : img = '';

    return {img, text: trademark?._source?.imageText};
  }

  get isAllClassSelected(): boolean {
    if (this.MKTU.length === 0 || this.MKTU.length === 45) {
      return true;
    }
  }

  onShowMKTUClasses() {
    this.clickMKTU.emit(true);
    this.toShowMKTUClasses.emit();
  }
}
