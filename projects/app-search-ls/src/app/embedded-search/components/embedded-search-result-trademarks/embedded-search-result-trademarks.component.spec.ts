import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmbeddedSearchResultTrademarksComponent } from './embedded-search-result-trademarks.component';

describe('EmbeddedSearchResultTrademarksComponent', () => {
  let component: EmbeddedSearchResultTrademarksComponent;
  let fixture: ComponentFixture<EmbeddedSearchResultTrademarksComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EmbeddedSearchResultTrademarksComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmbeddedSearchResultTrademarksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
