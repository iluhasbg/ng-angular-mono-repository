import { Component, Input, OnInit } from '@angular/core';
import { DomainsDataInterface } from '../../models/embedded-search-result-domains.interface';
import { EmbItemCardInterface } from '../../models/emb-item-card.interface';

@Component({
  selector: 'app-ls-embedded-search-result-domains',
  templateUrl: './embedded-search-result-domains.component.html',
  styleUrls: ['./embedded-search-result-domains.component.scss']
})
export class EmbeddedSearchResultDomainsComponent implements OnInit {
  isShowResult = false;
  selectedDomain;
  slicedData: DomainsDataInterface[];

  @Input() dataDomains: DomainsDataInterface[];

  constructor() {
  }

  ngOnInit(): void {
  }

  closeModal() {
    this.selectedDomain = null;
    document.body.style.overflow = 'visible';
  }

  openModal(domain) {
    this.selectedDomain = domain;
    document.body.style.overflow = 'hidden';
  }

  getSlicedData(data: DomainsDataInterface[]) {
    this.slicedData = data;
  }

  getDomain(domainName) {
    return domainName._source.fullDomain.includes('XN--') ?
      `${domainName._source.domain}.${domainName._source.zone}`.toUpperCase()
      : domainName._source.fullDomain.toUpperCase();
  }

  getPureDataForView(domain: DomainsDataInterface): EmbItemCardInterface {
    return {
      img: '',
      text: this.getDomain(domain)
    };
  }

}
