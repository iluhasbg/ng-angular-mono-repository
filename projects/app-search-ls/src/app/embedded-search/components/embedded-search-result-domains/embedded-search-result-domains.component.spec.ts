import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmbeddedSearchResultDomainsComponent } from './embedded-search-result-domains.component';

describe('EmbeddedSearchResultDomainsComponent', () => {
  let component: EmbeddedSearchResultDomainsComponent;
  let fixture: ComponentFixture<EmbeddedSearchResultDomainsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EmbeddedSearchResultDomainsComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmbeddedSearchResultDomainsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
