import { Component, EventEmitter, HostListener, Input, OnChanges, OnInit, Output } from '@angular/core';
import { MKTUData } from './mktu';
import { ImmutableMKTUInterfaces } from '../../models/embedded-search-field-request.interface';
import { EmbSearchInitialDataInterface } from '../../models/embadded-search-request.interface';
import { SendSearchInterface } from '../../containers/embedded-search-container/embedded-search-container.interfaces';
import { IntercomEventsService } from 'projects/shared/services/intercom-events.service';


@Component({
  selector: 'app-ls-embedded-search-field',
  templateUrl: './embedded-search-field.component.html',
  styleUrls: ['./embedded-search-field.component.scss']
})
export class EmbeddedSearchFieldComponent implements OnInit, OnChanges {
  toCheckAllMKTU = false;
  isShowMKTUClasses = false;
  searchText: string;

  immutableMKTUClasses: ImmutableMKTUInterfaces[] = MKTUData;
  MKTUSearchArray: ImmutableMKTUInterfaces[];

  @Input() dataRequest: EmbSearchInitialDataInterface;

  @Output() textRequestOut = new EventEmitter();
  @Output() sendSearch = new EventEmitter<SendSearchInterface>();
  @Output() clickMKTU = new EventEmitter();

  constructor(private intercomEventsService: IntercomEventsService) {
  }

  @HostListener('document:keyup', ['$event']) // 13=ENTER
  keyup(event: KeyboardEvent): void {
    if (event.keyCode === 13 && document.activeElement.id === 'searchInput') {
      this.search();
    }
  }

  ngOnInit(): void {
    this.init();
  }

  ngOnChanges(): void {
    this.init();
  }

  init(): void {
    this.MKTUSearchArray = this.immutableMKTUClasses;

    // проверка есть ли сохраненные(входящие) данные для поиска
    if (this.dataRequest) {
      // проверяем есть ли входящие выбранные классы МКТУ
      if (this.dataRequest.mktu && this.dataRequest.mktu.length > 0) {
        // делаем входящие классы selected в MKTUSearchArray
        this.dataRequest.mktu.forEach(item => {
          this.MKTUSearchArray.filter(MKTUClass => MKTUClass.number === item.number).forEach(MKTUClassFiltered => {
            MKTUClassFiltered.selected = true;
          });
        });
      } else {
        this.toCancelSelectAllClasses(this.MKTUSearchArray);
      }

      // если поиск по всем классам МКТУ - делаем все классы неактивными
      if (!this.dataRequest.mktu || this.dataRequest.mktu.length === 45) {
        this.toCancelSelectAllClasses(this.MKTUSearchArray);
      }

      // подставляем в строку поиска входящий запрос, если он не пустой
      if (this.dataRequest.text.length > 0) {
        this.searchText = this.dataRequest.text;
      }
    } else {
      // по умолчанию все классы выбраны
      this.toSelectAllClasses(this.MKTUSearchArray);
    }
  }

  toSelectAllClasses(massive) {
    massive.forEach((Item) => {
      Item.selected = true;
    });
  }

  toCancelSelectAllClasses(massive) {
    massive.forEach((Item) => {
      Item.selected = false;
    });
  }

  addMKTUToSearch(item) {
    if (item.selected) {
      this.deleteMKTUFromSearch(item);
      return;
    }
    item.selected = true;
  }

  deleteMKTUFromSearch(item) {
    item.selected = false;

    this.MKTUSearchArray.forEach((MKTUItem) => {
      if (MKTUItem.number === item.number) {
        MKTUItem.selected = false;
      }
    });
  }

  search() {
    if (this.searchText.length === 0) {
      return;
    }
    this.textRequestOut.emit({
      text: this.searchText,
      mktu: this.MKTUSearchArray.filter(MKTUClass => MKTUClass.selected === true)
    });
  }

  public sendIntercomData(params): void {
    this.intercomEventsService.push(params);
  }

  isThereSelectedClasses() {
    return this.MKTUSearchArray.filter(MKTUClass => MKTUClass.selected === true).length === 0;
  }

  clickShowMKTU() {
    if (this.MKTUSearchArray.filter(el => el.selected).length === 45) {
      this.toCancelSelectAllClasses(this.MKTUSearchArray);
    }
    this.isShowMKTUClasses = !this.isShowMKTUClasses;
    this.clickMKTU.emit(true);
  }
}
