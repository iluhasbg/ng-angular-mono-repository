import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmbeddedSearchFieldComponent } from './embedded-search-field.component';

describe('EmbeddedSearchFieldComponent', () => {
  let component: EmbeddedSearchFieldComponent;
  let fixture: ComponentFixture<EmbeddedSearchFieldComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EmbeddedSearchFieldComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmbeddedSearchFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
