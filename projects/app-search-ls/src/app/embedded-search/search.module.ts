import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { EmbeddedSearchContainerComponent } from './containers/embedded-search-container/embedded-search-container.component';
import { EmbeddedSearchFieldComponent } from './components/embedded-search-field/embedded-search-field.component';
import { EmbeddedSearchResultDomainsComponent } from './components/embedded-search-result-domains/embedded-search-result-domains.component';
import { EmbeddedSearchResultLegalnamesComponent } from './components/embedded-search-result-legalnames/embedded-search-result-legalnames.component';
import { EmbeddedSearchResultTrademarksComponent } from './components/embedded-search-result-trademarks/embedded-search-result-trademarks.component';
import { IpIdSearchModule } from '../../../../app-search/src/app/app.module';
import { SharedModule } from '../../../../app-ipid/src/app/shared/shared.module';
import { EmbAccordionComponent } from './helpers/emb-accordion/emb-accordion.component';
import { EmbItemCardComponent } from './helpers/emb-item-card/emb-item-card.component';
import { EmbClassMktuComponent } from './helpers/emb-class-mktu/emb-class-mktu.component';


@NgModule({
  declarations: [
    EmbeddedSearchContainerComponent,
    EmbeddedSearchFieldComponent,
    EmbeddedSearchResultDomainsComponent,
    EmbeddedSearchResultLegalnamesComponent,
    EmbeddedSearchResultTrademarksComponent,
    EmbAccordionComponent,
    EmbItemCardComponent,
    EmbClassMktuComponent,
  ],
  exports: [
    EmbeddedSearchContainerComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    IpIdSearchModule,
    SharedModule
  ]
})

export class SearchModule {
}
