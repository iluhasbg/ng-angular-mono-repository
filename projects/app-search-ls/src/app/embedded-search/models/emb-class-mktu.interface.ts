export interface EmbClassMKTUInterface {
  style: EmbClassMKTUStyle;
}

export enum EmbClassMKTUStyle {
  Default = 'search__mktu-class',
  Active = 'search__mktu-class search__mktu-class--active'
}
