export interface ImmutableMKTUInterfaces {
  number: number;
  name: string;
  desc: string;
  selected?: boolean;
}

