export interface EmbSearchInitialDataInterface {
  text: string;
  mktu: MKTUInterface[];
}

export interface MKTUInterface {
  name: string;
  number: number;
}
