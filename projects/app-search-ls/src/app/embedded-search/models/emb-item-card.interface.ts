export interface EmbItemCardInterface {
  style?: EmbItemCardStyle;
  img?: string;
  text?: string;
  isDomain?: boolean;
}

export enum EmbItemCardStyle {
  Text = 'Текст',
  Img = 'Картинка',
  TextDomains = 'search-result-list__item search-result-list__item--domain'
}
