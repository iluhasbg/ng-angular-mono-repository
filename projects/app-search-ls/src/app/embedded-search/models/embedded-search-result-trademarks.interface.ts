export interface TrademarksDataInterface {
  _id?: string;
  _index?: string;
  _risk?: string;
  _score?: number;
  _source?: {
    PSRN?: number;
    TIN?: string;
    applicantName?: string;
    applicantStatus?: number;
    applicationDate?: string;
    applicationString?: string;
    expiryDate?: string;
    goodsServices?: [];
    imageText?: string;
    markImageFileName?: string;
    registrationDate?: string;
    registrationNumber?: number;
    registrationString?: string;
    imageUrl?: string;
  };
  _type?: string;
}

export interface TrademarksAllDataInterface {
  hits?: {
    hits?: TrademarksDataInterface[];
    max_score?: number;
    total?: {
      domains?: number;
      from?: number;
      legalEntities?: number;
      patents?: number;
      relation?: string;
      size?: number;
      tradeMarks?: number;
      value?: number;
    }
  };
  risks?: {
    custom?: number;
    legal_strategies?: number;
    max_risk?: string;
    mktu_completion?: number;
    sign_reworking?: number;
  };
  status?: number;
  timed_out?: boolean;
  took?: number;
  _shards?: {
    failed?: number;
    skipped?: number;
    successful?: number;
    total?: number;
  };
}
