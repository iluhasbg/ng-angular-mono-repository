export interface DomainsDataInterface {
  _id?: string;
  _index?: string;
  _risk?: string;
  _score?: number;
  _source?: {
    domain?: string;
    domainRegistrator?: string;
    free?: boolean;
    freeDate?: string;
    fullDomain?: string;
    payDate?: string;
    registrationDate?: string;
    zone?: string;
  };
  _type?: string;
}
