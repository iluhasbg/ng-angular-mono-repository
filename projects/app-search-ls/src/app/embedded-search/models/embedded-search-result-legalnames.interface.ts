export interface LegalNamesDataInterface {
  _id: string;
  _index: string;
  _risk: string;
  _score: number;
  _source: {
    OKVEDCodes: {
      code: string;
      description: string;
    }[],
    PSRN: number;
    TIN: string;
    TRRC: string;
    address: string;
    addressHistory: {
      regionName: string;
      regionType: string;
    } [];
    dateOfPSRN: string;
    directors: {
      TIN: string;
      name: string;
      patronymic: string;
      postTitle: string;
      recordDate: string;
      recordID: number;
      surname: string;
    }[];
    fullName: string;
    name: string;
  };
  _type: string;
}
