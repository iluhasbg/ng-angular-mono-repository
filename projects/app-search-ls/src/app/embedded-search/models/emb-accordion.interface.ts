export interface EmbAccordionInterface {
  style: EmbAccordionStyle;
  dataLength: number;
}

export enum EmbAccordionStyle {
  Default = 'accordion-tab__title',
  Empty = 'accordion-tab__title accordion-tab__title--no-result'
}
