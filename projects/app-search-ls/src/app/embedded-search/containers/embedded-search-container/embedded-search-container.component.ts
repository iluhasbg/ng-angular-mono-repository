import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from "@angular/core";
import {
  EmbSearchInitialDataInterface,
  MKTUInterface,
} from "../../models/embadded-search-request.interface";
import { EmbeddedSearchContainerService } from "./embedded-search-container.service";
import {
  TrademarksAllDataInterface,
  TrademarksDataInterface,
} from "../../models/embedded-search-result-trademarks.interface";
import { LegalNamesDataInterface } from "../../models/embedded-search-result-legalnames.interface";
import { DomainsDataInterface } from "../../models/embedded-search-result-domains.interface";
import {
  UserEventInterface,
  UserEventType,
} from "./embedded-search-container.interfaces";
import { UserEventService } from "../../services/user-event.service";
import { ActivatedRoute } from "@angular/router";
import { Location } from "@angular/common";

@Component({
  selector: "app-ls-embedded-search-container",
  templateUrl: "./embedded-search-container.component.html",
  styleUrls: ["./embedded-search-container.component.scss"],
})
export class EmbeddedSearchContainerComponent implements OnInit {
  private externalSearchDataValue: EmbSearchInitialDataInterface;

  // входящий сохраненный поиск, если есть
  @Input() set externalSearchData(
    value: string | EmbSearchInitialDataInterface
  ) {
    // из elements мы не можем передавать объекты поэтому такое)
    if (typeof value === "string") {
      try {
        this.externalSearchDataValue = JSON.parse(value);
      } catch {}
    } else if (typeof value === "object" && value?.text) {
      this.externalSearchDataValue = value;
    }
  }

  get externalSearchData() {
    return this.externalSearchDataValue;
  }

  @Output() userEvent = new EventEmitter<UserEventInterface>();

  @ViewChild("searchField") searchFieldComponent;

  constructor(
    public searchService: EmbeddedSearchContainerService,
    readonly userEventService: UserEventService,
    private route: ActivatedRoute,
    private location: Location
  ) {}

  ngOnInit(): void {
    // перекидываем пользовательские события в output потому что надо для встраивания
    this.userEventService.events$.subscribe((event) =>
      this.userEvent.emit(event)
    );
    console.log("externalSearchData", this.externalSearchData);

    if (this.externalSearchData) {
      this.searchService.searchRequestInitial(this.externalSearchData);
      return;
    } else {
      const url = new URLSearchParams(location.search);
      if (url.has("q")) {
        this.searchService.searchRequestInitialByQuery({
          q: url.get("q"),
          mktu: url.get("mktu"),
        });
      }
    }
  }

  emitEventToChild() {
    this.searchFieldComponent.clickShowMKTU();
  }

  onClickMKTU() {
    this.userEventService.push({ event: UserEventType.clickMKTU });
  }

  getSearchResult(evt: EmbSearchInitialDataInterface) {
    this.searchService.getSearchResult(evt);
    this.userEventService.push({
      event: UserEventType.sendSearch,
      data: {
        query: evt.text,
        mktu: evt.mktu,
      },
    });
  }

  searchTrademarks(evt: EmbSearchInitialDataInterface): void {
    this.searchService.searchTrademarks(evt);
  }

  // searchDomains(evt: EmbSearchInitialDataInterface): void {
  //   this.searchService.searchDomains(evt);
  // }
  //
  // searchLegalNames(evt: EmbSearchInitialDataInterface): void {
  //   this.searchService.searchLegalNames(evt);
  // }

  get searchRequest(): EmbSearchInitialDataInterface {
    return this.searchService.searchRequest;
  }

  get MKTU(): MKTUInterface[] {
    return this.searchService.MKTU;
  }

  get searchText(): string {
    return this.searchService.searchText;
  }

  get allTrademarks(): TrademarksAllDataInterface {
    return this.searchService.allTrademarks;
  }

  get dataTrademarks(): TrademarksDataInterface[] {
    return this.searchService.dataTrademarks;
  }

  get isLoadingAll(): boolean {
    return this.searchService.isLoadingAll;
  }

  // get dataDomains(): DomainsDataInterface[] {
  //   return this.searchService.dataDomains;
  // }
  //
  // get dataLegalNames(): LegalNamesDataInterface[] {
  //   return this.searchService.dataLegalNames;
  // }
}
