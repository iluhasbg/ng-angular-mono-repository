import { MKTUInterface } from '../../models/embadded-search-request.interface';

export interface SearchResultsInterface {
  // dataDomains: any;
  // dataLegalNames: any;
  dataTrademarks: any;
}

export interface SendSearchInterface {
  event: string;
  query: string;
  mktu: MKTUInterface[];
}

export interface UserEventInterface<T = any> {
  event: UserEventType;
  data?: T;
}

export enum UserEventType {
  sendSearch = 'sendSearch', // Отправлен поисковый запрос
  clickMKTU = 'clickMKTU', // Клик открывающий список всех МКТУ
  highRiskPage = 'highRiskPage', // Результат поиска с Высоким риском
  middleRiskPage = 'middleRiskPage', // Результат поиска со Средним риском
  lowRiskPage = 'lowRiskPage', // Результат поиска с Низким риском
}
