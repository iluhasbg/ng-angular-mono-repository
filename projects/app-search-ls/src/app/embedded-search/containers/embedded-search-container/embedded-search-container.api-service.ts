import { ISearchRequestData } from '../../../../../../app-search/src/app/models/search.interfaces';
import { HttpService } from '../../../../../../app-search/src/app/services/http.service';
import { Injectable } from '@angular/core';
import { environment } from '../../../../../../app-search/src/environments/environment';
import { map } from 'rxjs/operators';
import Base64 from 'crypto-js/enc-base64';
import sha1 from 'crypto-js/sha1';

const hostUrl = `${window.location.protocol}//${window.location.host}`;

const HOST = (hostUrl.includes('localhost') || hostUrl.includes('127.0.0.1') || hostUrl.includes('10.1.33.225'))
  ? `${environment.BASE_URL_IMG}/tm`
  : `${hostUrl}/storage/tm`;

const RUTM_PATH = `${HOST}/RUTM`; // Товарный знак
const WKTM_PATH = `${HOST}/WKTM`; // Общеизвестный товарный знак
const WOTM_PATH = `${HOST}/WOTM`; // Международный товарный знак
const RUTMAP_PATH = `${HOST}/RUTMAP`; // Заявка на товарный знак


@Injectable({
  providedIn: 'root'
})

export class EmbeddedSearchContainerApiService {
  private user = 'aa';
  private password = 'Aa123456';

  constructor(private httpService: HttpService) {
  }

  getHash() {
    return Base64.stringify(sha1(this.user.toLowerCase() + this.password));
  }

  // https://cf.9958258.ru/pages/viewpage.action?pageId=68812852
  getData(query: ISearchRequestData) {
    const SHA1hash = this.getHash();
    return this.httpService
      .post({path: '_search', body: {...query, SHA1hash}})
      .pipe(this.pipeTrademarkImages());
  }

  pipeTrademarkImages() {
    return map((data: any) => {
      if (data && data.hits && data.hits.hits) {
        data.hits.hits.forEach((item) => {
          let id = item._source.registrationString;
          // todo: проверять есть ли картинка
          if (item._index === 'rutmap' && item._source.applicationString) {
            id = item._source.applicationString;
          }
          if (id && item._source.markImageFileName) {
            item._source.imageUrl = this.getImgUrl(
              item._index,
              parseInt(id, 10),
              item._source.markImageFileName,
              id
            );
          }
        });
      }

      return data;
    });
  }

  getImgUrl(
    type: 'rutm' | 'wktm' | 'wotm' | 'rutmap',
    id: number,
    markImageFileName: string,
    fullId?: string
  ): string {
    switch (type) {
      case 'rutm':
        return `${RUTM_PATH}/${this.generatePath(id)}/${(fullId || id.toString()).replace('/', 'A')}/${markImageFileName}`;
      case 'wktm':
        return `${WKTM_PATH}/${this.generatePath(id)}/${(fullId || id.toString()).replace('/', 'A')}/${markImageFileName}`;
      case 'wotm':
        return `${WOTM_PATH}/${this.generatePath(id)}/${markImageFileName}`;
      case 'rutmap':
        const year = id.toString().slice(0, 4);
        const realId = parseInt(id.toString().slice(4), 10);
        return `${RUTMAP_PATH}/NEW${year}/${this.generatePath(realId)}/${id}/${markImageFileName}`;
    }
  }

  generatePath(num) {
    if (num < 1000) {
      return `0`;
    }
    const paths: string[] = [];
    const n = num.toString();
    for (let i = 1; n.length - i >= 3; i++) {
      paths.push(n.slice(0, i) + '0'.repeat(n.length - i));
    }
    return paths.join('/');
  }
}
