import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmbeddedSearchContainerComponent } from './embedded-search-container.component';

describe('EmbeddedSearchContainerComponent', () => {
  let component: EmbeddedSearchContainerComponent;
  let fixture: ComponentFixture<EmbeddedSearchContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EmbeddedSearchContainerComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmbeddedSearchContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
