import {
  TrademarksAllDataInterface,
  TrademarksDataInterface,
} from "../../models/embedded-search-result-trademarks.interface";
import {
  EmbSearchInitialDataInterface,
  MKTUInterface,
} from "../../models/embadded-search-request.interface";
import { Injectable, OnInit } from "@angular/core";
import { EmbeddedSearchContainerApiService } from "./embedded-search-container.api-service";
import { forkJoin, Observable } from "rxjs";
import {
  SearchResultsInterface,
  UserEventType,
} from "./embedded-search-container.interfaces";
import { map } from "rxjs/operators";
import { UserEventService } from "../../services/user-event.service";
import { RiskLevel } from "../../models/risk-level.enum";
import { ActivatedRoute, Router } from "@angular/router";
import { MKTUData } from "../../components/embedded-search-field/mktu";
import { Location } from "@angular/common";
import { IntercomEventsService } from "projects/shared/services/intercom-events.service";

@Injectable({
  providedIn: "root",
})
export class EmbeddedSearchContainerService {
  // dataDomains: DomainsDataInterface[];

  // dataLegalNames: LegalNamesDataInterface[];

  dataTrademarks: TrademarksDataInterface[];

  allTrademarks: TrademarksAllDataInterface;

  searchRequest: EmbSearchInitialDataInterface;

  MKTU: MKTUInterface[];

  searchText: string;

  isLoadingAll = false;

  constructor(
    private searchService: EmbeddedSearchContainerApiService,
    private readonly userEventService: UserEventService,
    readonly intercomEventsService: IntercomEventsService,
    private readonly location: Location,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  // сохраненный поиск в url
  searchRequestInitialByQuery(query: Record<string, string>): void {
    const text = query.q ?? "";
    const mktuList = query.mktu ? query.mktu.split(",") : [];

    const mktu = mktuList.map((num) =>
      MKTUData.find((i) => i.number === parseInt(num, 10))
    );

    this.searchRequestInitial({ text, mktu });
  }

  // сохраненный поиск
  searchRequestInitial(data): void {
    this.getSearchResult(data);
    this.searchRequest = {
      text: data.text,
      mktu: data.mktu,
    };
  }

  public searchByName(
    evt: EmbSearchInitialDataInterface
  ): Observable<SearchResultsInterface> {
    // const domains$ = this.searchDomains(evt);
    // const legalNames$ = this.searchLegalNames(evt);
    const tradeMarks$ = this.searchTrademarks(evt);

    return forkJoin([tradeMarks$]).pipe(
      map(([dataTrademarks]) => {
        return { dataTrademarks };
      })
    );
  }

  getSearchResult(evt: EmbSearchInitialDataInterface) {
    this.isLoadingAll = true;
    this.MKTU = evt.mktu;
    this.searchText = evt.text;
    this.searchRequest = { text: evt.text, mktu: evt.mktu };

    let path = this.location.path();
    if (path.includes("?")) {
      path = path.slice(0, path.indexOf("?"));
    }

    let query = `q=${evt.text}`;
    if (evt.mktu.length > 0 && evt.mktu.length !== 45) {
      query += `&mktu=${evt.mktu.map((i) => i.number).join(",")}`;
    }

    this.location.replaceState(path, query);

    this.searchByName(evt).subscribe((data: SearchResultsInterface) => {
      this.isLoadingAll = false;
      // domains
      // if (data.dataDomains.hits && data.dataDomains.hits.hits) {
      //   this.dataDomains = data.dataDomains.hits.hits;
      // }

      // legalNames
      // if (data.dataLegalNames.hits && data.dataLegalNames.hits.hits) {
      //   this.dataLegalNames = data.dataLegalNames.hits.hits;
      // }

      // trademarks
      if (data.dataTrademarks.hits && data.dataTrademarks.hits?.hits?.length) {
        this.intercomEventsService.push({ event: "searchResultNoUnique" });
      } else {
        this.intercomEventsService.push({ event: "searchResultUnique" });
      }
      if (data.dataTrademarks.hits?.hits) {
        data.dataTrademarks.hits.hits.forEach((item) => {
          let id = item._source.registrationString;
          if (item._index === "rutmap" && item._source.applicationString) {
            id = item._source.applicationString;
          }
          item._source.imageUrl = this.searchService.getImgUrl(
            item._index,
            parseInt(id, 10),
            item._source.markImageFileName,
            id
          );
        });
        this.allTrademarks = data.dataTrademarks;

        // не выводим товарные знаки с низким риском
        this.dataTrademarks = [];
        data.dataTrademarks.hits.hits.forEach((i) => {
          if (i._risk === "High" || i._risk === "Medium") {
            this.dataTrademarks.push(i);
          }
        });

        // нам надо отправлять user-event о том какая страница отобразилась
        const maxRisk = data.dataTrademarks?.risks?.max_risk;
        if (maxRisk === RiskLevel.high) {
          this.userEventService.push({ event: UserEventType.highRiskPage });
        } else if (maxRisk === RiskLevel.medium) {
          this.userEventService.push({ event: UserEventType.middleRiskPage });
        } else {
          this.userEventService.push({ event: UserEventType.lowRiskPage });
        }
      }
    });
  }

  searchTrademarks(
    evt: EmbSearchInitialDataInterface
  ): Observable<EmbSearchInitialDataInterface> {
    return this.searchService.getData({
      text: evt.text,
      gsNumber: this.getMKTUClassesForRequest(evt.mktu),
    });
  }
  //
  // searchDomains(evt: EmbSearchInitialDataInterface): Observable<EmbSearchInitialDataInterface> {
  //   return this.searchService.getData({
  //     text: evt.text,
  //     gsNumber: this.getMKTUClassesForRequest(evt.mktu),
  //     searchDomains: true
  //   });
  // }
  //
  // searchLegalNames(evt: EmbSearchInitialDataInterface): Observable<EmbSearchInitialDataInterface> {
  //   return this.searchService.getData({
  //     text: evt.text,
  //     gsNumber: this.getMKTUClassesForRequest(evt.mktu),
  //     searchLegalEntities: true
  //   });
  // }

  getMKTUClassesForRequest(mktu: MKTUInterface[]) {
    return mktu.length === 45 ? [] : mktu.map((i) => i.number);
  }
}
