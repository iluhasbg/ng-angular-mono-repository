import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { UserEventInterface } from '../containers/embedded-search-container/embedded-search-container.interfaces';

@Injectable({providedIn: 'root'})
export class UserEventService {
  private eventsSubj = new Subject<UserEventInterface>();
  readonly events$ = this.eventsSubj.asObservable();

  push(userEvent: UserEventInterface) {
    this.eventsSubj.next(userEvent);
  }
}
