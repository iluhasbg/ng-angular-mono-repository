import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule, Injector, ApplicationRef, DoBootstrap } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MatTooltipModule } from '@angular/material/tooltip';
import { SharedModule } from '../../../app-ipid/src/app/shared/shared.module';
import { IntercomService } from '../../../app-ipid/src/app/shared/services/intercom.service';
import { SearchModule } from './embedded-search/search.module';
import { EmbeddedSearchContainerComponent } from './embedded-search/containers/embedded-search-container/embedded-search-container.component';
import {ToastrModule} from 'ngx-toastr';
import {ErrorService} from '../../../app-ipid/src/app/shared/services/error-service';
import { NgxsModule } from '@ngxs/store';
import { WebModule } from '@web/web.module';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent
  ],
  exports: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    MatTooltipModule,
    SharedModule,
    SearchModule,
    ToastrModule.forRoot(),
    NgxsModule.forRoot(),
    WebModule.forRoot({
      production: environment.production,
      BASE_URL_API: environment.BASE_URL_API,
      BASE_ELASTIC_URL_API: '',
      isDebit: false,
    })
  ],
  providers: [
    IntercomService,
    ErrorService
  ],
  entryComponents: [
    EmbeddedSearchContainerComponent
  ]
})
export class IpIdLsSearchModule implements DoBootstrap {
  static isElements = false;

  constructor(private injector: Injector) {
  }

  ngDoBootstrap(appRef: ApplicationRef) {
    if (IpIdLsSearchModule.isElements) {
      const el = createCustomElement(EmbeddedSearchContainerComponent, {injector: this.injector});
      customElements.define('app-ls-embedded-search-container', el);
    } else {
      return appRef.bootstrap(AppComponent);
    }
  }
}
