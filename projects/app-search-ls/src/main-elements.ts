import { IpIdLsSearchModule } from './app/app.module';
import { environment } from './environments/environment';
import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

IpIdLsSearchModule.isElements = true;

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(IpIdLsSearchModule)
  .catch(err => console.error(err));
