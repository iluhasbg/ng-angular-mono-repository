# Установка

## Интеграция

Для интеграции поиска необходимо подключить файл search.js

[search.zip](search.zip ':ignore')

```html
<script src="embed-search/search.js"></script>
```

Далее в нужном месте вставляем тег

```html
  <app-ls-embedded-search-container></app-ls-embedded-search-container>
```

## Параметры

| Параметр                  | Описание                            | Пример        |
| :---                      |    :----:                           |          ---: |
| externalSearchData        | Параметры поиска по умолчанию       | {"text": "yandex", "mktu": [{"name": "Продукты питания", "number": 42}]}   |

## Пример передачи параметра поиску

Параметры передаются в виде аттрибутов тега `app-ls-embedded-search-container`.

Например:

```html
<app-ls-embedded-search-container 
  external-search-data='{"text": "yandex", "mktu": [{"name": "Продукты питания", "number": 42}]}'
></app-ls-embedded-search-container>
```

# События

## Прослушивание событий

```html
<script src="embed-search/search.js"></script>
<script>
  const appDiv = document.getElementsByTagName('app-ls-embedded-search-container')[0];

  appDiv.addEventListener('userEvent', function(userEvent) {
    // userEvent.detail = { event: 'sendSearch', data: { text: 'yandex' } }
  });
</script>
```

## Список событий

### sendSearch

Отправлен поисковый запрос

```js
{ event: 'sendSearch', query: 'Запрос', mktu: [{ number: 42, name: 'название мкту' }] }
```

### clickMKTU

Клик открывающий список всех МКТУ

```js
{ event: 'clickMKTU' }
```

### highRiskPage

Результат поиска с Высоким риском

```js
{ event: 'highRiskPage' }
```

### middleRiskPage

Результат поиска со Средним риском

```js
{ event: 'middleRiskPage' }
```

### lowRiskPage

Результат поиска с Низким риском

```js
{ event: 'lowRiskPage' }
```
