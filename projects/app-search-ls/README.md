## IP-ID Angular Elements Search v2

### Брендирование
- legal-support
- brand-search
- open (версия для банка Открытие)

### Локальный запуск

1. Устанавливаем зависимости проекта `yarn`
2. Локальный запуск `yarn serve:ls`

Локальный билд `yarn build:elements-ls`

###  CI

Ветка для CI билда: ls/search

После пуша в ветку билдится и деплоятся 3 версии поиска для разных лэндингов

#### 1) Версия для brand-search

http://ls-search.ls-search.lkdev.9958258.ru/brandsearch

документация по интеграции:

http://ls-search.ls-search.lkdev.9958258.ru/brandsearch/docs/#

#### 2) Версия для банка открытие

http://ls-search.ls-search.lkdev.9958258.ru/open/

документация по интеграции:

http://ls-search.ls-search.lkdev.9958258.ru/open/dosc/#

#### 1) Версия для legal-support (По состоянию на 17.11.2022 используется Angular Elements Search v1)

http://ls-search.ls-search.lkdev.9958258.ru/legalsupport/

документация по интеграции:

http://ls-search.ls-search.lkdev.9958258.ru/legalsupport/docs/#

### Документация по интеграции
[./docs/README.md](docs/README.md)
