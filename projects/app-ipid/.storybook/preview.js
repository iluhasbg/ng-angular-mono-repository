import { setCompodocJson } from "@storybook/addon-docs/angular";
const docJson = require('../documentation.json');

setCompodocJson(docJson);
