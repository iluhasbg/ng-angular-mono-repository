import { environment as env } from './environment';

export const environment = {
  ...env,
  production: true,
  BASE_URL_API: `${window.location.protocol}//${window.location.host}/api`,
  BASE_ELASTIC_URL_API: '',
};
