import { baseEnvironment } from './base.environment';

export const environment = {
  ...baseEnvironment,
  BASE_ELASTIC_URL_API: '',
  BASE_URL_OPEN: '',
  FRONT_URL: '',

  // Константы для поиска /search страница
  SEARCH_STYLES: {
    SEARCH_POSIBILITY_TEXT: 'Высокая с IP-ID',
    SEARCH_SPEEDOMETER_BACKGROUND: '#FFFFFF',
    SEARCH_SPEEDOMETER_OUTLINE: '#E1E5EB',
  },
};
