export const enum PAYMENT_TYPE {
  ONLINE = 'online',
  OFFLINE = 'offline',
  ACT = 'act',
};
