export const enum REQUEST_TYPE {
  TRADEMARK_SEARCH = 'trademarkSearch', // Заявка на "Поиск и оценку охраноспособности";
  TRADEMARK = 'trademark', // Заявка на "Товарный знак";
  PATENT = 'patent', // Заявка на "Патент";
  SOFT = 'soft', // Заявка на "Программу/Базу данных";
  RUMCC = 'rumcc', // Заявка на "Внесение ПО в реестр";
  CONTRACT = 'contract', // Заявка на "Договор";
  MONITORING = 'monitoring', // Заявка на "Мониторинг";
};
