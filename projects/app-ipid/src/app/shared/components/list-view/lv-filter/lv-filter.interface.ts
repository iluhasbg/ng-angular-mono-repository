export interface ILvFilter {
  readonly type: string;
  getFilterFn: () => (row: any) => boolean;
}
