import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentYandexComponent } from './payment-yandex.component';

describe('PaymentYandexComponent', () => {
  let component: PaymentYandexComponent;
  let fixture: ComponentFixture<PaymentYandexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PaymentYandexComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentYandexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
