export function downloadBaseFile(
  base64: string,
  fileName: string,
  mime: string
) {
  const a = document.createElement('a');
  a.href = `data:${mime};base64,${base64}`;
  a.download = fileName;
  a.click();
}

export function downloadBase64Xlsx(base64: string, fileName: string) {
  return downloadBaseFile(base64, fileName, 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
}
