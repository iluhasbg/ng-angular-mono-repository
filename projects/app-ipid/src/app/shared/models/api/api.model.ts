export interface IApiCountersResponse {
  portfolio: {
    tradermarks: number;
    patents: number;
    soft: number;
    domains: number;
    authorRights: number;
  },

  risks: {
    critical: number,
    high: number,
    medium: number,
    recommendation: number,
    unconnected: number
  },

  activeRequests: number;

  monitoring: number;
}
