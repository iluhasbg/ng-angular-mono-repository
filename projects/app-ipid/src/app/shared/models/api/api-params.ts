export class ApiParams {
  isElastic: boolean;
  isFile: boolean;
  token: string;
  isMoqPostman: boolean;

  constructor(init?: Partial<ApiParams>) {
    Object.assign(this, init);
  }
}