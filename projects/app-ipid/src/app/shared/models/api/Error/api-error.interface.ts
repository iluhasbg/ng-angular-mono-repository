export interface IApiError {
  code: string;
  messages: string[];
}

export class ApiError implements IApiError {
  code: string;
  messages: string[];

  constructor(init?: Partial<ApiError>) {
    Object.assign(this, init);
  }
}