import { ICountry, IMktu, IOkved } from 'projects/app-search/src/app/models/search.interfaces';
import {IImage} from '../../../../../web/features/corp/corp-search/services/corp-search-api.service';

export interface ISearchHistorySearchListResult {
  searches: ISearchHistorySearchListItem[];
}

export type ISearchHistoryReportResult = ISearchHistoryReportResultItem[];

// todo: тип
export type ISearchHistoryReportResultItem = any;


export interface ISearchHistorySearchListItem {
  searchID: string;
  date: string;
  user: string;
  right?: boolean;
  searchParameters: ISearchHistoryParams;
  count: number;
  countReport: number;
}

export interface ISearchHistorySearchResultItem {
  entity: ISearchHistorySearchResultItemEntity;
  result: any;
}


export interface ISearchHistoryDetail {
  searchParameters: ISearchHistoryParams;
  results: ISearchHistoryDetailResult[];
}

export interface ISearchHistoryDetailResult {
  entity: ISearchHistorySearchResultItemEntity;
  result: any;
}

export interface ISearchHistoryParams {
  searchType: string;
  text?: string;
  mktuList?: IMktu[];
  okvedList?: IOkved[];
  countriesList?: ICountry[];
  image?: IImage;
  registrationDate?: string[];
  applicationDate?: string[];
  priorityDate?: string;
  searchNumber?: string;
  searchLegalId?: string;
}

export interface ISearchHistoryAddToReportParams {
  searchID: string;

  entities: Array<{
    entity: ISearchHistorySearchResultItemEntity;
    strings: Array<{
      stringID: number;
      mark: boolean;
    }>,
  }>;
}

export interface ISearchHistoryPayload {
  searchID: string;
  limit?: number;
  offset?: number;
  entity?: ISearchHistorySearchResultItemEntity;
}

export type ISearchHistorySearchResultItemEntity = 'domain' | 'legalname' | 'trademark';
