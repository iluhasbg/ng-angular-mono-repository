export interface ILogin {
  answer?: string;
  token?: string;
  user?: any;
  caseID?: string;
  kontragents?: any;
}

export interface IQueryByRemoveObject {
  objects: any[];
  organization?: string;
}

export interface IUser {
  email: string;
  name: string;
  phone: string;
  userID: string;
  image?: string;
  role?: string;
  userInfo?: IUsersInfo;
}

export interface IUserInfo {
  userInfo: IUsersInfo;
}

export interface IUsersInfo {
  login?: string;
  name?: string;
  phone?: string;
  userID?: string;
  email?: string;
  position?: string;
  surname?: string;
  image?: string;
  userId?: any;
  certificate_of_completion: boolean;
  role?: string;
}

export interface IResponse {
  answer?: string;
  result?: any;
}

export interface IModalDataT {
  imgSrc?: any;
  name?: any;
  position?: any;
  title?: any;
  popupText?: any;
  date?: any;
  files?: any;
  type?: any;
  id?: any;
  flagBlue?: boolean;
  status?: any;
}

export interface ISearchObjects {
  index?: string;
  name?: string;
  number?: string;
  _source?: string[];
  SHA1hash?: string;
}

export interface ISearchINN {
  id?: string;
  searchTradeMarks?: boolean;
  searchDomains?: boolean;
  SHA1hash?: string;
}

export interface IMainData {
  hits?: IHitsData;
  max_score?: any;
  total?: {};
  timed_out?: any;
  took?: any;
  _shards?: {};
}

export interface IHitsData {
  hits?: any[];
}

export interface IMainSearchData {
  _id?: string;
  _index?: any;
  _score?: number;
  _source: IAllPossibleFields;
  _type?: any;
}

export interface IAllPossibleFields {
  _index?: any;
  registrationString?: any;
  applicationString?: any;
  imageText?: any;
  expiryDate?: any;
  TIN?: any;
  PSRN?: any;
  goodsServicesClassNumber?: any;
  goodsServices?: any;
  applicantName?: any;
  markImageFileName?: any;
  inventionName?: any;
  patentStartingDate?: any;
  patentGrantPublishDate?: any;
  patentGrantPublishNumber?: any;
  programName?: any;
  applicationNumber?: any;
  registrationNumber?: any;
  registrationDate?: any;
  authors?: any;
  patentHolders?: any;
  actual?: any;
  dbName?: any;
  domain?: any;
  zone?: any;
  fullDomain?: any;
  domainRegistrator?: any;
  imgUrl?: any;
  rightHolders?: string;
  checked?: boolean;
  payDate?: string;
}

export interface IRutBasicResponse {
  _index?: any;
  registrationString?: any;
  applicationString?: any;
  imageText?: any;
  TIN?: any;
  PSRN?: any;
  goodsServicesClassNumber?: any;
  markImageFileName?: any;
}

export interface IPatentResponse {
  inventionName?: any;
  patentStartingDate?: any;
  patentGrantPublishDate?: any;
  patentGrantPublishNumber?: any;
  applicationNumber?: any;
  registrationNumber?: any;
  registrationDate?: any;
  authors?: any;
  patentHolders?: any;
}

export interface IProgramsResponse {
  programName?: any;
  applicationNumber?: any;
  registrationNumber?: any;
  registrationDate?: any;
  authors?: any;
  patentHolders?: any;
  actual?: any;
}

export interface IRudbResponse {
  dbName?: any;
  applicationNumber?: any;
  registrationNumber?: any;
  registrationDate?: any;
  authors?: any;
  patentHolders?: any;
  actual?: any;
}

export interface IDomainResponse {
  domain?: any;
  zone?: any;
  fullDomain?: any;
  TIN?: any;
  PSRN?: any;
}

export interface ITrademarks {
  name: string;
  id: string;
  type: string;
  imgUrl: string;
  author: string;
  rightHolder: string;
  validUntil: string;
}

export interface IContractsParrent {
  contracts: IContracts;
}

export interface IContracts {
  assignee?: IAssignee;
  assignor?: IAssignor;
  contractDate?: string;
  contractNumber?: string;
  contractTerms?: string;
  contractType?: string;
  dateOfValidity?: string;
  comment?: string;
  tradeMarks?: ITrademarks[];
  valid?: boolean;
  type?: string;
  name?: string;
  customName?: string;
  _id?: string;
  id?: string;
  _options?: string[];
  changesContract?: any[];
}

export interface IPatents {
  author?: string;
  id?: string;
  index?: {
    id: string;
  }[];
  isMonitoring?: boolean;
  name?: string;
  registrationDate?: string;
  rightHolder?: string;
  showId?: string;
  type?: string;
  isRequest?: boolean;
  status?: any;
}

export interface ISigns {
  id?: string;
  imageURL?: string;
  index?: {
    id: string;
  }[];
  isMonitoring?: boolean;
  name?: string;
  registrationDate?: string;
  applicationDate?: string;
  rightHolder?: string;
  showId?: string;
  type?: string;
  validUntil?: string;
  solutionIsRequired?: any;
  isRequest?: boolean;
  status?: any;
  stage?: string;
  dateEnd?: any;
  client?: string;
}

export interface IDataBases {
  author?: string;
  id?: string;
  index?: {
    id: string;
  }[];
  isMonitoring?: boolean;
  name?: string;
  registrationDate?: string;
  rightHolder?: string;
  showId?: string;
  type?: string;
  isRequest?: boolean;
  checked?: boolean;
}

export interface IDomains {
  domainAdministrator?: string;
  id?: string;
  index?: {
    id: string;
  }[];
  isMonitoring?: boolean;
  name?: string;
  showId?: string;
  type?: string;
  validUntil?: string;
  isRequest?: boolean;
  status?: any;
  domainOwner?: string;
}


export interface IAssignee {
  PSRN?: string;
  TIN?: string;
  address?: string;
  applicantStatus?: number;
  countryCode?: string;
  name?: string;
  statusOfMatching?: string;
}

export interface IAssignor {
  PSRN?: string;
  TIN?: string;
  address?: string;
  applicantStatus?: number;
  countryCode?: string;
  name?: string;
  statusOfMatching?: string;
}

export interface ITrademarks {
  goodsServices?: IGoods[];
}

export interface IGoods {
  imageText?: string;
  index?: string;
  registrationDate?: string;
  registrationString?: string;
  name?: string;
  id?: string;
  type?: string;
  imgUrl?: string;
  author?: string;
  rightHolder?: string;
  validUntil?: string;
}

export interface IOrg {
  expiringAttorneys?: IExpAttorneys[];
  organizations?: IOrganization[];
}

export interface IExpAttorneys {
  user?: string;
  shortName?: string;
  date?: string;
}

export interface IOrganization {
  name?: string;
  id?: string;
  TIN?: string;
  Icases?: IActiveCases[];
  PSRN?: string;
  director?: string;
  legalAdress?: string;
  pending?: boolean;
  representatives?: IRepresentatives[];
}

export interface IRepresentatives {
  surname?: string;
  name?: string;
  middleName?: string;
  performance?: string;
  email?: string;
  image?: string;
  id?: string;
  position?: string;
  role?: string;
  status?: string;
}

export interface IActiveCases {
  description?: string;
  number?: string;
  type?: string;
  title?: string;
  status?: string;
  opened?: boolean;
}

export interface ICasesCount {
  count: number;
}

export interface ITasks {
  tasks: ITasksEntry;
}

export interface ITasksEntry {
  displayMode?: number;
  items: ITasksItems[];
}

export interface IEvents {
  events: IEventItem;
}

export interface IEventItem {
  items: IEventItems;
}

export interface IEventItems {
  warn?: boolean;
  doc?: boolean;
  imgSrc?: string;
  flagBlue?: boolean;
  expertName?: string;
  expertPosition?: string;
  date?: string;
  status?: string;
  title?: string;
  eventID?: string;
  readStatus?: boolean;
  Type?: string;
  file?: {
    fileSrc?: string;
    fileText?: string;
  };
  popupTitle?: string;
  popupText?: string;
  popupFooterDate?: string;
  popupFiles?: IPopupFile[];
  popupAgreeBtn?: boolean;
  last?: boolean;
}

export interface IPopupFile {
  file?: {
    name?: string;
    link?: string;
  };
}

export interface ITracks {
  tracks: ITrackItem;
}

export interface ITrackItem {
  items: ITrackItems;
}

export interface ITrackItems {
  name?: string;
  reach?: boolean;
  first?: boolean;
  date?: string;
  stagedate?: string;
  class?: string;
  svgClass?: string;
}

export interface ITasksItems {
  ID?: string;
  OKVED?: any;
  active?: true;
  designationType?: string;
  designationValue?: string;
  details?: { title?: string, description: string, progress: number };
  director?: string;
  employeeSelectClasses?: boolean;
  info?: ITasksInfo[];
  inn?: string;
  lastStage?: number;
  legalAdress?: string;
  number?: string;
  opened?: boolean;
  status?: string;
  tariff?: string;
  toAgreement?: number;
  type?: string;
  warn?: boolean;
}

export interface IProxies {
  attorneys?: IProxieObject[];
}

export interface IFileProxy {
  name?: string;
  path?: string;
}

export interface IProxieObject {
  attorney: IProxie[];
  file?: IFileProxy;
  checked?: boolean;
}

export interface IProxie {
  id?: string;
  user?: string;
  userID?: string;
  status?: string;
  dateLoad?: string;
  dateStart?: string;
  dateEnd?: string;
  organization?: string;
}

export interface IPayments {
  payments?: IPayment[];
}

export interface IPayment {
  date?: string;
  name?: string;
  number?: string;
  client?: string;
  document?: string;
  status?: string;
  sum?: number;
  deposit?: boolean;
}

export interface ITasksInfo {
  title?: string;
  text?: string;
}

export interface IOrgDefault {
  error?: any;
  success?: any;
}

export interface IFiles {
  name?: string;
  publicPath?: string;
  size?: number;
}

export interface IFileArray {
  files?: any;
}

export interface IRights {
  rights: IRightsList[];

}

export interface IRightsList {
  title: string;
  data: IRightsItem[];
}

export interface IServicesSlider {
  items?: IServicesItem;
}

export interface IServicesItem {
  id?: string;
  name?: string;
  nameEng?: string;
  value?: boolean;
}

export interface IServices {
  items?: IServiceCards;
}

export interface IServiceCards {
  cards?: IServiceCard;
  title?: string;
  checked?: boolean;
}

export interface IServiceCard {
  description?: string;
  links?: IServiceCardLink;
  title?: string;
}

export interface IServiceCardLink {
  id?: string;
  name?: string;
  stateBody?: IServiceState;
}

export interface IServiceDetails {
  rates?: IServiceRates;
  title?: string;
  fastRegistration?: boolean;
}

export interface IServiceRates {
  about?: IServiceAbout;
  boards?: IServiceBoards[];
  default?: boolean;
  id?: string;
  informationPrice?: IServicePrice;
  name?: string;
  recommendation?: IServiceRecommendation;
  serviceComposition?: IServiceComposition[];
}

export interface IServiceAbout {
  title?: string;
  items?: IAboutItems;
}

export interface IAboutItems {
  name?: string;
  value?: string;
}

export interface IServiceBoards {
  description?: string;
  title?: string;
  value?: string;
}

export interface IServicePrice {
  description?: string;
  tablePrice?: IColumnItem[];
  leftTitle?: string;
  rightTitle?: string;
  title?: string;
}

export interface IColumnItem {
  name?: string;
  leftValue?: string;
  rightValue?: string;
}

export interface IServiceRecommendation {
  text?: string;
  title?: string;
}

export interface IServiceComposition {
  title?: string;
  item?: string[];
}

export interface IServiceState {
  id?: string;
}

export interface IState {
  items?: string[];
}

// @ts-ignore
export type ICompanySearchParam = { TIN: string, PSRN?: string } | { PSRN: string, TIN?: string } | { PSRN: Array[string], TIN?: string };


export interface IRightsItem {
  id?: string;
  name?: string;
  value?: string | boolean;
  checked?: boolean;
  nameEng?: string;
}

export interface IPromoCode {
  PromocodeID?: string;
}

// @ts-ignore
