export interface Disputs {
  status: string;
  applicant: string;
  respondent: string;
  number: string;
  court: string;
  stage: string;
}

export interface Courts {
  status: string;
  applicant: string;
  respondent: string;
  number: string;
  court: string;
  stage: string;
}

export interface Claims {
  type: string;
  obj: string;
  objItem: string;
  badSide: string;
  reqSum: string;
  reqText: string;
  place: string;
  date: string;
  state: string;
}

export interface ItemForSelection {
  id: number;
  title: string;
  isChecked: boolean;
}
