export interface ICrudParams {
  path: string;
  subjectType?: string; // used for popup messages that tell what was updated
}

export interface IGetParams extends ICrudParams {
  params?: any;
  isFile?: boolean;
}

export interface IPostParams extends ICrudParams {
  body?: any;
  isFile?: boolean;
  isRequestToElastic?: boolean;
  customToken?: string;
  params?: any;
  responseType?: IResponseType;
}

export type IResponseType = 'arraybuffer' | 'blob' | 'json' | 'text';

export interface IPutParams extends ICrudParams {
  body?: any;
  isFile?: boolean;
}

export interface IDeleteParams extends ICrudParams {
  id?: string | number;
}
