import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MktuModalComponent } from './mktu-modal.component';

describe('MktuModalComponent', () => {
  let component: MktuModalComponent;
  let fixture: ComponentFixture<MktuModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MktuModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MktuModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
