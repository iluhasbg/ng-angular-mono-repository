import { Component, OnInit } from '@angular/core';
import { filter, map, take } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { IntegrationService } from '../../../../shared/services/integration.service';

@Component({
  selector: 'app-ref-risk-integration',
  templateUrl: './risk-integration.component.html',
  styleUrls: ['./risk-integration.component.scss']
})
export class RiskIntegrationComponent implements OnInit {

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly integrationService: IntegrationService,
  ) { }

  ngOnInit(): void {
    this.activatedRoute.queryParamMap
      .pipe(
        map(i => i.get('id')?.toString()),
        filter(i => !!i),
        take(1)
      )
      .subscribe((refRiskId) => {
        this.integrationService.setRiskId(refRiskId);

        this.router.navigate(['/monitoring/landing'], { queryParams: {id: refRiskId}});
        // if (!localStorage.getItem('currentUserData')) {
        //   this.router.navigate(['/']);
        // }
        // else {
        //   if (localStorage.getItem('integration/alfa_is_registration_page')) {
        //     localStorage.removeItem('integration/alfa_is_registration_page');
        //     this.router.navigate(['/registration']);
        //   } else {
        //     this.router.navigate(['/login']);
        //   }
        // }
      });
  }

}
