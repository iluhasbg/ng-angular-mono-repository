import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TariffForContestationComponent } from './tariff-for-contestation.component';

describe('TariffForContestationComponent', () => {
  let component: TariffForContestationComponent;
  let fixture: ComponentFixture<TariffForContestationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TariffForContestationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TariffForContestationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
