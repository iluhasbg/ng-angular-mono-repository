import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RiskTrademarkModalComponent } from './risk-trademark-modal.component';

describe('RiskTrademarkModalComponent', () => {
  let component: RiskTrademarkModalComponent;
  let fixture: ComponentFixture<RiskTrademarkModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RiskTrademarkModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RiskTrademarkModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
