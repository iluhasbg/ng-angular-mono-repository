import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RiskPatentModalComponent } from './risk-patent-modal.component';

describe('RiskPatentModalComponent', () => {
  let component: RiskPatentModalComponent;
  let fixture: ComponentFixture<RiskPatentModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RiskPatentModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RiskPatentModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
