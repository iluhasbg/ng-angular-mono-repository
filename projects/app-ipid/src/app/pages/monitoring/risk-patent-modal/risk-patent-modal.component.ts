import { Component, EventEmitter, HostListener, Input, OnInit, Output } from '@angular/core';
import { SupportChatService } from '../../../../../../shared/services/support-chat.service';

@Component({
  selector: 'app-risk-patent-modal',
  templateUrl: './risk-patent-modal.component.html',
  styleUrls: ['./risk-patent-modal.component.scss']
})
export class RiskPatentModalComponent implements OnInit {

  @Output() modalEvent = new EventEmitter();
  status = 'Рекомендация';

  constructor(private supportChatService: SupportChatService
  ) {
  }

  ngOnInit(): void {
  }

  closeModal() {
    this.modalEvent.emit();
  }

  @HostListener('document:keyup', ['$event']) // 27=esc
  keyup(event: KeyboardEvent): void {
    if (event.keyCode === 27) {
      this.closeModal();
    }
  }

  onClickBtn(btnTitle: string) {
    this.supportChatService.open(btnTitle);
  }
}
