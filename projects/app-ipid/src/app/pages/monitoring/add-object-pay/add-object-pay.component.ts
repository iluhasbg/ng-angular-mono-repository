import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IntercomEventsService, INTERCOM_EVENT_NAME } from 'projects/shared/services/intercom-events.service';
import { PAYMENT_TYPE } from '../../../shared/enums/payment-type';
import { REQUEST_TYPE } from '../../../shared/enums/request-type';
import { MonitoringAddService } from '../shared/monitoring-add.service';

@Component({
  selector: 'app-add-object-pay',
  templateUrl: './add-object-pay.component.html',
  styleUrls: ['./add-object-pay.component.scss']
})
export class AddObjectPayComponent implements OnInit {

  returnAfterPayment = false;
  yaLastPaymentId;
  error;
  paymentSum;

  constructor(
    readonly activatedRoute: ActivatedRoute,
    readonly monitoringAddService: MonitoringAddService,
    private readonly intercomEventsService: IntercomEventsService,
  ) { }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe((params) => {
      this.returnAfterPayment = params.returnAfterPayment === 'true';
      this.yaLastPaymentId = params.t;
    });
    this.paymentSum = this.monitoringAddService.totalPrice;
  }

  get idApplication() {
    return this.monitoringAddService.data.responseID;
  }

  switchPaymentType(type: PAYMENT_TYPE) {
    this.intercomEventsService.push({
      event: type === PAYMENT_TYPE.ONLINE
        ? INTERCOM_EVENT_NAME.CLICK_PAYMENT_ONLINE
        : type === PAYMENT_TYPE.OFFLINE && INTERCOM_EVENT_NAME.CLICK_PAYMENT_BILL,
      itemType: REQUEST_TYPE.MONITORING,
    });
    this.monitoringAddService.setPaymentType(type);
  }

  selectPayer(data) {
    this.monitoringAddService.setPayer(data);
  }

  get payer() {
    return this.monitoringAddService.payer;
  }


  onlineError(error) {
    this.error = error;
  }

  toSuccessPage(data?) {
    this.monitoringAddService.goSuccessStage();
    this.intercomEventsService.push({
      event: Boolean(data)
        ? INTERCOM_EVENT_NAME.FORM_PAYMENT_ONLINE_SUCCESS
        : INTERCOM_EVENT_NAME.FORM_PAYMENT_BILL_SUCCESS,
      itemType: REQUEST_TYPE.MONITORING,
    });
  }

  paymentOnlineSuccessed(data) {
    if (data.status === 'succeeded') {
      this.toSuccessPage(data);
    }
  }

  get paymentType() {
    return this.monitoringAddService.paymentType;
  }

  backStep() {
    this.monitoringAddService.data.stage = 1;
  }
}
