import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddObjectPayComponent } from './add-object-pay.component';

describe('AddObjectPayComponent', () => {
  let component: AddObjectPayComponent;
  let fixture: ComponentFixture<AddObjectPayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddObjectPayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddObjectPayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
