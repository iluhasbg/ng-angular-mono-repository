export interface IMonitoringAddObjects {
  objects: IMonitoringObject[];
}

export interface IMonitoringAddObjectsStage1 extends IMonitoringAddObjects {
  organization: string;
}

export interface IMonitoringAddObjectsStage2 extends IMonitoringAddObjects {
  responseID: string;
  tariffID?: string;
}

export interface IMonitoringAddObjectsStage3 {
  responseID: string;
  inn?: string;
  ogrn?: string;
  name?: string;
  address?: string;
  sum?: string;
  type: string;
}

export interface IMonitoringAddObjectsResponse {
  responseID: string;
}

export interface IMonitoringAddObjectsDetail {
  stage: 1 | 2 | 3;
  responseID: string;
  tariffID?: string;
  objects: IMonitoringObject[];
  excludedObjects: IMonitoringObject[];
  organizationId?: string;
  interestData?: object;
}

export interface IMonitoringObject {
  ID: number;
  index: string[];
  name: string;
  type: string;
}
