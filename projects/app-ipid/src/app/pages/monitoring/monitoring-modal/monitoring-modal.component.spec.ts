import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonitoringModalComponent } from './monitoring-modal.component';

describe('MonitoringModalComponent', () => {
  let component: MonitoringModalComponent;
  let fixture: ComponentFixture<MonitoringModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonitoringModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonitoringModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
