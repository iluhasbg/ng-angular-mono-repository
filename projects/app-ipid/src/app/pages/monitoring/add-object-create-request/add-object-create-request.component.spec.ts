import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddObjectCreateRequestComponent } from './add-object-create-request.component';

describe('AddObjectCreateRequestComponent', () => {
  let component: AddObjectCreateRequestComponent;
  let fixture: ComponentFixture<AddObjectCreateRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AddObjectCreateRequestComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddObjectCreateRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

