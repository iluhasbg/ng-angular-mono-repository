import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RiskContractModalComponent } from './risk-contract-modal.component';

describe('RiskContractModalComponent', () => {
  let component: RiskContractModalComponent;
  let fixture: ComponentFixture<RiskContractModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RiskContractModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RiskContractModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
