import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonitoringLandingAlfaComponent } from './monitoring-landing-alfa.component';

describe('MonitoringLandingAlfaComponent', () => {
  let component: MonitoringLandingAlfaComponent;
  let fixture: ComponentFixture<MonitoringLandingAlfaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonitoringLandingAlfaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonitoringLandingAlfaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
