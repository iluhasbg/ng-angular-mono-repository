import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RiskDomainModalComponent } from './risk-domain-modal.component';

describe('RiskDomainModalComponent', () => {
  let component: RiskDomainModalComponent;
  let fixture: ComponentFixture<RiskDomainModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RiskDomainModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RiskDomainModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
