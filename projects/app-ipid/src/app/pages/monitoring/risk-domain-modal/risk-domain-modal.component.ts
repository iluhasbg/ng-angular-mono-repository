import { Component, EventEmitter, HostListener, OnInit, Output } from '@angular/core';
import { SupportChatService } from '../../../../../../shared/services/support-chat.service';

@Component({
  selector: 'app-risk-domain-modal',
  templateUrl: './risk-domain-modal.component.html',
  styleUrls: ['./risk-domain-modal.component.scss']
})
export class RiskDomainModalComponent implements OnInit {
  status = 'Рекомендация';
  @Output() modalEvent = new EventEmitter();

  constructor(private supportChatService: SupportChatService
  ) {
  }

  ngOnInit(): void {
  }

  closeModal() {
    this.modalEvent.emit();
  }

  @HostListener('document:keyup', ['$event']) // 27=esc
  keyup(event: KeyboardEvent): void {
    if (event.keyCode === 27) {
      this.closeModal();
    }
  }

  onClickBtn(btnTitle: string) {
    this.supportChatService.open(btnTitle);
  }
}
