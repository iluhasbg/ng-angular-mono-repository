import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpenIndexComponent } from './open-index.component';

describe('OpenIndexComponent', () => {
  let component: OpenIndexComponent;
  let fixture: ComponentFixture<OpenIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpenIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
