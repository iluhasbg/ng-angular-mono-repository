import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OpenTopMenuComponent } from './open-top-menu.component';

describe('OpenTopMenuComponent', () => {
  let component: OpenTopMenuComponent;
  let fixture: ComponentFixture<OpenTopMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OpenTopMenuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenTopMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
