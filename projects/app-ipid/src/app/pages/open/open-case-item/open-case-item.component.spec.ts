import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OpenCaseItemComponent } from './open-case-item.component';

describe('OpenCaseItemComponent', () => {
  let component: OpenCaseItemComponent;
  let fixture: ComponentFixture<OpenCaseItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OpenCaseItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenCaseItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
