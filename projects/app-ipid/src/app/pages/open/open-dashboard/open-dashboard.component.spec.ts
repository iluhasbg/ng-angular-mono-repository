import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpenDashboardComponent } from './open-dashboard.component';

describe('OpenDashboardComponent', () => {
  let component: OpenDashboardComponent;
  let fixture: ComponentFixture<OpenDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpenDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
