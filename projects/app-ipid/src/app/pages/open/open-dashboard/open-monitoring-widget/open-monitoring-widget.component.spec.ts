import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OpenMonitoringWidgetComponent } from './open-monitoring-widget.component';

describe('OpenMonitoringWidgetComponent', () => {
  let component: OpenMonitoringWidgetComponent;
  let fixture: ComponentFixture<OpenMonitoringWidgetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OpenMonitoringWidgetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenMonitoringWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
