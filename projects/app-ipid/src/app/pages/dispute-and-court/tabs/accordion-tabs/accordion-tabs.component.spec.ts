import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccordionTabsComponent } from './accordion-tabs.component';

describe('AccordionTabsComponent', () => {
  let component: AccordionTabsComponent;
  let fixture: ComponentFixture<AccordionTabsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AccordionTabsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccordionTabsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
