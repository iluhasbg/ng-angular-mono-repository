import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocsTabComponent } from './docs-tab.component';

describe('DocsTabComponent', () => {
  let component: DocsTabComponent;
  let fixture: ComponentFixture<DocsTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DocsTabComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocsTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
