import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourtsIndexTableComponent } from './courts-index-table.component';

describe('CourtsIndexTableComponent', () => {
  let component: CourtsIndexTableComponent;
  let fixture: ComponentFixture<CourtsIndexTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CourtsIndexTableComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourtsIndexTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
