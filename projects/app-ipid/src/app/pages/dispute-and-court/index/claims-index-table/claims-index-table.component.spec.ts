import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClaimsIndexTableComponent } from './claims-index-table.component';

describe('ClaimsIndexTableComponent', () => {
  let component: ClaimsIndexTableComponent;
  let fixture: ComponentFixture<ClaimsIndexTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ClaimsIndexTableComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClaimsIndexTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
