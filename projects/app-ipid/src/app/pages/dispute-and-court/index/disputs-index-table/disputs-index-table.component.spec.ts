import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisputsIndexTableComponent } from './disputs-index-table.component';

describe('DisputsIndexTableComponent', () => {
  let component: DisputsIndexTableComponent;
  let fixture: ComponentFixture<DisputsIndexTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DisputsIndexTableComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisputsIndexTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
