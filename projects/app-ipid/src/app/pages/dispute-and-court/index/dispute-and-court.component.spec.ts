import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisputeAndCourtComponent } from './dispute-and-court.component';

describe('DisputeAndCourtComponent', () => {
  let component: DisputeAndCourtComponent;
  let fixture: ComponentFixture<DisputeAndCourtComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DisputeAndCourtComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisputeAndCourtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
