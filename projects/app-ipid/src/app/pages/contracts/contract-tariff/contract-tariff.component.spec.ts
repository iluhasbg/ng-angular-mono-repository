import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractTariffComponent } from './contract-tariff.component';

describe('ContractTariffComponent', () => {
  let component: ContractTariffComponent;
  let fixture: ComponentFixture<ContractTariffComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractTariffComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractTariffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
