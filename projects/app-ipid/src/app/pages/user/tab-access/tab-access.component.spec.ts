import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabAccessComponent } from './tab-access.component';

describe('TabAccessComponent', () => {
  let component: TabAccessComponent;
  let fixture: ComponentFixture<TabAccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabAccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabAccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
