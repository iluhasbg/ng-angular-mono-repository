import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabUsersRightsComponent } from './tab-users-rights.component';

describe('UsersRightsComponent', () => {
  let component: TabUsersRightsComponent;
  let fixture: ComponentFixture<TabUsersRightsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabUsersRightsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabUsersRightsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
