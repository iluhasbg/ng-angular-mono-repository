import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabPaymentHistoryComponent } from './tab-payment-history.component';

describe('TabPaymentHistoryComponent', () => {
  let component: TabPaymentHistoryComponent;
  let fixture: ComponentFixture<TabPaymentHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabPaymentHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabPaymentHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
