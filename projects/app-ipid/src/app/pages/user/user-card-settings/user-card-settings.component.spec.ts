import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserCardSettingsComponent } from './user-card-settings.component';

describe('UserCardSettingsComponent', () => {
  let component: UserCardSettingsComponent;
  let fixture: ComponentFixture<UserCardSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserCardSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserCardSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
