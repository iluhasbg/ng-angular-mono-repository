import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabAttorneyComponent } from './tab-attorney.component';

describe('TabAttorneyComponent', () => {
  let component: TabAttorneyComponent;
  let fixture: ComponentFixture<TabAttorneyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabAttorneyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabAttorneyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
