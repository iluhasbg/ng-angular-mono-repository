import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabOrganizationsIpComponent } from './tab-organizations-ip.component';

describe('TabOrganizationsIpComponent', () => {
  let component: TabOrganizationsIpComponent;
  let fixture: ComponentFixture<TabOrganizationsIpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabOrganizationsIpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabOrganizationsIpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
