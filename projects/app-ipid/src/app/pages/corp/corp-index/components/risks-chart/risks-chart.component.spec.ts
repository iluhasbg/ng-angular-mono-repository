import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RisksChartComponent } from './risks-chart.component';

describe('RisksChartComponent', () => {
  let component: RisksChartComponent;
  let fixture: ComponentFixture<RisksChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RisksChartComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RisksChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
