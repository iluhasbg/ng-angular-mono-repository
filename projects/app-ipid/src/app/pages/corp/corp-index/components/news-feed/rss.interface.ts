export interface RssAnswerInterface {
  rss: RssInterface;
}

export interface RssInterface {
  $: VersionInterface;
  channel: ChannelInterface;
}

export interface VersionInterface {
  version: string;
}

export interface ChannelInterface {
  description: string;
  item: NewsItem[];
  lastBuildDate: string;
  link: string;
  title: string;
  ttl: string;
}

export interface NewsItem {
  category: string;
  description: string;
  link: string;
  pubDate: string;
  title: string;
}
