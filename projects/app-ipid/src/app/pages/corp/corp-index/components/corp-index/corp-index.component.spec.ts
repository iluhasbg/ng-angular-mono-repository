import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CorpIndexComponent } from './corp-index.component';

describe('IndexCorpComponent', () => {
  let component: CorpIndexComponent;
  let fixture: ComponentFixture<CorpIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CorpIndexComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CorpIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
