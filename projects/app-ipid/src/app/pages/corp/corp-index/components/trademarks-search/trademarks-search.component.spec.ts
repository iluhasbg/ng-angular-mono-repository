import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrademarksSearchComponent } from './trademarks-search.component';

describe('TrademarksSearchComponent', () => {
  let component: TrademarksSearchComponent;
  let fixture: ComponentFixture<TrademarksSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TrademarksSearchComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrademarksSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
