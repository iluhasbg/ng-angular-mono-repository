export interface NewsInterface {
  title: string;
  date: string;
  link: string;
}
