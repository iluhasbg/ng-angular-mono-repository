import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormNotResidentComponent } from './form-not-resident.component';

describe('FormNotResidentComponent', () => {
  let component: FormNotResidentComponent;
  let fixture: ComponentFixture<FormNotResidentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormNotResidentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormNotResidentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
