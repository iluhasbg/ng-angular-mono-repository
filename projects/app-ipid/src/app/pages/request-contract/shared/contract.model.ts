
// export interface IRequestContract {
//   type: 'contract';
// }
export interface IRequestRecipient {
  notResident: boolean;
  inn?: string;
  ogrn?: string;
  name: string;
  shortName?: string;
  bik?: string;
  account?: string;
  kio?: string;
  director?: string;
  address: string;
}
/**
 * Модель для отправки Сторон договора
 */
export interface IRequestContractStage2 {
  responseID?: string;
  owners: IRequestRecipient[];
  recipients: IRequestRecipient[];
}
/**
 * Модель для отправки Объектов договора
 */
export interface IRequestContractStage3 {
  responseID?: string;
  Objects: IRequestContractObject[];
}

/**
 * Модель для ответа по запросам
 */
export interface IRequestContractResponse {
  responseID: string;
}
/**
 * Модель для ответа по запросу риска
 */
export interface IRequestContractResponseRisk {
  responseID: string;
  risk: boolean;
  usrle: IRiskSide;
  rospatent: IRiskSide;
}

/**
 * Модель для восстановления из черновика
 */
export interface IRequestContractDetail {
  stage: 1 | 2 | 3 | 4 | 5;
}

/**
 * Модель для получения объектов договора
 */
export interface IRequestContractObject {
  id: string;
  index: string;
  name?: string;
  imageURL?: string;
  rightholder?: string;
  date?: string;
  elasticObject?: boolean;
  checked?: boolean;
}

/**
 * Модель для риска
 */
export interface IRiskSide {
  name: string;
  address: string;
}
/**
 * Модель для отправки Вида договора
 */
export interface IRequestContractStage1 {
  responseID: string;
  contractType?: TypeContract;
  contractAction?: ActionContract;
}

/**
 * Типы договоров
 */
export type TypeContract = 'license' |
  'concession' |
  'completeAlienation' |
  'pledge' |
  'sublicense' |
  'subconcession' |
  'partialAlienation' |
  'repledge' |
  'change' |
  'termination' |
  'unknown';

/**
 * Действие с договором
 */
export type ActionContract = 'change' | 'termination' | 'registration';

/**
 * Модель для отправки Предмета договора
 */
export interface IRequestContractStage4 {
  responseID: string;
  contractType: TypeContract;
  subject: IRequestContractSubject;
}

/**
 * Модель Предмета договора
 */
export interface IRequestContractSubject {
  exclusivity?: string;
  territorialLimits ?: string;
  territorialSubject ?: string[];
  territorialLimitsString?: string;
  periodType?: string;
  periodMonths?: number;
  periodYears?: number;
  periodDate?: number;
  amountOfCompensation: string;
  currencyType: string;
  VAT: string;
  mannerOfPayment: string;
  mannerOfPaymentString: string;
  paymentStart: string;
  paymentStartString: string;
  subconcessionRight?: string;
  subconcessionRightString?: string;
  complex?: string;
  goodsAndServices: string;
  goodsAndServicesString: string;
  fieldsOfApplication?: {
    goods: boolean;
    services: boolean;
    documentation: boolean;
    advertising: boolean;
    internet: boolean;
    textOfApplication: string;
  };
  misc?: string;
  files?: object[];
}

/**
 * Модель для отправки Тарифа
 */
export interface IRequestContractStage5 {
  responseID: string;
  tariffID: string;
}

/**
 * Модель для отправки Оплаты
 */
export interface IRequestContractStage6 {
  responseID: string;
  inn?: string;
  ogrn?: string;
  name?: string;
  address?: string;
  sum?: string;
  type: string;
  yandexResponse: object;
}
