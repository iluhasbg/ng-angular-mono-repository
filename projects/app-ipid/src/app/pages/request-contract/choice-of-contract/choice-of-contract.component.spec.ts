import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChoiceOfContractComponent } from './choice-of-contract.component';

describe('ChoiceOfContractComponent', () => {
  let component: ChoiceOfContractComponent;
  let fixture: ComponentFixture<ChoiceOfContractComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChoiceOfContractComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChoiceOfContractComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
