/**
 * Модель для ответа по запросам
 */
export interface IRequestCheckLabelResponse {
  responseID: string;
}

/**
 * Модель для отправки данных с первого шага
 */
export interface IRequestCheckLabelStage3 {
  responseID: string;
  tariffID: string;
  labels: string[];
  owner: object;
  tags?: string[];
  product?: string;
  mktu: object[];
  comment: string;
}

/**
 * Модель для отправки Оплаты
 */
export interface IRequestCheckLabelStage4 {
  responseID: string;
  inn?: string;
  ogrn?: string;
  name?: string;
  address?: string;
  sum?: string;
  type: string;
  yandexResponse: object;
}
