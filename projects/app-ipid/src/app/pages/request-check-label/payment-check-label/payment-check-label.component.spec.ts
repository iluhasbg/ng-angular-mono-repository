import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentCheckLabelComponent } from './payment-check-label.component';

describe('PaymentCheckLabelComponent', () => {
  let component: PaymentCheckLabelComponent;
  let fixture: ComponentFixture<PaymentCheckLabelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaymentCheckLabelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentCheckLabelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
