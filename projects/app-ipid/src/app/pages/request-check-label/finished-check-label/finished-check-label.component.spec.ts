import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FinishedCheckLabelComponent } from './finished-check-label.component';

describe('FinishedCheckLabelComponent', () => {
  let component: FinishedCheckLabelComponent;
  let fixture: ComponentFixture<FinishedCheckLabelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FinishedCheckLabelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FinishedCheckLabelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
