import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestCheckLabelComponent } from './request-check-label.component';

describe('RequestCheckLabelComponent', () => {
  let component: RequestCheckLabelComponent;
  let fixture: ComponentFixture<RequestCheckLabelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RequestCheckLabelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestCheckLabelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
