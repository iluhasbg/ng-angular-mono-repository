import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LabelOwnerComponent } from './label-owner.component';

describe('LabelOwnerComponent', () => {
  let component: LabelOwnerComponent;
  let fixture: ComponentFixture<LabelOwnerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LabelOwnerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LabelOwnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
