import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ServicesComponent } from './services.component';
import { ServiceItemComponent } from './service-item/service-item.component';

const systemRoutes: Routes = [
  {
    path: '',
    component: ServicesComponent,
  },
  {
    path: 'service-item',
    component: ServiceItemComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(systemRoutes)],
  exports: [RouterModule]
})
export class ServicesRoutingModule {
}
