import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabWithTableComponent } from './tab-with-table.component';

describe('TabWithTableComponent', () => {
  let component: TabWithTableComponent;
  let fixture: ComponentFixture<TabWithTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabWithTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabWithTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
