import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabComplexFormComponent } from './tab-complex-form.component';

describe('TabComplexFormComponent', () => {
  let component: TabComplexFormComponent;
  let fixture: ComponentFixture<TabComplexFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabComplexFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabComplexFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
