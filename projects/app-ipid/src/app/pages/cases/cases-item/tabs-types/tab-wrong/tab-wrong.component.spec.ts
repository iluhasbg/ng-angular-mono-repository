import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabWrongComponent } from './tab-wrong.component';

describe('TabWrongComponent', () => {
  let component: TabWrongComponent;
  let fixture: ComponentFixture<TabWrongComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabWrongComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabWrongComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
