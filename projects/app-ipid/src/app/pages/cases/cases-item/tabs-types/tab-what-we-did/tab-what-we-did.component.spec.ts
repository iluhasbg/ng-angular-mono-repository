import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabWhatWeDidComponent } from './tab-what-we-did.component';

describe('TabWhatWeDidComponent', () => {
  let component: TabWhatWeDidComponent;
  let fixture: ComponentFixture<TabWhatWeDidComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabWhatWeDidComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabWhatWeDidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
