import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CasesItemComponent } from './cases-item.component';

describe('CasesItemComponent', () => {
  let component: CasesItemComponent;
  let fixture: ComponentFixture<CasesItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CasesItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CasesItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
