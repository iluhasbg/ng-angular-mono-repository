import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceListWidgetComponent } from './service-list-widget.component';

describe('ServiceListWidgetComponent', () => {
  let component: ServiceListWidgetComponent;
  let fixture: ComponentFixture<ServiceListWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServiceListWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceListWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
