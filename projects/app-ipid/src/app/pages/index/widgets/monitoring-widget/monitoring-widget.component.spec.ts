import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonitoringWidgetComponent } from './monitoring-widget.component';

describe('MonitoringWidgetComponent', () => {
  let component: MonitoringWidgetComponent;
  let fixture: ComponentFixture<MonitoringWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonitoringWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonitoringWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
