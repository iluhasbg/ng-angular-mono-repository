import { Component, OnInit } from '@angular/core';
import { DemoService } from '../../../../shared/services/demo.service';
import { HttpService } from '../../../../shared/services/http.service';
import { AsyncDataLoader } from '../../../../shared/helpers/async-data-loader/async-data-loader';

@Component({
  selector: 'app-monitoring-widget',
  templateUrl: './monitoring-widget.component.html',
  styleUrls: ['./monitoring-widget.component.scss']
})
export class MonitoringWidgetComponent implements OnInit {

  constructor(
    readonly httpService: HttpService
  ) { }

  risks;
  isLoadingRisks = false;

  ngOnInit(): void {
    this.getRisks();
  }

  getRisks() {
    this.httpService.get({ path: 'objects/count' }).subscribe((data: any) => {
      this.risks = data.risks;
      this.isLoadingRisks = true;
    });
  }

  get hasRisks(): boolean {
    return this.risks && (this.risks.critical || this.risks.high || this.risks.medium || this.risks.recommendation);
  }

  // get myDeal() {
  //   return this.demoService.indexPage.myDeal;
  // }

}
