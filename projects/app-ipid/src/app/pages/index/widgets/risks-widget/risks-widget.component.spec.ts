import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {RisksWidgetComponent} from './risks-widget.component';

describe('RisksWidgetComponent', () => {
  let component: RisksWidgetComponent;
  let fixture: ComponentFixture<RisksWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RisksWidgetComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RisksWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
