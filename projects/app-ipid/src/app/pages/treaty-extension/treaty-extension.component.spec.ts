import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TreatyExtensionComponent } from './treaty-extension.component';

describe('TreatyExtensionComponent', () => {
  let component: TreatyExtensionComponent;
  let fixture: ComponentFixture<TreatyExtensionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TreatyExtensionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreatyExtensionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
