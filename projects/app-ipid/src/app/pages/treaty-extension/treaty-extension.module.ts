import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TreatyExtensionRoutingModule } from './treaty-extension-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule } from '../layout/layout.module';
import { IvyCarouselModule } from 'angular-responsive-carousel';
import { TextFieldModule } from '@angular/cdk/text-field';
import { TreatyExtensionComponent } from './treaty-extension.component';
import {UserPhoneModule} from '../../../../../shared/components/user-phone/user-phone.module';


@NgModule({
  declarations: [
    TreatyExtensionComponent
  ],
    imports: [
        CommonModule,
        TreatyExtensionRoutingModule,
        CommonModule,
        SharedModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        IvyCarouselModule,
        TextFieldModule,
        UserPhoneModule,
    ]
})
export class TreatyExtensionModule { }
