export interface Tariff {
  descriptions: TariffDescriptions;
  default: boolean;
  sumDiscount: number;
  sum: number;
  hit: boolean;
  discount: number;
  title: string;
  id: string;
}

export interface TariffDescriptions {
  hint: string;
  description: string;
}
