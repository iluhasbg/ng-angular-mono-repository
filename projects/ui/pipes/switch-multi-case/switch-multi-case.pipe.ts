import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'switchMultiCase', pure: true })
export class SwitchMultiCasePipe implements PipeTransform {
  public transform(cases: string[], switchOption: string): string | boolean {
    return cases.includes(switchOption) ? switchOption : !switchOption;
  }
}
