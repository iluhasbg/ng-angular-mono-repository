import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'toLocaleString', pure: true })
export class ToLocaleStringPipe implements PipeTransform {
  public transform(value: number): string {
    return typeof value !== undefined ? `${value.toLocaleString('ru')} ₽` : '';
  }
}
