import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToLocaleStringPipe } from './to-locale-string.pipe';

@NgModule({
  declarations: [ToLocaleStringPipe],
  imports: [CommonModule],
  exports: [ToLocaleStringPipe],
})
export class ToLacaleStringModule { }
