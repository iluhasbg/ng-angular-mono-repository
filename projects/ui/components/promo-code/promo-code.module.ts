import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PromoCodeComponent } from './promo-code.component';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [PromoCodeComponent],
  imports: [CommonModule, FormsModule],
  exports: [PromoCodeComponent],
})
export class PromoCodeModule { }
