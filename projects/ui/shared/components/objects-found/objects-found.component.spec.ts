import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ObjectsFoundComponent } from './objects-found.component';

describe('ObjectsFoundComponent', () => {
  let component: ObjectsFoundComponent;
  let fixture: ComponentFixture<ObjectsFoundComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ObjectsFoundComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ObjectsFoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
