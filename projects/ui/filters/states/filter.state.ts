import { Action, Selector, State, StateContext, StateToken } from '@ngxs/store';
import { Injectable } from '@angular/core';
import { FilterActions } from '@ui/filters/states/filter.actions';
import { FilterPaginationInterface } from '@ui/filters/models/filter-pagination.interface';

const FILTER_STATE_TOKEN = new StateToken<FilterStateModel>('filter');

@State({
  name: FILTER_STATE_TOKEN,
  defaults: {
    commonFilter: {},
    paginationFilter: {
      limit: 10,
      offset: 0
    }
  }
})
@Injectable()
export class FilterState {
  @Selector()
  static getParams(state: FilterStateModel) {
    return {...state.commonFilter, ...state.paginationFilter};
  }

  @Selector()
  static getCurrentPage(state: FilterStateModel) {
    return state.paginationFilter.offset + 1;
  }

  @Action(FilterActions.Apply)
  apply(ctx: Context, { filter }: FilterActions.Apply) {
    const { paginationFilter } = ctx.getState();
    const filledFilters: Record<string, string> = {};
    for (const key of Object.keys(filter)) {
      if (filter[key].toString()) {
        filledFilters[key] = filter[key].toString();
      }
    }
    ctx.patchState({
      paginationFilter: {...paginationFilter, offset: 0},
      commonFilter: filledFilters
    });
  }

  @Action(FilterActions.ChangePage)
  changePage(ctx: Context, { page }: FilterActions.ChangePage) {
    const { paginationFilter } = ctx.getState();
    ctx.patchState({paginationFilter: {...paginationFilter, offset: page - 1}});
  }
}

type Context = StateContext<FilterStateModel>;

export interface FilterStateModel {
  commonFilter: Record<string, string>;
  paginationFilter: FilterPaginationInterface;
}
