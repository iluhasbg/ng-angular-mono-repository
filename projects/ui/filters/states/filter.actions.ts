export namespace FilterActions {
  export class Apply {
    static readonly type = '[filter] Apply';

    constructor(public filter: Record<string, any>) {}
  }

  export class ChangePage {
    static readonly type = '[filter] ChangePage';

    constructor(public page: number) {}
  }
}
