export type Filters<T extends IFilterDataSource> = Filter<T>[];

export interface Filter<T extends IFilterDataSource> {
  type: 'input' | 'select' | 'multiselect' | 'date' | 'autocomplete';
  name: string;
  placeholder?: string;
  options?: IFilterOption[];
  filter?: (items: T) => T;
  filterItem?: (
    item: PickArrayItem<GetObjectTypes<T>>,
    filterValue: any
  ) => boolean;
}

export interface IFilterOption {
  id: any;
  label: string;
}

export type IFilterDataSource = { [key: string]: any[] } | any[];

type IService = { id: number; name: string };
type ISlider = { id: string; name: number };

const filters: Filters<IService[]> = [
  {
    type: 'input',
    name: 'query',
    filterItem: (item) => {
      return true;
    },
  },
  {
    type: 'select',
    name: 'category',
    filterItem: (item) => {
      return true;
    },
  },
];

export interface FiltersLayout {
  lg?: string[];
  md: string[];
  sm?: string[];
}

type PickArrayItem<T extends any[]> = T extends Array<infer U>
  ? U
  : T extends { [key: string]: Array<infer U2> }
  ? U2
  : never;

type GetObjectTypes<T extends IFilterDataSource> = T extends {
  [key: string]: infer U;
}
  ? U
  : T;
