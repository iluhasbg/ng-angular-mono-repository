export interface FilterPaginationInterface {
  limit: number;
  offset: number;
}
