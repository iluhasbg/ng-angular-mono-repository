@import 'variables';

.filter-container {
  position: relative;
  width: 100%;

  &__mobile {
    @media(max-width: 793px) {
      position: fixed;
      top: 0;
      right: 0;
      left: 0;
      z-index: 1000;
      display: flex;
      flex-direction: column;
      padding: 0;
      gap: 10px;
      height: 100vh;
      background: $g__body_background;
    }
  }

  &__form {
    display: grid;
    grid-gap: 5px;
    grid-auto-rows: auto;
    width: 100%;

    @media(max-width: 793px) {
      display: none;

      &_mobile {
        display: block;
        padding: 20px;
        order: 2;
      }
    }
  }

  &__buttons {
    display: none;

    @media(max-width: 793px) {
      position: fixed;
      left: 22px;
      right: 22px;
      bottom: 20px;
      padding: 0;
      display: flex;
      justify-content: space-between;
    }
  }

  &__controls-open {
    display: none;

    @media (max-width: 793px) {
      display: block;
    }

    p {
      cursor: pointer;
      top: 0;
      width: 100%;
      margin: 0 auto;
      font-size: 13px;
      line-height: 18px;
      padding: 10px 15px 10px 15px;
      position: relative;
      background: #FFFFFF;
      border: 1px solid rgba(21, 36, 56, 0.15);
      box-sizing: border-box;
      border-radius: 30px;
      z-index: 2;
      text-align: center;
    }

    &_active {
      //text-transform: uppercase;
      font-weight: bold;
      padding: 5px 5px 5px 15px;
      order: 1;
      background: #FFFFFF;

      @media (max-width: 793px) {
        display: block;

        p {
          margin: 0;
          cursor: default;
          top: 0;
          left: 6px;
          border: none;
          font-size: 16px;
          line-height: 20px;
          padding-left: 0;
          text-align: left;
          &::before {
            display: none;
          }
        }
      }
    }
  }

  &__controls-close {
    position: fixed;
    top: 14px;
    right: 15px;
    height: 35px;
    width: 35px;
    background-color: transparent;
    border: none;
    padding: 0;
    display: block;
    z-index: 1001;
    cursor: pointer;

    &::before, &::after {
      position: absolute;
      content: '';
      height: 24px;
      width: 3px;
      top: 0;
      background-color: $g__text_color_primary;
    }
    &::before {
      transform: rotate(45deg);
    }
    &::after {
      transform: rotate(-45deg);
    }
    @media (min-width: 794px) {
      display: none;
    }
  }
}
.search-image {
  content: "";
  width: 25px;
  height: 15px;
  background-repeat: no-repeat;
  background-size: 100%;
  background-position: center;
  z-index: 30;
  padding-right: 10px;
  margin-bottom: -3px;
}

.autocomplete {
  position: relative;

  &-dropdown {
    z-index: 1;
    width: 100%;
    padding: 15px;
    cursor: pointer;
    margin-top: 2px;
    margin-right: 5px;
    border-radius: 5px;
    position: absolute;
    box-sizing: border-box;
    background: $g__white_background;
    border: 1px solid $g__outline_color;
    box-shadow: 0 2px 4px rgba(21, 36, 56, 0.1);

    @media (max-width: 793px) {
      min-width: 250px;
      margin-bottom: 10px;
    }
  
  
    &:last-of-type {
      margin-right: 0;
    }
  
    &--calendar {
      width: 159px;
    }

    .option {
      margin-bottom: 5px;
    }
  }
}
