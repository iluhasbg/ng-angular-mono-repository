import {NgModule} from '@angular/core';
import {UserPhoneComponent} from './component/user-phone.component';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    UserPhoneComponent
  ],
  exports: [
    UserPhoneComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})

export class UserPhoneModule {
}
