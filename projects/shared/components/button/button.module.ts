import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonComponent } from './button/button.component';
import { ButtonSettingsComponent } from './button-settings/button-settings.component';


@NgModule({
  declarations: [
    ButtonSettingsComponent,
    ButtonComponent,
  ],
  exports: [
    ButtonSettingsComponent,
    ButtonComponent,
  ],
  imports: [
    CommonModule
  ]
})
export class ButtonModule {
}
