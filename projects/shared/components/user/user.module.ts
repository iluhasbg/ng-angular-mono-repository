import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChangeProfileComponent } from './change-profile/change-profile.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    ChangeProfileComponent
  ],
  exports: [
    ChangeProfileComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class UserSharedModule { }
