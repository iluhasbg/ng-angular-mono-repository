import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmbLoaderComponent } from './emb-loader.component';

describe('EmbLoaderComponent', () => {
  let component: EmbLoaderComponent;
  let fixture: ComponentFixture<EmbLoaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EmbLoaderComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmbLoaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
