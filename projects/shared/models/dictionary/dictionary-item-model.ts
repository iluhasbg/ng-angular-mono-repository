export class DictionaryItem<TKey, TValue> {
  public key: TKey;
  public value: TValue;

  constructor(init?: Partial<DictionaryItem<TKey, TValue>>) {
    Object.assign(this, init);
  }
}
