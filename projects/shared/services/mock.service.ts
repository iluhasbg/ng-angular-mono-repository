import { Injectable } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { filter, tap } from 'rxjs/operators';
import { of } from 'rxjs';
import { HttpClient } from '@angular/common/http';

const MOCK_CACHE_KEY = 'mock-cache';

@Injectable({providedIn: 'root'})
export class MockService {
  cache: Record<string, IMockCache> = {};

  constructor(
    private readonly router: Router,
    private readonly httpClient: HttpClient
  ) {
    this.loadCache();

    // если у нас указан get параметр ?reset-mock то сбрасываем весь кеш
    this.router.events.pipe(
      filter((event) => event instanceof NavigationEnd)
    ).subscribe((event: NavigationEnd) => {
      if (event.url.includes('reset-mock')) {
        this.cache = {};
        this.saveCache();
      }
    });
  }

  get(url: string) {
    const cache = this.cache[url];
    if (cache && cache.expireIn > Date.now()) {
      return of(cache.data);
    } else {
      return this.httpClient.get(url)
        .pipe(
          tap(data => {
            this.cache[url] = {data, expireIn: this.expireFromNow()};
            this.saveCache();
          })
        );
    }
  }

  private loadCache() {
    const cache = localStorage.getItem(MOCK_CACHE_KEY);
    if (cache) {
      this.cache = JSON.parse(cache);
    }
  }

  private saveCache() {
    localStorage.setItem(MOCK_CACHE_KEY, JSON.stringify(this.cache));
  }

  private expireFromNow() {
    return Date.now() + (3600 * 1000); // 1 час
  }

}

interface IMockCache {
  expireIn: number;
  data: any;
}
