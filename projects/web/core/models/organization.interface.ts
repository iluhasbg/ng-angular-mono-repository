export interface OrganizationInterface {
  PSRN: string;
  TIN: string;
  cases: CaseInterface[];
  director: string;
  email: string;
  id: string;
  legalAdress: string;
  name: string;
  pending: boolean;
  phone: string;
  representatives: RepresentativeInterface[];
  shortName: string;
  nonResident: boolean;
  individual: boolean;
}

export interface RepresentativeInterface {
  email: string;
  id: string;
  image: string;
  middleName: string;
  name: string;
  performance: string;
  position: string;
  role: string;
  status: string;
  surname: string;
}

interface CaseInterface {
  description: string;
  number: string;
  opened: boolean;
  status: string;
  title: string;
  type: string;
}

export interface AttorneyInterface {
  date: string;
  shortName: string;
  user: string;
}
