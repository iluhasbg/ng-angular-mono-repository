export interface RightsItemInterface {
  id?: string;
  name?: string;
  value?: boolean;
  checked?: boolean;
  nameEng?: string;
}

export interface RightsListInterface {
  title: string;
  data: RightsItemInterface[];
}

export interface RightsResponse {
  rights: RightsListInterface[];
}
