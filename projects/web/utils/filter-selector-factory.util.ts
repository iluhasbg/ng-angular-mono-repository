import { createSelector } from '@ngxs/store';

export function filterSelectorFactory<T>(
  value: any,
  fn: (state: T, value: any) => T,
  selector: any
) {
  return createSelector([selector], (state: T) => {
    return fn(state, value);
  });
}
