export function trimString(str, count) {
  return str?.length > count ? str.slice(0, count) + '...' : str;
}
