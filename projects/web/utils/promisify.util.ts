import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';

export function promisify<T>(stream: Observable<T>): Promise<T> {
  return new Promise((resolve, reject) => {
    stream.pipe(take(1)).subscribe(value => resolve(value), error => reject(error));
  });
}
