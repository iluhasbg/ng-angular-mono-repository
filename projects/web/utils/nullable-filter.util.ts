import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';

export function nullableFilter(): <T>(stream$: Observable<T | null>) => Observable<T> {
  return stream$ => stream$.pipe(filter(value => !!value));
}
