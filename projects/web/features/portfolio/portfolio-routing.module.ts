import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SearchObjectsGuard } from './services/search-objects.guard';
import { APP_ROUTES_NAMES } from '@web/core/models/app-routes-names';
import { PortfolioContainerComponent } from './containers/portfolio-container/portfolio-container.component';
import { TechnicalTaskContainerComponent } from './containers/technical-task-container/technical-task-container.component';

const PORTFOLIO_ROUTES: Routes = [
  {
    path: '',
    component: PortfolioContainerComponent,
  },
  {
    path: `${APP_ROUTES_NAMES.TECHNICAL_TASK}`,
    runGuardsAndResolvers: 'paramsOrQueryParamsChange',
    canActivate: [SearchObjectsGuard],
    component: TechnicalTaskContainerComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(PORTFOLIO_ROUTES)],
  providers: [SearchObjectsGuard],
  exports: [RouterModule],
})
export class PortfolioRoutingModule { }
