import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { Observable, ReplaySubject } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { APP_ROUTES_NAMES } from '@web/core/models/app-routes-names';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { OrganizationState } from '@web/core/states/organization.state';
import { OrganizationInterface } from '@web/core/models/organization.interface';
import { ActionsExecuting, actionsExecuting } from '@ngxs-labs/actions-executing';
import {
  Owner,
  ADD_BUTTON_LABEL,
  PortfolioTabType,
  PortfolioOtherTab,
  PortfolioGeneralTab,
  PortfolioFilterState,
  PortfolioFiltersData,
  PortfolioOtherTabType,
  CompositionWidgetData,
} from '../../models/portfolio.interfaces';
import { PortfolioState } from '../../states/portfolio.state';
import { PortfolioActions } from '../../states/portfolio.actions';

@UntilDestroy()
@Component({
  selector: 'app-portfolio-container',
  templateUrl: './portfolio-container.component.html',
  styleUrls: ['./portfolio-container.component.scss'],
})
export class PortfolioContainerComponent implements OnInit {
  @Select(PortfolioState.totalCout) totalCout$:  Observable<number>;
  @Select(PortfolioState.tableValueData) values$: Observable<any[]>;
  @Select(PortfolioState.tabs) tabs$: Observable<PortfolioGeneralTab[]>;
  @Select(PortfolioState.filters) filters$: Observable<PortfolioFiltersData>;
  @Select(PortfolioState.currentTab) currentTab$: Observable<PortfolioTabType>;
  @Select(PortfolioState.tableValueCount) tableValueCount$: Observable<number>;
  @Select(PortfolioState.otherTabs) otherTabs$: Observable<PortfolioOtherTab[]>;
  @Select(PortfolioState.isEmptyPortfolio) isEmptyPortfolio$: Observable<boolean>;
  @Select(PortfolioState.composition) composition$: Observable<CompositionWidgetData>;
  @Select(PortfolioState.filtersValue) filtersValue$: Observable<PortfolioFiltersData>;
  @Select(PortfolioState.domainAdministrator) domainAdministrator$: Observable<Owner[]>;
  @Select(PortfolioState.currentOtherTab) currentOtherTab$: Observable<PortfolioOtherTabType>;
  @Select(PortfolioState.currentTable) currentTable$: Observable<PortfolioTabType & PortfolioOtherTabType>;
  @Select(PortfolioState.isShowModal) isShowModal$: Observable<boolean>;
  @Select(actionsExecuting([
    PortfolioActions.LoadObjectsByType,
    PortfolioActions.LoadPortfolioContracts,
  ])) isLoading$: Observable<ActionsExecuting>
  @Select(actionsExecuting([
    PortfolioActions.LoadCompositionData,
  ])) isLoadingComposition$: Observable<ActionsExecuting>;

  public kontragents: OrganizationInterface[] = [];
  public selectedCompany: OrganizationInterface | null = null;
  public addButtonLabel = ADD_BUTTON_LABEL.ADD_OBJECT;

  private _clearFilter = new ReplaySubject<void>();
  public clearFilter$ = this._clearFilter.asObservable();

  constructor(
    private readonly store: Store,
    private readonly router: Router,
  ) { }

  public get disabledAddObjects(): boolean {
    return !this.selectedCompany?.id && this.addButtonLabel === ADD_BUTTON_LABEL.ADD_OBJECT;
  }

  public ngOnInit(): void {
    this.store.select(OrganizationState.organizationsForSelect)
      .pipe(
        tap(organizations => this.kontragents = organizations),
        tap(() => this.initAddButtonLabel(this.kontragents)),
        untilDestroyed(this),
      )
      .subscribe(() => this.onSelectedCompany(this.store.selectSnapshot(PortfolioState.selectedCompany())?.id));

    this.store.select(PortfolioState.selectedCompany())
      .pipe(untilDestroyed(this))
      .subscribe(selectedCompany => this.selectedCompany = selectedCompany);
  }

  public onSelectedCompany(event: string | null): void {
    this.store.dispatch(new PortfolioActions.GetInformationByCompanyId(event));
  }

  public changeCurrentTab(tab: PortfolioTabType): void {
    this.store.dispatch(new PortfolioActions.SetPortfolioCurrentTab(tab));
  }

  public toggleAddButton(): void {
    this.addButtonLabel === ADD_BUTTON_LABEL.ADD_OBJECT
      ? this.openModalAddObject()
      : void this.router.navigate(['/', APP_ROUTES_NAMES.USER, APP_ROUTES_NAMES.ADD_ORGANIZATION]);
  }

  public applyFilters(event: PortfolioFilterState): void {
    this.store.dispatch(new PortfolioActions.SetPortfolioFilters(event));
  }

  public openModalAddObject(): void {
    this.store.dispatch(new PortfolioActions.ToggleShowModal());
  }

  public clearFilters(): void {
    this._clearFilter.next();
    this.store.dispatch(new PortfolioActions.ClearPortfolioFilters());
  }

  private initAddButtonLabel(kontragents: OrganizationInterface[]): void {
    this.addButtonLabel = kontragents.length > 1
      ? ADD_BUTTON_LABEL.ADD_OBJECT
      : ADD_BUTTON_LABEL.ADD_ORGANIZATION;
  }
}
