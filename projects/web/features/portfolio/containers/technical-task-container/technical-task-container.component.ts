import { Select, Store } from '@ngxs/store';
import { AuthState } from '@web/core/states/auth.state';
import { filter, Observable, switchMap, tap } from 'rxjs';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { APP_ROUTES_NAMES } from '@web/core/models/app-routes-names';
import { OrganizationState } from '@web/core/states/organization.state';
import { PortfolioState } from '@web/features/portfolio/states/portfolio.state';
import { OrganizationInterface } from '@web/core/models/organization.interface';
import { PortfolioActions } from '@web/features/portfolio/states/portfolio.actions';
import { TechnicalTaskState } from '@web/features/portfolio/states/technical-task.state';
import {
  AddObjectsModal,
  TypeOfObjectSelector,
  THECHNICAL_TASK_INDEX,
  ChangePageTechnicalTask,
  DEFAULT_DROPDOWN_OPTIONS,
  technicalTaskRightMapper,
  SearchPortfolioObjectData,
} from '@web/features/portfolio/models/technical-task.interfaces';
import { ActionsExecuting, actionsExecuting } from '@ngxs-labs/actions-executing';
import { TechnicalTaskActions } from '@web/features/portfolio/states/technical-task.actions';

@UntilDestroy()
@Component({
  selector: 'app-technical-task-container',
  templateUrl: './technical-task-container.component.html',
  styleUrls: ['./technical-task-container.component.scss'],
})
export class TechnicalTaskContainerComponent implements OnInit, OnDestroy {
  @Select(TechnicalTaskState.trademarksData) trademarksData$: Observable<SearchPortfolioObjectData>;
  @Select(TechnicalTaskState.patentsData) patentsData$: Observable<SearchPortfolioObjectData>;
  @Select(TechnicalTaskState.domainsData) domainsData$: Observable<SearchPortfolioObjectData>;
  @Select(TechnicalTaskState.swdbData) swdbData$: Observable<SearchPortfolioObjectData>;
  @Select(TechnicalTaskState.currentPageTrademarks) currentPageTrademarks$: Observable<number>;
  @Select(TechnicalTaskState.currentPagePatents) currentPagePatents$: Observable<number>;
  @Select(TechnicalTaskState.currentPageDomains) currentPageDomains$: Observable<number>;
  @Select(TechnicalTaskState.currentPageSWDB) currentPageSWDB$: Observable<number>;
  @Select(TechnicalTaskState.objectsLength) objectsLength$: Observable<number>;
  @Select(TechnicalTaskState.errorModalData) errorModalData: Observable<AddObjectsModal>;
  @Select(TechnicalTaskState.allTrademarksChecked) allTrademarksChecked$: Observable<boolean>;
  @Select(TechnicalTaskState.allPatentsChecked) allPatentsChecked$: Observable<boolean>;
  @Select(TechnicalTaskState.allDomainsChecked) allDomainsChecked$: Observable<boolean>;
  @Select(TechnicalTaskState.allSWDBChecked) allSWDBChecked$: Observable<boolean>;
  @Select(TechnicalTaskState.legalName) legalName: Observable<string>;
  @Select(actionsExecuting([
    TechnicalTaskActions.SearchObjects,
    TechnicalTaskActions.GetTrademarksByINN,
    TechnicalTaskActions.GetDomainsByINN,
    TechnicalTaskActions.GetPatentsByINN,
    TechnicalTaskActions.GetSWDBByINN,
  ])) isLoading$: Observable<ActionsExecuting>;
  @Select(actionsExecuting([
    TechnicalTaskActions.AddObjectsToPortfolio,
  ])) isLoadingAdd$: Observable<ActionsExecuting>;

  public fieldName: string = '';
  public searchText: string = '';
  public typeRequest: string = '';
  public kontragents: OrganizationInterface[] = [];
  public dropdownOptions = DEFAULT_DROPDOWN_OPTIONS;
  public selectedType: TypeOfObjectSelector = this.dropdownOptions[0];
  public selectedCompany: OrganizationInterface | null = null;

  constructor(
    public  activatedRoute: ActivatedRoute,
    private readonly store: Store,
    private readonly router: Router,
  ) { }

  public ngOnInit(): void {
    this.store.select(OrganizationState.organizations)
      .pipe(untilDestroyed(this))
      .subscribe(organizations => this.kontragents = organizations);

    this.store.select(PortfolioState.selectedCompanyId)
      .pipe(untilDestroyed(this))
      .subscribe(selectedCompanyId => this.initSelectedCompany(selectedCompanyId));
    
    this.store.select(AuthState.getRights)
      .pipe(
        filter(rights => Boolean(rights)),
        tap(() => {
          !this.store.selectSnapshot(AuthState.getRightByName(technicalTaskRightMapper.get(THECHNICAL_TASK_INDEX.TRADEMARKS)))
            && this.filterDropdownOptions(THECHNICAL_TASK_INDEX.TRADEMARKS);
          !this.store.selectSnapshot(AuthState.getRightByName(technicalTaskRightMapper.get(THECHNICAL_TASK_INDEX.PATENTS)))
            && this.filterDropdownOptions(THECHNICAL_TASK_INDEX.PATENTS);
          !this.store.selectSnapshot(AuthState.getRightByName(technicalTaskRightMapper.get(THECHNICAL_TASK_INDEX.SWDB)))
            && this.filterDropdownOptions(THECHNICAL_TASK_INDEX.SWDB);
        }),
        switchMap(() => this.activatedRoute.queryParams),
        untilDestroyed(this),
      )
      .subscribe((queryParams) => this.parseSearchParams(queryParams));
  }

  public ngOnDestroy(): void {
    this.store.dispatch(new TechnicalTaskActions.ResetSearch());
  }

  public cancelCheckedObjects(): void {
    this.store.dispatch(new TechnicalTaskActions.ClearAllCheckingObjects());
  }

  public selectCompany(event: string): void {
    this.store.dispatch(new PortfolioActions.SetSelectedCompanyId(event));
  }

  public changeOption(option: TypeOfObjectSelector): void {
    this.getDataForLoader(option.text);
    this.store.dispatch(new TechnicalTaskActions.ResetSearch());
    void this.router.navigate(
      ['/', APP_ROUTES_NAMES.PORTFOLIO, APP_ROUTES_NAMES.TECHNICAL_TASK],
      { queryParams: { index: option.index }, replaceUrl: true, },
    );
  }

  public changeSearchText(value: string): void {
    this.searchText = value;
    this.updateLegalName();
  }

  public searchObjects(): void {
    void this.router.navigate(
      ['/', APP_ROUTES_NAMES.PORTFOLIO, APP_ROUTES_NAMES.TECHNICAL_TASK],
      { queryParams: { name: this.searchText.trim() }, queryParamsHandling: 'merge' },
    );
  }

  public changePage($event: ChangePageTechnicalTask): void {
    this.store.dispatch(new TechnicalTaskActions.ChangePage($event));
  }

  public addObjectsToPortfolio(): void {
    this.store.dispatch(new TechnicalTaskActions.AddObjectsToPortfolio(this.selectedCompany.id));
  }

  public closeModalError(): void {
    this.store.dispatch(new TechnicalTaskActions.CloseModalError());
  }

  private initSelectedCompany(selectedCompanyId: string | null): void {
    if (selectedCompanyId) {
      this.selectedCompany = this.kontragents.find(k => k.id === selectedCompanyId);
    } else {
      this.selectCompany(this.kontragents[0].id)
    }
  }

  private updateLegalName(): void {
    this.store.dispatch(new TechnicalTaskActions.ResetLegalName());
    if (
      this.selectedType.index === THECHNICAL_TASK_INDEX.INN &&
      [10, 12, 13, 15].indexOf(this.searchText.trim().length) > -1
    ) {
      this.store.dispatch(new TechnicalTaskActions.GetLegal({ id: this.searchText.trim() }));
    }
  }

  private getDataForLoader(text: string): void {
    switch (text) {
      case 'Все объекты по ИНН/ОГРН':
        this.typeRequest = 'all';
        this.fieldName = 'Домены';
        break;
      case 'Товарные знаки':
        this.typeRequest = 'firstAndSecond';
        break;
      case 'Домены':
        this.typeRequest = 'third';
        this.fieldName = 'Домены';
        break;
      case 'Патенты':
        this.typeRequest = 'third';
        this.fieldName = 'Патенты';
        break;
      case 'Программы и базы данных':
        this.typeRequest = 'third';
        this.fieldName = 'Программы и базы данных';
        break;
    }
  }

  private parseSearchParams(queryParams: Params): void {
    const currentOption = this.dropdownOptions.find(item => item.index === queryParams?.index);
    if (currentOption) {
      this.selectedType = currentOption;
      this.getDataForLoader(currentOption.text);
    } else {
      this.changeOption(this.dropdownOptions[0]);
    }
    if (queryParams?.name) {
      this.changeSearchText(queryParams?.name);
    }
  }

  private filterDropdownOptions(index: THECHNICAL_TASK_INDEX): void {
    this.dropdownOptions = this.dropdownOptions.filter(option => option.index !== index);
  }
}
