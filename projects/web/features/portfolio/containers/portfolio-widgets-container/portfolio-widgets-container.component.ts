import { Observable } from "rxjs";
import { tap } from 'rxjs/operators';
import { Store, Select } from "@ngxs/store";
import { UntilDestroy, untilDestroyed } from "@ngneat/until-destroy";
import { OrganizationState } from "@web/core/states/organization.state";
import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { OrganizationInterface } from "@web/core/models/organization.interface";
import { ActionsExecuting, actionsExecuting } from "@ngxs-labs/actions-executing";
import {
  ADD_BUTTON_LABEL,
  CompositionWidgetData,
  PortfolioCostWidgetData,
  SecurityRisksWidgetData,
} from "../../models/portfolio.interfaces";
import { PortfolioState } from "../../states/portfolio.state";
import { PortfolioActions } from "../../states/portfolio.actions";

@UntilDestroy()
@Component({
  selector: 'app-portfolio-widgets-container',
  templateUrl: './portfolio-widgets-container.component.html',
  styleUrls: ['./portfolio-widgets-container.component.scss'],
})
export class PortfolioWidgetsContainerComponent implements OnInit {
  // Переменные для управления прелоадером загрузки данных;
  @Select(actionsExecuting([PortfolioActions.LoadCompositionData])) isLoadingComposition$: Observable<ActionsExecuting>;
  @Select(actionsExecuting([PortfolioActions.LoadPortfolioSecurity])) isLoadindSecurityRisks$: Observable<ActionsExecuting>;
  @Select(actionsExecuting([PortfolioActions.LoadPortfolioCost])) isLoadingPortfolioCost$: Observable<ActionsExecuting>;

  // Данные для конкретных виджетов;
  @Select(PortfolioState.composition) composition$: Observable<CompositionWidgetData>;
  @Select(PortfolioState.securityRisks) securityRisks$: Observable<SecurityRisksWidgetData>;
  @Select(PortfolioState.portfolioCost) portfolioCost$: Observable<PortfolioCostWidgetData>;

  // Переменные с общей информацией для всех виджетов;
  @Select(PortfolioState.selectedCompany()) selectedCompany$: Observable<OrganizationInterface>;
  public kontragents: OrganizationInterface[] = [];
  public addButtonLabel = ADD_BUTTON_LABEL.ADD_OBJECT;

  @Output() openModal: EventEmitter<void> = new EventEmitter<void>();

  constructor(private readonly store: Store) { }

  public ngOnInit(): void {
    document.body.style.setProperty('overflow', 'visible');

    this.store.select(OrganizationState.organizationsForSelect)
      .pipe(
        tap(organizations => this.initAddButtonLabel(organizations)),
        untilDestroyed(this),
      )
      .subscribe(organizations => this.kontragents = organizations);
  }

  private initAddButtonLabel(kontragents: OrganizationInterface[]): void {
    this.addButtonLabel = kontragents.length > 1
      ? ADD_BUTTON_LABEL.ADD_OBJECT
      : ADD_BUTTON_LABEL.ADD_ORGANIZATION;
  }
}
