import { Observable } from "rxjs";
import { Component } from "@angular/core";
import { Select, Store } from "@ngxs/store";
import { PortfolioState } from "../../states/portfolio.state";
import { PortfolioActions } from "../../states/portfolio.actions";
import { TableColumn, PortfolioTabType } from "../../models/portfolio.interfaces";

@Component({
  selector: 'app-portfolio-table-container',
  templateUrl: './portfolio-table-container.component.html',
  styleUrls: ['./portfolio-table-container.component.scss'],
})
export class PortfolioTableContainerComponent {
  @Select(PortfolioState.currentTab) currentTab$: Observable<PortfolioTabType>;
  @Select(PortfolioState.columns) columns$: Observable<TableColumn[]>;
  @Select(PortfolioState.tableValueData) values$: Observable<any[]>;
  @Select(PortfolioState.currentPage) currentPage$: Observable<number>;
  @Select(PortfolioState.tableValueCount) tableValueCount$: Observable<number>;

  constructor(private readonly store: Store) { }

  public changePage(page: number): void {
    this.store.dispatch(new PortfolioActions.ChangePaginatorPage(page));
  }
}
