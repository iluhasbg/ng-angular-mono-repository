import {
  TableColumn,
  TableValue,
  TableValueData,
  PortfolioTabType,
  PortfolioTabName,
  PortfolioOtherTab,
  PortfolioGeneralTab,
  PortfolioFilterState,
  PortfolioFiltersData,
  CompositionWidgetData,
  PortfolioOtherTabName,
  PortfolioOtherTabType,
  PortfolioCostWidgetData,
  SecurityRisksWidgetData
} from './portfolio.interfaces';

export const DEFAULT_GENERAL_TABS: PortfolioGeneralTab[] = [
  { header: PortfolioTabName.TRADEMARKS, type: PortfolioTabType.TRADEMARKS },
  { header: PortfolioTabName.PATENTS, type: PortfolioTabType.PATENTS },
  { header: PortfolioTabName.PROGRAMS, type: PortfolioTabType.PROGRAMS },
  { header: PortfolioTabName.CONTRACTS, type: PortfolioTabType.CONTRACTS },
  { header: PortfolioTabName.OTHER, type: PortfolioTabType.OTHER }
];

export const DEFAULT_OTHER_TABS: PortfolioOtherTab[] = [
  { header: PortfolioOtherTabName.COPYRIGHT, type: PortfolioOtherTabType.COPYRIGHT },
  { header: PortfolioOtherTabName.BRAND_NAMES, type: PortfolioOtherTabType.BRAND_NAMES },
  { header: PortfolioOtherTabName.DOMAINS, type: PortfolioOtherTabType.DOMAINS }
];

export const DEFAULT_COMPOSITION_DATA: CompositionWidgetData = {
  count: 0, bids: 0, tradeMarks: 0, patents: 0, programs: 0, domains: 0, copyright: 0, brandNames: 0, contracts: 0
};

export const DEFAULT_SECURITY_RISK: SecurityRisksWidgetData = {
  totalEvents: 0, critical: 0, fatal: 0, possible: 0, recommendation: 0, allCount: 0
};

export const DEFAULT_PORTFOLIO_COST: PortfolioCostWidgetData = {
  before: 0, after: 0
};

export const DEFAULT_PORTFOLIO_FILTERS_DATA: PortfolioFiltersData = {
  [PortfolioTabType.TRADEMARKS]: { owners: [] },
  [PortfolioTabType.PATENTS]: { owners: [] },
  [PortfolioTabType.PROGRAMS]: { owners: [] },
  [PortfolioTabType.CONTRACTS]: { owners: [] },
  [PortfolioOtherTabType.DOMAINS]: { owners: [], domainAdministrators: [] }
};

export const DEFAULT_TRADEMARKS_FILTERS: PortfolioFilterState = {
  searchQuery: '', owner: [], types: [], index: [], status: []
};

export const DEFAULT_PATENTS_FILTERS: PortfolioFilterState = {
  searchQuery: '', owner: [], types: [], status: []
};

export const DEFAULT_PROGRAMS_FILTERS: PortfolioFilterState = {
  searchQuery: '', owner: [], types: []
};

export const DEFAULT_CONTRACTS_FILTERS: PortfolioFilterState = {
  searchQuery: '', owner: [], types: []
};

export const DEFAULT_DOMAINS_FILTERS: PortfolioFilterState = {
  searchQuery: '', owner: [], types: [], domainAdministrator: [], status: []
};

export const DEFAULT_TRADEMARKS_COLUMNS: TableColumn[] = [
  { header: 'Товарный знак', field: 'imageURL' },
  { header: 'Тип', field: 'type' },
  { header: 'Номер', field: 'number' },
  { header: 'Правообладатель/Заявитель', field: 'rightHolder' },
  { header: 'Дата', field: 'date' },
  { header: 'Статус', field: 'status' }
];

export const DEFAULT_PATENTS_COLUMNS: TableColumn[] = [
  { header: 'Тип', field: 'type' },
  { header: 'Номер', field: 'number' },
  { header: 'Наименование', field: 'name' },
  { header: 'Дата регистраци', field: 'registrationDate' },
  { header: 'Статус', field: 'status' }
];

export const DEFAULT_PROGRAMS_COLUMNS: TableColumn[] = [
  { header: 'Тип', field: 'type' },
  { header: 'Номер', field: 'number' },
  { header: 'Наименование', field: 'name' },
  { header: 'Дата регистрации', field: 'registrationDate' }
];

export const DEFAULT_CONTRACTS_COLUMNS: TableColumn[] = [
  { header: 'Номер и обозначение', field: 'number' },
  { header: 'Стороны договора', field: 'parties' },
  { header: 'Кол-во объектов', field: 'count' },
  { header: 'Статус', field: 'status' }
];

export const DEFAULT_TRADEMARKS_TABLE_VALUE: TableValueData = {
  data: [], page: 1, count: 0,
  columns: DEFAULT_TRADEMARKS_COLUMNS,
  filters: { ...DEFAULT_TRADEMARKS_FILTERS }
};

export const DEFAULT_PATENTS_TABLE_VALUE: TableValueData = {
  data: [], page: 1, count: 0,
  columns: DEFAULT_PATENTS_COLUMNS,
  filters: { ...DEFAULT_PATENTS_FILTERS }
};

export const DEFAULT_PROGRAMS_TABLE_VALUE: TableValueData = {
  data: [], page: 1, count: 0,
  columns: DEFAULT_PROGRAMS_COLUMNS,
  filters: { ...DEFAULT_PROGRAMS_FILTERS }
};

export const DEFAULT_CONTRACTS_TABLE_VALUE: TableValueData = {
  data: [], page: 1, count: 0,
  columns: DEFAULT_CONTRACTS_COLUMNS,
  filters: { ...DEFAULT_CONTRACTS_FILTERS }
};

export const DEFAULT_DOMAINS_TABLE_VALUE: TableValueData = {
  data: [], page: 1, count: 0, columns: [],
  filters: { ...DEFAULT_DOMAINS_FILTERS }
};

export const DEFAULT_TABLE_VALUE: TableValue = {
  [PortfolioTabType.TRADEMARKS]: { ...DEFAULT_TRADEMARKS_TABLE_VALUE },
  [PortfolioTabType.PATENTS]: { ...DEFAULT_PATENTS_TABLE_VALUE },
  [PortfolioTabType.PROGRAMS]: { ...DEFAULT_PROGRAMS_TABLE_VALUE },
  [PortfolioTabType.CONTRACTS]: { ...DEFAULT_CONTRACTS_TABLE_VALUE },
  [PortfolioOtherTabType.DOMAINS]: { ...DEFAULT_DOMAINS_TABLE_VALUE }
};
