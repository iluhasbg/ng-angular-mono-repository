export interface SearchPortfolioObjectsBody {
  name: string;
  index: TypeObjects | string;
  size?: number;
  from?: number;
}

export interface SearchPortfolioObjectsByINNBody {
  id: string;
  searchDomains?: boolean;
  searchPatents?: boolean;
  searchTradeMarks?: boolean;
  searchSwdb?: boolean;
  size?: number;
  from?: number;
}

export interface AddObjectsToPortfolioBody {
  objects?: PortfolioObject[];
  organization?: string;
}

export interface GetLegalBody {
  id: string;
}

export interface AddObjectsToPortfolioResponse {
  error: PortfolioObject[];
  success: PortfolioObject[];
}

export interface SearchPortfolioObjectsResponse {
  took: number;
  timed_out: boolean;
  _shards: SearchPortfolioObjectsResponseShards;
  hits: SearchPortfolioObjectsResponseHits;
}

export interface SearchPortfolioObjectsResponseShards {
  total: number;
  successful: number;
  skipped: number;
  failed: number;
}
export interface SearchPortfolioObjectsResponseHits {
  total: SearchPortfolioObjectsResponseHitsTotal;
  max_score: number;
  hits: SearchPortfolioObjectsResponseResult[];
}

export interface SearchPortfolioObjectsResponseHitsTotal {
  domains?: number;
  from?: number;
  patents?: number;
  size?: number;
  tradeMarks?: number;
  swdb?: number;
  value: number;
  relation: string;
}

export interface SearchPortfolioObjectsResponseResult {
  checked: boolean;
  _index: string;
  _type: string;
  _id: string;
  _score: number;
  title?: string;
  risk?: string;
  _source: SearchPortfolioObjectsResponseResultSourceTrademarks
    | SearchPortfolioObjectsResponseResultSourceDomains
    | SearchPortfolioObjectsResponseResultSourcePatents
    | SearchPortfolioObjectsResponseResultSourceSWDB
    | any;
}

export interface SearchPortfolioObjectsResponseResultSourceDomains {
  fullDomain: string;
  zone: string;
  domain: string;
  registrationDate: string;
  domainRegistrator: string;
  free: boolean;
  freeDate: string;
  payDate: string;
}

export interface SearchPortfolioObjectsResponseResultSourceTrademarks {
  PSRN: number;
  applicantName: string;
  applicantStatus: number;
  applicationString: string;
  markImageFileName: string;
  imgUrl: string;
  expiryDate: string;
  goodsServices: GoodServicesInterface[];
  imageText: string;
  registrationCountryCode: string[];
  registrationDate: string;
  registrationNumber: number;
  registrationString: string;
}

export interface GoodServicesInterface {
  classNumber: number;
}

export interface SearchPortfolioObjectsResponseResultSourcePatents {
  actual: string | boolean;
  applicationNumber: number;
  patentGrantPublishDate: string;
  authors: string;
  patentStartingDate: string;
  registrationNumber: number;
  patentHolders: string;
  registrationDate: string;
  inventionName: string;
  patentGrantPublishNumber: string;
}

export interface SearchPortfolioObjectsResponseResultSourceSWDB {
  actual: string | boolean;
  applicationNumber: number;
  registrationNumber: number;
  programName: string;
  registrationDate: string;
  rightHolders: string;
  authors?: string;
}

export interface GetLegalResponse {
  took: number;
  timed_out: boolean;
  _shards: GetLegalResponseShards;
  hits: GetLegalResponseHits;
}

export interface GetLegalResponseShards {
  total: number;
  successful: number;
  skipped: number;
  failed: number;
}
export interface GetLegalResponseHits {
  total: GetLegalResponseHitsTotal;
  max_score: number;
  hits: GetLegalResponseResult;
}

export interface GetLegalResponseHitsTotal {
  value: number;
  relation: string;
}

export interface GetLegalResponseResult {
  _id: string;
  _index: string;
  _score: number;
  risk?: string;
  _type: string;
  _source: GetLegalSource;
}

export interface GetLegalSource {
  OKVEDCodes: OKVEDCodes[];
  PSRN: number;
  TIN: string;
  TRRC: string;
  address: string;
  addressHistory: AddressHistory[];
  dateOfPSRN: string;
  directors: Directors[];
  fullName: string;
  name: string;
}

export interface OKVEDCodes {
  code: string;
  description: string;
}

export interface AddressHistory {
  regionName: string;
  regionType: string;
}

export interface Directors {
  TIN: string;
  name: string;
  patronymic: string;
  postTitle: string;
  recordDate: string;
  recordID: number;
  surname: string;
}

export interface PortfolioObjectTrademark extends PortfolioObject{
  showId: string;
  imageURL: string;
  author: string;
  rightHolder: string;
  registrationDate: string;
  validUntil: string;
  applicationDate: string;
}

export interface PortfolioObjectDomain extends PortfolioObject{
  domainAdministrator: string;
  validUntil: string;
}

export interface PortfolioObjectPatent extends PortfolioObject{
  author: string;
  rightHolder: string;
  registrationDate: string;
}

export interface PortfolioObjectSWDB extends PortfolioObject {
  showId: string;
  author: string;
  rightHolder: string;
  registrationDate: string;
}

export interface PortfolioObject {
  id: string;
  name: string;
  index: string;
}

export interface Pagination {
  size: number;
  from: number;
}

export interface SearchPortfolioObjectData {
  result: SearchPortfolioObjectsResponseResult[];
  pagination: Pagination;
  totalCount: number;
}

export interface ChangePageTechnicalTask {
  page: number;
  type: TypeObjects;
}

export interface AddObjectsModal {
  errorModal: boolean;
  portfolioObjectsError: PortfolioObject[];
  successLength: number;
}

export interface TypeOfObjectSelector {
  text: string;
  label: string;
  index: THECHNICAL_TASK_INDEX;
  id: number;
}

export const enum THECHNICAL_TASK_INDEX {
  INN = 'INN',
  TRADEMARKS = 'trademarks',
  PATENTS = 'patents',
  DOMAINS = 'domains',
  SWDB = 'swdb',
}

export type TypeObjects = 'trademarks' | 'patents' | 'domains' | 'swdb' | any;

export const DEFAULT_DROPDOWN_OPTIONS: TypeOfObjectSelector[] = [
  {
    id: 1,
    index: THECHNICAL_TASK_INDEX.INN,
    text: 'Все объекты по ИНН/ОГРН',
    label: 'Введите ИНН или ОГРН',
  },
  {
    id: 2,
    index: THECHNICAL_TASK_INDEX.TRADEMARKS,
    text: 'Товарные знаки',
    label: 'Укажите номер, название товарного знака или заявки',
  },
  {
    id: 3,
    index: THECHNICAL_TASK_INDEX.PATENTS,
    text: 'Патенты',
    label: 'Укажите наименование, номер патента или заявки',
  },
  {
    id: 4,
    index: THECHNICAL_TASK_INDEX.SWDB,
    text: 'Программы и базы данных',
    label: 'Укажите наименование или номер программы и базы данных',
  },
  {
    id: 5,
    index: THECHNICAL_TASK_INDEX.DOMAINS,
    text: 'Домены',
    label: 'Укажите домен',
  },
];

export const technicalTaskRightMapper = new Map()
  .set(THECHNICAL_TASK_INDEX.TRADEMARKS, 'trademarks_search-viewing')
  .set(THECHNICAL_TASK_INDEX.PATENTS, 'patents_search-viewing')
  .set(THECHNICAL_TASK_INDEX.SWDB, 'soft_search-viewing');