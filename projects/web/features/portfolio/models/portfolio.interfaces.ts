import { IAssignee, IAssignor } from "projects/app-debit/src/app/models/main.interfaces";

export interface InformationObject {
  id: string;
  type: string;
  isRequest: boolean;
}


export interface InformationObjectResponse {
  totalCount: number;
  filteredCount: number;
  objects: unknown[];
};

export interface LoadContractsResponse {
  totalCount: number;
  filteredCount: number;
  contracts: Contract[];
}

export interface InformationObjectStatus {
  id: number;
  label: string;
  status: string;
}

export interface RemoveInformationObject {
  id: string;
  index: string;
}

export interface IQueryByRemoveObject {
  objects: RemoveInformationObject[];
  organization?: string;
}

export interface TradeMark extends InformationObject {
  client: string;
  stage?: string;
  showId?: string;
  imageURL: string;
  validUntil: string;
  rightHolder?: string;
  applicationDate: string;
  status: number;
  index: {  id: string }[];
  isMonitoring: boolean;
  mappedStatus?: InformationObjectStatus;
}

export interface Patent extends InformationObject {
  name: string;
  status: number;
  registrationDate: string;
  index: {  id: string }[];
  isMonitoring: boolean;
  mappedStatus?: InformationObjectStatus;
}

export interface Program extends InformationObject {
  name: string;
  registrationDate: string;
  index: {  id: string }[];
  isMonitoring: boolean;
}

export interface Contract extends InformationObject {
  name: string;
  date?: string;
  valid: boolean;
  childCount: number;
  Objects: unknown[];
  assignor?: IAssignor;
  assignee?: IAssignee;
  contractNumber: string;
  comment?: string;
  customName?: string;
}

export interface Domain extends InformationObject {
  name: string;
  validUntil: string;
  domainOwner: string;
  status: number;
  index: {  id: string }[];
  isMonitoring: boolean;
  domainAdministrator: string;
  mappedStatus?: InformationObjectStatus;
}

export type PortfolioGeneralTab = PortfolioTab<PortfolioTabName, PortfolioTabType>;
export type PortfolioOtherTab = PortfolioTab<PortfolioOtherTabName, PortfolioOtherTabType>;
export interface PortfolioTab<Name, Type> {
  header: Name;
  type: Type,
}

export type PortfolioGeneralTabData = PortfolioTabData<PortfolioTabName, PortfolioTabType>;
export type PortfolioOtherTabData = PortfolioTabData<PortfolioOtherTabName, PortfolioOtherTabType>;
export interface PortfolioTabData<Name, Type> extends PortfolioTab<Name, Type> {
  count: number;
}

export interface CompositionWidgetData {
  count: number;
  bids: number;
  tradeMarks: number;
  patents: number;
  programs: number;
  domains: number;
  copyright: number;
  brandNames: number;
  contracts: number;
}

export interface SecurityRisksWidgetData {
  totalEvents: number;
  critical: number;
  recommendation: number;
  fatal: number;
  possible: number;
  allCount: number;
}

export interface PortfolioCostWidgetData {
  before: number;
  after: number;
}

export interface LoadSecurityRiskResponse {
  risk: number;
  values: number;
};

export interface LoadPortfolioCostResponse {
  found?: boolean;
  annualRecords: AnnualRecord[];
}

export interface LoadSecurityRiskPayload {
  PSRN: string | string[];
};

export interface AnnualRecord {
  forecastValueOfKnowHow: number;
  ratioToAnnualRevenue: number;
  ratioToTotalAssetValue: number;
  ratioToValueOfNonCurrentAssets: number;
  valueOfKnowHow: number;
  year: SVGStringList;
};

export const enum ADD_BUTTON_LABEL {
  ADD_OBJECT = 'Добавить объект',
  ADD_ORGANIZATION = 'Добавить организацию',
};

export const enum PortfolioTabName {
  TRADEMARKS = 'Товарные знаки',
  PATENTS = 'Патенты',
  PROGRAMS = 'Программы ЭВМ/БД',
  CONTRACTS = 'Договоры',
  OTHER = 'Другие',
}

export const enum PortfolioTabType {
  TRADEMARKS = 'tradeMarks',
  PATENTS = 'patents',
  PROGRAMS = 'programs',
  CONTRACTS = 'contracts',
  OTHER = 'other',
}

export interface TableColumn {
  header: string;
  field: string;
}

export const enum PortfolioOtherTabName {
  COPYRIGHT = 'Авторское право',
  BRAND_NAMES = 'Фирменные наименования',
  DOMAINS = 'Домены',
}

export const enum PortfolioOtherTabType {
  COPYRIGHT = 'copyright',
  BRAND_NAMES = 'brandNames',
  DOMAINS = 'domains',
}

export const enum SecurityRiskType {
  HIGH = 'высокая',
  MIDDLE = 'средняя',
  LOW = 'низкая',
}

export interface TableValue {
  [PortfolioTabType.TRADEMARKS]: TableValueData;
  [PortfolioTabType.PATENTS]: TableValueData;
  [PortfolioTabType.PROGRAMS]: TableValueData;
  [PortfolioTabType.CONTRACTS]: TableValueData;
  [PortfolioOtherTabType.DOMAINS]: TableValueData;
}

export interface TableValueData {
  data: unknown[];
  page: number;
  count: number;
  columns: TableColumn[];
  filters: PortfolioFilterState;
}

export interface LoadCompositionDataResponse {
  activeRequests: number;
  portfolio: CompositonDataResponsePortfolio;
}

export interface CompositonDataResponsePortfolio {
  patents: number;
  domains: number;
  soft: number;
  tradermarks: number;
}

export interface LoadPortfolioInfoByOrganization {
  organization: string;
}

export interface PortfolioFiltersDataResponse {
  trademarks: FilterData;
  patents: FilterData;
  programs: FilterData;
  contracts: FilterData;
  domens: DomainsFiltersData;
}

export interface PortfolioFiltersData {
  [PortfolioTabType.TRADEMARKS]: FilterData;
  [PortfolioTabType.PATENTS]: FilterData;
  [PortfolioTabType.PROGRAMS]: FilterData;
  [PortfolioTabType.CONTRACTS]: FilterData;
  [PortfolioOtherTabType.DOMAINS]: DomainsFiltersData;
}

export interface FilterData {
  owners: Owner[];
}

export interface DomainsFiltersData extends FilterData {
  domainAdministrators: Owner[];
}

export interface Owner {
  name: string;
}

export interface PortfolioFilterParams {
  limit: number;
  offset: number;
  types?: string;
  owner?: string;
  index?: string;
  status?: string;
  assignee?: string;
  searchQuery?: string;
  organization?: string;
  domainAdministrator?: string;
}

export interface PortfolioFilterState {
  searchQuery: string;
  owner: string[];
  types: string[];
  index?: string[];
  status?: string[];
  domainAdministrator?: string[];
}

export interface PopupInfoData {
  id: string | null;
  index: string | null;
  isMonitoring: boolean | null;
}

export type PortfolioTable = Omit<PortfolioTabType, 'other'> | PortfolioOtherTabType;