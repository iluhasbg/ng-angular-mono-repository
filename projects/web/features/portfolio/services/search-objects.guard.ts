import { Store } from "@ngxs/store";
import { Injectable } from "@angular/core";
import { AuthState } from '@web/core/states/auth.state';
import { APP_ROUTES_NAMES } from "@web/core/models/app-routes-names";
import { TechnicalTaskActions } from "../states/technical-task.actions";
import { catchError, filter, Observable, of, switchMap, take } from "rxjs";
import { technicalTaskRightMapper, THECHNICAL_TASK_INDEX } from "../models/technical-task.interfaces";
import { ActivatedRouteSnapshot, CanActivate, Params, Router, UrlTree } from "@angular/router";

type CanActivateReturn = boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree>;

@Injectable()
export class SearchObjectsGuard implements CanActivate {
  constructor(
    private readonly store: Store,
    private readonly router: Router,
  ) { }

  public canActivate(route: ActivatedRouteSnapshot): CanActivateReturn {
    return this.checkStore(route.queryParams).pipe(
      switchMap(result => of(result)),
      catchError(() => of(false)),
    );
  }

  private checkStore(queryParams: Params): Observable<boolean | UrlTree> {
    return this.store.select(AuthState.getRights).pipe(
      filter(rights => Boolean(rights)),
      switchMap(() => {
        if (queryParams?.name === '') {
          this.store.dispatch(new TechnicalTaskActions.ResetSearch());
          return of(this.navigateByIndex(queryParams.index));
        }

        if (!queryParams?.name) {
          if (this.checkRightByIndex(queryParams.index)) {
            return of(true);
          }
          return of(this.navigateByIndex(THECHNICAL_TASK_INDEX.INN));
        }

        if (queryParams.index !== THECHNICAL_TASK_INDEX.INN) {
          const { index, name } = queryParams;
          if (this.checkRightByIndex(index)) {
            this.store.dispatch(new TechnicalTaskActions.SearchObjects({ index, name }));
            return of(true);
          }
          return of(this.navigateByIndex(index));
        }

        if (queryParams.index === THECHNICAL_TASK_INDEX.INN && this.getINNValidation(queryParams.name)) {
          this.store.dispatch(new TechnicalTaskActions.SearchObjectsByINN({ id: queryParams.name }));
        }

        return of(true);
      }),
      take(1),
    );
  }

  private navigateByIndex(index: THECHNICAL_TASK_INDEX): UrlTree {
    return this.router.createUrlTree(['/', APP_ROUTES_NAMES.PORTFOLIO, APP_ROUTES_NAMES.TECHNICAL_TASK], { queryParams: { index } });
  }

  private getINNValidation(INN: string): boolean {
    return /^(?=[0-9]*$)(?:.{10}|.{12}|.{13}|.{15})$/.test(INN);
  }
  
  private checkRightByIndex(index: THECHNICAL_TASK_INDEX): boolean {
    if (this.hasRightsList(index)) {
      return this.store.selectSnapshot(AuthState.getRightByName(technicalTaskRightMapper.get(index)));
    }
    return true;
  }

  private hasRightsList(index: THECHNICAL_TASK_INDEX): boolean {
    return technicalTaskRightMapper.has(index);
  }
}
