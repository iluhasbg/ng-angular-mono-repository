import { Injectable } from "@angular/core";
import { Domain, InformationObjectStatus, Patent, PortfolioOtherTabType, PortfolioTable, PortfolioTabType, TradeMark } from "../models/portfolio.interfaces";

@Injectable()
export class PortfolioStatusService {
  public mapPortfolioObjectsStatus(type: PortfolioTable, objects: unknown[]): unknown[] {
    switch(type) {
      case PortfolioTabType.TRADEMARKS:
        return objects.map(tradeMark => this.mapTradeMarkStatus(tradeMark as TradeMark));
      case PortfolioTabType.PATENTS:
        return objects.map(patent => this.mapPatentStatus(patent as Patent));
      case PortfolioOtherTabType.DOMAINS:
        return objects.map(domain => this.mapDomainStatus(domain as Domain));
      default:
        return objects;
    }
  }

  public getTradeMarkStatusNumber(status: string): number {
    switch(status.toLowerCase()) {
      case 'действует': return 1;
      case 'срок действия истек': return 2;
      case 'прекратил действие': return 3;
      case 'пошлина за регистрацию не оплачена': return 10;
      case 'формальная экспертиза': return 14;
      case 'экспертиза заявленного обозначения': return 15;
      case 'заявка отозвана': return 16;
      case 'принято решение о регистрации товарного знака': return 17;
      case 'принято решение об отказе в регистрации товарного знака': return 18;
      case 'регистрация товарного знака': return 19;
      case 'принято решение о признании заявки отозванной': return 20;
      case 'заявка отозвана по просьбе заявителя': return 21;
      default: return 0;
    }
  }

  public mapPatentStatuses(statuses: string[]): string {
    const map = new Map()
      .set('Действует', 1)
      .set('Mожет прекратить свое действие', 2)
      .set('Прекратил действие, но может быть восстановлен', 3)
      .set('Не действует', 4)
    return statuses.map(status => map.get(status)).join(',');
  }

  public mapDomainStatuses(statuses: string[]): string {
    const map = new Map().set('занят', 0).set('свободен', 1);
    return statuses.map(status => map.get(status)).join(',');
  }

  private mapTradeMarkStatus(tradeMark: TradeMark): TradeMark {
    return { ...tradeMark, mappedStatus: this.getStatusTradeMark(tradeMark) };
  }

  private mapPatentStatus(patent: Patent): Patent {
    return { ...patent, mappedStatus: this.getStatusPatent(patent.status) };
  }

  private mapDomainStatus(domain: Domain): Domain {
    return { ...domain, mappedStatus: this.getStatusDomain(domain.index[0].id, domain.status) };
  }

  private getStatusTradeMark(tradeMark: TradeMark): InformationObjectStatus {
    const index = tradeMark.index[0].id, typeNum = tradeMark.status;
    if (index === 'заявка') {
      return { id: typeNum, label: tradeMark.stage, status: tradeMark.stage };
    }
    switch(typeNum) {
      case 0: return { id: typeNum, label: 'Нет данных', status: '' };
      case 1: return { id: typeNum, label: 'Действует', status: 'действует' };
      case 2: return { id: typeNum, label: 'Срок действия истек', status: 'не действует' };
      case 3: return { id: typeNum, label: 'Прекратил действие', status: 'не действует' };
      case 10: return { id: typeNum, label: 'Пошлина за регистрацию не оплачена', status: 'не действует' };
      case 14: return { id: typeNum, label: 'Формальная экспертиза', status: 'регистрация' };
      case 15: return { id: typeNum, label: 'Экспертиза заявленного обозначения', status: 'регистрация' };
      case 16: return { id: typeNum, label: 'Заявка отозвана', status: 'не действует' };
      case 17: return { id: typeNum, label: 'Принято решение о регистрации товарного знака', status: 'регистрация' };
      case 18: return { id: typeNum, label: 'Принято решение об отказе в регистрации товарного знака', status: 'не действует' };
      case 19: return { id: typeNum, label: 'Регистрация товарного знака', status: 'не действует' };
      case 20: return { id: typeNum, label: 'Принято решение о признании заявки отозванной', status: 'не действует' };
      case 21: return { id: typeNum, label: 'Заявка отозвана по просьбе заявителя', status: 'не действует' };
    }
  }

  private getStatusPatent(typeNum: number): InformationObjectStatus {
    switch(typeNum) {
      case 0: return { id: typeNum, label: 'Нет данных', status: '' };
      case 1: return { id: typeNum, label: 'Действует', status: 'действует' };
      case 2: return { id: typeNum, label: 'Mожет прекратить свое действие', status: 'действует' };
      case 3: return { id: typeNum, label: 'Прекратил действие, но может быть восстановлен', status: 'не действует' };
      case 4: return { id: typeNum, label: 'Не действует', status: 'не действует' };
    }
  }

  private getStatusDomain(index: string, typeNum: number): InformationObjectStatus {
    if (index === 'domains' && typeNum === 0) {
      return { id: typeNum, label: 'занят', status: '' };
    }
    if (index === 'domains' && typeNum === 1) {
      return { id: typeNum, label: 'свободен', status: '' };
    }
  }
}
