import {
  PortfolioObject,
  PortfolioObjectSWDB,
  PortfolioObjectDomain,
  PortfolioObjectPatent,
  PortfolioObjectTrademark,
  SearchPortfolioObjectsResponseResult
} from '@web/features/portfolio/models/technical-task.interfaces';
import {patch, updateItem} from '@ngxs/store/operators';

export class TechnicalTaskMapper {
  public static mapObjectsPortfolio(obj: SearchPortfolioObjectsResponseResult, type: string) {
    switch (type) {
      case 'trademarks':
        return this.mapTrademark(obj);
      case 'patents':
        return this.mapPatent(obj);
      case 'domains':
        return this.mapDomain(obj);
      case 'swdb':
        return this.mapSWDB(obj);
    }
  }

  public static mapTrademark(obj: SearchPortfolioObjectsResponseResult) {
    const id = obj._index === 'rutmap' ? obj._source.applicationString : obj._source.registrationString;
    const objectPortfolio: PortfolioObjectTrademark = {
      name: obj._source.imageText,
      id: obj._id,
      showId: id,
      index: obj._index,
      imageURL: obj._source.imgUrl,
      author: 'test',
      rightHolder: obj._source.applicantName,
      registrationDate: obj._source.registrationDate,
      validUntil: obj._source.expiryDate,
      applicationDate: obj._index === 'rutmap' ? obj._source.applicationDate : undefined
    };
    return objectPortfolio;
  }

  public static mapDomain(obj: SearchPortfolioObjectsResponseResult) {
    const objectPortfolio: PortfolioObjectDomain = {
      name: this.getDomain(obj),
      id: obj._id,
      index: obj._index,
      domainAdministrator: obj._source.domainRegistrator,
      validUntil: obj._source.payDate
    };
    return objectPortfolio;
  }

  public static getDomain(domainName) {
    if (domainName) {
      return domainName._source.fullDomain.includes('XN--')
        ? `${domainName._source.domain}.${domainName._source.zone}`.toUpperCase()
        : domainName._source.fullDomain.toUpperCase();
    }
  }

  public static mapPatent(obj: SearchPortfolioObjectsResponseResult) {
    const objectPortfolio: PortfolioObjectPatent = {
      name: obj._source.inventionName,
      id: obj._id,
      index: obj._index,
      author: obj._source.authors,
      rightHolder: obj._source.patentHolders,
      registrationDate: obj._source.registrationDate,
    };
    return objectPortfolio;
  }

  public static mapSWDB(obj: SearchPortfolioObjectsResponseResult) {
    const objectPortfolio: PortfolioObjectSWDB = {
      name: obj._index === 'rudb' ? obj._source.dbName : obj._index === 'rusw' ? obj._source.programName : '',
      id: obj._id,
      showId: obj._source.registrationString,
      index: obj._index,
      author: obj._source.authors,
      rightHolder: obj._source.rightHolders,
      registrationDate: obj._source.registrationDate
    };
    return objectPortfolio;
  }

  public static mapCheckedObjects(objects: SearchPortfolioObjectsResponseResult[]): any {
    const checked = objects.every(el => el.checked === false)
       ? { checked: true }
       : { checked: false };
    return objects.map((r, i) => updateItem<SearchPortfolioObjectsResponseResult>(i, patch(checked)));
  }

  public static deleteObjects(objectsPortfolio: PortfolioObject[], result) {
    const indexes = [];
    result.forEach(el => indexes.push(el._index));
    return objectsPortfolio.filter(objPortfolio => {
      if (!indexes.includes(objPortfolio.index)) {
        return objPortfolio;
      }
    });
  }

  public static mapUnchecked(objects: SearchPortfolioObjectsResponseResult[]): any {
    return objects?.map((r, i) => updateItem<SearchPortfolioObjectsResponseResult>(i, patch({checked: false})));
  }
}
