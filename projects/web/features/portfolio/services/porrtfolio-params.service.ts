import { Store } from "@ngxs/store";
import {
  PortfolioTable,
  TableValueData,
  PortfolioTabType,
  PortfolioFilterParams,
  PortfolioOtherTabType,
} from "../models/portfolio.interfaces";
import { Injectable } from "@angular/core";
import { PortfolioState } from "../states/portfolio.state";
import { PortfolioStatusService } from "./portfolio-status.service";

@Injectable()
export class PortfolioParamsService {
  constructor(
    private readonly store: Store,
    private readonly portfolioStatusService: PortfolioStatusService,
  ) { }

  public buildHttpParams(type: PortfolioTable): PortfolioFilterParams {
    switch (type) {
      case PortfolioTabType.TRADEMARKS:
        return this.buildTradeMarksParams();
      case PortfolioTabType.PATENTS:
        return this.buildPatentsParams();
      case PortfolioTabType.PROGRAMS:
        return this.buildProgramsParams();
      case PortfolioTabType.CONTRACTS:
        return this.buildContractsParams();
      case PortfolioOtherTabType.DOMAINS:
        return this.buildDomainsParams();
    }
  }

  private buildTradeMarksParams(): PortfolioFilterParams {
    const currentTableValue = this.store.selectSnapshot(PortfolioState.tableValueByType(PortfolioTabType.TRADEMARKS));
    const params: PortfolioFilterParams = this.initializeParams(PortfolioTabType.TRADEMARKS);
    if (currentTableValue.filters.index) {
      params.index = this.mapTradeMarkIndexes(currentTableValue.filters.index);
    }
    if (currentTableValue.filters.status) {
      params.status = currentTableValue.filters.status
        .map((status: string) => this.portfolioStatusService.getTradeMarkStatusNumber(status))
        .join(',');
    }

    return params;
  }

  private buildPatentsParams(): PortfolioFilterParams {
    const currentTableValue = this.store.selectSnapshot(PortfolioState.tableValueByType(PortfolioTabType.PATENTS)) as TableValueData;
    const params: PortfolioFilterParams = this.initializeParams(PortfolioTabType.PATENTS);
    if (currentTableValue.filters.status) {
      params.status = this.portfolioStatusService.mapPatentStatuses(currentTableValue.filters.status);
    }

    return params;
  }

  private buildProgramsParams(): PortfolioFilterParams {
    return this.initializeParams(PortfolioTabType.PROGRAMS);
  }

  private buildContractsParams(): PortfolioFilterParams {
    return this.initializeParams(PortfolioTabType.CONTRACTS);
  }

  private buildDomainsParams(): PortfolioFilterParams {
    const currentTableValue = this.store.selectSnapshot(PortfolioState.tableValueByType(PortfolioOtherTabType.DOMAINS)) as TableValueData;
    const params: PortfolioFilterParams = this.initializeParams(PortfolioOtherTabType.DOMAINS);
    if (currentTableValue.filters.domainAdministrator) {
      params.domainAdministrator = currentTableValue.filters.domainAdministrator.join(',');
    }
    if (currentTableValue.filters.status) {
      params.status = this.portfolioStatusService.mapDomainStatuses(currentTableValue.filters.status);
    }

    return params;
  }

  private initializeParams(type: PortfolioTabType | PortfolioOtherTabType): PortfolioFilterParams {
    const currentTableValue = this.store.selectSnapshot(PortfolioState.tableValueByType(type));
    const params: PortfolioFilterParams = {
      limit: 10,
      offset: (currentTableValue.page - 1) * 10,
      types: currentTableValue.filters.types && currentTableValue.filters.types.length
        ? currentTableValue.filters.types.join(',')
        : typesMapper.get(type),
    };
    if (currentTableValue.filters.searchQuery.trim().length) {
      params.searchQuery = currentTableValue.filters.searchQuery;
    }
    if (currentTableValue.filters.owner.length && type !== PortfolioTabType.CONTRACTS) {
      params.owner = currentTableValue.filters.owner;
    }
    if (currentTableValue.filters.owner.length && type === PortfolioTabType.CONTRACTS) {
      params.assignee = currentTableValue.filters.owner;
    }

    return this.addOrganizationToParams(params);
  }

  private mapTradeMarkIndexes(index: string[]): string {
    const map = new Map().set('Россия', 'rutm').set('Международная', 'wotm');
    return index.map(idx => map.get(idx)).join(',');
  }

  private addOrganizationToParams(params: PortfolioFilterParams): PortfolioFilterParams {
    const selectedCompanyId = this.store.selectSnapshot(PortfolioState.selectedCompanyId);
    if (selectedCompanyId) params.organization = selectedCompanyId;

    return params;
  }
}

const typesMapper = new Map()
  .set(PortfolioTabType.TRADEMARKS, 'нмпт,товарный знак,заявка на нмпт,заявка на товарный знак,международный товарный знак,общеизвестный товарный знак')
  .set(PortfolioTabType.PATENTS, 'патент на изобретение,патент на полезную модель,патент на промышленный образец')
  .set(PortfolioTabType.PROGRAMS, 'программа для эвм,база данных')
  .set(PortfolioTabType.CONTRACTS, '')
  .set(PortfolioOtherTabType.DOMAINS, 'домен');
