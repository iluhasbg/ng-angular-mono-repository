import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { WebModuleConfig } from '@web/core/services/config.service';
import { HttpClient, HttpContext, HttpParams } from '@angular/common/http';
import {
  IQueryByRemoveObject,
  PortfolioFilterParams,
  LoadContractsResponse,
  LoadSecurityRiskPayload,
  LoadSecurityRiskResponse,
  LoadPortfolioCostResponse,
  InformationObjectResponse,
  LoadCompositionDataResponse,
  PortfolioFiltersDataResponse,
  LoadPortfolioInfoByOrganization,
} from '../models/portfolio.interfaces';
import {
  GetLegalBody,
  GetLegalResponse,
  AddObjectsToPortfolioBody,
  SearchPortfolioObjectsBody,
  AddObjectsToPortfolioResponse,
  SearchPortfolioObjectsByINNBody,
  SearchPortfolioObjectsResponse
} from '@web/features/portfolio/models/technical-task.interfaces';
import { environment } from '../../../../app-ipid/src/environments/environment';
import { BASE_REQUEST_CONTEXT, ELASTIC_SEARCH_CONTEXT } from '@web/utils/auth.interceptor';

@Injectable()
export class PortfolioAPIService {
  constructor(
    private readonly httpClient: HttpClient,
    private readonly config: WebModuleConfig,
  ) { }

  public loadCompositionData(options?: LoadPortfolioInfoByOrganization): Observable<LoadCompositionDataResponse> {
    return this.httpClient.get<LoadCompositionDataResponse>(`${this.config.BASE_URL_API}/objects/count`, {
      params: new HttpParams({ fromObject: { ...options } }),
      context: new HttpContext().set(BASE_REQUEST_CONTEXT, true),
    });
  }

  public loadPortfolioSecurity(body: LoadSecurityRiskPayload): Observable<LoadSecurityRiskResponse[]> {
    return this.httpClient.post<LoadSecurityRiskResponse[]>(`${environment.BASE_ELASTIC_URL_API}/_qualifyrisksbypsrn`, body, {
      context: new HttpContext().set(ELASTIC_SEARCH_CONTEXT, true),
    });
  }

  public loadPortfolioCost(body: LoadSecurityRiskPayload): Observable<LoadPortfolioCostResponse> {
    return this.httpClient.post<LoadPortfolioCostResponse>(`${environment.BASE_ELASTIC_URL_API}/_annual_reports`, body, {
      context: new HttpContext().set(ELASTIC_SEARCH_CONTEXT, true),
    });
  }

  public loadObjectsByType(params: PortfolioFilterParams): Observable<InformationObjectResponse> {
    return this.httpClient.get<InformationObjectResponse>(`${this.config.BASE_URL_API}/objects`, {
      params: new HttpParams({ fromObject: { ...params } }),
      context: new HttpContext().set(BASE_REQUEST_CONTEXT, true),
    });
  }

  public loadContracts(params: PortfolioFilterParams): Observable<LoadContractsResponse> {
    return this.httpClient.get<LoadContractsResponse>(`${environment.BASE_URL_API}/objects/contracts`, {
      params: new HttpParams({ fromObject: { ...params } }),
      context: new HttpContext().set(BASE_REQUEST_CONTEXT, true),
    });
  }

  public loadPortfolioFilters(options?: LoadPortfolioInfoByOrganization): Observable<PortfolioFiltersDataResponse> {
    return this.httpClient.get<PortfolioFiltersDataResponse>(`${this.config.BASE_URL_API}/objects/filters`, {
      params: new HttpParams({ fromObject: { ...options } }),
      context: new HttpContext().set(BASE_REQUEST_CONTEXT, true),
    });
  }

  public removeInformationObject(options: IQueryByRemoveObject): Observable<void> {
    return this.httpClient.post<void>(`${environment.BASE_URL_API}/objects/delete`, options, {
      context: new HttpContext().set(BASE_REQUEST_CONTEXT, true),
    });
  }
  
  public searchPortfolioObjects(body: SearchPortfolioObjectsBody): Observable<SearchPortfolioObjectsResponse> {
    return this.httpClient.post<SearchPortfolioObjectsResponse>(`${environment.BASE_ELASTIC_URL_API}/_searchobjects`, body, {
      context: new HttpContext().set(ELASTIC_SEARCH_CONTEXT, true),
    });
  }

  public searchPortfolioObjectsByINN(body: SearchPortfolioObjectsByINNBody): Observable<SearchPortfolioObjectsResponse> {
    return this.httpClient.post<SearchPortfolioObjectsResponse>(`${environment.BASE_ELASTIC_URL_API}/_getObjectsByLegalId`, body, {
      context: new HttpContext().set(ELASTIC_SEARCH_CONTEXT, true),
    });
  }

  public addObjectsToPortfolio(body: AddObjectsToPortfolioBody): Observable<AddObjectsToPortfolioResponse> {
    return this.httpClient.post<AddObjectsToPortfolioResponse>(`${environment.BASE_URL_API}/objects/load`, body, { 
      context: new HttpContext().set(BASE_REQUEST_CONTEXT, true),
    });
  }

  public loadLegalName(body: GetLegalBody): Observable<GetLegalResponse> {
    return this.httpClient.post<GetLegalResponse>(`${environment.BASE_ELASTIC_URL_API}/_legal`, body, {
      context: new HttpContext().set(ELASTIC_SEARCH_CONTEXT, true),
    });
  }
}
