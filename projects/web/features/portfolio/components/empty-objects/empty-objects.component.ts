import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from "@angular/core";

@Component({
  selector: 'app-empty-objects',
  templateUrl: './empty-objects.component.html',
  styleUrls: ['./empty-objects.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EmptyObjectsComponent {
  @Input() disabled: boolean = false;
  @Output() openModal: EventEmitter<void> = new EventEmitter<void>();
}
