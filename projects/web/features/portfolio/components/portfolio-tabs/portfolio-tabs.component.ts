import { Component, EventEmitter, Input, Output, ChangeDetectionStrategy } from '@angular/core';
import { PortfolioGeneralTabData, PortfolioTabType,  } from '../../models/portfolio.interfaces';

@Component({
  selector: 'app-portfolio-tabs',
  templateUrl: './portfolio-tabs.component.html',
  styleUrls: ['./portfolio-tabs.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PortfolioTabsComponent {
  @Input() tabs: PortfolioGeneralTabData[] = [];
  @Input() currentTab: PortfolioTabType | null = null;
  @Output() changeTab: EventEmitter<PortfolioTabType> = new EventEmitter<PortfolioTabType>();

  public changeCurrentTab(tab: PortfolioTabType): void {
    this.changeTab.emit(tab);
  }
}
