import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { Component } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { APP_ROUTES_NAMES } from '@web/core/models/app-routes-names';
import { PortfolioState } from '@web/features/portfolio/states/portfolio.state';
import { PortfolioActions } from '@web/features/portfolio/states/portfolio.actions';
import { PortfolioStatusService } from '@web/features/portfolio/services/portfolio-status.service';
import { PopupInfoData, RemoveInformationObject, TradeMark } from '@web/features/portfolio/models/portfolio.interfaces';

@Component({
  selector: 'app-trademarks-table',
  templateUrl: './trademarks-table.component.html',
  styleUrls: [
    '../../../containers/portfolio-container/portfolio-container.component.scss',
    './trademarks-table.component.scss',
  ],
})
export class TrademarksTableComponent {
  @Select(PortfolioState.tableValueData) values$: Observable<TradeMark[]>;

  public showPopupInfoTM: boolean = false;
  public showPopupInfoTMA: boolean = false;
  public popupInfoData: PopupInfoData = { id: null, index: null, isMonitoring: null };

  constructor(
    public portfolioStatusService: PortfolioStatusService,
    private readonly router: Router,
    private readonly store: Store,
  ) { }

  public toCasesPage(id: string): void {
    this.router.navigate([`${APP_ROUTES_NAMES.CASES}/${APP_ROUTES_NAMES.CASES_ITEM}/${id}`], { queryParams: { isDraft: false } });
  }

  public openPopupInfo(tradeMark: TradeMark): void {
    this.popupInfoData = { id: tradeMark.id, index: tradeMark.index[0].id, isMonitoring: tradeMark.isMonitoring };
    this.showPopupInfoTM = ['rutm', 'wktm', 'wotm'].includes(tradeMark.index[0].id);
    this.showPopupInfoTMA = !['rutm', 'wktm', 'wotm'].includes(tradeMark.index[0].id);
  }

  public closePopupInfo(): void {
    this.popupInfoData = { id: null, index: null, isMonitoring: null };
    this.showPopupInfoTM = false;
    this.showPopupInfoTMA = false;
  }

  public removeObject(event: RemoveInformationObject): void {
    this.closePopupInfo();
    this.store.dispatch(new PortfolioActions.DeleteInformationObject(event));
  }

  public isImageUrl(value: string): boolean {
    const path = value.split('/');
    return path.some(item => ['https:', 'tm'].includes(item));
  }
}
