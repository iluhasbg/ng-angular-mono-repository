import { Observable } from 'rxjs';
import { Component } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { PortfolioState } from '@web/features/portfolio/states/portfolio.state';
import { PortfolioActions } from '@web/features/portfolio/states/portfolio.actions';
import { Patent, RemoveInformationObject } from '@web/features/portfolio/models/portfolio.interfaces';

@Component({
  selector: 'app-patents-table',
  templateUrl: './patents-table.component.html',
  styleUrls: [
    '../../../containers/portfolio-container/portfolio-container.component.scss',
    './patents-table.component.scss',
  ],
})
export class PatentsTableComponent {
  @Select(PortfolioState.tableValueData) values$: Observable<Patent[]>;
  
  public showPopupInfoPatent: boolean = false;
  public popupInfoData = { id: null, index: null, isMonitoring: null };

  constructor(private readonly store: Store) { }

  public openPopupInfo(patent: Patent): void {
    this.showPopupInfoPatent = true;
    this.popupInfoData = { id: patent.id, index: patent.index[0].id, isMonitoring: patent.isMonitoring };
  }

  public closePopupInfo(): void {
    this.showPopupInfoPatent = false;
    this.popupInfoData = { id: null, index: null, isMonitoring: null };
  }

  public removeObject(event: RemoveInformationObject): void {
    this.closePopupInfo();
    this.store.dispatch(new PortfolioActions.DeleteInformationObject(event));
  }
}
