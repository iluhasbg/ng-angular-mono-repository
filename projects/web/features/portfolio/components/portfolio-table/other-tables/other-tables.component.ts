import { Observable } from 'rxjs';
import { Component } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import {
  Domain,
  PortfolioOtherTab,
  PortfolioOtherTabData,
  PortfolioOtherTabType,
  RemoveInformationObject,
} from '@web/features/portfolio/models/portfolio.interfaces';
import { PortfolioState } from '@web/features/portfolio/states/portfolio.state';
import { PortfolioActions } from '@web/features/portfolio/states/portfolio.actions';

@Component({
  selector: 'app-other-tables',
  templateUrl: './other-tables.component.html',
  styleUrls: ['../../../containers/portfolio-container/portfolio-container.component.scss', './other-tables.component.scss'],
})
export class OtherTablesComponent {
  @Select(PortfolioState.tableValueData) values$: Observable<Domain[]>;
  @Select(PortfolioState.otherTabs) otherTabs$: Observable<PortfolioOtherTab[]>;
  @Select(PortfolioState.currentOtherTab) currentOtherTab$: Observable<PortfolioOtherTabType>;

  public showPopupInfoDomain = false;
  public popupInfoData = { id: null, index: null, isMonitoring: null };

  constructor(private readonly store: Store) { }

  public changeCurrentTab(tab: PortfolioOtherTabData): void {
    this.store.dispatch(new PortfolioActions.SetPortfolioOtherCurrentTab(tab.type));
  }

  public openPopupInfo(domain: Domain): void {
    this.showPopupInfoDomain = true;
    this.popupInfoData = { id: domain.id, index: domain.index[0].id, isMonitoring: domain.isMonitoring };
  }

  public closePopupInfo(): void {
    this.showPopupInfoDomain = false;
    this.popupInfoData = { id: null, index: null, isMonitoring: null };
  }

  public removeObject(event: RemoveInformationObject): void {
    this.closePopupInfo();
    this.store.dispatch(new PortfolioActions.DeleteInformationObject(event));
  }
}
