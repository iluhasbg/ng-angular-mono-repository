import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'app-copyright-table',
  templateUrl: './copyright-table.component.html',
  styleUrls: ['./copyright-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CopyrightTableComponent {
  @Input() values: any[] = [];
}
