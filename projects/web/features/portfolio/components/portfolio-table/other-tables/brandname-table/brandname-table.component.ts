import { ChangeDetectionStrategy, Component, Input } from "@angular/core";

@Component({
  selector: 'app-brandname-table',
  templateUrl: './brandname-table.component.html',
  styleUrls: ['./brandname-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BrandnameTableComponent {
  @Input() values: any[] = [];
}
