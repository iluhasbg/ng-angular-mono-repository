import { Observable } from 'rxjs';
import { Component } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { PortfolioState } from '@web/features/portfolio/states/portfolio.state';
import { PortfolioActions } from '@web/features/portfolio/states/portfolio.actions';
import { Program, RemoveInformationObject } from '@web/features/portfolio/models/portfolio.interfaces';

@Component({
  selector: 'app-programs-table',
  templateUrl: './programs-table.component.html',
  styleUrls: [
    '../../../containers/portfolio-container/portfolio-container.component.scss',
    './programs-table.component.scss',
  ],
})
export class ProgramsTableComponent {
  @Select(PortfolioState.tableValueData) values$: Observable<Program[]>;
  
  public showPopupInfoDb: boolean = false;
  public popupInfoData = { id: null, index: null, isMonitoring: null };

  constructor(private readonly store: Store) { }

  public openPopupInfo(program: Program): void {
    this.showPopupInfoDb = true;
    this.popupInfoData = { id: program.id, index: program.index[0].id, isMonitoring: program.isMonitoring };
  }

  public closePopupInfo(): void {
    this.showPopupInfoDb = false;
    this.popupInfoData = { id: null, index: null, isMonitoring: null };
  }

  public removeObject(event: RemoveInformationObject): void {
    this.closePopupInfo();
    this.store.dispatch(new PortfolioActions.DeleteInformationObject(event));
  }
}
