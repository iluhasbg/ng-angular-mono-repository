import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { Component } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Contract } from '@web/features/portfolio/models/portfolio.interfaces';
import { PortfolioState } from '@web/features/portfolio/states/portfolio.state';

@Component({
  selector: 'app-contracts-table',
  templateUrl: './contracts-table.component.html',
  styleUrls: [
    '../../../containers/portfolio-container/portfolio-container.component.scss',
    './contracts-table.component.scss',
  ],
})
export class ContractsTableComponent {
  @Select(PortfolioState.tableValueData) values$: Observable<Contract[]>;

  constructor(
    private readonly store: Store,
    private readonly router: Router,
  ) { }

  public getContract(contract: Contract): void {
    const { id: contractID, comment, name, customName } = contract;
    this.router.navigate(['contracts'], { queryParams: { contractID, selectedCompanyId: this.store.selectSnapshot(PortfolioState.selectedCompanyId), comment, name, customName } });
    window.scrollTo(0, 0);
  }
}
