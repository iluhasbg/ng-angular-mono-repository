import { Component, EventEmitter, HostListener, Input, OnInit, Output } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { SearchService } from '../../../../../app-search/src/app/services/search.service';
import { Subject } from 'rxjs';
import { PortfolioService } from '../../../../../app-ipid/src/app/shared/services/portfolio.service';
import { HttpService } from '../../../../../app-ipid/src/app/shared/services/http.service';
import risksData from '../../../../../app-ipid/src/app/pages/monitoring/json-data-risks/json-data-risks';
import { Router } from '@angular/router';
import { CorpSearchService } from '../../../corp/corp-search/services/corp-search.service';
import allowedTypes from '../../../../../shared/types/allowed-types';
import * as _ from 'lodash';
import { SupportChatService } from '../../../../../shared/services/support-chat.service';
import {DownloadFilesService} from '../../../../../shared/services/download-files.service';
import { IntercomEventsService, INTERCOM_EVENT_NAME } from 'projects/shared/services/intercom-events.service';

@Component({
  selector: 'app-portfolio-logotype-item',
  templateUrl: './portfolio-logotype-item.component.html',
  styleUrls: ['./portfolio-logotype-item.component.scss'],
})
export class PortfolioLogotypeItemComponent implements OnInit {
  risksData = risksData;
  events = false;

  @Input() data;
  @Input() showRemoveBtn = false;
  private readonly onDestroy = new Subject<void>();
  sourceData: any = {};
  imageUrl;
  viewClassesCount = 'short';
  contractsData = [];

  monitoringEvents = [];
  monitoringRisks = 0;

  files = [];
  fileError = '';
  maxRiskLevel = 1;
  isLoadingMaxRiskLevel = false;
  noMonitoringRisks = [];

  constructor(
    private searchService: SearchService,
    private portfolioService: PortfolioService,
    private httpService: HttpService,
    private router: Router,
    private corp: CorpSearchService,
    private supportChatService: SupportChatService,
    readonly downloadFiles: DownloadFilesService,
    private readonly intercomEventsService: IntercomEventsService,
  ) {
  }

  @Output() modalEvent = new EventEmitter();
  @Output() removeObject = new EventEmitter();

  ngOnInit(): void {
    this.getDocById();
    this.getFiles();

    if (this.data.isMonitoring) {
      this.getMonitoringRisk();
    } else {
      this.getRisks();
    }
    this.intercomEventsService
      .push({ event: INTERCOM_EVENT_NAME.OPEN_ITEM, item_type: this.getTitle('trademark', this.sourceData) });
  }

  getFiles(): void {
    const query = {
      objectID: this.data.id,
      index: this.data.index
    };
    this.portfolioService.getFilesByObject(query).subscribe((data: any) => {
      this.files = data.files;
    });
  }

  getDocById(): void {
    const query = {
      _id: this.data.id,
      _index: this.data.index
    };
    this.searchService.getDocumentById(query).pipe(takeUntil(this.onDestroy)).subscribe((data: any) => {
      if (data?.hits?.hits?.length) {
        this.sourceData = data.hits.hits[0];
        this.sourceData.indexType = this.searchService.getIndexType(this.sourceData);
        const id = this.sourceData._source.registrationString;
        if (this.sourceData._source.markImageFileName) {
          this.imageUrl = this.searchService.getImgUrl(
            this.sourceData._index,
            parseInt(id, 10),
            this.sourceData._source.markImageFileName,
            id);
        }
        this.createContractsData();
      }
    });
  }

  getMonitoringRisk() {
    this.portfolioService.getTopRisksByObject({
      id: this.data.id,
      index: this.data.index
    }).subscribe((data) => {
      this.monitoringRisks = data.countRisks;
      this.monitoringEvents = data.risks.sort((a, b) => b.level - a.level);
      this.monitoringEvents.forEach(item => {
        item.riskData = this.risksData.find(el => el.type === +item.type);
      });
      if (this.monitoringEvents[0]?.level) {
        this.maxRiskLevel = this.monitoringEvents[0].level;
      }
      this.isLoadingMaxRiskLevel = true;
    });
  }

  getRisks(): void {
    const query = {
      object_id: this.data.id,
      object_index: this.data.index
    };
    this.portfolioService.getRisks(query).pipe(takeUntil(this.onDestroy)).subscribe((data: any) => {
      if (data && data.length) {
        data.forEach(item => {
          if (item.risk !== 0) {
            this.monitoringRisks += item.values;
          }
        });
        this.noMonitoringRisks = data.filter(el => el.risk !== 0).sort((a, b) => b.risk - a.risk);
        this.maxRiskLevel = this.noMonitoringRisks[0].risk;
      }
      this.isLoadingMaxRiskLevel = true;
    });
  }

  get isLoading(): boolean {
    return this.isLoadingMaxRiskLevel;
  }

  getTitle(type: string, obj): string {
    switch (type) {
      case 'trademark':
        switch (obj._index) {
          case 'rutm':
            return `Товарный знак ${obj._source.registrationString}`;
          case 'wktm':
            return `Oбщеизвестный товарный знак ${obj._source.registrationString}`;
          case 'wotm':
            return `Международный товарный знак ${obj._source.registrationString}`;
          default:
            return '';
        }
    }
  }

  getStatusTrademark(sourceData): string {
    let status = '';
    switch (sourceData._source.statusFIPS) {
      case 1:
        status = 'Действует';
        break;
      case 2:
        status = 'Может прекратить действие';
        break;
      case 3:
        status = 'Не действует';
        break;
      case 0:
      default:
        break;
    }
    return status;
  }

  closeModal(): void {
    this.modalEvent.emit();
  }

  @HostListener('document:keyup', ['$event']) // 27=esc
  keyup(event: KeyboardEvent): void {
    if (event.keyCode === 27) {
      this.closeModal();
    }
  }

  getMktuClasses() {
    if (this.viewClassesCount === 'long') {
      return this.sourceData._source.goodsServices;
    } else {
      return this.sourceData._source.goodsServices.slice(0, 3);
    }
  }

  createContractsData(): void {
    if (this.sourceData._source.contracts) {
      this.sourceData._source.contracts.forEach(item => {
        if (item.contractNumber !== 'Нет договора') {
          this.contractsData.push(item);
        }
        if (item.subContracts && item.subContracts.length > 0) {
          this.contractsData = this.contractsData.concat(item.subContracts);
        }
      });
    }
  }

  showAllClasses(): void {
    this.viewClassesCount = 'long';
  }

  getRiskByLevel(level: number): string {
    switch (level) {
      case 1:
        return 'Нет риска';
      case 2:
        return 'Средний риск';
      case 3:
        return 'Высокий риск';
      case 4:
        return 'Критичный риск';
    }
  }

  getRiskTitleByLevel(level: number): string {
    switch (level) {
      case 1:
        return 'Рекоммендаций';
      case 2:
        return 'Средних рисков';
      case 3:
        return 'Высоких рисков';
      case 4:
        return 'Критичных рисков';
    }
  }

  confirming(name: string) {
    if (confirm('Вы уверены что хотите удалить Товарный знак №' + name + ' из портфеля?')) {
      console.log('agreed');
    }
  }

  deleteObject(): void {
    this.removeObject.emit({id: this.data.id, index: this.data.index});
  }

  /** Событие загрузки файла
   */
  fileChangeEvent(fileInput: any) {
    if (this.fileError?.length) {
      this.fileError = '';
    }
    if (fileInput.target.files && fileInput.target.files[0]) {
      const file = fileInput.target.files[0];
      if (file) {
        const maxSize = 2000000;
        if (fileInput.target.files[0].size > maxSize) {
          this.fileError = 'Максимальный размер файла ' + maxSize / 1000000 + 'Mb';
          return false;
        }
        if (!_.includes(allowedTypes, fileInput.target.files[0].type)) {
          this.fileError = 'Недопустимый тип файла';
          return false;
        }
        if (!this.fileError?.length) {
          const formData: FormData = new FormData();
          formData.append('file', file, file.name);
          this.httpService.post({
            path: 'objects/file/add',
            params: {
              objectID: this.data.id,
              index: this.data.index
            },
            body: formData,
            isFile: true
          })
            .subscribe((data: any) => {
              this.files.push({
                name: fileInput.target.files[0].name,
                publicPath: data.path,
                size: fileInput.target.files[0].size
              });
              fileInput.target.value = '';
            });
        }
      }
    }
  }

  removeFile(path: string): void {
    this.httpService.post({
      path: 'objects/file/delete',
      params: {
        objectID: this.data.id,
        index: this.data.index
      },
      body: {path}
    }).subscribe(() => {
      this.getFiles();
    });
  }

  getExternalLink(index) {
    return this.corp.getExternalUrl(index);
  }

  public toLeadForm(id): void {
    const data = {
      formIdentity: 'Оспорить риск',
      objectId: this.data.id,
      objectIndex: this.data.index,
      riskId: id
    };
    this.router.navigate(['/treaty-extension'], {queryParams: {data: JSON.stringify(data)}});
  }

  onClickBtn(btnTitle: string) {
    this.supportChatService.open(btnTitle);
  }
}
