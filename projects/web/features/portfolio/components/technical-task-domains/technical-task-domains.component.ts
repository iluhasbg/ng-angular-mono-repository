import {Component, EventEmitter, Input, Output} from '@angular/core';
import {
  ChangePageTechnicalTask,
  SearchPortfolioObjectData,
  SearchPortfolioObjectsResponseResult,
} from '@web/features/portfolio/models/technical-task.interfaces';
import {Store} from '@ngxs/store';
import {TechnicalTaskActions} from '@web/features/portfolio/states/technical-task.actions';
import { TechnicalTaskState } from '../../states/technical-task.state';

@Component({
  selector: 'app-technical-task-domains',
  templateUrl: './technical-task-domains.component.html',
  styleUrls: ['./technical-task-domains.component.scss'],
})
export class TechnicalTaskDomainsComponent {
  @Input() domains: SearchPortfolioObjectData | null = null;
  @Input() currentPage: number;
  @Input() allDomainsChecked: boolean;

  @Output() changePage = new EventEmitter<ChangePageTechnicalTask>();

  public showPopupInfoDomain = false;
  public popupInfoData = { id: null, index: null, isMonitoring: null };

  constructor(private store: Store) { }

  public checkDomain(event: MouseEvent, obj: SearchPortfolioObjectsResponseResult): void {
    event.stopPropagation();
    this.store.dispatch(new TechnicalTaskActions.CheckTechnicalTaskObject('domains', obj));
  }

  public checkAllDomains(): void {
    this.store.dispatch(new TechnicalTaskActions.CheckAllTechnicalTaskObject('domains'));
  }

  public trimString(str: string, count: number): string {
    return str?.length > count ? str.slice(0, count) + '...' : str;
  }

  public onChangePage(page: number) {
    this.changePage.emit({page, type: 'domains'});
  }

  public getDomain(domainName) {
    if (domainName) {
      return domainName._source.fullDomain.includes('XN--')
        ? `${domainName._source.domain}.${domainName._source.zone}`.toUpperCase()
        : domainName._source.fullDomain.toUpperCase();
    }
  }

  public openPopupInfo(domain: any): void {
    this.popupInfoData = { id: domain._id, index: domain._index, isMonitoring: domain.isMonitoring };
    this.showPopupInfoDomain = true;
  }

  public closePopupInfo(): void {
    this.popupInfoData = { id: null, index: null, isMonitoring: null };
    this.showPopupInfoDomain = false;
  }

  public getDomainChecked(id: string): boolean {
    return this.store.selectSnapshot(TechnicalTaskState.checked).findIndex(item => item.id === id) > -1;
  }
}
