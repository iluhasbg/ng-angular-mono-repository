import * as _ from 'lodash';
import { filter } from 'rxjs/operators';
import { Router } from '@angular/router';
import allowedTypes from 'projects/shared/types/allowed-types';
import { SupportChatService } from 'projects/shared/services/support-chat.service';
import { SearchService } from 'projects/app-search/src/app/services/search.service';
import { HttpService } from 'projects/app-ipid/src/app/shared/services/http.service';
import {DownloadFilesService} from 'projects/shared/services/download-files.service';
import { CorpSearchService } from '../../../corp/corp-search/services/corp-search.service';
import { Component, EventEmitter, HostListener, OnInit, Output, Input } from '@angular/core';
import { PortfolioService } from 'projects/app-ipid/src/app/shared/services/portfolio.service';
import risksData from 'projects/app-ipid/src/app/pages/monitoring/json-data-risks/json-data-risks';
import { IntercomEventsService, INTERCOM_EVENT_NAME } from 'projects/shared/services/intercom-events.service';

@Component({
  selector: 'app-portfolio-db-evm',
  templateUrl: './portfolio-db-evm.component.html',
  styleUrls: ['./portfolio-db-evm.component.scss'],
})
export class PortfolioDbEvmComponent implements OnInit {
  public risksData = risksData;
  public events: boolean = false;
  @Input() data;
  @Input() showRemoveBtn = false;

  public monitoringEvents = [];

  public sourceData: any = {};
  public monitoringRisks = 0;
  public files = [];
  public fileError = '';
  public maxRiskLevel = 1;
  public noMonitoringRisks = [];

  constructor(
    private readonly router: Router,
    private readonly corp: CorpSearchService,
    private readonly httpService: HttpService,
    private readonly searchService: SearchService,
    public readonly downloadFiles: DownloadFilesService,
    private readonly portfolioService: PortfolioService,
    private readonly supportChatService: SupportChatService,
    private readonly intercomEventsService: IntercomEventsService,
  ) { }

  @Output() modalEvent = new EventEmitter();
  @Output() removeObject = new EventEmitter();F

  public ngOnInit(): void {
    this.getDocById();
    this.getFiles();
    this.data.isMonitoring ? this.getMonitoringRisk() : this.getRisks();
    this.intercomEventsService.push({
      event: INTERCOM_EVENT_NAME.OPEN_ITEM,
      item_type: this.sourceData._index === 'rudb' ? 'База данных' : 'Программа для ЭВМ',
    });
  }

  public getFiles(): void {
    const query = { objectID: this.data.id, index: this.data.index };
    this.portfolioService.getFilesByObject(query).subscribe((data: any) => this.files = data.files);
  }

  public getDocById(): void {
    const query = { _id: this.data.id, _index: this.data.index };
    this.searchService.getDocumentById(query)
      .pipe(filter((data: any) => data?.hits?.hits?.length))
      .subscribe((data: any) => this.sourceData = data.hits.hits[0]);
  }

  public getRisks(): void {
    const query = { object_id: this.data.id,  object_index: this.data.index };
    this.portfolioService.getRisks(query).subscribe((data: any) => {
      if (data && data.length) {
        data.forEach(item => {
          if (item.risk !== 0) {
            this.monitoringRisks += item.values;
          }
        });
        this.noMonitoringRisks = data.filter(el => el.risk !== 0).sort((a, b) => b.risk - a.risk);
        this.maxRiskLevel = this.noMonitoringRisks[0].risk;
      }
    });
  }

  public closeModal(): void {
    this.modalEvent.emit();
  }

  public confirming(name: string) {
    if (confirm('Вы уверены что хотите удалить Патент №' + name + ' из портфеля?')) {
      console.log('agreed');
    }
  }

  @HostListener('document:keyup', ['$event']) // 27=esc
  keyup(event: KeyboardEvent): void {
    if (event.keyCode === 27) {
      this.closeModal();
    }
  }

  public getMonitoringRisk() {
    this.portfolioService.getTopRisksByObject({id: this.data.id,
      index: this.data.index}).subscribe((data) => {
      this.monitoringRisks = data.countRisks;
      this.monitoringEvents = data.risks.sort((a, b) => b.level - a.level);
      this.monitoringEvents.forEach(item => {
        item.riskData = this.risksData.find(el => el.type === +item.type);
      });
      if (this.monitoringEvents[0]?.level) {
        this.maxRiskLevel = this.monitoringEvents[0].level;
      }
    });
  }

  public getRiskByLevel(level: number): string {
    switch (level) {
      case 1: return 'Нет риска';
      case 2: return 'Средний риск';
      case 3: return 'Высокий риск';
      case 4: return 'Критичный риск';
    }
  }

  public deleteObject(): void {
    this.removeObject.emit({id: this.data.id, index: this.data.index});
  }

  public getRiskTitleByLevel(level: number): string {
    switch (level) {
      case 1: return 'Рекоммендаций';
      case 2: return 'Средних рисков';
      case 3: return 'Высоких рисков';
      case 4: return 'Критичных рисков';
    }
  }


  public fileChangeEvent(fileInput: any) {
    if (this.fileError?.length) {
      this.fileError = '';
    }
    if (fileInput.target.files && fileInput.target.files[0]) {
      const file = fileInput.target.files[0];
      if (file) {
        const maxSize = 2000000;
        if (fileInput.target.files[0].size > maxSize) {
          this.fileError = 'Максимальный размер файла ' + maxSize / 1000000 + 'Mb';
          return false;
        }
        if (!_.includes(allowedTypes, fileInput.target.files[0].type)) {
          this.fileError = 'Недопустимый тип файла';
          return false;
        }
        if (!this.fileError?.length) {
          const formData: FormData = new FormData();
          formData.append('file', file, file.name);
          this.httpService.post({
            path: 'objects/file/add',
            params: {
              objectID: this.data.id,
              index: this.data.index
            },
            body: formData,
            isFile: true
          })
            .subscribe((data: any) => {
              this.files.push({
                name: fileInput.target.files[0].name,
                publicPath: data.path,
                size: fileInput.target.files[0].size
              });
              fileInput.target.value = '';
            });
        }
      }
    }
  }

  public removeFile(path: string): void {
    this.httpService.post({
      path: 'objects/file/delete',
      params: { objectID: this.data.id, index: this.data.index },
      body: { path },
    }).subscribe(() => this.getFiles());
  }

  public getExternalLink(index): string {
    return this.corp.getExternalUrl(index);
  }

  public toLeadForm(id): void {
    const data = {
      formIdentity: 'Оспорить риск',
      objectId: this.data.id,
      objectIndex: this.data.index,
      riskId: id,
    };
    this.router.navigate(['/treaty-extension'], { queryParams: { data: JSON.stringify(data) } });
  }

  public onClickBtn(btnTitle: string): void {
    this.supportChatService.open(btnTitle);
  }
}
