import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-portfolio-loading-modal',
  templateUrl: './portfolio-loading-modal.component.html',
  styleUrls: ['./portfolio-loading-modal.component.scss'],
})
export class PortfolioLoadingModalComponent implements OnInit {
  @Input() brandsearch;
  @Input() legalsupport;
  @Input() typeRequest;
  @Input() fieldName: string;

  public bases: boolean;
  public domains: boolean;
  public basesInternational: boolean;
  public loadingBases: boolean = false;
  public loadingReestr: boolean = false;
  public loadingInternational: boolean = false;

  public ngOnInit(): void {
    if (this.typeRequest === 'all') {
      this.bases = true;
      this.basesInternational = true;
      this.domains = true;

      setTimeout(() => {
        this.loadingBases = true;
      }, 300);

      setTimeout(() => {
        this.loadingInternational = true;
      }, 700);

      setTimeout(() => {
        this.loadingReestr = true;
      }, 1000);
    }

    if (this.typeRequest === 'firstAndSecond') {
      this.bases = true;
      this.basesInternational = true;
      this.domains = false;
    }

    if (this.typeRequest === 'third') {
      this.bases = false;
      this.basesInternational = false;
      this.domains = true;
    }
  }
}
