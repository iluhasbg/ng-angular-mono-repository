import { OrganizationInterface } from '@web/core/models/organization.interface';
import { SecurityRisksWidgetData, SecurityRiskType } from '../../models/portfolio.interfaces';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnChanges, OnDestroy, OnInit } from '@angular/core';

@Component({
  selector: 'app-portfolio-security',
  templateUrl: './portfolio-security.component.html',
  styleUrls: ['./portfolio-security.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PortfolioSucurityComponent implements OnInit, OnDestroy, OnChanges {
  @Input() isLoading: boolean = true;
  @Input() selectedCompany: OrganizationInterface | null = null;
  @Input() data: SecurityRisksWidgetData;

  constructor(private readonly ref: ChangeDetectorRef) { }
  
  public securityLevelLabel: string = '';
  public mobileOpenSecondChart: boolean =  window.innerWidth <= 793 ? false : true;

  public ngOnInit(): void {
    window.addEventListener('resize', () => this.onResizeWindow());
  }

  public ngOnDestroy(): void {
    window.removeEventListener('resize', () => this.onResizeWindow());
    window.removeEventListener('resize', () => this.onResizeOpenSecondCard());
  }

  public ngOnChanges(): void {
    this.addLabelForSecurityLevel();
  }

 public mobileOpenSecondCard(): void {
    if (window.innerWidth <= 793) {
      this.mobileOpenSecondChart = !this.mobileOpenSecondChart;
      window.addEventListener('resize', () => this.onResizeOpenSecondCard());
    }
  }

  private onResizeWindow(): void {
    this.ref.detectChanges();
    this.mobileOpenSecondChart = window.innerWidth <= 793 ? false : true
  }

  private onResizeOpenSecondCard(): void {
    if (window.innerWidth <= 793) {
      this.mobileOpenSecondChart = !this.mobileOpenSecondChart;
    }
  }

  private addLabelForSecurityLevel(): void {
    if (this.data.critical > 0 || this.data.fatal > 0) {
      this.securityLevelLabel = SecurityRiskType.LOW;
    } else if (this.data.possible > 0 && this.data.critical === 0 && this.data.fatal === 0) {
      this.securityLevelLabel = SecurityRiskType.MIDDLE;
    } else if (this.data.critical === 0 && this.data.fatal === 0 && this.data.possible === 0) {
      this.securityLevelLabel = SecurityRiskType.HIGH;
    }
  }
}
