import {Component, EventEmitter, Input, Output} from '@angular/core';
import {
  ChangePageTechnicalTask,
  SearchPortfolioObjectData,
  SearchPortfolioObjectsResponseResult,
} from '@web/features/portfolio/models/technical-task.interfaces';
import {Store} from '@ngxs/store';
import {TechnicalTaskActions} from '@web/features/portfolio/states/technical-task.actions';
import { TechnicalTaskState } from '../../states/technical-task.state';

@Component({
  selector: 'app-technical-task-swdb',
  templateUrl: './technical-task-swdb.component.html',
  styleUrls: ['./technical-task-swdb.component.scss'],
})
export class TechnicalTaskSwdbComponent {
  @Input() swdb: SearchPortfolioObjectData | null = null;
  @Input() currentPage: number;
  @Input() allSWDBChecked: boolean;

  @Output() changePage = new EventEmitter<ChangePageTechnicalTask>();

  public showPopupInfoDb: boolean = false;
  public popupInfoData = { id: null, index: null, isMonitoring: null };

  constructor(private store: Store) { }

  public checkSWDB(event: MouseEvent, obj: SearchPortfolioObjectsResponseResult): void {
    event.stopPropagation();
    this.store.dispatch(new TechnicalTaskActions.CheckTechnicalTaskObject('swdb', obj));
  }

  public checkAllSWDBs(): void {
    this.store.dispatch(new TechnicalTaskActions.CheckAllTechnicalTaskObject('swdb'));
  }

  public trimString(str: string, count: number): string {
    return str?.length > count ? str.slice(0, count) + '...' : str;
  }

  public onChangePage(page: number) {
    this.changePage.emit({page, type: 'swdb'});
  }

  public openPopupInfo(program: any): void {
    this.popupInfoData = { id: program._id, index: program._index, isMonitoring: program.isMonitoring };
    this.showPopupInfoDb = true;
  }

  public closePopupInfo(): void {
    this.popupInfoData = { id: null, index: null, isMonitoring: null };
    this.showPopupInfoDb = false;
  }

  public getSWDBChecked(id: string): boolean {
    return this.store.selectSnapshot(TechnicalTaskState.checked).findIndex(item => item.id === id) > -1;
  }
}
