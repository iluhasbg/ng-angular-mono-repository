import { Store } from '@ngxs/store';
import {
  ChangePageTechnicalTask,
  SearchPortfolioObjectData,
  SearchPortfolioObjectsResponseResult,
} from '@web/features/portfolio/models/technical-task.interfaces';
import { PopupInfoData } from '../../models/portfolio.interfaces';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { TechnicalTaskActions } from '@web/features/portfolio/states/technical-task.actions';
import { TechnicalTaskState } from '../../states/technical-task.state';

@Component({
  selector: 'app-technical-task-trademarks',
  templateUrl: './technical-task-trademarks.component.html',
  styleUrls: ['./technical-task-trademarks.component.scss'],
})

export class TechnicalTaskTrademarksComponent {
  @Input() trademarks: SearchPortfolioObjectData | null = null;
  @Input() currentPage: number;
  @Input() allTrademarksChecked: boolean;

  @Output() changePage = new EventEmitter<ChangePageTechnicalTask>();

  public showPopupInfoTM: boolean = false;
  public showPopupInfoTMA: boolean = false;
  public popupInfoData: PopupInfoData = { id: null, index: null, isMonitoring: null };

  constructor(private store: Store) { }

  public checkTrademark(event: MouseEvent, obj: SearchPortfolioObjectsResponseResult): void {
    event.stopPropagation();
    this.store.dispatch(new TechnicalTaskActions.CheckTechnicalTaskObject('trademarks', obj));
  }

  public checkAllTrademarks(): void {
    this.store.dispatch(new TechnicalTaskActions.CheckAllTechnicalTaskObject('trademarks'));
  }

  public trimString(str: string, count: number): string {
    return str?.length > count ? str.slice(0, count) + '...' : str;
  }

  public onChangePage(page: number) {
    this.changePage.emit({page, type: 'trademarks'});
  }

  public openPopupInfo(tradeMark: any): void {
    this.popupInfoData = { id: tradeMark._id, index: tradeMark._index, isMonitoring: tradeMark.isMonitoring };
    this.showPopupInfoTM = ['rutm', 'wktm', 'wotm'].includes(tradeMark._index) ? true : false;
    this.showPopupInfoTMA = ['rutm', 'wktm', 'wotm'].includes(tradeMark._index) ? false : true;
  }

  public closePopupInfo(): void {
    this.popupInfoData = { id: null, index: null, isMonitoring: null };
    this.showPopupInfoTM = false;
    this.showPopupInfoTMA = false;
  }

  public getTrademarkChecked(id: string): boolean {
    return this.store.selectSnapshot(TechnicalTaskState.checked).findIndex(item => item.id === id) > -1;
  }
}
