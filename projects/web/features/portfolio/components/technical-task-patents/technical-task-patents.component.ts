import { Component, EventEmitter, Input, Output } from '@angular/core';
import {
  ChangePageTechnicalTask,
  SearchPortfolioObjectData,
  SearchPortfolioObjectsResponseResult,
} from '@web/features/portfolio/models/technical-task.interfaces';
import { Store } from '@ngxs/store';
import { TechnicalTaskActions } from '@web/features/portfolio/states/technical-task.actions';
import { TechnicalTaskState } from '../../states/technical-task.state';


@Component({
  selector: 'app-technical-task-patents',
  templateUrl: './technical-task-patents.component.html',
  styleUrls: ['./technical-task-patents.component.scss'],
})
export class TechnicalTaskPatentsComponent {
  @Input() patents: SearchPortfolioObjectData | null = null;
  @Input() currentPage: number;
  @Input() allPatentsChecked: boolean;

  @Output() changePage = new EventEmitter<ChangePageTechnicalTask>();

  public showPopupInfoPatent: boolean = false;
  public popupInfoData = { id: null, index: null, isMonitoring: null };

  constructor(private store: Store) { }

  public checkPatent(event: MouseEvent, obj: SearchPortfolioObjectsResponseResult): void {
    event.stopPropagation();
    this.store.dispatch(new TechnicalTaskActions.CheckTechnicalTaskObject('patents', obj));
  }

  public checkAllPatents(): void {
    this.store.dispatch(new TechnicalTaskActions.CheckAllTechnicalTaskObject('patents'));
  }

  public trimString(str: string, count: number): string {
    return str?.length > count ? str.slice(0, count) + '...' : str;
  }

  public onChangePage(page: number) {
    this.changePage.emit({page, type: 'patents'});
  }

  public openPopupInfo(patent: any): void {
    this.popupInfoData = { id: patent._id, index: patent._index, isMonitoring: patent.isMonitoring };
    this.showPopupInfoPatent = true;
  }

  public closePopupInfo(): void {
    this.popupInfoData = { id: null, index: null, isMonitoring: null };
    this.showPopupInfoPatent = false;
  }

  public getPatentChecked(id: string): boolean {
    return this.store.selectSnapshot(TechnicalTaskState.checked).findIndex(item => item.id === id) > -1;
  }
}
