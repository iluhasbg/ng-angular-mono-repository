import { Router } from '@angular/router';
import { APP_ROUTES_NAMES } from '@web/core/models/app-routes-names';
import { OrganizationInterface } from '@web/core/models/organization.interface';
import { ADD_BUTTON_LABEL, CompositionWidgetData } from '../../models/portfolio.interfaces';
import {
  Input,
  OnInit,
  Output,
  OnDestroy,
  Component,
  EventEmitter,
  ChangeDetectorRef,
  ChangeDetectionStrategy,
} from '@angular/core';

@Component({
  selector: 'app-portfolio-composition',
  templateUrl: './portfolio-composition.component.html',
  styleUrls: ['./portfolio-composition.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PortfolioCompositionComponent implements OnInit, OnDestroy {
  @Input() isLoading: boolean = true;
  @Input() data: CompositionWidgetData;
  @Input() kontragents: OrganizationInterface[] = [];
  @Input() selectedCompany: OrganizationInterface | null = null;
  @Output() openModal: EventEmitter<void> = new EventEmitter<void>();

  public mobileOpenFirstChart: boolean = window.innerWidth <= 793 ? false : true;
  public addButtonLabel = ADD_BUTTON_LABEL.ADD_OBJECT;

  constructor(
    private readonly router: Router,
    private readonly ref: ChangeDetectorRef,
  ) { }

  public get disabledAddObjects(): boolean {
    return !this.selectedCompany?.id && this.addButtonLabel === ADD_BUTTON_LABEL.ADD_OBJECT;
  }

  public get getMaxComposition(): number {
    return Math.max(this.data.tradeMarks, this.data.patents, this.data.programs, this.data.domains) as number;
  }

  public ngOnInit(): void {
    window.addEventListener('resize', () => this.onResizeWindow());
    this.addButtonLabel = this.kontragents.length > 1
      ? ADD_BUTTON_LABEL.ADD_OBJECT
      : ADD_BUTTON_LABEL.ADD_ORGANIZATION;
  }

  public ngOnDestroy(): void {
    window.removeEventListener('resize', () => this.onResizeWindow());
    window.removeEventListener('resize', () => this.onResizeOpenFirstCard());
  }

  public mobileOpenFirstCard(): void {
    if (window.innerWidth <= 793) {
      this.mobileOpenFirstChart = !this.mobileOpenFirstChart;
      window.addEventListener('resize', () => this.onResizeOpenFirstCard());
    }
  }

  public toggleAddButton(): void {
    this.addButtonLabel === ADD_BUTTON_LABEL.ADD_OBJECT
      ? this.openModal.emit()
      : void this.router.navigate(['/', APP_ROUTES_NAMES.USER, APP_ROUTES_NAMES.ADD_ORGANIZATION]);
  }

  private onResizeWindow(): void {
    this.ref.detectChanges();
    this.mobileOpenFirstChart = window.innerWidth <= 793 ? false : true
  }

  private onResizeOpenFirstCard(): void {
    if (window.innerWidth <= 793) {
      this.mobileOpenFirstChart = !this.mobileOpenFirstChart;
    }
  }
}

