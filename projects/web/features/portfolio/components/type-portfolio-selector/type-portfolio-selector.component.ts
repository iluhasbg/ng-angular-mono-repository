import { TypeOfObjectSelector } from '@web/features/portfolio/models/technical-task.interfaces';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-type-portfolio-selector',
  templateUrl: './type-portfolio-selector.component.html',
  styleUrls: ['./type-portfolio-selector.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TypePortfolioSelectorComponent {
  @Input() selected: TypeOfObjectSelector;
  @Input() options: TypeOfObjectSelector[] = [];
  @Output() selectOption = new EventEmitter<TypeOfObjectSelector>();

  public open: boolean = false;

  public changeDropdown(): void {
    this.open = !this.open;
  }

  public changeOption(option: TypeOfObjectSelector): void {
    this.selected = option;
    this.selectOption.emit(this.selected);
  }
}
