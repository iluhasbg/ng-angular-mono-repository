import { Store } from '@ngxs/store';
import { Router } from '@angular/router';
import { AuthState } from '@web/core/states/auth.state';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { PortfolioActions } from '../../states/portfolio.actions';
import { APP_ROUTES_NAMES } from '@web/core/models/app-routes-names';
import { PortfolioAPIService } from '../../services/portfolio.service';
import { IntercomEventsService, INTERCOM_EVENT_NAME } from 'projects/shared/services/intercom-events.service';

@Component({
  selector: 'app-portfolio-add-object',
  templateUrl: './portfolio-add-object.component.html',
  styleUrls: ['./portfolio-add-object.component.scss'],
})
export class PortfolioAddObjectComponent implements OnInit, OnDestroy {
  public legalName: string = '';
  public searchText: string = '';

  public disableSoft = false;
  public disablePatents = false;
  public disableTradeMarks = false;
  public desktop = window.innerWidth > 768;

  constructor(
    private readonly store: Store,
    private readonly router: Router,
    private readonly portfolioService: PortfolioAPIService,
    private readonly intercomEventsService: IntercomEventsService,
  ) { }

  public ngOnInit(): void {
    this.updateObjectsByRights();
    const bodyStyles = document.body.style;
    bodyStyles.setProperty('overflow', 'hidden');
    bodyStyles.setProperty('height', '100%');
    window.addEventListener('resize', () => this.onResizeWindow());
  }

  public ngOnDestroy(): void {
    this.setBodyStyles();
    window.removeEventListener('resize', () => this.onResizeWindow());
  }

  public closeModal(): void {
    this.store.dispatch(new PortfolioActions.ToggleShowModal())
  }

  public searchObjects(): void {
    this.setBodyStyles();
    void this.router.navigate(
      ['/', APP_ROUTES_NAMES.PORTFOLIO, APP_ROUTES_NAMES.TECHNICAL_TASK],
      { queryParams: { index: 'INN', name: this.searchText } },
    );
  }

  public navigateToTechnicalTask(index: string): void {
    this.setBodyStyles();
    void this.router.navigate(
      ['/', APP_ROUTES_NAMES.PORTFOLIO, APP_ROUTES_NAMES.TECHNICAL_TASK],
      { queryParams: { index } },
    );
  }

  public legalChanged(): void {
    this.legalName = '';
    this.searchLegalName();
    this.intercomEventsService.push({ event: INTERCOM_EVENT_NAME.PRINT_INN_RESULTS });
  }

  public getDynamicMarginForLegalName(): { left: string } {
    switch (this.searchText.trim().length) {
      case 10:
        return {left: '112px'};
      case 12:
        return {left: '130px'};
      case 13:
        return {left: '138px'};
      case 15:
        return {left: '154px'};
    }
  }

  public trimString(str: string, count: number): string {
    return str?.length > count ? str.slice(0, count) + '...' : str;
  }

  private setBodyStyles(): void {
    const bodyStyles = document.body.style;
    bodyStyles.setProperty('overflow', 'visible');
  }

  private onResizeWindow(): void {
    this.desktop = window.innerWidth > 768;
  }

  private searchLegalName(): void {
    if ([10, 12, 13, 15].indexOf(this.searchText.trim().length) > -1) {
      this.portfolioService.loadLegalName({ id: this.searchText }).subscribe(response => {
        this.legalName = response?.hits?.hits[0]?._source?.name
          ? `/ ${response?.hits?.hits[0]?._source?.name}`
          : response?.hits?.hits[0]?._source?.fullName
            ? `/ ${response?.hits?.hits[0]?._source?.fullName}`
            : '';
      });
    }
  }

  private updateObjectsByRights(): void {
    const rights = this.store.selectSnapshot(AuthState.getRights);
    if (rights) {
      rights.rights.forEach(() => {
        this.disableTradeMarks = !this.store.selectSnapshot(AuthState.getRightByName('trademarks_search-viewing'));
        this.disablePatents = !this.store.selectSnapshot(AuthState.getRightByName('patents_search-viewing'));
        this.disableSoft = !this.store.selectSnapshot(AuthState.getRightByName('soft_search-viewing'));
      });
    }
  }
}
