import { Component, ViewChild, ElementRef, AfterViewInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-portfolio-cost-widget',
  templateUrl: './portfolio-cost-widget.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PortfolioCostWidgetComponent implements AfterViewInit {
  @ViewChild('canvasEl') public canvasEl: ElementRef<HTMLCanvasElement>;

  private context: CanvasRenderingContext2D;

  public ngAfterViewInit(): void {
    this.context = this.canvasEl.nativeElement.getContext('2d');
    this.draw();
  }

  private draw(): void {
    this.context.beginPath();
    this.context.moveTo(0, 90);
    this.context.lineTo(0, 120);
    this.context.lineTo(344, 120);
    this.context.lineTo(344, 20);
    this.context.bezierCurveTo(155, 30, 230, 100, 0, 105);
    this.context.fill();

    const gradientColor = this.context.createLinearGradient(0, 0, 344, 0);
    gradientColor.addColorStop(0, '#FF5E03');
    gradientColor.addColorStop(0.5, 'rgba(204, 255, 139, 0.74)');
    this.context.fillStyle = gradientColor;
    this.context.fill();


    this.context.beginPath();
    this.context.moveTo(0, 90);
    this.context.lineTo(0, 120);
    this.context.lineTo(344, 120);
    this.context.lineTo(344, 20);
    this.context.bezierCurveTo(155, 30, 230, 100, 0, 105);
    this.context.fill();

    const gradientGray = this.context.createLinearGradient(0, 0, 0, 120);
    gradientGray.addColorStop(0, 'rgba(71, 82, 99, 0)');
    gradientGray.addColorStop(1, '#475263');
    this.context.fillStyle = gradientGray;
    this.context.fill();


    const gradientColorLine = this.context.createLinearGradient(0, 50, 344, 0);
    gradientColorLine.addColorStop(0, '#FF5E03');
    gradientColorLine.addColorStop(0.5, 'rgba(204, 255, 139, 1)');

    this.context.shadowColor = 'rgba(204, 255, 0, 0.21)';
    this.context.shadowBlur = 3;
    this.context.shadowOffsetX = 0;
    this.context.shadowOffsetY = 0;
    this.context.lineWidth = 2;
    this.context.strokeStyle = gradientColorLine;
    this.context.beginPath();
    this.context.moveTo(0, 105);
    this.context.bezierCurveTo(235, 100, 150, 30, 344, 20);
    this.context.stroke();
    this.context.strokeStyle = 'linear-gradient(90deg, #FF9960 0%, #CAFBBA 100%)';
  }
}
