import { Pipe, PipeTransform } from "@angular/core";
import { PortfolioCostWidgetData } from "../../models/portfolio.interfaces";

type Format = 'before' | 'after';

@Pipe({ name: 'formatCostValue' })
export class FormatCostValuePipe implements PipeTransform {
  public transform(value: PortfolioCostWidgetData, type: Format) {
    return this.formatCost(value ? value[type] : 0);
  }

  private formatCost(cost: number): { cost: number, suffix: string } {
    let c = {cost, suffix: 'тыс'};
    if (cost > 999999) {
      c = {cost: Math.floor(cost / 1000000), suffix: 'млрд'};
    } else if (cost > 999) {
      c = {cost: Math.floor(cost / 1000), suffix: 'млн'};
    }

    return c;
  }
}