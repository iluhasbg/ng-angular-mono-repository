import { Router } from '@angular/router';
import { APP_ROUTES_NAMES } from '@web/core/models/app-routes-names';
import { PortfolioCostWidgetData } from '../../models/portfolio.interfaces';
import { OrganizationInterface } from '@web/core/models/organization.interface';
import { environment } from '../../../../../app-ipid/src/environments/environment';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnDestroy, OnInit } from '@angular/core';

@Component({
  selector: 'app-portfolio-value',
  templateUrl: './portfolio-value.component.html',
  styleUrls: ['./portfolio-value.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PortfolioValueComponent implements OnInit, OnDestroy {
  @Input() data: PortfolioCostWidgetData;
  @Input() isLoading: boolean = true;
  @Input() selectedCompany: OrganizationInterface | null = null;

  public brandName = environment.BRAND;
  public mobileOpenThirdChart: boolean = window.innerWidth <= 793 ? false : true;

  constructor(
    private readonly router: Router,
    private readonly ref: ChangeDetectorRef,
  ) { }

  public ngOnInit(): void {
    window.addEventListener('resize', () => this.onResizeWindow());
  }

  public ngOnDestroy(): void {
    window.removeEventListener('resize', () => this.onResizeWindow());
    window.removeEventListener('resize', () => this.onResizeOpenThirdCard());
  }

  public mobileOpenThirdCard(): void {
    if (window.innerWidth <= 793) {
      this.mobileOpenThirdChart = !this.mobileOpenThirdChart;
      window.addEventListener('resize', () => this.onResizeOpenThirdCard());
    }
  }

  public toLeadForm(identity: string): void {
    const data = {
      formIdentity: identity,
      selectedCompanyId: this.selectedCompany?.id ? this.selectedCompany.id : null,
    };
    this.router.navigate(['/', APP_ROUTES_NAMES.TREATY_EXTENSION], { queryParams: { data: JSON.stringify(data) } });
  }

  private onResizeWindow(): void {
    this.ref.detectChanges();
    this.mobileOpenThirdChart = window.innerWidth <= 793 ? false : true;
  }

  private onResizeOpenThirdCard(): void {
    if (window.innerWidth <= 793) {
      this.mobileOpenThirdChart = !this.mobileOpenThirdChart;
    }
  }
}
