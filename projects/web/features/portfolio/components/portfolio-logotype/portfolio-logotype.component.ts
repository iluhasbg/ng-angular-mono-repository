import { Component, EventEmitter, HostListener, Input, OnInit, Output } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { SearchService } from '../../../../../app-search/src/app/services/search.service';
import { Subject } from 'rxjs';
import { PortfolioService } from '../../../../../app-ipid/src/app/shared/services/portfolio.service';
import { HttpService } from '../../../../../app-ipid/src/app/shared/services/http.service';
import risksData from '../../../../../app-ipid/src/app/pages/monitoring/json-data-risks/json-data-risks';
import { Router } from '@angular/router';
import { CorpSearchService } from '../../../corp/corp-search/services/corp-search.service';
import allowedTypes from '../../../../../shared/types/allowed-types';
import * as _ from 'lodash';
import { SupportChatService } from '../../../../../shared/services/support-chat.service';
import {DownloadFilesService} from '../../../../../shared/services/download-files.service';

@Component({
  selector: 'app-portfolio-logotype',
  templateUrl: './portfolio-logotype.component.html',
  styleUrls: ['./portfolio-logotype.component.scss'],
})
export class PortfolioLogotypeComponent implements OnInit {

  risksData = risksData;

  @Input() data;
  @Input() showRemoveBtn = false;
  sourceData: any = {};
  imageUrl: string;
  viewClassesCount = 'short';
  private readonly onDestroy = new Subject<void>();

  monitoringEvents = [];

  monitoringRisks = 0;

  files = [];
  fileError = '';
  maxRiskLevel = 1;
  noMonitoringRisks = [];

  constructor(
    private searchService: SearchService,
    private portfolioService: PortfolioService,
    private httpService: HttpService,
    private router: Router,
    private corp: CorpSearchService,
    private supportChatService: SupportChatService,
    readonly downloadFiles: DownloadFilesService
  ) {
  }
  @Output() modalEvent = new EventEmitter();
  @Output() removeObject = new EventEmitter();

  ngOnInit(): void {
    this.getDocById();
    this.getFiles();

    if (this.data.isMonitoring) {
      this.getMonitoringRisk();
    } else {
      this.getRisks();
    }
  }

  getRiskByLevel(level: number): string {
    switch (level) {
      case 1:
        return 'Нет риска';
      case 2:
        return 'Средний риск';
      case 3:
        return 'Высокий риск';
      case 4:
        return 'Критичный риск';
    }
  }

  getFiles(): void {
    const query = {
      objectID: this.data.id,
      index: this.data.index
    };
    this.portfolioService.getFilesByObject(query).subscribe((data: any) => {
      this.files = data.files;
    });
  }

  getDocById(): void {
    const query = {
      _id: this.data.id,
      _index: this.data.index
    };
    this.searchService.getDocumentById(query).pipe(takeUntil(this.onDestroy)).subscribe((data: any) => {
      if (data?.hits?.hits?.length) {
        this.sourceData = data.hits.hits[0];
        this.sourceData.indexType = this.searchService.getIndexType(this.sourceData);
        const id = this.sourceData._source.applicationString;
        this.imageUrl = this.searchService.getImgUrl(
          this.sourceData._index,
          parseInt(id, 10),
          this.sourceData._source.markImageFileName,
          id);
      }
    });
  }

  getRisks(): void {
    const query = {
      object_id: this.data.id,
      object_index: this.data.index
    };
    this.portfolioService.getRisks(query).pipe(takeUntil(this.onDestroy)).subscribe((data: any) => {
      if (data && data.length) {
        data.forEach(item => {
          if (item.risk !== 0) {
            this.monitoringRisks += item.values;
          }
        });
        this.noMonitoringRisks = data.filter(el => el.risk !== 0).sort((a, b) => b.risk - a.risk);
        this.maxRiskLevel = this.noMonitoringRisks[0].risk;
      }
    });
  }


  getMonitoringRisk() {
    this.portfolioService.getTopRisksByObject({id: this.data.id,
      index: this.data.index}).subscribe((data) => {
      this.monitoringRisks = data.countRisks;
      this.monitoringEvents = data.risks.sort((a, b) => b.level - a.level);
      this.monitoringEvents.forEach(item => {
        item.riskData = this.risksData.find(el => el.type === +item.type);
      });
      if (this.monitoringEvents[0]?.level) {
        this.maxRiskLevel = this.monitoringEvents[0].level;
      }
    });
  }

  getMktuClasses() {
    if (this.viewClassesCount === 'long') {
      return this.sourceData._source.goodsServices;
    } else {
      return this.sourceData._source.goodsServices.slice(0, 3);
    }
  }

  closeModal(): void {
    this.modalEvent.emit();
  }

  @HostListener('document:keyup', ['$event']) // 27=esc
  keyup(event: KeyboardEvent): void {
    if (event.keyCode === 27) {
      this.closeModal();
    }
  }

  getStatusTrademark(sourceData): string {
    let status = '';
    switch (sourceData._source.statusFIPS) {
      case 10:
        status = 'Пошлина за регистрацию не оплачена';
        break;
      case 14:
        status = 'Формальная экспертиза';
        break;
      case 15:
        status = 'Экспертиза заявленного обозначения';
        break;
      case 16:
        status = 'Заявка отозвана';
        break;
      case 17:
        status = 'Принято решение о регистрации товарного знака';
        break;
      case 18:
        status = 'Принято решение об отказе в регистрации товарного знака';
        break;
      case 19:
        status = 'Регистрация товарного знака';
        break;
      case 20:
        status = 'Принято решение о признании заявки отозванной';
        break;
      case 21:
        status = 'Заявка отозвана по просьбе заявителя';
        break;
    }
    return status;
  }

  showAllClasses(): void {
    this.viewClassesCount = 'long';
  }

  getChangeCode(change): string {
    let changeCode = '';
    if (change.changeType === 'Исходящая корреспонденция') {
      switch (change.changeCode) {
        case '730':
          changeCode = 'Решение о принятии заявки к рассмотрению';
          break;
        case '950':
          changeCode = 'Запрос формальной экспертизы';
          break;
        case '920':
          changeCode = 'Решение об отказе в принятии заявки к рассмотрению';
          break;
        case '200':
          changeCode = 'Уведомление о необходимости соглашения по заявкам на тождественные знаки';
          break;
        case '700':
          changeCode = 'Уведомление о результатах проверки соответствия';
          break;
        case '002':
          changeCode = 'Уведомление о представлении перевода документов';
          break;
        case '003':
          changeCode = 'Письмо по вопросам делопроизводства';
          break;
        case '100':
          changeCode = 'Запрос экспертизы заявленного обозначения';
          break;
        case '010':
          changeCode = 'Решение о регистрации';
          break;
        case '020':
          changeCode = 'Решение об отказе в регистрации';
          break;
        case '740':
          changeCode = 'Уведомление о невозможности выдачи свидетельства';
          break;
        case '211':
          changeCode = 'Уведомление об удовлетворении ходатайства';
          break;
        case '212':
          changeCode = 'Уведомление об отказе в удовлетворении ходатайства';
          break;
        case '221':
          changeCode = 'Уведомление об удовлетворении ходатайства';
          break;
        case '222':
          changeCode = 'Уведомление об отказе в удовлетворении ходатайства';
          break;
        case '231':
          changeCode = 'Уведомление об удовлетворении ходатайства';
          break;
        case '232':
          changeCode = 'Уведомление об отказе в удовлетворении ходатайства';
          break;
        case '013':
          changeCode = 'Решение об изменении наименования заявителя';
          break;
        case '051':
        case '052':
        case '053':
          changeCode = 'Решение о признании заявки отозванной';
          break;
        case '710':
          changeCode = 'Уведомление о принятии к сведению заявления об отзыве';
          break;
        case '006':
          changeCode = 'Уведомление об удовлетворении просьбы ';
          break;
        case '201':
          changeCode = 'Уведомление об удовлетворении ходатайства';
          break;
        case '202':
          changeCode = 'Уведомление об отказе в удовлетворении ходатайства ';
          break;
        case '281':
          changeCode = 'Уведомление об удовлетворении ходатайства ';
          break;
        case '282':
          changeCode = 'Уведомление об отказе в удовлетворении ходатайства';
          break;
        case '283':
          changeCode = 'Уведомление о представлении документов';
          break;
        case '284':
          changeCode = 'Уведомление о признании ходатайства неподанным';
          break;
        case '009':
          changeCode = 'Письмо о высылке копий';
          break;
        case '190':
          changeCode = 'Письмо об отказе в высылке копий';
          break;
        case '180':
          changeCode = 'Уведомление о представлении документов';
          break;
        case '721':
          changeCode = 'Письмо об учете пошлины';
          break;
        case '722':
          changeCode = 'Письмо о необходимости доплаты пошлины';
          break;
        case '723':
          changeCode = 'Письмо о том, что пошлина не учтена';
          break;
        case '008':
          changeCode = 'Письмо по вопросам делопроизводства';
          break;
        case '290':
          changeCode = 'Письмо о пошлине';
          break;
        case '401':
          changeCode = 'Уведомление об удовлетворении просьбы';
          break;
        case '402':
          changeCode = 'Уведомление об отказе в удовлетворении просьбы';
          break;
        case '403':
          changeCode = 'Уведомление о представлении документов ';
          break;
        case '005':
          changeCode = 'Письмо об уточнении адреса';
          break;
        case '404':
          changeCode = 'Письмо по вопросам делопроизводства';
          break;
        case '001':
          changeCode = 'Письмо произвольной формы';
          break;
        case '280':
          changeCode = 'Напоминание о необходимости уплаты пошлин';
          break;
        case '940':
          changeCode = 'Уведомление о поступлении документов заявки';
          break;
        case '731':
          changeCode = 'Решение о принятии заявки к рассмотрению';
          break;
        case '102':
          changeCode = 'Запрос пошлины за ЭЗО';
          break;
        case '103':
          changeCode = 'Запрос об уточнении перечня товаров и /или услуг';
          break;
        case '004':
          changeCode = 'Письмо о соответствии перечня товаров и/или услуг требованиям Кодекса';
          break;
        case '951':
        case '724':
          changeCode = 'Уведомление о результатах проверки пошлины';
          break;
        case '407':
          changeCode = 'Уведомление об отказе в совершении юр. действия';
          break;
        case '054':
          changeCode = 'Решение о признании заявки отозванной';
          break;
        case '081':
        case '082':
          changeCode = 'Решение об отмене ранее принятого решения';
          break;
      }
    } else {
      switch (change.changeCode) {
        case 'ПЕР':
          changeCode = 'Перевод документов';
          break;
        case 'ДОВ':
          changeCode = 'Доверенность';
          break;
        case 'КОН':
          changeCode = 'Просьба об установлении конвенционного приоритета';
          break;
        case 'КПЗ':
          changeCode = 'Копия первой заявки';
          break;
        case 'ВЫС':
          changeCode = 'Просьба об установлении выставочного приоритета';
          break;
        case 'ДПП':
          changeCode = 'Подтверждение правомерности испрашивания приоритета';
          break;
        case 'ОТД':
          changeCode = 'Досрочное прекращение доверенности';
          break;
        case 'ДПМ':
          changeCode = 'Дополнительные материалы';
          break;
        case 'ДОМ':
          changeCode = 'Дополнительные материалы';
          break;
        case 'ТЗК':
          changeCode = 'Ходатайство о преобразовании заявки';
          break;
        case 'КТЗ':
          changeCode = 'Ходатайство о преобразовании заявки';
          break;
        case 'ТТЗ':
          changeCode = 'Сообщение о соглашении по заявкам на тождественные знаки';
          break;
        case 'ХВЗ':
          changeCode = 'Просьба об установлении приоритета выделенной заявки';
          break;
        case 'ПСР':
          changeCode = 'Ходатайство о продлении установленного срока ';
          break;
        case 'ВСР':
          changeCode = 'Ходатайство о восстановлении пропущенного срока ';
          break;
        case 'ИЗП':
          changeCode = 'Заявление о внесении изменений в документы заявки';
          break;
        case 'ИЗЗ':
          changeCode = 'Заявление об изменении наименования заявителя';
          break;
        case 'ИПО':
          changeCode = 'Заявление об изменении сведений о заявителе в случае передачи или перехода права';
          break;
        case 'ИЗМ':
          changeCode = 'Заявление о внесении исправлений в документы заявки';
          break;
        case 'АДР':
          changeCode = 'Ходатайство о внесении изменений в адрес';
          break;
        case 'ДТП':
          changeCode = 'Заявление о внесении исправлений';
          break;
        case 'ОТЗ':
          changeCode = 'Просьба заявителя об отзыве заявки';
          break;
        case 'ООТ':
          changeCode = 'Отзыв корреспонденции';
          break;
        case 'ПВК':
          changeCode = 'Просьба выслать копии противопоставленных материалов';
          break;
        case 'ТРФ':
          changeCode = 'Просьба об оказании услуги по тарифу';
          break;
        case 'ППД':
        case 'ПВД':
        case 'ПДК':
        case 'ПОШ':
        case 'ППЭ':
          changeCode = 'Платежный документ';
          break;
        case 'ЗАП':
          changeCode = 'Ходатайство о зачете пошлины';
          break;
        case 'ВЗП':
          changeCode = 'Ходатайство о возврате пошлины';
          break;
        case 'ДОТ':
          changeCode = 'Письмо для ответа';
          break;
        case 'СВЕ':
          changeCode = 'Письмо, не требующее ответа';
          break;
        case 'КОП':
          changeCode = 'Письмо для ответа на контроле дирекции';
          break;
        case 'ОБР':
          changeCode = 'Обращение';
          break;
        case 'ФКС':
          changeCode = 'Корреспонденция, поступившая по факсу';
          break;
        case 'ВИК':
          changeCode = 'Возврат почтового отправления';
          break;
        case 'ДТУ':
          changeCode = 'Дополнительные материалы';
          break;
        case 'ЛИК':
          changeCode = 'Ходатайство о ведении переписки через личный кабинет';
          break;
        case 'ЛКФ':
          changeCode = 'Корреспонденция, поступившая через личный кабинет и требующаяпредставления оригинала документа';
          break;
        default:
          changeCode = 'Заявка';
      }
    }
    return changeCode;
  }

  confirming(name: string) {
    if (confirm('Вы уверены что хотите удалить Заявку на товарный знак №' + name + ' из портфеля?')) {
      console.log('agreed');
    }
  }

  deleteObject(): void {
    this.removeObject.emit({id: this.data.id, index: this.data.index});
  }

  /** Событие загрузки файла
   */
  fileChangeEvent(fileInput: any) {
    if (this.fileError?.length) {
      this.fileError = '';
    }
    if (fileInput.target.files && fileInput.target.files[0]) {
      const file = fileInput.target.files[0];
      if (file) {
        const maxSize = 2000000;
        if (fileInput.target.files[0].size > maxSize) {
          this.fileError = 'Максимальный размер файла ' + maxSize / 1000000 + 'Mb';
          return false;
        }
        if (!_.includes(allowedTypes, fileInput.target.files[0].type)) {
          this.fileError = 'Недопустимый тип файла';
          return false;
        }
        if (!this.fileError?.length) {
          const formData: FormData = new FormData();
          formData.append('file', file, file.name);
          this.httpService.post({
            path: 'objects/file/add',
            params: {
              objectID: this.data.id,
              index: this.data.index
            },
            body: formData,
            isFile: true
          })
            .subscribe((data: any) => {
              this.files.push({
                name: fileInput.target.files[0].name,
                publicPath: data.path,
                size: fileInput.target.files[0].size
              });
              fileInput.target.value = '';
            });
        }
      }
    }
  }

  getExternalLink(index) {
    return this.corp.getExternalUrl(index);
  }

  removeFile(path: string): void {
    this.httpService.post({
      path: 'objects/file/delete',
      params: {
        objectID: this.data.id,
        index: this.data.index
      },
      body: {path}
    }).subscribe(() => {
      this.getFiles();
    });
  }

  getRiskTitleByLevel(level: number): string {
    switch (level) {
      case 1:
        return 'Рекоммендаций';
      case 2:
        return 'Средних рисков';
      case 3:
        return 'Высоких рисков';
      case 4:
        return 'Критичных рисков';
    }
  }

  public toLeadForm(id): void {
    const data = {
      formIdentity: 'Оспорить риск',
      objectId: this.data.id,
      objectIndex: this.data.index,
      riskId: id
    };
    this.router.navigate(['/treaty-extension'], {queryParams: {data: JSON.stringify(data)}});
  }

  onClickBtn(btnTitle: string) {
    this.supportChatService.open(btnTitle);
  }
}
