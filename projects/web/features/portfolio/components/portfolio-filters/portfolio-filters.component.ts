import { Filters } from "@ui/filters/models/filters.interface";
import { ChangeDetectionStrategy, Component, Input, Output, EventEmitter, OnChanges } from "@angular/core";
import {
  Owner,
  PortfolioTabType,
  PortfolioFiltersData,
  PortfolioFilterState,
  PortfolioOtherTabType,
} from "../../models/portfolio.interfaces";
import {
  PATENT_FILTERS,
  DOMAIN_FILTERS,
  PROGRAM_FILTERS,
  CONTRACT_FILTERS,
  TRADEMARK_FILTERS,
} from "./portfolio-filters.default";
import { Observable, ReplaySubject } from "rxjs";

@Component({
  selector: 'app-portfolio-filters',
  templateUrl: './portfolio-filters.component.html',
  styleUrls: ['./portfolio-filters.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PortfolioFiltersComponent implements OnChanges {
  @Input() clear$: Observable<void>;
  @Input() filters: PortfolioFiltersData;
  @Input() filtersValue: PortfolioFiltersData;
  @Input() domainAdministrator: Owner[] = [];
  @Input() currentTable: PortfolioTabType | PortfolioOtherTabType;
  @Output() applyFilters = new EventEmitter<PortfolioFilterState>();

  public tradeMarks: Filters<unknown[]> = TRADEMARK_FILTERS;
  public patents: Filters<unknown[]> = PATENT_FILTERS;
  public programs: Filters<unknown[]> = PROGRAM_FILTERS;
  public contracts: Filters<unknown[]> = CONTRACT_FILTERS;
  public domains: Filters<unknown[]> = DOMAIN_FILTERS;

  private _clearFilter = new ReplaySubject<void>();
  clearFilter$ = this._clearFilter.asObservable();

  public ngOnChanges(): void {
    for (let input of  this.tradeMarks) {
      if (input.name === 'owner') {
        input.options = this.filters[PortfolioTabType.TRADEMARKS].owners.map((item, index) => ({ id: index, label: item.name }));
      }
    }
    for (let input of  this.patents) {
      if (input.name === 'owner') {
        input.options = this.filters[PortfolioTabType.PATENTS].owners.map((item, index) => ({ id: index, label: item.name }));
      }
    }
    for (let input of  this.programs) {
      if (input.name === 'owner') {
        input.options = this.filters[PortfolioTabType.PROGRAMS].owners.map((item, index) => ({ id: index, label: item.name }));
      }
    }
    for (let input of  this.contracts) {
      if (input.name === 'owner') {
        input.options = this.filters[PortfolioTabType.CONTRACTS].owners.map((item, index) => ({ id: index, label: item.name }));
      }
    }
    for (let input of  this.domains) {
      if (input.name === 'owner') {
        input.options = this.filters[PortfolioOtherTabType.DOMAINS].owners.map((item, index) => ({ id: index, label: item.name }));
      }
      if (input.name === 'domainAdministrator') {
        input.options = this.domainAdministrator.map((item, index) => ({ id: index, label: item.name }));
      }
    }
  }

  public applyFilter(event: PortfolioFilterState): void {
    this.applyFilters.emit(event);
  }

  public clearFilter(): void {
    this._clearFilter.next();
  }
}
