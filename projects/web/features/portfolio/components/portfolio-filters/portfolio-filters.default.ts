import { Filters } from "@ui/filters/models/filters.interface";

const DEFAULT_TRADEMARKS_STATUSES = [
  {id: 0, label: 'Нет статуса'},
  {id: 1, label: 'Действует'},
  {id: 2, label: 'Срок действия истек'},
  {id: 3, label: 'Прекратил действие'},
  {id: 10, label: 'Пошлина за регистрацию не оплачена'},
  {id: 14, label: 'Формальная экспертиза'},
  {id: 15, label: 'Экспертиза заявленного обозначения'},
  {id: 16, label: 'Заявка отозвана'},
  {id: 17, label: 'Принято решение о регистрации товарного знака'},
  {id: 18, label: 'Принято решение об отказе в регистрации товарного знака'},
  {id: 19, label: 'Регистрация товарного знака'},
  {id: 20, label: 'Принято решение о признании заявки отозванной'},
  {id: 21, label: 'Заявка отозвана по просьбе заявителя'},
];

const DEFAULT_TRADEMARKS_TYPES = [
  { id: 1, label: 'Товарный знак' },
  { id: 2, label: 'Международный товарный знак' },
  { id: 3, label: 'Заявка на товарный знак' },
  { id: 19, label: 'НМПТ' },
  { id: 20, label: 'Общеизвестный товарный знак' },
  { id: 21, label: 'заявка на НМПТ' },
];

const DEFAULT_AREAS = [
  { id: 4, label: 'Россия' },
  { id: 5, label: 'Международная' },
];

const DEFAULT_PATENT_TYPES = [
  { id: 2, label: 'Патент на изобретение' },
  { id: 3, label: 'Патент на полезную модель' },
  { id: 4, label: 'Патент на промышленный образец' },
];

const DEFAULT_PATENT_STATUSES = [
  { id: 0, label: '' },
  { id: 1, label: 'Действует' },
  { id: 2, label: 'Mожет прекратить свое действие' },
  { id: 3, label: 'Прекратил действие, но может быть восстановлен' },
  { id: 4, label: 'Не действует' },
];

const DEFAULT_PROGRAM_TYPES = [
  { id: 0, label: 'Программа для ЭВМ' },
  { id: 1, label: 'База данных' },
];

const DEFAULT_CONTRACT_TYPES = [
  { id: 1, label: 'Договор лицензии' },
  { id: 2, label: 'Договор сублицензии' },
  { id: 3, label: 'Договор концессии' },
  { id: 4, label: 'Договор субконцессии' },
];

const DEFAULT_DOMAIN_STATUSES = [
  { id: 1, label: 'занят' },
  { id: 2, label: 'свободен' },
];

export const TRADEMARK_FILTERS: Filters<unknown[]> = [
  {
    type: 'input',
    name: 'searchQuery',
    placeholder: 'Поиск по объектам',
  },
  {
    type: 'autocomplete',
    name: 'owner',
    placeholder: 'Правообладатель',
    options: [],
  },
  {
    type: 'select',
    name: 'types',
    placeholder: 'Статус и вид',
    options: DEFAULT_TRADEMARKS_TYPES,
  },
  {
    type: 'select',
    name: 'index',
    placeholder: 'Территория действия',
    options: DEFAULT_AREAS,
  },
  {
    type: 'select',
    name: 'status',
    placeholder: 'Статус',
    options: DEFAULT_TRADEMARKS_STATUSES,
  },
];

export const PATENT_FILTERS: Filters<unknown[]> = [
  {
    type: 'input',
    name: 'searchQuery',
    placeholder: 'Поиск по объектам',
  },
  {
    type: 'autocomplete',
    name: 'owner',
    placeholder: 'Правообладатель',
    options: [],
  },
  {
    type: 'select',
    name: 'types',
    placeholder: 'Тип патента',
    options: DEFAULT_PATENT_TYPES,
  },
  {
    type: 'select',
    name: 'status',
    placeholder: 'Статус',
    options: DEFAULT_PATENT_STATUSES,
  },
]

export const PROGRAM_FILTERS: Filters<unknown[]> = [
  {
    type: 'input',
    name: 'searchQuery',
    placeholder: 'Поиск по объектам',
  },
  {
    type: 'autocomplete',
    name: 'owner',
    placeholder: 'Правообладатель',
    options: [],
  },
  {
    type: 'select',
    name: 'types',
    placeholder: 'Тип объекта',
    options: DEFAULT_PROGRAM_TYPES,
  },
];

export const CONTRACT_FILTERS: Filters<unknown[]> = [
  {
    type: 'input',
    name: 'searchQuery',
    placeholder: 'Поиск по объектам',
  },
  {
    type: 'autocomplete',
    name: 'owner',
    placeholder: 'Стороны договора',
    options: [],
  },
  {
    type: 'select',
    name: 'types',
    placeholder: 'Тип договора',
    options: DEFAULT_CONTRACT_TYPES,
  },
];

export const DOMAIN_FILTERS: Filters<unknown[]> = [
  {
    type: 'input',
    name: 'searchQuery',
    placeholder: 'Поиск по объектам',
  },
  {
    type: 'autocomplete',
    name: 'owner',
    placeholder: 'Правообладатель',
    options: [],
  },
  {
    type: 'select',
    name: 'domainAdministrator',
    placeholder: 'Регистратор',
    options: [],
  },
  {
    type: 'select',
    name: 'status',
    placeholder: 'Статус',
    options: DEFAULT_DOMAIN_STATUSES,
  },
];
