import { Component, EventEmitter, HostListener, OnInit, Output, Input } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { SearchService } from '../../../../../app-search/src/app/services/search.service';
import { Subject } from 'rxjs';
import { PortfolioService } from '../../../../../app-ipid/src/app/shared/services/portfolio.service';
import { HttpService } from '../../../../../app-ipid/src/app/shared/services/http.service';
import risksData from '../../../../../app-ipid/src/app/pages/monitoring/json-data-risks/json-data-risks';
import { Router } from '@angular/router';
import { CorpSearchService } from '../../../corp/corp-search/services/corp-search.service';
import allowedTypes from '../../../../../shared/types/allowed-types';
import * as _ from 'lodash';
import { SupportChatService } from '../../../../../shared/services/support-chat.service';
import {DownloadFilesService} from '../../../../../shared/services/download-files.service';
import { IntercomEventsService, INTERCOM_EVENT_NAME } from 'projects/shared/services/intercom-events.service';

@Component({
  selector: 'app-portfolio-patent',
  templateUrl: './portfolio-patent.component.html',
  styleUrls: ['./portfolio-patent.component.scss']
})
export class PortfolioPatentComponent implements OnInit {

  risksData = risksData;

  events = false;
  @Input() data;
  @Input() showRemoveBtn = false;

  allImages = false;

  monitoringEvents = [];
  maxRiskLevel = 1;
  private readonly onDestroy = new Subject<void>();

  sourceData: any = {};
  monitoringRisks = 0;
  files = [];
  fileError = '';
  noMonitoringRisks = [];

  constructor(
    private searchService: SearchService,
    private portfolioService: PortfolioService,
    private httpService: HttpService,
    private router: Router,
    private corp: CorpSearchService,
    private supportChatService: SupportChatService,
    readonly downloadFiles: DownloadFilesService,
    private readonly intercomEventsService: IntercomEventsService,
  ) {
  }

  @Output() modalEvent = new EventEmitter();
  @Output() removeObject = new EventEmitter();

  ngOnInit(): void {
    this.getDocById();
    this.getFiles();
    console.log(this.data);

    if (this.data.isMonitoring) {
      this.getMonitoringRisk();
    } else {
      this.getRisks();
    }
    this.intercomEventsService.push({ event: INTERCOM_EVENT_NAME.OPEN_ITEM, item_type: 'Патент' });
  }

  getFiles(): void {
    const query = {
      objectID: this.data.id,
      index: this.data.index
    };
    this.portfolioService.getFilesByObject(query).subscribe((data: any) => {
      this.files = data.files;
    });
  }

  getDocById(): void {
    const query = {
      _id: this.data.id,
      _index: this.data.index
    };
    // this.isLoading = true;
    this.searchService.getDocumentById(query).pipe(takeUntil(this.onDestroy)).subscribe((data: any) => {
      if (data?.hits?.hits?.length) {
        this.sourceData = data.hits.hits[0];
        if (this.sourceData._source.drawings) {
          const imagesUrl = [];
          this.sourceData._source.drawings.forEach((item: string) => {
            if (!item.includes('tif')) {
              imagesUrl.push(this.getImageUrlById(this.data.index, this.data.id, item));
            }
          });
          this.sourceData._source.drawings = imagesUrl;
        }
      }
    });
  }

  getImageUrlById(index: string, id, fileName: string): string {
    let returnUtl = 'https://dpp-fips.9958258.ru/dpp/' + index.toUpperCase();
    returnUtl = returnUtl + '/' + this.generatePath(id) + '/' + id + '/' + fileName;
    return returnUtl;
  }

  generatePath(num): string {
    if (num < 1000) {
      return `0`;
    }
    const paths: string[] = [];
    const n = num.toString();
    for (let i = 1; n.length - i >= 3; i++) {
      paths.push(n.slice(0, i) + '0'.repeat(n.length - i));
    }
    return paths.join('/');
  }

  getRisks(): void {
    const query = {
      object_id: this.data.id,
      object_index: this.data.index
    };
    this.portfolioService.getRisks(query).pipe(takeUntil(this.onDestroy)).subscribe((data: any) => {
      if (data && data.length) {
        data.forEach(item => {
          if (item.risk !== 0) {
            this.monitoringRisks += item.values;
          }
        });
        this.noMonitoringRisks = data.filter(el => el.risk !== 0).sort((a, b) => b.risk - a.risk);
        this.maxRiskLevel = this.noMonitoringRisks[0].risk;
      }
    });
  }

  closeModal(): void {
    this.modalEvent.emit();
  }

  confirming(name: string) {
    if (confirm('Вы уверены что хотите удалить Патент №' + name + ' из портфеля?')) {
      console.log('agreed');
    }
  }

  @HostListener('document:keyup', ['$event']) // 27=esc
  keyup(event: KeyboardEvent): void {
    if (event.keyCode === 27) {
      this.closeModal();
    }
  }

  getMonitoringRisk() {
    this.portfolioService.getTopRisksByObject({
      id: this.data.id,
      index: this.data.index
    }).subscribe((data) => {
      this.monitoringRisks = data.countRisks;
      this.monitoringEvents = data.risks.sort((a, b) => b.level - a.level);
      this.monitoringEvents.forEach(item => {
        item.riskData = this.risksData.find(el => el.type === +item.type);
      });
      if (this.monitoringEvents[0]?.level) {
        this.maxRiskLevel = this.monitoringEvents[0].level;
      }
    });
  }

  getRiskByLevel(level: number): string {
    switch (level) {
      case 1:
        return 'Нет риска';
      case 2:
        return 'Средний риск';
      case 3:
        return 'Высокий риск';
      case 4:
        return 'Критичный риск';
    }
  }

  getRiskTitleByLevel(level: number): string {
    switch (level) {
      case 1:
        return 'Рекоммендаций';
      case 2:
        return 'Средних рисков';
      case 3:
        return 'Высоких рисков';
      case 4:
        return 'Критичных рисков';
    }
  }

  deleteObject(): void {
    this.removeObject.emit({id: this.data.id, index: this.data.index});
  }

  /** Событие загрузки файла
   */
  fileChangeEvent(fileInput: any) {
    if (this.fileError?.length) {
      this.fileError = '';
    }
    if (fileInput.target.files && fileInput.target.files[0]) {
      const file = fileInput.target.files[0];
      if (file) {
        const maxSize = 2000000;
        if (fileInput.target.files[0].size > maxSize) {
          this.fileError = 'Максимальный размер файла ' + maxSize / 1000000 + 'Mb';
          return false;
        }
        if (!_.includes(allowedTypes, fileInput.target.files[0].type)) {
          this.fileError = 'Недопустимый тип файла';
          return false;
        }
        if (!this.fileError?.length) {
          const formData: FormData = new FormData();
          formData.append('file', file, file.name);
          this.httpService.post({
            path: 'objects/file/add',
            params: {
              objectID: this.data.id,
              index: this.data.index
            },
            body: formData,
            isFile: true
          })
            .subscribe((data: any) => {
              this.files.push({
                name: fileInput.target.files[0].name,
                publicPath: data.path,
                size: fileInput.target.files[0].size
              });
              fileInput.target.value = '';
            });
        }
      }
    }
  }

  removeFile(path: string): void {
    this.httpService.post({
      path: 'objects/file/delete',
      params: {
        objectID: this.data.id,
        index: this.data.index
      },
      body: {path}
    }).subscribe(data => {
      this.getFiles();
    });
  }

  getExternalLink(index) {
    return this.corp.getExternalUrl(index);
  }

  public toLeadForm(id): void {
    const data = {
      formIdentity: 'Оспорить риск',
      objectId: this.data.id,
      objectIndex: this.data.index,
      riskId: id
    };
    this.router.navigate(['/treaty-extension'], {queryParams: {data: JSON.stringify(data)}});
  }

  onClickBtn(btnTitle: string) {
    this.supportChatService.open(btnTitle);
  }
}
