import { OrganizationInterface } from '@web/core/models/organization.interface';
import { environment } from '../../../../../app-ipid/src/environments/environment';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-selection-organizations',
  templateUrl: './selection-organizations.component.html',
  styleUrls: ['./selection-organizations.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class SelectionOrganizationsComponent {
  @Input() styleLink: string = '';
  @Input() color: string = '#000000';
  @Input() textHeader: string[] = ['Портфель', 'по'];
  @Input() kontragents: OrganizationInterface[] = [];
  @Input() selectedCompany: OrganizationInterface | null = null;
  @Output() selectCompany: EventEmitter<string | null> = new EventEmitter<string | null>();

  public showCompanyList: boolean = false;
  public corporate: boolean = environment.corporate;

  public hideShowCompanyList(): void {
    if (this.corporate) {
      this.showCompanyList = !this.showCompanyList;
    }
  }

  public trimString(str, count): string {
    return str?.length > count ? str.slice(0, count) + '...' : str;
  }

  public onSelect(company: OrganizationInterface): void {
    this.hideShowCompanyList();
    this.selectCompany.emit(company.id ? company.id : null);
  }
}
