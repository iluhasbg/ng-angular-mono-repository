import {
  PortfolioTable,
  PortfolioTabType,
  PortfolioFilterState,
  LoadContractsResponse,
  PortfolioOtherTabType,
  LoadSecurityRiskPayload,
  RemoveInformationObject,
  LoadSecurityRiskResponse,
  InformationObjectResponse,
  LoadPortfolioCostResponse,
  LoadCompositionDataResponse,
  PortfolioFiltersDataResponse,
} from '../models/portfolio.interfaces';

export namespace PortfolioActions {
  export class GetInformationByCompanyId {
    public static readonly type = '[Portfolio State] Get Information By Company Id';
    constructor(public readonly payload: string | null) { }
  }

  export class SetSelectedCompanyId {
    public static readonly type = '[Portfolio State] Set Selected Company Id';
    constructor(public readonly payload: string | null) { }
  }

  export class LoadCompositionData {
    public static readonly type = '[Portfolio State] Load Composition Data';
  }

  export class LoadCompositionDataSuccess {
    public static readonly type = '[Portfolio State] Load Composition Data Success';
    constructor(public readonly payload: LoadCompositionDataResponse) { }
  }

  export class LoadCompositionDataFail {
    public static readonly type = '[Portfolio State] Load Composition Data Fail';
  }

  export class LoadPortfolioSecurity {
    public static readonly type = '[Portfolio State] Load Portfolio Security';
    constructor(public readonly payload: LoadSecurityRiskPayload) { }
  }

  export class LoadPortfolioSecuritySuccess {
    public static readonly type = '[Portfolio State] Load Portfolio Security Success';
    constructor(public readonly payload: LoadSecurityRiskResponse[]) { }
  }

  export class LoadPortfolioSecurityFail {
    public static readonly type = '[Portfolio State] Load Portfolio Security Fail';
  }

  export class LoadPortfolioCost {
    public static readonly type = '[Portfolio State] Load Portfolio Cost';
    constructor(public readonly payload: LoadSecurityRiskPayload) { }
  }

  export class LoadPortfolioCostSuccess {
    public static readonly type = '[Portfolio State] Load Portfolio Cost Success';
    constructor(public readonly payload: LoadPortfolioCostResponse) { }
  }

  export class LoadPortfolioCostFail {
    public static readonly type = '[Portfolio State] Load Portfolio Cost Fail';
  }

  export class LoadObjectsByType {
    public static readonly type = '[Portfolio State] Load Objects By Type';
    constructor(
      public readonly payload: PortfolioTable,
      public readonly deleteObject: boolean = false
    ) {
    }
  }

  export class LoadObjectsByTypeSuccess {
    public static readonly type = '[Portfolio State] Load Objects By Type Success';
    constructor(
      public readonly type: PortfolioTable,
      public readonly payload: InformationObjectResponse,
      public readonly deleteObject: boolean = false
    ) { }
  }

  export class LoadObjectsByTypeFail {
    public static readonly type = '[Portfolio State] Load Objects By Type Fail';
  }

  export class LoadPortfolioContracts {
    public static readonly type = '[Portfolio State] Load Portfolio Contracts';
  }

  export class LoadPortfolioContractsSuccess {
    public static readonly type = '[Portfolio State] Load Portfolio Contracts Success';
    constructor(public readonly payload: LoadContractsResponse) { }
  }

  export class LoadPortfolioContractsFail {
    public static readonly type = '[Portfolio State] Load Portfolio Contracts Fail';
  }

  export class SetPortfolioCurrentTab {
    public static readonly type = '[Portfolio State] Set Portfolio Current Tab';
    constructor(public readonly payload: PortfolioTabType) { }
  }

  export class SetPortfolioOtherCurrentTab {
    public static readonly type = '[Portfolio State] Set Portfolio Other Current Tab';
    constructor(public readonly payload: PortfolioOtherTabType) { }
  }

  export class ChangePaginatorPage {
    public static readonly type = '[Portfolio State] Change Paginator Page';
    constructor(public readonly payload: number) { }
  }

  export class LoadFiltersData {
    public static readonly type = '[Portfolio State] Load Filters Data';
  }

  export class LoadFiltersDataSuccess {
    public static readonly type = '[Portfolio State] Load Filters Data Success';
    constructor(public readonly payload: PortfolioFiltersDataResponse) { }
  }

  export class LoadFiltersDataFail {
    public static readonly type = '[Portfolio State] Load Filters Data Fail';
  }

  export class SetPortfolioFilters {
    public static readonly type = '[Portfolio State] Set Portfolio Filters';
    constructor(public readonly payload: PortfolioFilterState) { }
  }

  export class LoadDataByCurrentTable {
    public static readonly type = '[Portfolio State] Load Data By Current Table';
    constructor(public readonly deleteObject: boolean = false) { }
  }

  export class DeleteInformationObject {
    public static readonly type = '[Portfolio State] Remove Information Object';
    constructor(public readonly payload: RemoveInformationObject) { }
  }

  export class ClearPortfolioFilters {
    public static readonly type = '[Portfolio State] Clear Portfolio Filters';
  }

  export class ToggleShowModal {
    public static readonly type = '[Portfolio State] Toggle Show Modal';
  }
};
