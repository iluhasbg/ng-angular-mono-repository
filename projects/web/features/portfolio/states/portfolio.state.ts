import { Injectable } from '@angular/core';
import { patch } from '@ngxs/store/operators';
import { catchError, map, filter } from 'rxjs/operators';
import { OrganizationState } from '@web/core/states/organization.state';
import { OrganizationInterface } from '@web/core/models/organization.interface';
import { Action, createSelector, Selector, State, StateContext, StateToken, Store } from '@ngxs/store';
import {
  DEFAULT_OTHER_TABS,
  DEFAULT_TABLE_VALUE,
  DEFAULT_GENERAL_TABS,
  DEFAULT_SECURITY_RISK,
  DEFAULT_PORTFOLIO_COST,
  DEFAULT_COMPOSITION_DATA,
  DEFAULT_PORTFOLIO_FILTERS_DATA,
} from '../models/portfolio.defaults';
import {
  TableValue,
  PortfolioTable,
  TableValueData,
  PortfolioTabType,
  PortfolioOtherTab,
  PortfolioGeneralTab,
  PortfolioFiltersData,
  IQueryByRemoveObject,
  PortfolioOtherTabType,
  CompositionWidgetData,
  PortfolioCostWidgetData,
  SecurityRisksWidgetData,
  LoadSecurityRiskResponse,
  LoadPortfolioCostResponse,
} from '../models/portfolio.interfaces';
import { PortfolioActions } from './portfolio.actions';
import { PortfolioAPIService } from '../services/portfolio.service';
import { PortfolioStatusService } from '../services/portfolio-status.service';
import { PortfolioParamsService } from '../services/porrtfolio-params.service';
import ChangePaginatorPage = PortfolioActions.ChangePaginatorPage;

export interface PortfolioStateModel {
  selectedCompanyId: string | null;
  currentTab: PortfolioTabType;
  currentTable: PortfolioTable;
  currentOtherTab: PortfolioOtherTabType;
  tabs: PortfolioGeneralTab[];
  otherTabs: PortfolioOtherTab[];
  composition: CompositionWidgetData;
  securityRisks: SecurityRisksWidgetData;
  portfolioCost: PortfolioCostWidgetData;
  table: TableValue;
  filters: PortfolioFiltersData;
  isShowModal: boolean;
}

export const PORTFOLIO_STATE_TOKEN = new StateToken<PortfolioStateModel>('portfolio');
export const portfolioInitialState: PortfolioStateModel = {
  selectedCompanyId: null,
  currentTab: PortfolioTabType.TRADEMARKS,
  currentTable: PortfolioTabType.TRADEMARKS,
  currentOtherTab: PortfolioOtherTabType.DOMAINS,
  tabs: DEFAULT_GENERAL_TABS,
  otherTabs: DEFAULT_OTHER_TABS,
  composition: { ...DEFAULT_COMPOSITION_DATA },
  securityRisks: { ...DEFAULT_SECURITY_RISK },
  portfolioCost: { ...DEFAULT_PORTFOLIO_COST },
  table: DEFAULT_TABLE_VALUE,
  filters: { ...DEFAULT_PORTFOLIO_FILTERS_DATA },
  isShowModal: false,
};

@State({
  name: PORTFOLIO_STATE_TOKEN,
  defaults: portfolioInitialState,
})
@Injectable()
export class PortfolioState {
  constructor(
    private readonly store: Store,
    private readonly portfolioService: PortfolioAPIService,
    private readonly portfolioParamsService: PortfolioParamsService,
    private readonly portfolioStatusService: PortfolioStatusService,
  ) { }

  public static selectedCompany() {
    return createSelector(
      [OrganizationState.organizationsForSelect, PortfolioState.selectedCompanyId],
      (organizations: OrganizationInterface[], selectedCompanyId: string | null) => {
        return selectedCompanyId
          ? organizations.find(item => item.id === selectedCompanyId)
          : organizations.length === 2
            ? organizations[1]
            : organizations[0];
    });
  }

  public static tableValueByType(type: PortfolioTabType | PortfolioOtherTabType) {
    return createSelector([PortfolioState], (state: PortfolioStateModel) => {
      return state.table[type];
    });
  }

  @Selector()
  public static selectedCompanyId(state: PortfolioStateModel) {
    return state.selectedCompanyId;
  }

  @Selector()
  public static composition(state: PortfolioStateModel) {
    return state.composition;
  }

  @Selector()
  public static securityRisks(state: PortfolioStateModel) {
    return state.securityRisks;
  }

  @Selector()
  public static portfolioCost(state: PortfolioStateModel) {
    return state.portfolioCost;
  }

  @Selector()
  public static tabs(state: PortfolioStateModel) {
    return state.tabs.map(tab => ({
      ...tab,
      count: tab.type === PortfolioTabType.OTHER
        ? state.composition.brandNames + state.composition.copyright + state.composition.domains
        : tab.type === PortfolioTabType.TRADEMARKS
          ? state.composition.tradeMarks + state.composition.bids
          : state.composition[tab.type],
    }));
  }

  @Selector()
  public static totalCout(state: PortfolioStateModel) {
    return state.currentTable === PortfolioTabType.TRADEMARKS
      ? state.composition[state.currentTable as string] + state.composition.bids
      : state.composition[state.currentTable as string];
  }

  @Selector()
  public static otherTabs(state: PortfolioStateModel) {
    return state.otherTabs.map(tab => ({ ...tab, count: state.composition[tab.type] }));
  }

  @Selector()
  public static currentTab(state: PortfolioStateModel) {
    return state.currentTab;
  }

  @Selector()
  public static currentOtherTab(state: PortfolioStateModel) {
    return state.currentOtherTab;
  }

  @Selector()
  public static currentTable(state: PortfolioStateModel) {
    return state.currentTable;
  }

  @Selector()
  public static columns(state: PortfolioStateModel) {
    return state.table[state.currentTab].columns;
  }

  @Selector()
  public static filtersValue(state: PortfolioStateModel) {
    return state.table[state.currentTable as string].filters;
  }

  @Selector()
  public static tableValueData(state: PortfolioStateModel) {
    const currentTab = state.currentTab === PortfolioTabType.OTHER ? state.currentOtherTab : state.currentTab;

    return state.table[currentTab].data;
  }

  @Selector()
  public static currentPage(state: PortfolioStateModel) {
    const currentTab = state.currentTab === PortfolioTabType.OTHER ? state.currentOtherTab : state.currentTab;

    return state.table[currentTab].page;
  }

  @Selector()
  public static isEmptyPortfolio(state: PortfolioStateModel) {
    return !state.composition.bids && !state.composition.count && !state.composition.contracts;
  }

  @Selector()
  public static tableValueCount(state: PortfolioStateModel) {
    return state.table[state.currentTable as string].count;
  }

  @Selector()
  public static domainAdministrator(state: PortfolioStateModel) {
    return state.filters[PortfolioOtherTabType.DOMAINS].domainAdministrators;
  }

  @Selector()
  public static filters(state: PortfolioStateModel) {
    return state.filters;
  }

  @Selector()
  public static isShowModal(state: PortfolioStateModel) {
    return state.isShowModal;
  }

  @Action(PortfolioActions.ToggleShowModal)
  public toggleShowModal(
    { getState, setState }: StateContext<PortfolioStateModel>,
  ) {
    setState(patch<PortfolioStateModel>({ isShowModal: !getState().isShowModal }));
  }

  @Action(PortfolioActions.GetInformationByCompanyId)
  public getInformationByCompanyId(
    { setState, dispatch }: StateContext<PortfolioStateModel>,
    { payload }: PortfolioActions.GetInformationByCompanyId,
  ) {
    setState(patch<PortfolioStateModel>({ ...portfolioInitialState }));
    dispatch([
      new PortfolioActions.SetSelectedCompanyId(payload),
      new PortfolioActions.LoadPortfolioContracts(),
      new PortfolioActions.LoadFiltersData(),
      new PortfolioActions.LoadCompositionData(),
      new PortfolioActions.LoadObjectsByType(PortfolioTabType.PATENTS),
      new PortfolioActions.LoadObjectsByType(PortfolioTabType.PROGRAMS),
      new PortfolioActions.LoadObjectsByType(PortfolioTabType.TRADEMARKS),
      new PortfolioActions.LoadObjectsByType(PortfolioOtherTabType.DOMAINS),
      new PortfolioActions.LoadPortfolioSecurity({ PSRN: this.currentKontragentPSRN() }),
      new PortfolioActions.LoadPortfolioCost({ PSRN: this.currentKontragentPSRN() }),
    ]);
  }

  @Action(PortfolioActions.SetSelectedCompanyId)
  public SetSelectedCompanyId(
    { setState }: StateContext<PortfolioStateModel>,
    { payload }: PortfolioActions.SetSelectedCompanyId
  ) {
    setState(patch<PortfolioStateModel>({ selectedCompanyId: payload }));
  }

  @Action(PortfolioActions.LoadCompositionData)
  public loadCompositionData(
    { getState, dispatch }: StateContext<PortfolioStateModel>,
  ) {
    const selectedCompanyId = getState().selectedCompanyId;

    return this.portfolioService.loadCompositionData(selectedCompanyId && { organization: selectedCompanyId }).pipe(
      map(response => dispatch(new PortfolioActions.LoadCompositionDataSuccess(response))),
      catchError(() => dispatch(new PortfolioActions.LoadCompositionDataFail())),
    );
  }

  @Action(PortfolioActions.LoadCompositionDataSuccess)
  public loadCompositionDataSuccess(
    { setState }: StateContext<PortfolioStateModel>,
    { payload: { activeRequests, portfolio } }: PortfolioActions.LoadCompositionDataSuccess,
  ) {
    const tradeMarks = portfolio.tradermarks ? portfolio.tradermarks : 0;
    const patents = portfolio.patents ? portfolio.patents : 0;
    const programs = portfolio.soft ? portfolio.soft : 0;
    const domains = portfolio.domains ? portfolio.domains : 0;
    const count = tradeMarks + patents + programs + domains;

    setState(patch<PortfolioStateModel>({
      composition: patch<CompositionWidgetData>({
        bids: activeRequests ? activeRequests : 0,
        tradeMarks, patents, programs, domains, count,
      })
    }));
  }

  @Action(PortfolioActions.LoadPortfolioSecurity)
  public loadPortfolioSecurity(
    { dispatch }: StateContext<PortfolioStateModel>,
    { payload }: PortfolioActions.LoadPortfolioSecurity,
  ) {
    return this.portfolioService.loadPortfolioSecurity(payload).pipe(
      map(response => dispatch(new PortfolioActions.LoadPortfolioSecuritySuccess(response))),
      catchError(() => dispatch(new PortfolioActions.LoadPortfolioSecurityFail())),
    );
  }

  @Action(PortfolioActions.LoadPortfolioSecuritySuccess)
  public loadPortfolioSecuritySuccess(
    { setState }: StateContext<PortfolioStateModel>,
    { payload }: PortfolioActions.LoadPortfolioSecuritySuccess,
  ) {
    setState(patch<PortfolioStateModel>({
      securityRisks: patch<SecurityRisksWidgetData>({
        ...this.mapSecurityRisksToData(payload),
      })
    }));
  }

  @Action(PortfolioActions.LoadPortfolioCost)
  public loadPortfolioCost(
    { dispatch }: StateContext<PortfolioStateModel>,
    { payload }: PortfolioActions.LoadPortfolioCost,
  ) {
    return this.portfolioService.loadPortfolioCost(payload).pipe(
      filter(response => !Boolean(response.found)),
      map(response => dispatch(new PortfolioActions.LoadPortfolioCostSuccess(response))),
      catchError(() => dispatch(new PortfolioActions.LoadPortfolioCostFail())),
    );
  }

  @Action(PortfolioActions.LoadPortfolioCostSuccess)
  public loadPortfolioCostSuccess(
    { setState }: StateContext<PortfolioStateModel>,
    { payload }: PortfolioActions.LoadPortfolioCostSuccess,
  ) {
    setState(patch<PortfolioStateModel>({
      portfolioCost: patch<PortfolioCostWidgetData>({
        ...this.mapCostToCostData(payload),
      }),
    }));
  }

  @Action(PortfolioActions.LoadObjectsByType)
  public loadObjectsByType(
    { dispatch }: StateContext<PortfolioStateModel>,
    { payload: type, deleteObject }: PortfolioActions.LoadObjectsByType,
  ) {
    const params = this.portfolioParamsService.buildHttpParams(type);

    return this.portfolioService.loadObjectsByType(params).pipe(
      map(response => dispatch(new PortfolioActions.LoadObjectsByTypeSuccess(type, response, deleteObject))),
      catchError(() => dispatch(new PortfolioActions.LoadObjectsByTypeFail())),
    );
  }

  @Action(PortfolioActions.LoadObjectsByTypeSuccess)
  public loadObjectsByTypeSuccess(
    { setState, dispatch, getState }: StateContext<PortfolioStateModel>,
    { type, payload, deleteObject }: PortfolioActions.LoadObjectsByTypeSuccess,
  ) {
    const objects = this.portfolioStatusService.mapPortfolioObjectsStatus(type, payload.objects);
    objects.length === 0 && deleteObject
      ? dispatch(new ChangePaginatorPage(getState().table[type as string].page - 1))
      : setState(patch<PortfolioStateModel>({
        table: patch<TableValue>({
          [type as string]: patch<TableValueData>({ data: objects, count: payload.filteredCount }),
        })
      }));
  }

  @Action(PortfolioActions.LoadPortfolioContracts)
  public loadPortfolioContracts(
    { dispatch }: StateContext<PortfolioStateModel>,
  ) {
    const params = this.portfolioParamsService.buildHttpParams(PortfolioTabType.CONTRACTS);

    return this.portfolioService.loadContracts(params).pipe(
      map(response => dispatch(new PortfolioActions.LoadPortfolioContractsSuccess(response))),
      catchError(() => dispatch(new PortfolioActions.LoadPortfolioContractsFail())),
    );
  }

  @Action(PortfolioActions.LoadPortfolioContractsSuccess)
  public loadPortfolioContractsSuccess(
    { setState }: StateContext<PortfolioStateModel>,
    { payload }: PortfolioActions.LoadPortfolioContractsSuccess,
  ) {
    setState(patch<PortfolioStateModel>({
      composition: patch<CompositionWidgetData>({ contracts: payload.totalCount }),
      table: patch<TableValue>({
        contracts: patch<TableValueData>({ data: payload.contracts, count: payload.filteredCount }),
      }),
    }));
  }

  @Action(PortfolioActions.SetPortfolioCurrentTab)
  public setPortfolioCurrentTab(
    { getState, setState }: StateContext<PortfolioStateModel>,
    { payload }: PortfolioActions.SetPortfolioCurrentTab,
  ) {
    setState(patch<PortfolioStateModel>({
      currentTab: payload,
      currentTable: payload !== PortfolioTabType.OTHER ? payload : getState().currentOtherTab,
    }));
  }

  @Action(PortfolioActions.SetPortfolioOtherCurrentTab)
  public setPortfolioOtherCurrentTab(
    { setState }: StateContext<PortfolioStateModel>,
    { payload }: PortfolioActions.SetPortfolioOtherCurrentTab,
  ) {
    setState(patch<PortfolioStateModel>({ currentOtherTab: payload, currentTable: payload }));
  }

  @Action(PortfolioActions.ChangePaginatorPage)
  public changePaginatorPage(
    { getState, setState, dispatch }: StateContext<PortfolioStateModel>,
    { payload }: PortfolioActions.ChangePaginatorPage,
  ) {
    const currentTab = getState().currentTab === PortfolioTabType.OTHER ? getState().currentOtherTab : getState().currentTab;
    setState(patch<PortfolioStateModel>({
      table: patch<TableValue>({
        [currentTab]: patch<TableValueData>({ page: payload }),
      }),
    }));
    dispatch(new PortfolioActions.LoadDataByCurrentTable());
  }

  @Action(PortfolioActions.LoadFiltersData)
  public loadFiltersData(
    { getState, dispatch }: StateContext<PortfolioStateModel>,
  ) {
    const selectedCompanyId = getState().selectedCompanyId;

    return this.portfolioService.loadPortfolioFilters(selectedCompanyId && { organization: selectedCompanyId }).pipe(
      map(response => dispatch(new PortfolioActions.LoadFiltersDataSuccess(response))),
      catchError(() => dispatch(new PortfolioActions.LoadFiltersDataFail())),
    );
  }

  @Action(PortfolioActions.LoadFiltersDataSuccess)
  public loadFiltersDataSuccess(
    { setState }: StateContext<PortfolioStateModel>,
    { payload }: PortfolioActions.LoadFiltersDataSuccess,
  ) {
    setState(patch<PortfolioStateModel>({
      filters: patch<PortfolioFiltersData>({
        ...payload,
        tradeMarks: payload.trademarks,
        domains: payload.domens,
      })
    }));
  }

  @Action(PortfolioActions.SetPortfolioFilters)
  public setPortfolioFilters(
    { getState, setState, dispatch }: StateContext<PortfolioStateModel>,
    { payload }: PortfolioActions.SetPortfolioFilters,
  ) {
    const currentTable = getState().currentTable as string;
    const currentState = getState().table[currentTable].filters;
    setState(patch<PortfolioStateModel>({
      table: patch<TableValue>({
        [currentTable]: patch<TableValueData>({ filters: payload, page: 1 }),
      })
    }));
    if (
      currentState.owner !== payload.owner ||
      currentState.searchQuery !== payload.searchQuery ||
      payload.types && JSON.stringify(payload.types) !== JSON.stringify(currentState.types) ||
      payload.index && JSON.stringify(payload.index) !== JSON.stringify(currentState.index) ||
      payload.status && JSON.stringify(payload.status) !== JSON.stringify(currentState.status) ||
      payload.domainAdministrator && JSON.stringify(payload.domainAdministrator) !== JSON.stringify(currentState.domainAdministrator)
    ) {
      dispatch(new PortfolioActions.LoadDataByCurrentTable());
    }
  }

  @Action(PortfolioActions.LoadDataByCurrentTable)
  public loadDataByCurrentTable(
    { getState, dispatch }: StateContext<PortfolioStateModel>,
    { deleteObject }: PortfolioActions.LoadDataByCurrentTable,
  ) {
    switch(getState().currentTable) {
      case PortfolioTabType.TRADEMARKS:
        return dispatch(new PortfolioActions.LoadObjectsByType(PortfolioTabType.TRADEMARKS, deleteObject));
      case PortfolioTabType.PATENTS:
        return dispatch(new PortfolioActions.LoadObjectsByType(PortfolioTabType.PATENTS, deleteObject));
      case PortfolioTabType.PROGRAMS:
        return dispatch(new PortfolioActions.LoadObjectsByType(PortfolioTabType.PROGRAMS, deleteObject));
      case PortfolioTabType.CONTRACTS:
        return dispatch(new PortfolioActions.LoadPortfolioContracts());
      case PortfolioOtherTabType.DOMAINS:
        return dispatch(new PortfolioActions.LoadObjectsByType(PortfolioOtherTabType.DOMAINS, deleteObject));
      default: return;
    }
  }

  @Action(PortfolioActions.DeleteInformationObject)
  public deleteInformationObject(
    { getState, dispatch }: StateContext<PortfolioStateModel>,
    { payload }: PortfolioActions.DeleteInformationObject,
  ) {
    const params: IQueryByRemoveObject = { objects: [payload] };
    const selectedCompanyId = getState().selectedCompanyId;
    if (selectedCompanyId) {
      params.organization = selectedCompanyId;
    }
    return this.portfolioService.removeInformationObject(params).pipe(
      map(() => dispatch([
        new PortfolioActions.LoadDataByCurrentTable(true),
        new PortfolioActions.LoadFiltersData(),
        new PortfolioActions.LoadCompositionData(),
        new PortfolioActions.LoadPortfolioSecurity({ PSRN: this.currentKontragentPSRN() }),
        new PortfolioActions.LoadPortfolioCost({ PSRN: this.currentKontragentPSRN() }),
      ])),
    );
  }

  @Action(PortfolioActions.ClearPortfolioFilters)
  public clearFilters(
    { getState, setState }: StateContext<PortfolioStateModel>,
  ) {
    const currentTable = getState().currentTable as string;
    setState(patch<PortfolioStateModel>({
      table: patch<TableValue>({
        [currentTable]: patch<TableValueData>({ filters: { ...DEFAULT_TABLE_VALUE[currentTable].filters }, page: 1 }),
      })
    }));
  }

  private currentKontragentPSRN(): string | string[] {
    const selectedCompanyId = this.store.selectSnapshot(PortfolioState.selectedCompanyId);
    const organizations = this.store.selectSnapshot(OrganizationState.organizations);
    if(selectedCompanyId) {
      const selectedCompany = organizations.find(item => item.id === selectedCompanyId);
      return this.trimSpaces(selectedCompany.PSRN);
    } else {
      const PSRNs = [];
      const kontragents = this.store.selectSnapshot(OrganizationState.organizations);
      kontragents.forEach(kontragent => {
        if (kontragent.PSRN) {
          PSRNs.push(this.trimSpaces(kontragent.PSRN));
        }
      });
      return PSRNs;
    }
  }

  private trimSpaces(str: string): string {
    return str.replace(/[^+\d]/g, '');
  }

  private mapSecurityRisksToData(securityRisks: LoadSecurityRiskResponse[]): SecurityRisksWidgetData {
    const currentSecurutyRisk = { ...DEFAULT_SECURITY_RISK };

    securityRisks.forEach(item => {
      switch (item.risk) {
        case 1:
          currentSecurutyRisk.recommendation += item.values;
          currentSecurutyRisk.allCount += item.values;
          break;
        case 2:
          currentSecurutyRisk.possible += item.values;
          currentSecurutyRisk.allCount += item.values;
          break;
        case 3:
          currentSecurutyRisk.critical += item.values;
          currentSecurutyRisk.allCount += item.values;
          break;
        case 4:
          currentSecurutyRisk.fatal += item.values;
          currentSecurutyRisk.allCount += item.values;
          break;
      }
    });

    return currentSecurutyRisk;
  }

  private mapCostToCostData(cost: LoadPortfolioCostResponse | null): PortfolioCostWidgetData | null {
    if (cost && cost.annualRecords) {
      const last = cost.annualRecords[cost.annualRecords.length - 1];

      return { before: last.valueOfKnowHow, after: last.forecastValueOfKnowHow };
    }

    return null;
  }
}
