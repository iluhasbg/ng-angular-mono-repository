import { Injectable } from '@angular/core';
import { catchError, map } from 'rxjs/operators';
import { Action, Selector, State, StateContext, StateToken } from '@ngxs/store';
import { PortfolioAPIService } from '@web/features/portfolio/services/portfolio.service';
import { TechnicalTaskActions } from '@web/features/portfolio/states/technical-task.actions';
import {
  AddObjectsModal,
  PortfolioObject,
  AddObjectsToPortfolioBody,
  SearchPortfolioObjectData,
  SearchPortfolioObjectsBody,
  SearchPortfolioObjectsByINNBody,
} from '@web/features/portfolio/models/technical-task.interfaces';
import { SearchService } from '../../../../app-search/src/app/services/search.service';
import { TechnicalTaskMapper } from '@web/features/portfolio/services/technical-task.mapper';
import { compose, insertItem, patch, removeItem } from '@ngxs/store/operators';

export interface TechnicalTaskStateModel {
  trademarks: SearchPortfolioObjectData;
  domains: SearchPortfolioObjectData;
  patents: SearchPortfolioObjectData;
  swdb: SearchPortfolioObjectData;
  objects: PortfolioObject[];
  body: SearchPortfolioObjectsBody;
  bodyINN: SearchPortfolioObjectsByINNBody;
  modalData: AddObjectsModal;
  legalName: string;
}

export const TECHNICAL_TASK_STATE_TOKEN = new StateToken<TechnicalTaskStateModel>('technicalTask');
export const technicalTaskInitialState: TechnicalTaskStateModel = {
  trademarks: {
    result: [],
    pagination: { size: 10, from: 0 },
    totalCount: 0,
  },
  domains: {
    result: [],
    pagination: { size: 10, from: 0 },
    totalCount: 0,
  },
  patents: {
    result: [],
    pagination: { size: 10, from: 0 },
    totalCount: 0,
  },
  swdb: {
    result: [],
    pagination: { size: 10, from: 0 },
    totalCount: 0,
  },
  objects: [],
  body: { name: null, index: 'trademarks' },
  bodyINN: { id: '' },
  modalData: {
    errorModal: false,
    portfolioObjectsError: null,
    successLength: null,
  },
  legalName: null,
};

@State({
  name: TECHNICAL_TASK_STATE_TOKEN,
  defaults: technicalTaskInitialState,
})
@Injectable()
export class TechnicalTaskState {

  constructor(
    private readonly portfolioService: PortfolioAPIService,
    private readonly searchApi: SearchService,
  ) { }

  @Selector()
  public static trademarksData(state: TechnicalTaskStateModel): SearchPortfolioObjectData {
    return state.trademarks;
  }

  @Selector()
  public static allTrademarksChecked(state: TechnicalTaskStateModel): boolean {
    return state.trademarks.result.every(el => state.objects.find(item => el._id === item.id));
  }

  @Selector()
  public static allPatentsChecked(state: TechnicalTaskStateModel): boolean {
    return state.patents.result.every(el => state.objects.find(item => el._id === item.id));
  }

  @Selector()
  public static allDomainsChecked(state: TechnicalTaskStateModel): boolean {
    return state.domains.result.every(el => state.objects.find(item => el._id === item.id));
  }

  @Selector()
  public static allSWDBChecked(state: TechnicalTaskStateModel): boolean {
    return state.swdb.result.every(el => state.objects.find(item => el._id === item.id));
  }

  @Selector()
  public static currentPageTrademarks(state: TechnicalTaskStateModel): number {
    return Math.floor(state.trademarks.pagination.from / state.trademarks.pagination.size) + 1;
  }
  @Selector()
  public static currentPagePatents(state: TechnicalTaskStateModel): number {
    return Math.floor(state.patents.pagination.from / state.patents.pagination.size) + 1;
  }

  @Selector()
  public static currentPageDomains(state: TechnicalTaskStateModel): number {
    return Math.floor(state.domains.pagination.from / state.domains.pagination.size) + 1;
  }

  @Selector()
  public static currentPageSWDB(state: TechnicalTaskStateModel): number {
    return Math.floor(state.swdb.pagination.from / state.swdb.pagination.size) + 1;
  }

  @Selector()
  public static patentsData(state: TechnicalTaskStateModel) {
    return state.patents;
  }

  @Selector()
  public static domainsData(state: TechnicalTaskStateModel) {
    return state.domains;
  }

  @Selector()
  public static swdbData(state: TechnicalTaskStateModel) {
    return state.swdb;
  }

  @Selector()
  public static objectsLength(state: TechnicalTaskStateModel) {
    return state.objects.length;
  }

  @Selector()
  public static errorModalData(state: TechnicalTaskStateModel) {
    return state.modalData;
  }

  @Selector()
  public static legalName(state: TechnicalTaskStateModel) {
    return state.legalName;
  }

  @Selector()
  public static checked(state: TechnicalTaskStateModel) {
    return state.objects;
  }

  @Action(TechnicalTaskActions.SearchObjects)
  public searchObjects(
    { dispatch, getState }: StateContext<TechnicalTaskStateModel>,
    { body }: TechnicalTaskActions.SearchObjects
  ) {
    body = {
      ...body,
      ...getState()[body.index].pagination
    };
    return this.portfolioService.searchPortfolioObjects(body).pipe(
      map(response => dispatch(new TechnicalTaskActions.SearchObjectsSuccess(response, body))),
      catchError(() => dispatch(new TechnicalTaskActions.SearchObjectsFail()))
    );
  }

  @Action(TechnicalTaskActions.SearchObjectsByINN)
  public searchObjectsByINN(
    { setState, dispatch }: StateContext<TechnicalTaskStateModel>,
    { bodyINN }: TechnicalTaskActions.SearchObjectsByINN
  ) {
    setState(
      patch({
        bodyINN
      })
    );
    dispatch([
      new TechnicalTaskActions.GetDomainsByINN(bodyINN),
      new TechnicalTaskActions.GetTrademarksByINN(bodyINN),
      new TechnicalTaskActions.GetPatentsByINN(bodyINN),
      new TechnicalTaskActions.GetSWDBByINN(bodyINN),
    ]);
  }

  @Action(TechnicalTaskActions.SearchObjectsSuccess)
  public searchObjectsSuccess(
    { setState }: StateContext<TechnicalTaskStateModel>,
    { body, payload }: TechnicalTaskActions.SearchObjectsSuccess
  ) {
    payload.hits.hits.forEach(el => el.checked = false);
    switch (body.index) {
      case 'trademarks':
        payload.hits.hits.forEach(el => {
          el._index === 'rutmap' && el._source.applicationString
            ? el._source.imgUrl = this.searchApi
              .getImgUrl(el._index, parseInt(el._source.applicationString, 10),
                el._source.markImageFileName, el._source.applicationString)
            : el._source.imgUrl = this.searchApi
              .getImgUrl(el._index, parseInt(el._source.registrationString, 10),
                el._source.markImageFileName, el._source.registrationString);

          switch (el._index) {
            case 'rutm':
              el.title = `Товарный знак ${el._source.registrationString}`;
              break;
            case 'rutmap':
              el.title = `Заявка на товарный знак ${el._source.applicationString}`;
              break;
            case 'wotm':
              el.title = `Международный товарный знак ${el._source.registrationString}`;
              break;
            case 'wktm':
              el.title = `Oбщеизвестный товарный знак ${el._source.registrationString}`;
              break;
          }
        });
        break;
      case 'patents':
        payload.hits.hits.forEach(el => {
          switch (el._index) {
            case 'rupat':
              el.title = `Патент на изобретение ${el._id}`;
              break;
            case 'ruum':
              el.title = `Патент на полезную модель ${el._id}`;
              break;
            case 'rude':
              el.title = `Патент на промышленный образец ${el._id}`;
              break;
          }
        });
    }
    setState(patch<TechnicalTaskStateModel>({
      [body.index]: {
        result: payload.hits.hits,
        totalCount: payload.hits.total.value,
        pagination: {
          from: body.from,
          size: body.size
        },
      },
      body
    }));
  }

  @Action(TechnicalTaskActions.GetTrademarksByINN)
  public getTrademarksByINN(
    { dispatch, getState }: StateContext<TechnicalTaskStateModel>,
    { payload }: TechnicalTaskActions.GetTrademarksByINN
  ) {
    const params: SearchPortfolioObjectsByINNBody = {
      id: payload.id,
      searchTradeMarks: true,
      ...getState().trademarks.pagination
    };
    return this.portfolioService.searchPortfolioObjectsByINN(params).pipe(
      map(response => dispatch(new TechnicalTaskActions.GetTrademarksByINNSuccess(response, params))),
      catchError(() => dispatch(new TechnicalTaskActions.GetTrademarksByINNFail()))
     );
  }

  @Action(TechnicalTaskActions.GetTrademarksByINNSuccess)
  public getTrademarksByINNSuccess(
    { setState }: StateContext<TechnicalTaskStateModel>,
    { body, payload }: TechnicalTaskActions.GetTrademarksByINNSuccess
  ) {
    payload.hits.hits.forEach(el => {
      el.checked = false;
      el._index === 'rutmap' && el._source.applicationString
        ? el._source.imgUrl = this.searchApi
          .getImgUrl(el._index, parseInt(el._source.applicationString, 10),
            el._source.markImageFileName, el._source.applicationString)
        : el._source.imgUrl = this.searchApi
          .getImgUrl(el._index, parseInt(el._source.registrationString, 10),
            el._source.markImageFileName, el._source.registrationString);

      switch (el._index) {
        case 'rutm':
          el.title = `Товарный знак ${el._source.registrationString}`;
          break;
        case 'rutmap':
          el.title = `Заявка на товарный знак ${el._source.applicationString}`;
          break;
        case 'wotm':
          el.title = `Международный товарный знак ${el._source.registrationString}`;
          break;
        case 'wktm':
          el.title = `Oбщеизвестный товарный знак ${el._source.registrationString}`;
          break;
      }
    });
    setState(
      patch(
        {
          trademarks: patch({
            result: payload.hits.hits,
            totalCount: payload.hits.total.tradeMarks,
            pagination: patch({
              size: body.size,
              from: body.from
            })
          })
        })
    );
  }

  @Action(TechnicalTaskActions.GetDomainsByINN)
  public getDomainsByINN(
    { dispatch, getState }: StateContext<TechnicalTaskStateModel>,
    { payload }: TechnicalTaskActions.GetDomainsByINN
  ) {
    const params: SearchPortfolioObjectsByINNBody = {
      id: payload.id,
      searchDomains: true,
      ...getState().domains.pagination
    };
    return this.portfolioService.searchPortfolioObjectsByINN(params).pipe(
      map(response => dispatch(new TechnicalTaskActions.GetDomainsByINNSuccess(response, params))),
      catchError(() => dispatch(new TechnicalTaskActions.GetDomainsByINNFail()))
    );
  }

  @Action(TechnicalTaskActions.GetDomainsByINNSuccess)
  public getDomainsByINNSuccess(
    { setState }: StateContext<TechnicalTaskStateModel>,
    { body, payload }: TechnicalTaskActions.GetDomainsByINNSuccess
  ) {
    payload.hits.hits.forEach(el => el.checked = false);
    setState(
      patch(
        {
          domains: patch({
            result: payload.hits.hits,
            totalCount: payload.hits.total.domains,
            pagination: patch({
              size: body.size,
              from: body.from
            })
          })
        })
    );
  }

  @Action(TechnicalTaskActions.GetPatentsByINN)
  public getPatentsByINN(
    { dispatch, getState }: StateContext<TechnicalTaskStateModel>,
    { payload }: TechnicalTaskActions.GetPatentsByINN
  ) {
    const params: SearchPortfolioObjectsByINNBody = {
      id: payload.id,
      searchPatents: true,
      ...getState().patents.pagination
    };
    return this.portfolioService.searchPortfolioObjectsByINN(params).pipe(
      map(response => dispatch(new TechnicalTaskActions.GetPatentsByINNSuccess(response, params))),
      catchError(() => dispatch(new TechnicalTaskActions.GetPatentsByINNFail()))
    );
  }

  @Action(TechnicalTaskActions.GetPatentsByINNSuccess)
  public getPatentsByINNSuccess(
    { setState }: StateContext<TechnicalTaskStateModel>,
    { body, payload }: TechnicalTaskActions.GetPatentsByINNSuccess
  ) {
    payload.hits.hits.forEach(el => {
      el.checked = false;
      switch (el._index) {
        case 'rupat':
          el.title = `Патент на изобретение ${el._id}`;
          break;
        case 'ruum':
          el.title = `Патент на полезную модель ${el._id}`;
          break;
        case 'rude':
          el.title = `Патент на промышленный образец ${el._id}`;
          break;
      }
    });
    setState(
      patch(
        {
          patents: patch({
            result: payload.hits.hits,
            totalCount: payload.hits.total.patents,
            pagination: patch({
              size: body.size,
              from: body.from
            })
          })
        })
    );
  }

  @Action(TechnicalTaskActions.GetSWDBByINN)
  public getSWDBByINN(
    { dispatch, getState }: StateContext<TechnicalTaskStateModel>,
    { payload }: TechnicalTaskActions.GetSWDBByINN
  ) {
    const params: SearchPortfolioObjectsByINNBody = {
      id: payload.id,
      searchSwdb: true,
      ...getState().swdb.pagination
    };
    return this.portfolioService.searchPortfolioObjectsByINN(params).pipe(
      map(response => dispatch(new TechnicalTaskActions.GetSWDBByINNSuccess(response, params))),
      catchError(() => dispatch(new TechnicalTaskActions.GetSWDBByINNFail()))
    );
  }

  @Action(TechnicalTaskActions.GetSWDBByINNSuccess)
  public getSWDBByINNSuccess(
    { setState }: StateContext<TechnicalTaskStateModel>,
    { body, payload }: TechnicalTaskActions.GetPatentsByINNSuccess
  ) {
    payload.hits.hits.forEach(el => el.checked = false);
    setState(
      patch(
        {
          swdb: patch({
            result: payload.hits.hits,
            totalCount: payload.hits.total.swdb,
            pagination: patch({
              size: body.size,
              from: body.from
            })
          })
        })
    );
  }

  @Action(TechnicalTaskActions.ChangePage)
  public changePage(
    { getState, dispatch }: StateContext<TechnicalTaskStateModel>,
    { payload }: TechnicalTaskActions.ChangePage
  ) {
    getState().bodyINN.id
      ? dispatch(new TechnicalTaskActions.ChangePageByINN(payload))
      : dispatch(new TechnicalTaskActions.ChangePageByType(payload));
  }

  @Action(TechnicalTaskActions.ChangePageByType)
  public changePageByType(
    { setState, getState, dispatch }: StateContext<TechnicalTaskStateModel>,
    { payload }: TechnicalTaskActions.ChangePageByType
  ) {
    setState(
      patch<TechnicalTaskStateModel>({
        [payload.type]: patch({
          pagination: patch({
            from: (payload.page - 1) * getState()[payload.type].pagination.size,
            size: getState()[payload.type].pagination.size
          })
        }),
        body: patch({
          name: getState().body.name,
          index: payload.type
        })
      })
    );
    dispatch(new TechnicalTaskActions.SearchObjects(getState().body));
  }

  @Action(TechnicalTaskActions.ChangePageByINN)
  public changePageByINN(
    { setState, getState, dispatch }: StateContext<TechnicalTaskStateModel>,
    { payload }: TechnicalTaskActions.ChangePageByINN
  ) {
    setState(
      patch<TechnicalTaskStateModel>({
        [payload.type]: patch({
          pagination: patch({
            from: (payload.page - 1) * getState()[payload.type].pagination.size,
            size: getState()[payload.type].pagination.size
          })
        }),
        bodyINN: patch({
          id: getState().bodyINN.id,
        })
      })
    );
    switch (payload.type) {
      case 'trademarks':
        dispatch(new TechnicalTaskActions.GetTrademarksByINN(getState().bodyINN));
        break;
      case 'patents':
        dispatch(new TechnicalTaskActions.GetPatentsByINN(getState().bodyINN));
        break;
      case 'domains':
        dispatch(new TechnicalTaskActions.GetDomainsByINN(getState().bodyINN));
        break;
      case 'swdb':
        dispatch(new TechnicalTaskActions.GetSWDBByINN(getState().bodyINN));
        break;
    }
  }

  @Action(TechnicalTaskActions.AddObjectsToPortfolio)
  public addObjectsToPortfolio(
    { getState, dispatch }: StateContext<TechnicalTaskStateModel>,
    { organization }: TechnicalTaskActions.AddObjectsToPortfolio
  ) {
    const body: AddObjectsToPortfolioBody = {
      organization,
      objects: getState().objects
    };
    return this.portfolioService.addObjectsToPortfolio(body).pipe(
      map(response => dispatch(new TechnicalTaskActions.AddObjectsSuccess(response))),
      catchError(() => dispatch(new TechnicalTaskActions.AddObjectsFail())));
  }

  @Action(TechnicalTaskActions.AddObjectsSuccess)
  public addObjectsSuccess(
    { setState }: StateContext<TechnicalTaskStateModel>,
    { response }: TechnicalTaskActions.AddObjectsSuccess
  ) {
    if (response.error?.length && !response.success?.length) {
      setState(patch({
        modalData: patch({
          errorModal: true,
          portfolioObjectsError: response.error
        })
      }));
    } else if (response.error?.length && response.success?.length) {
      setState(patch({
        modalData: patch({
          errorModal: true,
          portfolioObjectsError: response.error,
          successLength: response.success.length
        })
      }));
    } else if (!response.error.length && response.success?.length) {
      setState(patch({
        modalData: patch({
          errorModal: true,
          portfolioObjectsError: null,
          successLength: response.success.length
        })
      }));
    }
  }

  @Action(TechnicalTaskActions.GetLegal)
  public getLegal(
    { dispatch }: StateContext<TechnicalTaskStateModel>,
    { body }: TechnicalTaskActions.GetLegal
  ) {
    return this.portfolioService.loadLegalName(body).pipe(
      map(response => dispatch(new TechnicalTaskActions.GetLegalSuccess(response))),
      catchError(() => dispatch(new TechnicalTaskActions.GetLegalFail()))
    );
  }

  @Action(TechnicalTaskActions.GetLegalSuccess)
  public getLegalSuccess(
    { setState }: StateContext<TechnicalTaskStateModel>,
    { payload }: TechnicalTaskActions.GetLegalSuccess
  ) {
    setState(
      patch({
        legalName: payload?.hits?.hits[0]?._source?.name
          ? `/ ${payload?.hits?.hits[0]?._source?.name}`
          : payload?.hits?.hits[0]?._source?.fullName
            ? `/ ${payload?.hits?.hits[0]?._source?.fullName}`
            : ''
      })
    );
  }

  @Action(TechnicalTaskActions.ResetLegalName)
  public resetLegalName(
    { setState }: StateContext<TechnicalTaskStateModel>
  ) {
    setState(
      patch({
        legalName: null
      })
    );
  }

  @Action(TechnicalTaskActions.CloseModalError)
  public closeModalError(
    { setState, getState }: StateContext<TechnicalTaskStateModel>
  ) {
    setState(patch<TechnicalTaskStateModel>({
      modalData: patch({
        errorModal: false,
        portfolioObjectsError: null,
        successLength: null
      }),
      objects: [],
      trademarks: patch({
        result: compose(...TechnicalTaskMapper.mapUnchecked(getState().trademarks?.result))
      }),
      patents: patch({
        result: compose(...TechnicalTaskMapper.mapUnchecked(getState().patents?.result))
      }),
      domains: patch({
        result: compose(...TechnicalTaskMapper.mapUnchecked(getState().domains?.result))
      }),
      swdb: patch({
        result: compose(...TechnicalTaskMapper.mapUnchecked(getState().swdb?.result))
      })
    }));
  }

  @Action(TechnicalTaskActions.ResetSearch)
  public resetSearch(
    { setState }: StateContext<TechnicalTaskStateModel>,
  ) {
    setState(patch<TechnicalTaskStateModel>({ ...technicalTaskInitialState }));
  }

  @Action(TechnicalTaskActions.CheckTechnicalTaskObject)
  public checkTechnicalTaskObject(
    { getState, setState }: StateContext<TechnicalTaskStateModel>,
    { type, payload }: TechnicalTaskActions.CheckTechnicalTaskObject,
  ) {
    setState(patch<TechnicalTaskStateModel>({
      objects: getState().objects.find(el => el.id === payload._id)
        ? removeItem<PortfolioObject>(item => item.id === payload._id)
        : insertItem(TechnicalTaskMapper.mapObjectsPortfolio(payload, type)),
    }));
  }

  @Action(TechnicalTaskActions.CheckAllTechnicalTaskObject)
  public checkAllTechnicalTaskObject(
    { getState, setState }: StateContext<TechnicalTaskStateModel>,
    { type }: TechnicalTaskActions.CheckAllTechnicalTaskObject,
  ) {
    const currentObjects = getState()[type].result;
    const isAllChecked = currentObjects.every(item => getState().objects.find(el => el.id === item._id));
    setState(patch<TechnicalTaskStateModel>({
      objects: isAllChecked
        ? compose(...currentObjects.map(obj => removeItem<PortfolioObject>(el => el.id === obj._id)))
        : compose(...currentObjects.map(obj => insertItem<PortfolioObject>(TechnicalTaskMapper.mapObjectsPortfolio(obj, type)))),
    }));
    setState(patch<TechnicalTaskStateModel>({
      objects: Array.from(new Set<string>([ ...getState().objects.map(obj => JSON.stringify(obj)) ])).map(item => JSON.parse(item)),
    }));
  }

  @Action(TechnicalTaskActions.ClearAllCheckingObjects)
  public clearAllCheckingObjects(
    { setState }: StateContext<TechnicalTaskStateModel>,
  ) {
    setState(patch<TechnicalTaskStateModel>({ objects: [] }));
  }
}
