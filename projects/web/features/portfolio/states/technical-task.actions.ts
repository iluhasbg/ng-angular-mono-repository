import {
  AddObjectsToPortfolioResponse,
  ChangePageTechnicalTask,
  GetLegalBody,
  GetLegalResponse,
  SearchPortfolioObjectsBody,
  SearchPortfolioObjectsByINNBody,
  SearchPortfolioObjectsResponse, SearchPortfolioObjectsResponseResult, TypeObjects
} from '@web/features/portfolio/models/technical-task.interfaces';

export namespace TechnicalTaskActions {
  export class SearchObjects {
    public static readonly type = '[Technical Task State] Search Objects';
    constructor(public readonly body: SearchPortfolioObjectsBody) { }
  }

  export class SearchObjectsSuccess {
    public static readonly type = '[Technical Task State] Search Objects Success';
    constructor(
      public readonly payload: SearchPortfolioObjectsResponse,
      public readonly body: SearchPortfolioObjectsBody
    ) {
    }
  }

  export class SearchObjectsByINN{
    public static readonly type = '[Technical Task State] Search Objects By INN';
    constructor(public readonly bodyINN: SearchPortfolioObjectsByINNBody) { }
  }

  export class SearchObjectsByINNSuccess {
    public static readonly type = '[Technical Task State] Search Objects By INN Success';
    constructor(
      public readonly payload: SearchPortfolioObjectsResponse,
      public readonly body: SearchPortfolioObjectsByINNBody
    ) {
    }
  }

  export class SearchObjectsFail {
    public static readonly type = '[Technical Task State] Search Objects Fail';
  }

  export class ChangePage {
    public static readonly type = '[Technical Task State] Change Page';
    constructor(
      public readonly payload: ChangePageTechnicalTask
    ) {
    }
  }

  export class ChangePageByType {
    public static readonly type = '[Technical Task State] Change Page By Type';
    constructor(
      public readonly payload: ChangePageTechnicalTask
    ) {
    }
  }

  export class ChangePageByINN {
    public static readonly type = '[Technical Task State] Change Page By INN';
    constructor(
      public readonly payload: ChangePageTechnicalTask
    ) {
    }
  }

  export class AddObjectsToPortfolio {
    public static readonly type = '[Technical Task State] Add Objects To Portfolio';
    constructor(public readonly organization: string) {
    }
  }

  export class AddObjectsSuccess {
    public static readonly type = '[Technical Task State] Add Objects Success';
    constructor(public readonly response: AddObjectsToPortfolioResponse) {
    }
  }

  export class AddObjectsFail {
    public static readonly type = '[Technical Task State] Add Objects Fail';
  }

  export class CloseModalError {
    public static readonly type = '[Technical Task State] Close Modal Error';
  }

  export class GetTrademarksByINN {
    constructor(
      public readonly payload: SearchPortfolioObjectsByINNBody
    ) {
    }
    public static readonly type = '[Technical Task State] Get Trademarks By INN';
  }

  export class GetTrademarksByINNSuccess {
    public static readonly type = '[Technical Task State] Get Trademarks By INN Success';
    constructor(
      public readonly payload: SearchPortfolioObjectsResponse,
      public readonly body: SearchPortfolioObjectsByINNBody
    ) {
    }
  }

  export class GetTrademarksByINNFail {
    public static readonly type = '[Technical Task State] Get Trademarks By INN Fail';
  }

  export class GetDomainsByINN {
    public static readonly type = '[Technical Task State] Get Domains By INN';
    constructor(
      public readonly payload: SearchPortfolioObjectsByINNBody
    ) {
    }
  }

  export class GetDomainsByINNSuccess {
    public static readonly type = '[Technical Task State] Get Domains By INN Success';
    constructor(
      public readonly payload: SearchPortfolioObjectsResponse,
      public readonly body: SearchPortfolioObjectsByINNBody
    ) {
    }
  }

  export class GetDomainsByINNFail {
    public static readonly = '[Technical Task State] Get Domains By INN Fail';
  }

  export class GetPatentsByINN {
    public static readonly type = '[Technical Task State] Get Patents By INN';
    constructor(
      public readonly payload: SearchPortfolioObjectsByINNBody
    ) {
    }
  }

  export class GetPatentsByINNFail {
    public static readonly type = '[Technical Task State] Get Patents By INN Fail';
  }

  export class GetPatentsByINNSuccess {
    public static readonly type = '[Technical Task State] Get Patents By INN Success';
    constructor(
      public readonly payload: SearchPortfolioObjectsResponse,
      public readonly body: SearchPortfolioObjectsByINNBody
    ) {
    }
  }

  export class GetSWDBByINN {
    public static readonly type = '[Technical Task State] Get SWDB By INN';
    constructor(
      public readonly payload: SearchPortfolioObjectsByINNBody
    ) {
    }
  }

  export class GetSWDBByINNSuccess {
    public static readonly type = '[Technical Task State] Get SWDB By INN Success';
    constructor(
      public readonly payload: SearchPortfolioObjectsResponse,
      public readonly body: SearchPortfolioObjectsByINNBody
    ) {
    }
  }

  export class GetSWDBByINNFail {
    public static readonly = '[Technical Task State] Get SWDB By INN Fail';
  }

  export class ResetSearch {
    public static readonly type = '[Technical Task State] Reset Search';
  }

  export class GetLegal {
    public static readonly type = '[Technical Task State] Get Legal';
    constructor(
      public readonly body: GetLegalBody
    ) {
    }
  }

  export class GetLegalSuccess {
    public static readonly type = '[Technical Task State] Get Legal Success';
    constructor(
      public readonly payload: GetLegalResponse
    ) {
    }
  }

  export class GetLegalFail {
    public static readonly type = '[Technical Task State] Get Legal Fail';
  }

  export class ResetLegalName {
    public static readonly type = '[Technical Task State] Reset Legal Name';
  }

  export class CheckTechnicalTaskObject {
    public static readonly type = '[Technical Task State] Check Technical Task Object';
    constructor(
      public readonly type: TypeObjects,
      public readonly payload: SearchPortfolioObjectsResponseResult,
    ) { }
  }

  export class CheckAllTechnicalTaskObject {
    public static readonly type = '[Technical Task State] Check All Technical Task Object';
    constructor(public readonly type: TypeObjects) { }
  }

  export class ClearAllCheckingObjects {
    public static readonly type = '[Technical Task State] Clear All Checking Objects';
  }
}
