import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { PortfolioState } from './states/portfolio.state';
import { PortfolioAPIService } from './services/portfolio.service';
import { TechnicalTaskState } from './states/technical-task.state';
import { PortfolioStatusService } from './services/portfolio-status.service';
import { PortfolioParamsService } from './services/porrtfolio-params.service';

import { TextFieldModule } from '@angular/cdk/text-field';
import { FiltersModule } from '@ui/filters/filters.module';
import { UiSharedModule } from '@ui/shared/ui-shared.module';
import { PortfolioRoutingModule } from './portfolio-routing.module';
import { NgxsActionsExecutingModule } from '@ngxs-labs/actions-executing';
import { SharedModule } from 'projects/app-ipid/src/app/shared/shared.module';
import { LayoutModule } from 'projects/app-ipid/src/app/pages/layout/layout.module';

import { TableComponent } from './components/table/table.component';
import { FormatCostValuePipe } from './components/portfolio-value/format-cost-value.pipe';
import { EmptyObjectsComponent } from './components/empty-objects/empty-objects.component';
import { PortfolioTabsComponent } from './components/portfolio-tabs/portfolio-tabs.component';
import { PortfolioValueComponent } from './components/portfolio-value/portfolio-value.component';
import { PortfolioFiltersComponent } from './components/portfolio-filters/portfolio-filters.component';
import { OtherTablesComponent } from './components/portfolio-table/other-tables/other-tables.component';
import { PortfolioSucurityComponent } from './components/portfolio-security/portfolio-security.component';
import { PatentsTableComponent } from './components/portfolio-table/patents-table/patents-table.component';
import { TechnicalTaskSwdbComponent } from './components/technical-task-swdb/technical-task-swdb.component';
import { PortfolioContainerComponent } from './containers/portfolio-container/portfolio-container.component';
import { ProgramsTableComponent } from './components/portfolio-table/programs-table/programs-table.component';
import { PortfolioAddObjectComponent } from './components/portfolio-add-object/portfolio-add-object.component';
import { ContractsTableComponent } from './components/portfolio-table/contracts-table/contracts-table.component';
import { PortfolioCostWidgetComponent } from './components/portfolio-cost-widget/portfolio-cost-widget.component';
import { PortfolioCompositionComponent } from './components/portfolio-composition/portfolio-composition.component';
import { TrademarksTableComponent } from './components/portfolio-table/trademarks-table/trademarks-table.component';
import { TechnicalTaskPatentsComponent } from './components/technical-task-patents/technical-task-patents.component';
import { TechnicalTaskDomainsComponent } from './components/technical-task-domains/technical-task-domains.component';
import { TypePortfolioSelectorComponent } from './components/type-portfolio-selector/type-portfolio-selector.component';
import { SelectionOrganizationsComponent } from './components/selection-organizations/selection-organizations.component';
import { TechnicalTaskContainerComponent } from './containers/technical-task-container/technical-task-container.component';
import { PortfolioTableContainerComponent } from './containers/portfolio-table-container/portfolio-table-container.component';
import { CopyrightTableComponent } from './components/portfolio-table/other-tables/copyright-table/copyright-table.component';
import { BrandnameTableComponent } from './components/portfolio-table/other-tables/brandname-table/brandname-table.component';
import { TechnicalTaskTrademarksComponent } from './components/technical-task-trademarks/technical-task-trademarks.component';
import { PortfolioDbEvmComponent } from '@web/features/portfolio/components/portfolio-db-evm/portfolio-db-evm.component';
import { PortfolioPatentComponent } from '@web/features/portfolio/components/portfolio-patent/portfolio-patent.component';
import { PortfolioDomainComponent } from '@web/features/portfolio/components/portfolio-domain/portfolio-domain.component';
import { PortfolioWidgetsContainerComponent } from './containers/portfolio-widgets-container/portfolio-widgets-container.component';
import { PortfolioLogotypeComponent } from '@web/features/portfolio/components/portfolio-logotype/portfolio-logotype.component';
import { PortfolioLogotypeItemComponent } from '@web/features/portfolio/components/portfolio-logotype-item/portfolio-logotype-item.component';
import { PortfolioLoadingModalComponent } from './components/portfolio-loading-modal/portfolio-loading-modal.component';
import {ExpertModule} from '../../../app-ipid/src/app/pages/layout/expert/expert.mdoule';


@NgModule({
  declarations: [
    TableComponent,
    FormatCostValuePipe,
    OtherTablesComponent,
    PatentsTableComponent,
    EmptyObjectsComponent,
    ProgramsTableComponent,
    PortfolioTabsComponent,
    ContractsTableComponent,
    PortfolioValueComponent,
    PortfolioValueComponent,
    PortfolioDbEvmComponent,
    CopyrightTableComponent,
    BrandnameTableComponent,
    PortfolioPatentComponent,
    PortfolioDomainComponent,
    TrademarksTableComponent,
    PortfolioFiltersComponent,
    PortfolioFiltersComponent,
    PortfolioSucurityComponent,
    PortfolioSucurityComponent,
    TechnicalTaskSwdbComponent,
    PortfolioLogotypeComponent,
    PortfolioContainerComponent,
    PortfolioContainerComponent,
    PortfolioAddObjectComponent,
    PortfolioCostWidgetComponent,
    PortfolioCostWidgetComponent,
    PortfolioCompositionComponent,
    TechnicalTaskDomainsComponent,
    TechnicalTaskPatentsComponent,
    PortfolioCompositionComponent,
    PortfolioLogotypeItemComponent,
    TypePortfolioSelectorComponent,
    PortfolioLoadingModalComponent,
    SelectionOrganizationsComponent,
    SelectionOrganizationsComponent,
    TechnicalTaskContainerComponent,
    TechnicalTaskContainerComponent,
    PortfolioTableContainerComponent,
    PortfolioTableContainerComponent,
    TechnicalTaskTrademarksComponent,
    PortfolioWidgetsContainerComponent,
    PortfolioWidgetsContainerComponent,
  ],
  imports: [
    FormsModule,
    CommonModule,
    LayoutModule,
    SharedModule,
    LayoutModule,
    FiltersModule,
    UiSharedModule,
    TextFieldModule,
    ReactiveFormsModule,
    PortfolioRoutingModule,
    NgxsActionsExecutingModule.forRoot(),
    ExpertModule,
    NgxsModule.forFeature([PortfolioState, TechnicalTaskState]),
  ],
  providers: [
    PortfolioAPIService,
    PortfolioParamsService,
    PortfolioStatusService,
  ],
})
export class PortfolioModule { }
