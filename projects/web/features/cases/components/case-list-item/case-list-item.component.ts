import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CaseInterface } from '@web/features/cases/models/case.interface';

@Component({
  selector: 'app-case-list-item',
  templateUrl: './case-list-item.component.html',
  styleUrls: ['./case-list-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CaseListItemComponent implements OnInit {
  @Input() item: CaseInterface;
  @Input() currentLayout: 'sm' | 'md' | 'lg';
  @Input() listLayout: string;
  @Input() checked = false;
  @Output() check = new EventEmitter<string>();
  @Output() detail = new EventEmitter<CaseInterface>();

  readonly trackColors = ['yellow', 'blue', 'purple', 'dark-green', 'green', 'green', 'green'];

  constructor() { }

  ngOnInit(): void {
  }

  checkCase(id: string) {
    this.check.emit(id);
  }

  onDetail() {
    this.detail.emit(this.item);
  }
}
