import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CasesFiltersComponent } from './cases-filters.component';

describe('CasesFiltersComponent', () => {
  let component: CasesFiltersComponent;
  let fixture: ComponentFixture<CasesFiltersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CasesFiltersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CasesFiltersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
