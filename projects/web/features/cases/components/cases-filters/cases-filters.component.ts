import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
} from '@angular/core';
import { CasesStateModel } from '@web/features/cases/states/cases.state';
import { Filters } from '@ui/filters/models/filters.interface';
import { ReplaySubject } from 'rxjs';
import { FiltersValueInterface } from '@web/features/cases/models/filters-value.interface';
import {EnvService} from '../../../../../app-ipid/src/app/shared/services/env.service';

@Component({
  selector: 'app-cases-filters',
  templateUrl: './cases-filters.component.html',
  styleUrls: ['./cases-filters.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CasesFiltersComponent implements OnInit {

  @Input() state: CasesStateModel;

  @Input() layout: 'sm' | 'md' | 'lg' = 'md';

  @Input() filterValue: Record<string, any>;

  @Output() changeFilter = new EventEmitter();

  private _clearFilter = new ReplaySubject<void>();
  clearFilter$ = this._clearFilter.asObservable();

  readonly listLayout = 'minmax(auto, 2fr) 1fr 1fr 1.5fr 28px';

  def: Filters<any[]> | null = null;

  lg: Array<string>;

  constructor(
    private envService: EnvService
  ) {
  }

  ngOnInit(): void {
    this.setDef();
  }

  setDef() {
    this.def = [
      {
        type: 'input',
        name: 'searchQuery',
        placeholder: 'Поиск по тексту',
      },
      {
        type: 'select',
        name: 'types',
        placeholder: 'Тип работ:',
        options: this.filterTypes,
      },
      {
        type: 'select',
        name: 'statuses',
        placeholder: 'Статус:',
        options: this.filterStatuses,
      },
      {
        type: 'select',
        name: 'tags',
        placeholder: 'Тэги:',
        options: this.filterTags,
      },
      {
        type: 'select',
        name: 'clients',
        placeholder: 'Правообладатель:',
        options: this.filterClients,
      },
    ];
    if (this.def?.length && !this.envService.isX5) {
      this.def.splice(3, 1);
    }
    switch (this.def.length) {
      case 4:
        this.lg = ['1.5fr', '1fr', '1fr', '1fr'];
        break;
      case 5:
        this.lg = ['1.5fr', '1fr', '1fr', '1fr', '1fr'];
        break;
    }
  }

  applyFilter(event: Record<string, any> | FiltersValueInterface) {
    const currentFiltersValue = {...event};
    if (currentFiltersValue.statuses?.length) {
      currentFiltersValue.statuses = currentFiltersValue.statuses.map((statusName => {
        const index = this.state.filterData.statuses.findIndex((status) => status.name === statusName);
        return this.state.filterData.statuses[index].code;
      }));
    }
    if (currentFiltersValue.tags?.length) {
      currentFiltersValue.tags = currentFiltersValue.tags.map((tagName => {
        const index = this.state.filterData.tags.findIndex((tag) => tag.name === tagName);
        return this.state.filterData.tags[index].code;
      }));
    }
    if (currentFiltersValue.types?.length) {
      currentFiltersValue.types = currentFiltersValue.types.map((typeName => {
        const index = this.state.filterData.types.findIndex((type) => type === typeName);
        return this.state.filterData.types[index];
      }));
    }
    if (currentFiltersValue.clients?.length) {
      currentFiltersValue.clients = currentFiltersValue.clients.map((clientName => {
        const index = this.state.filterData.clients.findIndex((client) => client.name === clientName);
        return this.state.filterData.clients[index].code;
      }));
    }
    this.changeFilter.emit(currentFiltersValue);
  }

  clearFilter() {
    this._clearFilter.next();
  }

  get filterTypes(): { id: number, label: string }[] {
    return this.state.filterData.types.map((item, index) => {
      return { id: index, label: item };
    });
  }

  get filterStatuses(): { id: number, label: string }[] {
    return this.state.filterData.statuses.map((item) => {
      return { id: item.code, label: item.name };
    });
  }

  get filterClients(): { id: number, label: string, code: string }[] {
    return this.state.filterData.clients.map((item, index) => {
      return { id: index, label: item.name, code: item.code };
    });
  }

  get filterTags(): { id: number, label: string}[] {
    return this.state.filterData.tags.map((item, index) => {
      return { id: index, label: item.name };
    });
  }

  get isFiltersApplied(): boolean {
    return this.filterValue?.clients?.length || this.filterValue?.tags?.length
      || this.filterValue?.types?.length || this.filterValue?.statuses?.length || this.filterValue?.searchQuery?.length;
  }
}
