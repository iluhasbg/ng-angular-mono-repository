import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
} from '@angular/core';
import { CasesStateModel } from '@web/features/cases/states/cases.state';
import { DownloadCasesParamsInterface } from '@web/features/cases/models/download-cases.interface';
import { CaseInterface } from '@web/features/cases/models/case.interface';

@Component({
  selector: 'app-cases',
  templateUrl: './cases.component.html',
  styleUrls: ['./cases.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CasesComponent implements OnInit {

  @Input() state: CasesStateModel;

  @Input() layout: 'sm' | 'md' | 'lg' = 'md';

  @Input() filterValue: Record<string, any>;

  @Output() changeFilter = new EventEmitter();

  @Output() changePage = new EventEmitter<number>();

  @Input() currentPage: number;

  @Output() download = new EventEmitter<DownloadCasesParamsInterface>();

  @Output() detail = new EventEmitter<CaseInterface>();

  readonly listLayout = 'minmax(auto, 2fr) 1fr 1fr 1.5fr 28px';

  isAllChecked = false;

  checked = [];

  constructor() { }

  ngOnInit(): void {
  }

  applyFilter(event: Record<string, any>) {
    this.changeFilter.emit(event);
  }

  onChangePage(page: number) {
    this.changePage.emit(page);
  }

  checkAll() {
    this.isAllChecked = !this.isAllChecked;
    if (!this.isAllChecked) {
      this.checked = [];
    }
  }

  onCheck(id: string) {
    if (this.checked.includes(id)) {
      this.checked = this.checked.filter(value => value !== id);
    } else {
      this.checked.push(id);
    }
  }

  onDetail(caseItem: CaseInterface) {
    this.detail.emit(caseItem);
  }

  downloadReport() {
    if (!this.state.isDownloading && (this.isAllChecked || this.checked.length)) {
      this.download.emit(this.isAllChecked ? {every: true} : {caseID: this.checked.join(',')});
    }
  }
}
