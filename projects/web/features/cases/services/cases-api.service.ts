import { Injectable } from '@angular/core';
import { UraitApiService } from '@web/core/services/urait-api.service';
import { CasesResponseInterface } from '@web/features/cases/models/case.interface';
import { DownloadCasesResponseInterface } from '@web/features/cases/models/download-cases.interface';

@Injectable()
export class CasesApiService {
  private readonly url = 'cases';

  constructor(private apiService: UraitApiService) {}

  getCases(params): Promise<CasesResponseInterface> {
    return this.apiService.get(this.url, { params: {...params, version: 2} });
  }

  getDownloadLink(params: {cases?: string[], all?: boolean}): Promise<DownloadCasesResponseInterface> {
    return this.apiService.post<DownloadCasesResponseInterface>(
      `${this.url}/report`, params
    );
  }
}
