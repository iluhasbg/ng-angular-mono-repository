import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { CASES_STATE_TOKEN, CasesState } from '@web/features/cases/states/cases.state';
import { CasesActions } from '@web/features/cases/states/cases.actions';
import { AppState } from '@web/core/states/app.state';
import { DownloadCasesParamsInterface } from '@web/features/cases/models/download-cases.interface';
import { FiltersValueInterface } from '@web/features/cases/models/filters-value.interface';
import { CaseInterface } from '@web/features/cases/models/case.interface';

@Component({
  selector: 'app-cases-container',
  templateUrl: './cases-container.component.html',
  styleUrls: ['./cases-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CasesContainerComponent implements OnInit {
  state$ = this.store.select(CASES_STATE_TOKEN);
  layout$ = this.store.select(AppState.getLayout);
  currentPage$ = this.store.select(CasesState.currentPage);

  filterValue: Record<string, any> = {};

  constructor(private store: Store) { }

  ngOnInit(): void {
    this.store.dispatch(new CasesActions.Init());
  }

  changeFilter(filter: FiltersValueInterface) {
    this.store.dispatch(new CasesActions.ChangeFilters(filter));
  }

  download(params: DownloadCasesParamsInterface) {
    this.store.dispatch(new CasesActions.Download(params));
  }

  changePage(page: number) {
    this.store.dispatch(new CasesActions.ChangePage(page));
  }

  detail(caseItem: CaseInterface) {
    this.store.dispatch(new CasesActions.Detail(caseItem));
  }
}
