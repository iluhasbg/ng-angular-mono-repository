export interface FiltersValueInterface {
  searchQuery: string | null;
  clients: string[];
  tags: string[];
  types: string[];
  statuses: number[];
}
