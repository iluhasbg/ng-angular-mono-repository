export interface FilterDataInterface {
  clients: FilterDataClient[];
  types: string[];
  tags: FilterDataClient[];
  statuses: FilterDataStatus[];
}

export interface FilterDataClient {
  name: string;
  code: string;
}

export interface FilterDataStatus {
  name: string;
  code: number;
}
