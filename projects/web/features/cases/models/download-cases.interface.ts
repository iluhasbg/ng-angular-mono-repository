export interface DownloadCasesParamsInterface {
  caseID?: string;
  every?: true;
}

export interface DownloadCasesResponseInterface {
  report: string;
}
