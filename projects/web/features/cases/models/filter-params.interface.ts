export interface FilterParamsInterface {
  limit?: number;
  offset?: number;
}
