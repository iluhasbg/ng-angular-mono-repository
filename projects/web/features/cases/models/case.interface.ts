import { FilterDataClient, FilterDataStatus } from '@web/features/cases/models/filter-data.interface';

export interface CasesResponseInterface {
  cases: CaseInterface[];
  clients: FilterDataClient[];
  filteredCount: number;
  statuses: FilterDataStatus[];
  tags: FilterDataClient[];
  totalCount: number;
  types: string[];
}

export interface CaseInterface {
  ID: string;
  number: string;
  name: string;
  description: string;
  caseType: number;
  progress: number;
  status: string;
  toAgreement: boolean;
  imgSrc: string;
  tracks: TrackInterface[];
  tags?: { name: string; code: string }[];
  toReport: boolean;
  client?: string;
}

export interface TrackInterface {
  name: string;
  reach: boolean;
  first: boolean;
  date: string;
  stagedate: string;
}

