import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CasesContainerComponent } from '@web/features/cases/containers/cases-container/cases-container.component';

const routes: Routes = [
  {
    path: '',
    component: CasesContainerComponent,
    pathMatch: 'full',
  },
  // {
  //   path: ':id',
  //   component: CaseItemContainerComponent
  // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CasesRoutingModule {}
