import { DownloadCasesParamsInterface } from '@web/features/cases/models/download-cases.interface';
import { FiltersValueInterface } from '@web/features/cases/models/filters-value.interface';
import { CaseInterface } from '@web/features/cases/models/case.interface';

export namespace CasesActions {

  export class Init {
    static readonly type = '[cases] Init';
  }

  export class GetCases {
    static readonly type = '[cases] GetCases';
  }

  export class ChangeFilters {
    constructor(public filters: FiltersValueInterface) {}

    static readonly type = '[cases] ChangeFilters';
  }

  export class ChangePage {
    constructor(public page: number) {}

    static readonly type = '[cases] ChangePage';
  }

  export class Download {
    static readonly type = '[cases] Download';

    constructor(public params?: DownloadCasesParamsInterface) {}
  }

  export class Detail {
    static readonly type = '[cases] Detail';

    constructor(public caseItem: CaseInterface) {}
  }
}
