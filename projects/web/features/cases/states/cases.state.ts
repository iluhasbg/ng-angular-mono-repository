import { CaseInterface } from '@web/features/cases/models/case.interface';
import { Action, Selector, State, StateContext, StateToken } from '@ngxs/store';
import { Injectable } from '@angular/core';
import { CasesActions } from '@web/features/cases/states/cases.actions';
import { CasesApiService } from '@web/features/cases/services/cases-api.service';
import { FilterDataInterface } from '@web/features/cases/models/filter-data.interface';
import { FiltersValueInterface } from '@web/features/cases/models/filters-value.interface';
import { Navigate } from '@ngxs/router-plugin';
import { downloadBase64Xlsx } from '../../../../app-ipid/src/app/shared/utils/download-base-file';
import { APP_ROUTES_NAMES } from '@web/core/models/app-routes-names';

export const CASES_STATE_TOKEN = new StateToken<CasesStateModel>('cases');

@State({
  name: CASES_STATE_TOKEN,
  defaults: {
    isLoadingPage: true,
    isLoadingCases: false,
    isDownloading: false,
    error: null,
    cases: [],
    filterData: {
      clients: [],
      types: [],
      tags: [],
      statuses: [],
    },
    filtersValue: {
      searchQuery: null,
      clients: [],
      tags: [],
      types: [],
      statuses: [],
    },
    totalCount: 0,
    filteredCount: 0,
    limit: 10,
    offset: 0,
  }
})
@Injectable()
export class CasesState {
  constructor(private casesApi: CasesApiService) {}

  @Selector([CasesState])
  static currentPage(state: CasesStateModel): number {
    return Math.floor(state.offset / state.limit) + 1;
  }

  @Action(CasesActions.Init)
  async init(ctx: Context) {
    const { limit, offset } = ctx.getState();
    try {
      const casesResponse = await this.casesApi.getCases({ limit, offset } );
      ctx.patchState({
        filterData: {
          clients: casesResponse.clients,
          types: casesResponse.types,
          tags: casesResponse.tags,
          statuses: casesResponse.statuses,
        },
        totalCount: casesResponse.totalCount,
        filteredCount: casesResponse.filteredCount,
        cases: casesResponse.cases,
        isLoadingPage: false,
      });
    } catch (error) {
      ctx.patchState({ isLoadingPage: false, error });
    }
  }

  @Action(CasesActions.GetCases)
  async getCases(ctx: Context) {
    ctx.patchState({isLoadingCases: true, error: null});
    const { filtersValue, limit, offset } = ctx.getState();
    try {
      const params = this.removeEmptyFilters(filtersValue);
      const casesResponse = await this.casesApi.getCases({ ...params, limit, offset });
      ctx.patchState({isLoadingCases: false, cases: casesResponse.cases, filteredCount: casesResponse.filteredCount });
    } catch (error) {
      ctx.patchState({isLoadingCases: false, error});
    }
  }

  @Action(CasesActions.Download)
  async download(ctx: Context, { params }: CasesActions.Download) {
    ctx.patchState({isDownloading: true, error: null});
    try {
      const requestBody = {
        all: params.every || false,
        cases: params.caseID ? params.caseID.split(',') : []
      };
      const response = await this.casesApi.getDownloadLink(requestBody);
      downloadBase64Xlsx(response.report, 'Отчет');
      ctx.patchState({isDownloading: false});
    } catch (error) {
      ctx.patchState({isDownloading: false, error});
    }
  }

  @Action(CasesActions.ChangeFilters)
  async changeFilters(ctx: Context, { filters }: CasesActions.ChangeFilters) {
    ctx.patchState({ isLoadingCases: true, error: null, filtersValue: filters, limit: 10, offset: 0});
    ctx.dispatch(new CasesActions.GetCases());
  }

  @Action(CasesActions.ChangePage)
  async changePage(ctx: Context, { page }: CasesActions.ChangePage) {
    const { limit } = ctx.getState();
    ctx.patchState({ isLoadingCases: true, error: null, offset: (page - 1) * limit});
    ctx.dispatch(new CasesActions.GetCases());
  }

  @Action(CasesActions.Detail)
  async caseDetail(ctx: Context, { caseItem }: CasesActions.Detail) {
    return ctx.dispatch(
      new Navigate([`/${APP_ROUTES_NAMES.CASES}/${APP_ROUTES_NAMES.CASES_ITEM}/${caseItem.ID}`],
        { isDraft: caseItem.caseType === 0 }
      )
    );
  }


  removeEmptyFilters(filters: FiltersValueInterface) {
    const newFilter: any = {};
    if (filters.clients?.length) {
      newFilter.clients = filters.clients;
    }
    if (filters.searchQuery?.length) {
      newFilter.searchQuery = filters.searchQuery;
    }
    if (filters.statuses?.length) {
      newFilter.statuses = filters.statuses;
    }
    if (filters.tags?.length) {
      newFilter.tags = filters.tags;
    }
    if (filters.types?.length) {
      newFilter.types = filters.types;
    }
    return newFilter;
  }
}

type Context = StateContext<CasesStateModel>;

export interface CasesStateModel {
  isLoadingPage: boolean;
  isLoadingCases: boolean;
  isDownloading: boolean;
  cases: CaseInterface[];
  error: string | null;
  filterData: FilterDataInterface;
  filtersValue: FiltersValueInterface;
  offset: number;
  limit: number;
  totalCount: number;
  filteredCount: number;
}
