import { NgModule } from '@angular/core';
import { CasesContainerComponent } from './containers/cases-container/cases-container.component';
import { CasesComponent } from './components/cases/cases.component';
import { NgxsModule } from '@ngxs/store';
import { CasesRoutingModule } from '@web/features/cases/cases-routing.module';
import { CommonModule } from '@angular/common';
import { CasesState } from '@web/features/cases/states/cases.state';
import { CasesApiService } from '@web/features/cases/services/cases-api.service';
import { FiltersModule } from '@ui/filters/filters.module';
import { UiSharedModule } from '@ui/shared/ui-shared.module';
import { ListModule } from '@ui/list/list.module';
import { SharedModule } from '../../../app-ipid/src/app/shared/shared.module';
import { CaseListItemComponent } from './components/case-list-item/case-list-item.component';
import { WebModule } from '@web/web.module';
import { FilterState } from '@ui/filters/states/filter.state';
import { CasesFiltersComponent } from '@web/features/cases/components/cases-filters/cases-filters.component';
import { LayoutModule } from '../../../app-ipid/src/app/pages/layout/layout.module';
import {ExpertModule} from '../../../app-ipid/src/app/pages/layout/expert/expert.mdoule';

@NgModule({
  declarations: [
    CasesContainerComponent,
    CasesComponent,
    CaseListItemComponent,
    CasesFiltersComponent,
  ],
    imports: [
        NgxsModule.forFeature([CasesState, FilterState]),
        CasesRoutingModule,
        CommonModule,
        FiltersModule,
        UiSharedModule,
        ListModule,
        SharedModule,
        WebModule,
        LayoutModule,
        ExpertModule
    ],
  providers: [
    CasesApiService,
  ]
})
export class CasesModule {}
