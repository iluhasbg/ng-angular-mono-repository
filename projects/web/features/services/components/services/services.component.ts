import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { ServicesStateModel } from '../../states/services.state';
import { Filters } from '@ui/filters/models/filters.interface';
import { ServiceInterface } from '@web/features/services/models/service.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServicesComponent {

  @Input() def!: Filters<any[]>;

  @Input() state!: ServicesStateModel;

  @Input() filtersValue!: Record<string, any>;

  @Output() apply = new EventEmitter<Record<string, any>>();

  constructor(private router: Router) {}

  onApply(value: Record<string, any>) {
    this.apply.emit(value);
  }

  navigateToService(card: ServiceInterface) {
    this.router.navigate(['services', card.id]);
  }
}
