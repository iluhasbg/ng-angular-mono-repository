import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ServicesStateModel } from '@web/features/services/states/services.state';
import { OpenedServiceInterface, RateInterface } from '@web/features/services/models/service.interface';
import { Router } from '@angular/router';
import { IntercomEventsService, INTERCOM_EVENT_NAME } from 'projects/shared/services/intercom-events.service';

@Component({
  selector: 'app-service-item',
  templateUrl: './service-item.component.html',
  styleUrls: ['./service-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServiceItemComponent {

  isLoading = false;
  service: OpenedServiceInterface;
  current: RateInterface | null = null;

  noPackages = false;

  @Input() id!: string;

  @Input() set state(value: ServicesStateModel) {
    this.isLoading = value.isLoading;
    if (value.openedService) {
      this.service = value.openedService;
      if (
        value.openedService.rates
        && value.openedService.rates.length > 0
        && !value.openedService.rates[0]?.id
        && !value.openedService.rates[0]?.name
      ) {
        this.noPackages = true;
      }
      this.current = value.openedService.rates[0];
    }
  }

  constructor(
    private readonly router: Router,
    private readonly intercomEventsService: IntercomEventsService,
  ) {
    window.scroll(0, 0);
  }

  toSelectPackage(index: number) {
    this.current = this.service.rates[index];
  }

  toLeadForm(title: string) {
    const data = {
      formIdentity: title,
      serviceId: this.id,
      price: this.current.boards[0].value,
      serviceName: this.service.title,
      tariff: this.current.name,
    };
    this.intercomEventsService.push({
      event: INTERCOM_EVENT_NAME.CLICK_SERVICE_CALL2ACTION,
      button_name: title === 'Спросить у эксперта' ? title : 'Заказать услугу',
      service_name: this.service.title,
      price: this.current.boards[0].value,
      tariff: this.current.name,
      service_id: this.id
    });
    this.router.navigate(
      ['/treaty-extension'],
      {queryParams: {data: JSON.stringify(data)}}
    );
  }
}
