import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ServicesContainerComponent } from './containers/services-container/services-container.component';
import { ServiceItemContainerComponent } from './containers/service-item-container/service-item-container.component';

const routes: Routes = [
  {
    path: '',
    component: ServicesContainerComponent,
    pathMatch: 'full',
  },
  {
    path: ':id',
    component: ServiceItemContainerComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServicesRoutingModule {
}
