import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ServicesRoutingModule } from './services-routing.module';
import { ServicesContainerComponent } from './containers/services-container/services-container.component';
import { ServiceItemContainerComponent } from './containers/service-item-container/service-item-container.component';
import { NgxsModule } from '@ngxs/store';
import { ServicesComponent } from './components/services/services.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UiSharedModule } from '@ui/shared/ui-shared.module';
import { FiltersModule } from '@ui/filters/filters.module';
import { ServiceItemComponent } from './components/service-item/service-item.component';
import { ListModule } from '@ui/list/list.module';
import { LayoutModule } from '../../../app-ipid/src/app/pages/layout/layout.module';
import {ExpertModule} from '../../../app-ipid/src/app/pages/layout/expert/expert.mdoule';

@NgModule({
  declarations: [
    ServicesContainerComponent,
    ServiceItemContainerComponent,
    ServicesComponent,
    ServiceItemComponent,
  ],
    imports: [
        CommonModule,
        ServicesRoutingModule,
        NgxsModule.forFeature([]),
        FormsModule,
        ReactiveFormsModule,
        UiSharedModule,
        FiltersModule,
        ListModule,
        LayoutModule,
        ExpertModule
    ],
  providers: [],
})
export class ServicesModule {}
