import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Filters } from '@ui/filters/models/filters.interface';
import { objectTypesOptions } from '../../filters/object-types.options';
import { departmentsOptions } from '@web/features/services/filters/departments.options';
import { Store } from '@ngxs/store';
import { ServicesActions } from '@web/features/services/states/services.actions';
import { SERVICES_STATE_TOKEN } from '@web/features/services/states/services.state';
import { SectionInterface } from '@web/features/services/models/cards.interface';
import { ServiceInterface } from '@web/features/services/models/service.interface';

@Component({
  selector: 'app-services-container',
  templateUrl: './services-container.component.html',
  styleUrls: ['./services-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ServicesContainerComponent implements OnInit {
  def: Filters<any[]> = [
    {
      type: 'input',
      name: 'search',
      placeholder: 'Поиск по услугам',
      filterItem: (item, search) => {
        return search ? item.name.toLowerCase().includes(search.toLowerCase()) : true;
      }
    },
    {
      type: 'select',
      name: 'objectType',
      placeholder: 'Вид объекта:',
      options: objectTypesOptions,
      filterItem: (section: SectionInterface, objectType) => {
        return objectType ? objectType.includes(section.title) || !objectType.length : true;
      }
    },
    {
      type: 'select',
      name: 'gosDepartment',
      placeholder: 'Государственный орган:',
      options: departmentsOptions,
      filterItem: (service: ServiceInterface, gosDepartment) => {
        return gosDepartment && service.stateBody ? !!(service.stateBody.find(item => gosDepartment.includes(item.id))
          || !gosDepartment.length) : true;
      }
    },
  ];

  state$ = this.store.select(SERVICES_STATE_TOKEN);

  filtersValue = {};

  constructor(private store: Store) {
    this.store.dispatch(new ServicesActions.Init());
  }

  ngOnInit(): void {}

  onApply(filters) {
    this.filtersValue = {...filters};
  }
}
