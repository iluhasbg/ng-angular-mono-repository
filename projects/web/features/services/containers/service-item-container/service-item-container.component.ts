import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { SERVICES_STATE_TOKEN } from '@web/features/services/states/services.state';
import { ActivatedRoute, Router } from '@angular/router';
import { switchMap, take, tap } from 'rxjs/operators';
import { ServicesActions } from '@web/features/services/states/services.actions';

@Component({
  selector: 'app-service-item-container',
  templateUrl: './service-item-container.component.html',
  styleUrls: ['./service-item-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServiceItemContainerComponent implements OnInit {
  state$ = this.store.select(SERVICES_STATE_TOKEN);
  id: string;

  constructor(
    private store: Store,
    private activatedRoute: ActivatedRoute,
    private router: Router,
  ) {}

  ngOnInit(): void {
    this.activatedRoute.params
      .pipe(
        take(1),
        switchMap(({id}) => {
          this.id = id;
          return this.store.dispatch(new ServicesActions.LoadDetails(id));
        }),
        tap(globalState => {
          if (!globalState.services.openedService && globalState.services.error) {
            this.router.navigate(['/services']);
          }
        })
      ).subscribe();
  }
}
