import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceItemContainerComponent } from './service-item-container.component';

describe('ServiceItemContainerComponent', () => {
  let component: ServiceItemContainerComponent;
  let fixture: ComponentFixture<ServiceItemContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ServiceItemContainerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceItemContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
