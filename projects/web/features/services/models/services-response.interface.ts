import { OpenedServiceInterface } from '@web/features/services/models/service.interface';

export interface ServicesResponseInterface<T> {
  data: {
    items: T[]
  } | null;
  error: {
    code: string,
    messages: string[]
  } | null;
}

export interface OpenedServiceResponse {
  data: OpenedServiceInterface | null;
  error: {
    code: string,
    messages: string[]
  } | null;
}
