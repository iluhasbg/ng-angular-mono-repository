import { ServiceInterface } from './service.interface';

export interface SectionInterface {
  cards: CardInterface[];
  title: string;
}

export interface CardInterface {
  title: string;
  description: string;
  links: ServiceInterface[];
}
