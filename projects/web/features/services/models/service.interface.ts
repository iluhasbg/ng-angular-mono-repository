export interface ServiceInterface {
  id: string;
  name: string;
  stateBody?: {
    id: string;
  }[];
}

export interface OpenedServiceInterface {
  fastRegistration: boolean;
  rates: RateInterface[];
  title: string;
}

export interface RateInterface {
  about: AboutInterface;
  boards: BoardInterface[];
  default: boolean;
  id: string;
  informationPrice: InformationPriceInterface;
  name: string;
  recommendation: RecommendationInterface;
  serviceComposition: ServiceCompositionInterface[];
}

interface AboutInterface {
  items: {
    name: string;
    value: string;
  }[];
  title: string;
}

interface BoardInterface {
  title: string;
  description: string;
  value: string;
}

interface InformationPriceInterface {
  description: string;
  leftTitle: string;
  rightTitle: string;
  tablePrice: {
    leftValue: string;
    name: string;
    rightValue: string;
  }[];
  title: string;
}

interface RecommendationInterface {
  text: string;
  title: string;
}

interface ServiceCompositionInterface {
  item: string[];
  title: string;
}
