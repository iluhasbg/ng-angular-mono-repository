import { IFilterOption } from '@ui/filters/models/filters.interface';

export const departmentsOptions: IFilterOption[] = [
  {
    id: 7,
    label: 'Роспатент',
  },
  {
    id: 8,
    label: 'Арбитражный суд',
  },
  {
    id: 9,
    label: 'Суд по интеллектуальным правам',
  },
  {
    id: 10,
    label: 'Палата по патентным спорам (ППС)',
  },
  {
    id: 11,
    label: 'Федеральная таможенная служба',
  },
  {
    id: 12,
    label: 'Федеральная антимонопольная служба',
  },
  {
    id: 13,
    label: 'ВОИС',
  },
  {
    id: 14,
    label: 'РАО КОПИРУС',
  },
  {
    id: 15,
    label: 'Роспатент (ФИПС)',
  },
];
