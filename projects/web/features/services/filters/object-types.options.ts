import { IFilterOption } from '@ui/filters/models/filters.interface';

export const objectTypesOptions: IFilterOption[] = [
  {
    id: 1,
    label: 'Регистрация товарного знака',
  },
  {
    id: 2,
    label: 'Патентование',
  },
  {
    id: 3,
    label: 'Суды и споры',
  },
  {
    id: 4,
    label: 'Договоры',
  },
  {
    id: 5,
    label: 'Другие услуги',
  },
];
