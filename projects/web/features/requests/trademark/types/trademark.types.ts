
import { REQUEST_TYPES } from '@web/core/models/user.interface';
import { Tariff } from '@ui/features/tariffes/models/rates.interfaces';
import { PAYMENT_TYPE } from '@ui/features/payment/types/payment.types';

export type RequestType = REQUEST_TYPES.TRADEMARK_SEARCH | REQUEST_TYPES.TRADEMARK;

export interface LoadTrademarkDraftResponse {
  case: TrademarkDraftCase;
}

export interface TrademarkDraftCase {
  tasks: TrademarkDraftTasks;
}

export interface TrademarkDraftTasks {
  items: TrademarkDraft[];
}

export interface TrademarkDraft {
  ID: string;
  inn: string;
  number: number;
  tariff: string;
  director: string;
  lastStage: number;
  caseStage: string;
  legalAdress: string;
  type: REQUEST_TYPES;
  info: TrademarkDraftInfo[];
  details: TrademarkDraftDetails;
  promocode: TrademarkDraftPromocode;
}

export interface TrademarkDraftPromocode {
  id: string;
  code: string;
}

export interface TrademarkDraftDetails {
  title: string;
  imgSrc: string
}

export interface TrademarkDraftInfo {
  text: string;
  title: string;
}

export interface UploadImagePayload {
  body: UploadImagePayloadBody;
}

export interface UploadImagePayloadBody {
  name: string;
  image: string;
}

export interface UploadImageResponse {
  path: string;
}

export interface TmForm {
  imageText: string;
}

export interface OwnerForm {
  name: string;
  head: string;
  address: string;
}

export interface TINForm {
  TIN: string;
}

export interface SearchLegalPayload {
  id: string;
}

export interface SearchLegalResponse {
  hits: {
    hits: SearchLegalResponseHits[];
  };
}

export interface SearchLegalResponseHits {
  _id: string;
  _index: string;
  _type: string;
  _source: SearchLegalResponseSourse;
}

export interface SearchLegalResponseSourse {
  id?: string;
  TIN: string;
  TRRC: string;
  PSRN: number;
  name: string;
  address: string;
  fullName: string;
  dateOfPSRN: string;
  directors: Director[];
  OKVEDCodes: OKVEDCode[];
  addressHistory: AddressHistory[];
}

export interface Director {
  TIN: string;
  name: string;
  surname: string;
  recordID: number;
  postTitle: string;
  recordDate: string;
  patronymic: string;
}

export interface OKVEDCode {
  code: string;
  description?: string;
}

export interface OptionsMKTU {
  search: 'mktu';
  selectedMktu: number[];
  onlyClassSelect: boolean;
}

export interface LastRequestTrademark {
  _id: string;
  _index: string;
  _source: LastRequestTrademarkSource;
}

export interface LastRequestTrademarkSource {
  TIN: number;
  PSRN: number;
  imageText: string;
  imageUrl?: string;
  applicantName: string;
  applicantStatus: number;
  applicationDate: string;
  registrationDate: string;
  markImageFileName: string;
  applicationString: string;
  registrationNumber: number;
  registrationString: string;
  goodsServices: Array<{ classNumber: number }>;
}

export interface AddressHistory {
  regionType: string;
  regionName: string;
}

export interface LoadDocumentByIdResponse {
  took: number;
  timed_out: boolean;
  hits: LoadDocumentByIdResponseHits;
}

export interface LoadDocumentByIdResponseHits {
  total: unknown;
  max_score: number;
  hits: LoadDocumentByIdResponseHitsHits[];
}

export interface LoadDocumentByIdResponseHitsHits {
  _id: string;
  _type: string;
  _index: string;
  _score: number;
  _source: LoadDocumentByIdSource;
}

export interface LoadDocumentByIdSource {
  goodsServices: GoodService[];
}

export interface GoodService {
  classNumber: number;
  gsDescription: string;
  subClasses: { subclassNumber: number }[];
}

export interface InterestsData {
  clientId: string;
  fromUrl: string;
  inUrl: string;
  utmCampaign: string;
  utmContent: string;
  utmMedium: string;
  utmSource: string;
  utmTerm: string;
}

export interface CreateTrademarkDraftOwner {
  id?: string;
  inn: string;
  name: string;
  ogrn: string;
  address: string;
  director: string;
  shortName: string;
  isNonResident: boolean;
}

export interface UpdateTrademarkDraft {
  stage: number;
  mktu?: MKTU[];
  sum?: string;
  inn?: string;
	name?: string;
	ogrn?: string;
  imgURL?: string;
  address?: string;
  tariffID?: string;
  responseID?: string;
  promocode?: string;
  okved?: OKVEDCode[];
  designation?: string;
  interestData?: InterestsData;
  owner?: CreateTrademarkDraftOwner;
  type?: REQUEST_TYPES | PAYMENT_TYPE;
}

export interface LoadMktuByOkvedResponse {
  hits: LoadMktuByOkvedResponseHits;
}

export interface LoadMktuByOkvedResponseHits {
  hits: LoadMktuByOkvedResponseHitsHits[];
}

export interface LoadMktuByOkvedResponseHitsHits {
  _source: LoadMktuByOkvedResponseSource;
}

export interface LoadMktuByOkvedResponseSource {
  codeMKTU: number;
}

export interface MKTU {
  desc: string;
  name: string;
  number: number;
  selected: boolean;
}

export interface TariffPayload {
  responseID: string;
  promocode?: string;
}

export interface TariffResponse {
  duty: Duty;
  tariff: Tariff[];
}

export interface Duty {
  all: DutyAll;
  later: DutyLater;
  sumDiscount: number;
  sum: number;
  discount: number;
}

export interface DutyAll {
  sumDiscount: number;
  sum: number;
  discount: number;
}

export interface DutyLater {
  sumDiscount: number;
  sum: number;
  discount: number;
}

export interface TariffResult {
  paymentSum: number;
  sumBeforeDiscount: number;
}