import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'getShortOkved', pure: true })
export class GetShortOkvedPipe implements PipeTransform {
  public transform(value: string): string {
    return value.length > 40 ? value.slice(0, 40) + '...' : value;
  }
}
