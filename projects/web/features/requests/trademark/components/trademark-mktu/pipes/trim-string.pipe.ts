import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'trimString', pure: true })
export class TrimStringPipe implements PipeTransform {
  public transform(value: string, count: number): string {
    return value?.length > count ? value.slice(0, count) + '...' : value;
  }
}
