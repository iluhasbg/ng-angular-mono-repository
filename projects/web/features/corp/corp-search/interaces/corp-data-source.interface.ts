export interface ICorpDataSource {
  domains?: any[];
  legalNames?: any[];
  tradeMarks?: any[];
}

export type ICorpDataSourceType = 'tradeMarks' | 'domains' | 'legalNames';
