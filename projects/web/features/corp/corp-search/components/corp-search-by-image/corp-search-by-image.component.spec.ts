import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CorpSearchByImageComponent } from './corp-search-by-image.component';

describe('CorpSearchByImageComponent', () => {
  let component: CorpSearchByImageComponent;
  let fixture: ComponentFixture<CorpSearchByImageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CorpSearchByImageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CorpSearchByImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
