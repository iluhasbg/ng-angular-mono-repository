import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CorpSearchTmComponent } from './corp-search-tm.component';

describe('CorpSearchTmComponent', () => {
  let component: CorpSearchTmComponent;
  let fixture: ComponentFixture<CorpSearchTmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CorpSearchTmComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CorpSearchTmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
