import { TrademarkInterface } from '@web/features/corp/corp-search/states/models/search-image-response.interface';

export interface SearchNameResult {
  domains: SearchNameResponseInterface | null;
  legalNames: SearchNameResponseInterface | null;
  tradeMarks: SearchNameResponseInterface | null;
}


export interface SearchNameResponseInterface {
  hits: HitsSearchNameInterface;
  risks?: RisksSearchNameInterface;
  status?: number;
  timed_out?: boolean;
  took?: number;
}

export interface RisksSearchNameInterface {
  custom: number;
  legal_strategies: number;
  max_risk: string;
  mktu_completion: number;
  sign_reworking: number;
}

export interface HitsSearchNameInterface {
  hits: HitSearchNameInterface[];
  max_score?: number;
  total?: TotalSearchNameInterface;
}

export interface TotalSearchNameInterface {
  domains: number;
  from: number;
  legalEntities: number;
  patents: number;
  relation: string;
  size: number;
  tradeMarks: number;
  value: number;
}

export interface HitSearchNameInterface {
  matched_queries: string[];
  __indexNumber: number;
  _distance: number;
  _id: string;
  _index: string;
  _matched_length: number;
  _matches: number;
  _risk: string;
  _score: number;
  _soft_distance: number;
  _source: SourceDomainsSearchNameInterface | SourceLegalNamesSearchNameInterface | TrademarkInterface;
  _type: string;
  checked: boolean;
}

export interface SourceDomainsSearchNameInterface {
  domain: string;
  domainRegistrator: string;
  free: boolean;
  freeDate: string;
  fullDomain: string;
  payDate: string;
  registrationDate: string;
  zone: string;
}

export interface SourceLegalNamesSearchNameInterface {
  OKVEDCodes: OKVEDCodesInterface[];
  PSRN: number;
  TIN: string;
  TRRC: string;
  address: string;
  addressHistory: AddressHistoryInterface[];
  dateOfPSRN: string;
  directors: DirectorsInterface[];
  fullName: string;
  name: string;
}

export interface OKVEDCodesInterface {
  code: string;
  description: string;
}

export interface AddressHistoryInterface {
  regionName: string;
  regionType: string;
}

export interface DirectorsInterface {
  TIN: string;
  name: string;
  patronymic: string;
  postTitle: string;
  recordDate: string;
  recordID: number;
  surname: string;
}
