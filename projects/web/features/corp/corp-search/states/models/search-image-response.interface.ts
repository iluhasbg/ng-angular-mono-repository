export interface SearchImageResponseInterface {
  hits: HitsSearchImageInterface;
  status: number;
  timed_out: boolean;
  took: number;
}

export interface HitsSearchImageInterface {
  hits: TrademarksImageInterface[];
  max_score: number;
}

export interface TrademarksImageInterface {
  class_matching?: ClassMatchingInterface;
  _id?: string;
  _index?: string;
  _score?: number;
  _source?: TrademarkInterface;
  _type?: string;
  checked: boolean;
}

export interface ClassMatchingInterface {
  left: ClassInterface[];
  right: ClassInterface[];
}

export interface ClassInterface {
  code: string;
  type: number;
}

export interface TrademarkInterface {
  PSRN: number;
  TIN: string;
  applicantName: string;
  applicantStatus: number;
  applicationDate: string;
  applicationString: string;
  expiryDate: string;
  goodsServices: GoodServicesInterface[];
  imageUrl: string;
  imageText: string;
  markImageFileName: string;
  registrationCountryCode: string[];
  registrationDate: string;
  registrationNumber: number;
  registrationString: string;
}

export interface GoodServicesInterface {
  classNumber: number;
}

