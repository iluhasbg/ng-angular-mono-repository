export interface PaginationInterface {
  size: number;
  from: number;
}
