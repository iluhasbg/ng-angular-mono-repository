export interface ChangePageRequestInterface {
  page: number;
  size: number;
  type?: string;
}
