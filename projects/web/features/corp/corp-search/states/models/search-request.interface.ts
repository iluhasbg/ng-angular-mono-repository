export interface SearchRequestInterface {
  size?: number;
  from?: number;
  SHA1hash?: string;
  applicationDate?: string[];
  countryCodes?: string[];
  eaNumber?: string[];
  gsNumber?: number[];
  priorityDate?: string;
  registrationDate?: string[];
  searchDomains?: boolean;
  searchLegalEntities?: boolean;
  searchLegalId?: string;
  searchNumber?: string;
  text?: string;
  image?: ImageInterface;
  index?: string[];
}

export interface ImageInterface {
  filename: string;
  binary: string;
}
