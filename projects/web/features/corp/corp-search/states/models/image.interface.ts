export interface ImageInterface {
  imageBinary: string;
  eventBase64: string;
}
