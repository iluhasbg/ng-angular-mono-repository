import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CorpSearchHistoryFiltersComponent } from './corp-search-history-filters.component';

describe('CorpSearchHistoryFiltersComponent', () => {
  let component: CorpSearchHistoryFiltersComponent;
  let fixture: ComponentFixture<CorpSearchHistoryFiltersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CorpSearchHistoryFiltersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CorpSearchHistoryFiltersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
