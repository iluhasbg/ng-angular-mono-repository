import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HistorySearchItemComponent } from './history-search-item.component';

describe('HistorySearchItemComponent', () => {
  let component: HistorySearchItemComponent;
  let fixture: ComponentFixture<HistorySearchItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HistorySearchItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HistorySearchItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
