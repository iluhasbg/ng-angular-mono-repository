export interface HistoryPaginationInterface {
  limit: number;
  offset: number;
}
