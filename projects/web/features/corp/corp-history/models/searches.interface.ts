export interface SearchesInterface {
  count: number;
  countReport: number;
  date: string;
  right: boolean;
  searchID: string;
  searchParameters: SearchParametersInterface;
  user: string;
  userID: string;
}

export interface SearchParametersInterface {
  countriesList: CountriesInterface[];
  mktuList: MKTUInterface[];
  okvedList: OKVEDInterface[];
  searchLegalId: string;
  searchNumber: string;
  searchType: string;
  text: string;
  registrationDate: string;
  applicationDate: string;
  priorityDate: string;
  image: ImageSearchParamsInterface;
}

export interface ImageSearchParamsInterface {
  filename: string;
  url: string;
}

export interface CountriesInterface {
  checked: boolean;
  code: string;
  name: string;
  organizations: string;
  searchStr: string;
  selected: boolean;
}

export interface MKTUInterface {
  checkedMobile: boolean;
  desc: string;
  mktu: boolean;
  name: string;
  number: number;
  selected: boolean;
}

export interface OKVEDInterface {
  checkedMobile: boolean;
  description: string;
  okved: boolean;
  code: string;
  name: string;
  number: number;
  selected: boolean;
  _id: string;
}
