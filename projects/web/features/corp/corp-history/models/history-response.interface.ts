import { SearchesInterface } from '@web/features/corp/corp-history/models/searches.interface';
import { FilterDataUsers } from '@web/features/corp/corp-history/models/filters-data.interface';

export interface HistoryResponseInterface {
  filteredCount: number;
  searches: SearchesInterface[];
  totalCount: number;
  users: FilterDataUsers[];
}
