export interface FiltersValueInterface {
  users: string[];
  searchType: string[];
  searchQuery: string;
  startDate: string;
  finishDate: string;
  date?: string[];
}
