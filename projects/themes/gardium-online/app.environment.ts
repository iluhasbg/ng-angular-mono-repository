import { baseEnvironment } from 'projects/app-ipid/src/environments/base.environment';

export const environment = {
  ...baseEnvironment,

  BRAND: 'GARDIUM-ONLINE',
  TITLE: 'Гардиум.ОНЛАЙН',
  LOGO_PATH: '/assets/gardium-online/gardium_online_new.svg',
  LOGO2_PATH: '/assets/gardium-online/gardium_online_new.svg',
  LOGO_CASES_PATH: '/assets/gardium-online/gardium_online_new.svg',
  FOOTER_TEXT: `© ${new Date().getFullYear()} Гардиум.ОНЛАЙН. Все права защищены.`,
  STATIC_FILES: '/assets/static-files/gardium-online',

  // Константы для поиска /search страница
  SEARCH_STYLES: {
    SEARCH_POSIBILITY_TEXT: 'Высокая с Гардиум.ОНЛАЙН',
    SEARCH_SPEEDOMETER_BACKGROUND: '#FFFFFF',
    SEARCH_SPEEDOMETER_OUTLINE: '#E1E5EB',
  },
};
