import { baseEnvironment } from 'projects/app-ipid/src/environments/base.environment';

export const environment = {
  ...baseEnvironment,

  BRAND: 'TOCHKA',
  TITLE: 'IP-ID',
  LOGO_PATH: '/assets/images/Logo_IP_ID.svg',
  LOGO2_PATH: '/assets/images/logo-2.svg',
  FOOTER_TEXT: '© 2022 IP-ID. Все права защищены.',
  FAVICON: '/assets/fav/favicon.ipid.ico',
  STATIC_FILES: '/assets/static-files/tochka',

  // Константы для поиска /search страница
  SEARCH_STYLES: {
    SEARCH_POSIBILITY_TEXT: 'Высокая с IP-ID',
    SEARCH_SPEEDOMETER_BACKGROUND: '#FFFFFF',
    SEARCH_SPEEDOMETER_OUTLINE: '#E1E5EB',
  },
};
