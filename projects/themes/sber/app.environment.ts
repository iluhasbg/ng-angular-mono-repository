import { baseEnvironment } from 'projects/app-ipid/src/environments/base.environment';

export const environment = {
  ...baseEnvironment,

  BRAND: 'SBER',
  TITLE:
    'Гардиум.Онлайн — сервис по управлению интеллектуальной собственностью ',
  LOGO_PATH: '/assets/sber/images/sber-logo.svg',
  LOGO2_PATH: '/assets/sber/images/sber-logo.svg',
  LOGO_CASES_PATH: '/assets/sber/images/sber-logo.svg',
  FOOTER_TEXT: `© ${new Date().getFullYear()} Гардиум.Онлайн — партнер Сбербанка.`,
  FAVICON: '/assets/sber/images/favicon.png',
  STATIC_FILES: '/assets/static-files/sber',

  // Константы для поиска /search страница
  SEARCH_STYLES: {
    SEARCH_POSIBILITY_TEXT: 'Высокая с Гардиум.Онлайн',
    SEARCH_SPEEDOMETER_BACKGROUND: '#FFFFFF',
    SEARCH_SPEEDOMETER_OUTLINE: '#F2F4F7',
  },
};
