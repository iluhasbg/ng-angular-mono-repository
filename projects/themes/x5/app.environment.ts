import { environment as baseEnvironment } from 'projects/themes/ipid/app.environment';

export const environment = {
  ...baseEnvironment,
  corporate: true,

  BRAND: 'X5',
  TITLE: 'Гардиум.ПРО',
  LOGO_PATH: '/assets/x5/Logo_x5.svg',
  LOGO2_PATH: '/assets/x5/Logo_x5.svg',
  LOGO_CASES_PATH: '/assets/x5/Logo_x5.svg',
  FOOTER_TEXT: `© ${new Date().getFullYear()} Гардиум.ПРО. Все права защищены.`,
  STATIC_FILES: '/assets/static-files/x5',

  // Константы для поиска /search страница
  SEARCH_STYLES: {
    SEARCH_POSIBILITY_TEXT: 'Высокая с Гардиум.ПРО',
    SEARCH_SPEEDOMETER_BACKGROUND: '#FFFFFF',
    SEARCH_SPEEDOMETER_OUTLINE: '#E1E5EB',
  },
};
