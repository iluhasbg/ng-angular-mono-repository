import { baseEnvironment } from 'projects/app-ipid/src/environments/base.environment';

const colors = {
  $yellow_1c: '#FBDD60',
  $red_1c: '#FF1301',
  $blue_1c: '#198BFF',
  $gray_1c: '#9C9EA4',
  $gray0_1c: '#F6F6F6', // Самый светлый фон
  $gray2_1c: '#F1F1F1', // серый оттекающий фон
  $black_1c: '#505050', // Темный акцентный
};
export const environment = {
  ...baseEnvironment,
  BRAND: '1C-START',
  TITLE:
    'Всё о старте и ведении бизнеса в России простыми словами и с понятными примерами',
  LOGO_PATH: '/assets/1c-start/images/logo.svg',
  LOGO2_PATH: '/assets/1c-start/images/logo.svg',
  LOGO_CASES_PATH: '/assets/1c-start/images/logo.svg',
  FOOTER_TEXT: `© ${new Date().getFullYear()} 1С-Старт. Все права защищены.`,
  FAVICON: '/assets/fav/favicon.1c-start.ico',
  STATIC_FILES: '/assets/static-files/1c-start',

  // Константы для поиска /search страница
  SEARCH_STYLES: {
    SEARCH_POSIBILITY_TEXT: 'Высокая с 1С-Старт',
    SEARCH_SPEEDOMETER_BACKGROUND: '#F4F4F4',
    SEARCH_SPEEDOMETER_OUTLINE: 'rgba(80, 80, 80, 0.15)',
  },
};
