import { baseEnvironment } from 'projects/app-ipid/src/environments/base.environment';

export const environment = {
  ...baseEnvironment,

  BRAND: 'ALFA',
  TITLE:
    'Альфа-Банк - Гардиум.Онлайн: сервис для управления интеллектуальной собственностью',
  LOGO_PATH: '/assets/alfabank/logo-header-alfabank.svg',
  LOGO2_PATH: '/assets/alfabank/logo-alfabank.svg',
  LOGO_CASES_PATH: '/assets/alfabank/logo-alfabank.svg',
  FOOTER_TEXT: `© ${new Date().getFullYear()} Гардиум.Онлайн - партнер Альфа-Банка`,
  FAVICON: '/assets/fav/favicon.alfa.ico',
  STATIC_FILES: '/assets/static-files/alfa',

  // Константы для поиска /search страница
  SEARCH_STYLES: {
    SEARCH_POSIBILITY_TEXT: 'Высокая с Гардиум.Онлайн',
    SEARCH_SPEEDOMETER_BACKGROUND: '#FFFFFF',
    SEARCH_SPEEDOMETER_OUTLINE: '#E1E5EB',
  },
};
