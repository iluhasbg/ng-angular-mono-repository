import { baseEnvironment } from 'projects/app-ipid/src/environments/base.environment';

export const environment = {
  ...baseEnvironment,

  production: false,
  BRAND: 'TINKOFF',
  TITLE: 'Банк для малого и среднего бизнеса',
  LOGO_PATH: '/assets/tinkoff/images/logo.svg',
  LOGO2_PATH: '/assets/tinkoff/images/logo.svg',
  LOGO_CASES_PATH: '/assets/tinkoff/images/logo-black.svg',
  FOOTER_TEXT: '© 2022 АО «Тинькофф Банк». Все права защищены.',
  FAVICON: '/assets/fav/favicon.tinkoff.ico',
  STATIC_FILES: '/assets/static-files/tinkoff',

  // Константы для поиска /search страница
  SEARCH_STYLES: {
    SEARCH_POSIBILITY_TEXT: 'Высокая с IP-ID',
    SEARCH_SPEEDOMETER_BACKGROUND: '#F5F5F6',
    SEARCH_SPEEDOMETER_OUTLINE: 'rgba(80, 80, 80, 0.15)',
  },
};
