import { baseEnvironment } from 'projects/app-ipid/src/environments/base.environment';

const env = (window as any)?.GardiumEnv?.environment;

export let environment = env
  ? env
  : {
    ...baseEnvironment,

    BRAND: 'BRAND-SEARCH',
    TITLE: 'BRAND-SEARCH',
    STATIC_FILES: '/assets/static-files/ipid',
    LOGO_PATH: '/assets/brand-search/images/logo2.svg',
    LOGO2_PATH: '/assets/brand-search/images/logo.svg',
    LOGO_CASES_PATH: '/assets/brand-search/images/brand-search-logo.svg',
    FOOTER_TEXT: `© ${new Date().getFullYear()} Brand-Search.ru. Все права защищены.`,
    FAVICON: '/assets/brand-search/images/favicon.png',

    // Константы для поиска /search страница
    SEARCH_STYLES: {
      SEARCH_POSIBILITY_TEXT: 'Высокая с Brand-Search.ru',
      SEARCH_SPEEDOMETER_BACKGROUND: '#FFFFFF',
      SEARCH_SPEEDOMETER_OUTLINE: '#E1E5EB'
    }
  };
