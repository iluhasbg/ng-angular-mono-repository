import { baseEnvironmentDebt as env } from 'projects/app-debit/src/environments/environment.debt';

export const environment = {
  ...env,
  BRAND: 'LEGAL-DEBT',
  TITLE: 'Дебиторка для ALFA',
  production: true,
};
