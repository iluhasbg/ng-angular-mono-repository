import { baseEnvironmentDebt } from 'projects/app-debit/src/environments/environment.debt';

const env = (window as any)?.GardiumEnv?.environment;

export let environment = env
  ? env
  : {
    ...baseEnvironmentDebt,
    BRAND: 'LEGAL_FREE',
    production: false,
    BASE_URL_API: ``,
  };
