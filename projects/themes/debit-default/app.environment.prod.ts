import { baseEnvironmentDebt as env } from 'projects/app-debit/src/environments/environment.debt';

export const environment = {
  ...env,
  BRAND: 'LEGAL_FREE',
  production: true,
};
