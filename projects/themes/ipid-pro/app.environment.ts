import { environment as baseEnvironment } from 'projects/themes/ipid/app.environment';

export const environment = {
  ...baseEnvironment,
  corporate: true,

  BRAND: 'IPID-PRO',
  TITLE: 'Гардиум.ПРО',
  LOGO_PATH: '/assets/gardium-pro/gardium_pro_new.svg',
  LOGO2_PATH: '/assets/gardium-pro/gardium_pro_new.svg',
  LOGO_CASES_PATH: '/assets/gardium-pro/gardium_pro_new.svg',
  FOOTER_TEXT: `© ${new Date().getFullYear()} Гардиум.ПРО. Все права защищены.`,
  STATIC_FILES: '/assets/static-files/gardium-pro',

  // Константы для поиска /search страница
  SEARCH_STYLES: {
    SEARCH_POSIBILITY_TEXT: 'Высокая с Гардиум.ПРО',
    SEARCH_SPEEDOMETER_BACKGROUND: '#FFFFFF',
    SEARCH_SPEEDOMETER_OUTLINE: '#E1E5EB',
  },
};
