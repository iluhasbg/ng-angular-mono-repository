import { environment as env } from 'projects/themes/ipid-pro/app.environment';
import { baseEnvironmentProd as envProd } from 'projects/app-ipid/src/environments/base.environment.prod';

export const environment = {
  ...env,
  ...envProd,
  production: true,
};
