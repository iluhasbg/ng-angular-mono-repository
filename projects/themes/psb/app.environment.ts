import { baseEnvironment } from 'projects/app-ipid/src/environments/base.environment';

export const environment = {
  ...baseEnvironment,
  BRAND: 'PSB',
  TITLE:
    'Банк для бизнеса - банковские услуги для малого бизнеса на выгодных условиях в банке «Промсвязьбанк»',
  LOGO_PATH: '/assets/psb/images/logo.svg',
  LOGO2_PATH: '/assets/psb/images/logo.svg',
  FOOTER_TEXT: '© 2022 ПAO «Промсвязьбанк». Все права защищены.',
  FAVICON: '/assets/psb/images/favicon.png',
  STATIC_FILES: '/assets/static-files/psb',

  // Константы для поиска /search страница
  SEARCH_STYLES: {
    SEARCH_POSIBILITY_TEXT: 'Высокая с IP-ID',
    SEARCH_SPEEDOMETER_BACKGROUND: '#FFFFFF',
    SEARCH_SPEEDOMETER_OUTLINE: '#F1F1F1',
  },

  SEARCH_CUSTOM_STYLES: `

    .submit_red {
      box-shadow: none;
    }

    .ip-id-search-risks.help-enabled.risk-green .ip-id-search-risks-content .risks-notes.left > div > i {
      background: #F5F5F5!important;
      color: #9E9E9E!important;
      border: 1px solid rgba(80, 80, 80, 0.15);
    }
  `,
};
