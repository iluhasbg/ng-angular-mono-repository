import { environment as env } from 'projects/themes/psb/app.environment';
import { baseEnvironmentProd as envProd } from 'projects/app-ipid/src/environments/base.environment.prod';

export const environment = {
  ...env,
  ...envProd,
};
