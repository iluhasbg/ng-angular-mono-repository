import { environment as env } from 'projects/themes/open/app.environment';
import { baseEnvironmentProd as envProd } from 'projects/app-ipid/src/environments/base.environment.prod';

export const environment = {
  ...env,
  ...envProd,
  production: true,
  BASE_URL_API: `https://open.gardium.online/api`,
  BASE_URL_OPEN: `https://bp.open.ru`,
  FRONT_URL: `open.gardium.online`,
};
