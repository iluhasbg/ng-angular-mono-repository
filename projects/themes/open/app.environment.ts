import { baseEnvironment } from 'projects/app-ipid/src/environments/base.environment';

export const environment = {
  ...baseEnvironment,

  BASE_URL_API: ``,
  BASE_URL_OPEN: ``,
  FRONT_URL: `dev.ip-id.ru`,
  BRAND: 'OPEN',
  TITLE: 'Банковские услуги для малого и среднего бизнеса',
  LOGO_PATH: '/assets/open/images/logo.svg',
  LOGO2_PATH: '/assets/open/images/logo.svg',
  LOGO_CASES_PATH: '/assets/open/images/logo.svg',
  FOOTER_TEXT: `© ${new Date().getFullYear()} ПАО Банк «ФК Открытие». Все права защищены.`,
  FAVICON: '/assets/fav/favicon.open.ico',
  STATIC_FILES: '/assets/static-files/open',

  // Константы для поиска /search страница
  SEARCH_STYLES: {
    SEARCH_POSIBILITY_TEXT: 'Высокая с Открытие',
    SEARCH_SPEEDOMETER_BACKGROUND: '#FFFFFF',
    SEARCH_SPEEDOMETER_OUTLINE: 'rgba(80, 80, 80, 0.15)',
  },
};
