import { baseEnvironment } from 'projects/app-ipid/src/environments/base.environment';

export const environment = {
  ...baseEnvironment,

  BRAND: 'MTS-KASSA',
  STATIC_FILES: '/assets/static-files/mts',

  // Константы для поиска /search страница
  SEARCH_STYLES: {
    SEARCH_POSIBILITY_TEXT: 'Высокая с IP-ID',
    SEARCH_SPEEDOMETER_BACKGROUND: '#FFFFFF',
    SEARCH_SPEEDOMETER_OUTLINE: '#E1E5EB',
  },
};
