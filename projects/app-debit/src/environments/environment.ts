import { baseEnvironmentDebt as env } from './environment.debt';

export const environment = {
  ...env,
};
