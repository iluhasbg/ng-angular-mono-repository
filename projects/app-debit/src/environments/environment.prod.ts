import { baseEnvironmentDebt as env } from './environment.debt';

export const environment = {
  ...env,
  BASE_URL_API:``,
  production: true,
};
