import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import { DynamicFormSchemaElement } from '../../interfaces/dynamic-form-schema.interface';
import { DynamicFormActionEvent } from '../../interfaces/dynamic-form-action-event.interface';
import {
  DynamicFormState,
  IDynamicFormStateForm
} from '../../interfaces/dynamic-form-state.interface';
import { DynamicFormValueChangeEvent } from '../../interfaces/dynamic-form-value-change-event.interface';
import {DynamicFormService} from '../../services/dynamic-form.service';

@Component({
  selector: 'app-dynamic-form-renderer',
  templateUrl: './dynamic-form-renderer.component.html',
  styleUrls: ['./dynamic-form-renderer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DynamicFormRendererComponent implements OnInit, OnChanges {
  @Input() state: IDynamicFormStateForm;
  @Input() formState: DynamicFormState;
  @Input() formId: string[];
  @Input() isSending: boolean;
  @Input() filesLength: boolean;
  @Input() noDocs: boolean;
  @Input() controls: DynamicFormSchemaElement[];
  @Input() showColumnsFooter: boolean;
  @Output() action = new EventEmitter<DynamicFormActionEvent>();
  @Output() valueChange = new EventEmitter<DynamicFormValueChangeEvent>();

  finalControls: DynamicFormControlItem[] = [];

  constructor(
    readonly dynamicFormService: DynamicFormService
  ) {
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
    this.calculateFinalControls();
  }

  trackControl(index, item: DynamicFormControlItem){
    return item.control.id;
  }

  // здесь мы один раз строим дерево (не совсем) отрисовки
  // с добавленными динамическими формами
  private calculateFinalControls() {
    const controls: DynamicFormControlItem[] = [];

    for (const control of this.controls) {
      controls.push({ formId: this.formId, control });
      const controlsAfterThis = this.getDynamicControlsAfterOne(control);
      if (controlsAfterThis) {
        controls.push(...controlsAfterThis);
      }
    }

    this.finalControls = controls;
  }

  private getDynamicControlsAfterOne(control: DynamicFormSchemaElement): DynamicFormControlItem[] {
    const childForms = this.dynamicFormService.state.forms[this.dynamicFormService.state.currentForm].children.filter(i => i.appendAfter === control.id);
    const results: DynamicFormControlItem[] = [];
    for (const form of childForms) {
      if (form?.form?.elements?.length) {
        for (const item of form?.form?.elements) {
          results.push({ formId: [...this.formId, form.formId], control: item });
        }
      }
    }

    return results;
  }


  onValueChange(controlId: string, value: unknown) {
    this.valueChange.emit({formId: this.formId, controlId, value});
  }

  onAction(action: DynamicFormActionEvent) {
    this.action.emit(action);
    // Костыль для случаев когда произошел appendForm
    // соответственно change-detection не отработал т.к. инпуты не изменились
    this.calculateFinalControls();
  }

}

type DynamicFormControlItem = { formId: string[]; control: DynamicFormSchemaElement };
