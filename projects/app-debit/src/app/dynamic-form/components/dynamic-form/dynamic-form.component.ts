import { Component, Input, OnInit } from '@angular/core';
import { IDynamicFormSchema } from '../../interfaces/dynamic-form-schema.interface';
import { DynamicFormService } from '../../services/dynamic-form.service';
import { DynamicFormHelpersService } from '../../services/dynamic-form-helpers.service';
import { DynamicFormActionEvent } from '../../interfaces/dynamic-form-action-event.interface';
import { DynamicFormValueChangeEvent } from '../../interfaces/dynamic-form-value-change-event.interface';
import {DynamicFormState} from '../../interfaces/dynamic-form-state.interface';

@Component({
  selector: 'app-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  styleUrls: ['./dynamic-form.component.scss'],
  providers: [DynamicFormService]
})
export class DynamicFormComponent implements OnInit {
  @Input() schema: IDynamicFormSchema;
  @Input() formState: DynamicFormState;
  @Input() type: string;
  @Input() eventID: string;
  @Input() caseID: string;
  @Input() showColumnsFooter: boolean;

  constructor(
    readonly dynamicFormService: DynamicFormService,
    private readonly dynamicFormHelpersService: DynamicFormHelpersService,
  ) { }

  ngOnInit(): void {
    this.dynamicFormService.eventID = this.eventID;
    this.dynamicFormService.caseID = this.caseID;
    this.dynamicFormService.eventType = this.type;
    if (this.formState) {
      this.dynamicFormService.state = this.formState;
      this.dynamicFormService.state.currentForm = this.dynamicFormService.state.mainForm;
      this.dynamicFormService.savedFormState = true;
    }
    this.dynamicFormService.init(this.schema);
  }

  onValueChange(event: DynamicFormValueChangeEvent) {
    this.dynamicFormService.onValueChange(event);
  }

  onAction(event: DynamicFormActionEvent) {
    this.dynamicFormService.onAction(event);
  }

  get hasFooter() {
    // Да костыль, но простите, так уже отдают данные
    return this.elements.some(el => this.dynamicFormHelpersService.isFooterControl(el));
  }

  get elements() {
    return this.currentForm.elements;
  }

  get currentFormId() {
    return this.dynamicFormService.currentFormId;
  }

  get currentForm() {
    return this.dynamicFormService.currentForm;
  }

  get state() {
    return this.dynamicFormService.currentFormState;
  }

}
