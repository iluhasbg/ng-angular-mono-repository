import { DynamicFormState } from '../interfaces/dynamic-form-state.interface';
import { IDynamicFormSchema } from '../interfaces/dynamic-form-schema.interface';
import { DynamicFormActionType } from '../interfaces/dynamic-form-action.interface';
import { DynamicFormActionEvent } from '../interfaces/dynamic-form-action-event.interface';
import { DynamicForm, DynamicFormSchema } from '../core/dynamic-form';
import {DynamicFormValueChangeEvent} from '../interfaces/dynamic-form-value-change-event.interface';
import {HttpService} from '../../../../../app-ipid/src/app/shared/services/http.service';
import {Injectable} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {SurchargeRequestService} from '../../pages/index/index-item/surcharge-request/services/surcharge-request.service';

@Injectable()
export class DynamicFormService {
  state: DynamicFormState;
  private schema: DynamicFormSchema;
  eventID = '';
  caseID = '';
  eventType = '';
  backAction;
  backActionForChange;
  disabledValueChange = false;
  savedFormState = false; // Состояние отправленной формы
  isSendingForm = false;
  files = [];
  noDocs;

  constructor(
    private httpService: HttpService,
    private toastrService: ToastrService,
    private router: Router,
    private surchargeRequestService: SurchargeRequestService
  ) {
  }

  init(schema: IDynamicFormSchema) {
    this.schema = new DynamicFormSchema(schema);
    if (!this.state) {
      this.state = this.initialState;
    }
  }

  onValueChange(event: DynamicFormValueChangeEvent) {
    console.log('value change event', event);
    const currentForm = this.state.forms[this.state.currentForm];
    const lastElement = currentForm.actions[currentForm.actions.length - 1];
    if (event.value || typeof event.value === 'boolean') {
      if (!this.savedFormState) {
        this.backAction = '';
        currentForm.actions.push({
          [event.controlId]: event.value
        });
      }
      currentForm.values[event.controlId] = event.value;
    }
  }

  onAction(event: DynamicFormActionEvent) {
    const { type } = event.action;

    if (type === DynamicFormActionType.ChangeForm) {
      // Просто меняем форму
      this.changeFormAction(event);
    } else if (type === DynamicFormActionType.AppendForm) {
      // Добавляем форму внутрь
      this.appendFormAction(event);
    } else if (type === DynamicFormActionType.Back) {
      // Возвращаемся на предыдущую форму
      this.forwardBackAction(event);
    } else if (type === DynamicFormActionType.Submit || type === DynamicFormActionType.Payment) {
      // Отправляем форму
      this.submitAction(event);
    }
  }

  submitAction(event: DynamicFormActionEvent) {
    this.isSendingForm = true;
    this.currentFormState.submitId = event.action.id;
    this.httpService.post({
      path: 'events/load',
      body: {
        caseID: this.caseID,
        eventID: this.eventID,
        eventType: this.eventType,
        formState: this.state
      }
    }).subscribe((data: any) => {
      if (data && data.statusCode === 200) {
        this.isSendingForm = false;
        this.disabledValueChange = true;
        this.savedFormState = true;
        this.state.currentForm = Object.keys(this.state.forms)[0];
        this.toastrService.success('Форма успешно отправлена');
        if (event.action.type === 'payment') {
          this.surchargeRequestService.caseID = this.caseID;
          this.surchargeRequestService.eventID = this.eventID;
          this.surchargeRequestService.extraPayType = event.action.paymentData.paySubTypeID;
          this.router.navigate(['/surcharge-request']);
        }
      }
    });
  }

  forwardBackAction(event: DynamicFormActionEvent) {
    // Возвращаемся на предыдущую форму
    this.currentFormState.submitId = event.action.id;
    this.backAction = this.backActionForChange = event.action.type;
    this.state.currentForm = this.state.forms[this.state.currentForm].parentFormId;
  }

  changeFormAction(event: DynamicFormActionEvent) {
    if (!this.savedFormState) {
      this.state.forms[event.action.formId] = {
        actions: [],
        values: {},
        submitId: null,
        childFormId: null,
        parentFormId: this.state.currentForm,
        children: [],
      };
    }
    this.state.currentForm = event.action.formId;
    // TODO: Подумать чо тут
    // if (this.backActionForChange) {
    //   this.backActionForChange = '';
    //   console.log(Object.values(this.state.forms));
    //   Object.values(this.state.forms)[Object.values(this.state.forms).length - 3].childFormId = event.action.formId;
    //   Object.values(this.state.forms)[Object.values(this.state.forms).length - 3].submitId = event.action.id;
    // } else {
    //   this.previousFormState.childFormId = event.action.formId;
    //   this.previousFormState.submitId = event.action.id;
    // }
    this.previousFormState.childFormId = event.action.formId;
    this.previousFormState.submitId = event.action.id;
  }

  appendFormAction(event: DynamicFormActionEvent) {
    const { controlId, action } = event;
    const currentForm = this.state.forms[this.state.currentForm];
    // удаляем все добавленные этой формой формы
    currentForm.children = currentForm.children.filter(i => i.controlId !== controlId);

    let appendAfter = action.appendAfter;
    if (!appendAfter) {
      appendAfter = this.currentForm.lastElement.id;
    }

    // и добавляем новую
    currentForm.children.push({
      controlId,
      appendAfter,
      form: this.schema.getForm(action.formId),
      formId: action.formId
    });
    this.changeFormAction(event);
  }

  get childForms() {
    return this.currentFormState.children;
  }

  get currentFormState() {
    return Object.values(this.state.forms)[Object.values(this.state.forms).length - 1];
  }

  get previousFormState() {
    return Object.values(this.state.forms)[Object.values(this.state.forms).length - 2];
  }

  get currentForm(): DynamicForm {
    return this.schema.getForm(this.currentFormId);
  }
  get currentFormId(): string {
    return this.state.currentForm || this.schema.mainForm;
  }

  private get initialState(): DynamicFormState {
    return {
      mainForm: this.schema.mainForm,
      currentForm: this.schema.mainForm,
      forms: {
        [this.schema.mainForm]: {
          actions: [],
          values: {},
          submitId: null,
          childFormId: null,
          parentFormId: null,
          children: [],
       },
      }
    };
  }
}
