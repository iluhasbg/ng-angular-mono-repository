import { Injectable } from '@angular/core';
import { DynamicFormSchemaElement } from '../interfaces/dynamic-form-schema.interface';

@Injectable()
export class DynamicFormHelpersService {

  isFooterControl(controlDef: DynamicFormSchemaElement) {
    return controlDef.type === 'section' && controlDef.data.style === 'ВПодвале';
  }

}
