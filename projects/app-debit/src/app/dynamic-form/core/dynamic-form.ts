import {
  DynamicFormSchemaForm, IDynamicFormSchema,
} from '../interfaces/dynamic-form-schema.interface';

export class DynamicFormSchema {
  constructor(private schema: IDynamicFormSchema) {
  }

  getForm(id: string) {
    return new DynamicForm(this.schema.forms[id]);
  }

  get mainForm() {
    return this.schema.mainForm;
  }
}

export class DynamicForm {
  constructor(private form: DynamicFormSchemaForm) {
  }

  get lastElement() {
    return this.form.elements[this.form.elements.length - 1];
  }

  get elements() {
    return this.form.elements;
  }

  get raw() {
    return this.form;
  }

}
