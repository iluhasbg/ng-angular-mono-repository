import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DynamicFormComponent } from './components/dynamic-form/dynamic-form.component';
import { DynamicFormRendererComponent } from './components/dynamic-form-renderer/dynamic-form-renderer.component';
import { DynamicFormControlsModule } from '../dynamic-form-controls/dynamic-form-controls.module';
import { DynamicFormHelpersService } from './services/dynamic-form-helpers.service';


@NgModule({
  declarations: [
    DynamicFormComponent,
    DynamicFormRendererComponent
  ],
  imports: [
    CommonModule,
    DynamicFormControlsModule
  ],
  providers: [
    DynamicFormHelpersService
  ],
  exports: [
    DynamicFormComponent
  ]
})
export class DynamicFormModule { }
