import { DynamicForm } from '../core/dynamic-form';

export interface DynamicFormState {
  mainForm: string; // главная форма
  currentForm: string; // текущая форма
  forms: IDynamicFormStateForm;
}

export interface IDynamicFormStateForm {
  [key: string]: {
    actions: any[]; // массив действий на форме. Заполняется последовательно с действиями пользователя. Значение добавляется тогда, когда последнее записанное значение имеет ключ, отличный от нового.
    values: object | FileFormValue[]; // актуальные значения. Перезаписываются.
    submitId: string; // ид действия по которому было завершено действие с формой (смена формы или отправка данных).
    childFormId: string; // id формы, на которую перешли.
    parentFormId: string; // id формы с которой перешли на эту форму.
    children: IDynamicFormStateChildForm[];
  };
}

export interface IDynamicFormStateChildForm {
  controlId: string;
  appendAfter: string;
  form: DynamicForm;
  formId: string;
}

export type DynamicFormValue = {
  [key: string]: string | number | boolean | DynamicFormValue
};

export interface FileFormValue {
  file: {} | undefined;
  path: string;
  size: number;
  fileName: string;
}
