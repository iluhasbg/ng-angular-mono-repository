export interface DynamicFormValueChangeEvent {
  formId: string[];
  controlId: string;
  value: any;
}
