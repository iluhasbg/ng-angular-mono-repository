export interface DynamicFormAction {
  type: DynamicFormActionType;
  formId?: string;
  id?: string;
  appendAfter?: string; // добавить форму после элемента с appendAfter ид
  paymentData?: IPaymentData; // данные для доплаты
}

export enum DynamicFormActionType {
  Back = 'back',
  ChangeForm = 'changeForm',
  AppendForm = 'appendForm',
  Submit = 'submit',
  Payment = 'payment'
}

export interface IPaymentData {
  id: string;
  paySubTypeID: string;
  payType: string;
}
