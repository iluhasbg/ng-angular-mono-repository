export interface IDynamicFormSchema {
  mainForm: string;
  forms: { [key: string]: DynamicFormSchemaForm };
}

export interface DynamicFormSchemaForm {
  type: string;
  elements: DynamicFormSchemaElement[];
}

export interface DynamicFormSchemaElement {
  id: string;
  type: string;
  data: any;
}
