import { DynamicFormAction } from './dynamic-form-action.interface';

export interface DynamicFormActionEvent {
  controlId: string;
  action: DynamicFormAction;
}
