import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import {
  IDeleteParams,
  IGetParams,
  IPostParams,
  IPutParams,
} from '../models/rest.interfaces';
import { ToastrService } from 'ngx-toastr';

const baseUrl = environment.BASE_URL_API;

@Injectable({
  providedIn: 'root',
})
export class HttpService {
  constructor(private http: HttpClient, private toastr: ToastrService) {}

  // TODO: Implement that shit
  public getHeaders(isFile = false, customToken = ''): HttpHeaders {
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'text/plain');
    headers = headers.append(
      'Authorization',
      this.getToken() ? this.getToken() : ''
    );
    // const xToken = JSON.parse(localStorage.getItem("PG_API_TOKEN_STORAGE_KEY"))["token"];
    // if (!isFile) {
    //   return {
    //     'Content-Type': 'application/json',
    //     'X-Token': xToken,
    //   };
    // } else {
    //   return {
    //     'X-Token': xToken,
    //   };
    // }
    // return {'Content-Type': 'application/json'};
    return headers;
  }

  getToken() {
    return localStorage.getItem('currentUserIPID');
  }

  get<T>(getParams: IGetParams): Observable<T> {
    const { path, isFile = false, params = {} } = getParams;

    return this.http
      .get<T>(`${baseUrl}/${path}`, {
        headers: this.getHeaders(isFile),
        params,
      })
      .pipe(catchError(this.handleError(`get ${path}`, null)));
  }

  post<T>(postParams: IPostParams): Observable<T> {
    const {
      path,
      body,
      isFile = false,
      customToken = '',
      subjectType = '',
      params = {},
    } = postParams;

    return (
      this.http
        .post<T>(`${baseUrl}/${path}`, body, {
          headers: this.getHeaders(isFile, customToken),
          params,
        })
        // return this.http.post<T>(`${baseUrl}/${path}`, body)
        .pipe(
          tap(() => {
            console.log('ok');
            // this.success(`Successfully created ${subjectType}`)
          }),
          catchError(this.handleError(`create ${path}`, null))
        )
    );
  }

  postWithErr<T>(postParams: IPostParams): Observable<T> {
    const {
      path,
      body,
      isFile = false,
      customToken = '',
      subjectType = '',
    } = postParams;
    return this.http.post<T>(`${baseUrl}/${path}`, body, {
      headers: this.getHeaders(isFile, customToken),
    });
  }

  put<T>(putParams: IPutParams): Observable<T> {
    const { path, body, isFile = false, subjectType = '' } = putParams;

    return this.http
      .put<T>(`${baseUrl}/${path}`, body, { headers: this.getHeaders(isFile) })
      .pipe(
        tap(() => {
          // this.success(`Successfully updated ${subjectType}`)
        }),
        catchError(this.handleError(`update ${path}`, null))
      );
  }

  delete<T>(deleteParams: IDeleteParams) {
    const { path, id, subjectType = '' } = deleteParams;

    return this.http
      .delete(`${baseUrl}/${path}/${id}`, { headers: this.getHeaders() })
      .pipe(
        tap(() => {
          // this.success(`Successfully deleted ${subjectType}`)
        }),
        catchError(this.handleError(`delete ${path}`, null))
      );
  }

  handleError<T>(
    operation = 'operation',
    result: T = null
  ): (error) => Observable<T> {
    return (error) => {
      // TODO: send the error to remote logging infrastructure
      console.error(operation, error); // log to console instead
      this.error(
        `Operation ${operation} failed with error: ${
          (error && error.error && error.error.message) || 'undefined error'
        }`
      );

      // TODO: better job of transforming error for user consumption
      // this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result);
    };
  }

  success(message) {
    this.toastr.success(message);
  }

  error(message) {
    this.toastr.error(message);
  }
}
