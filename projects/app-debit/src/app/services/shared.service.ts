import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  public closeModals: Subject<any> = new Subject();
  public draft: Subject<any> = new Subject();
  public openModal: Subject<any> = new Subject();
  // todo: это вообще юзается?
  public totalCount: number;
  public paginationTotalCount: number;

  constructor() { }
}
