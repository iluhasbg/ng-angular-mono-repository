export interface ILogin {
  answer?: string;
  token?: string;
  user?: any;
  caseID?: string;
}
export interface IResponse {
  answer?: string;
  result?: any;
}
export interface IModalDataT {
  imgSrc?: any;
  name?: any;
  position?: any;
  title?: any;
  popupText?: any;
  date?: any;
  files?: any;
  type?: any;
  id?: any;
  flagBlue?: boolean;
  status?: any;
}

export interface ITasks {
  stages?: IStageItem;
  tasks: ITasksEntry;
  totalCount?: number;
  paginationTotalCount?: number;
  debtors?: IDebtor[];
  regions?: IRegionsSelect[];
}

export interface IRegionsSelect {
  id?: number;
  name?: string;
  checked?: boolean;
}

export interface IStagesSelect {
  id?: number;
  name?: string;
  checked?: boolean;
}

export interface IDebtor {
  id?: number;
  name?: string;
  checked?: boolean;
}

export interface IStageItem {
  claims: number;
  completed: number;
  penalty: number;
  tribunal: number;
}

export interface ITasksEntry {
  displayMode?: number;
  items: ITasksItems[];
}

export interface ILogin {
  answer?: string;
  token?: string;
  user?: any;
  caseID?: string;
  kontragents?: any;
  validation?: boolean;
}

export interface IUser {
  login: string;
  name: string;
  phone: string;
  userID: string;
}

export interface IResponse {
  answer?: string;
  result?: any;
}

export interface IModalDataT {
  imgSrc?: any;
  name?: any;
  position?: any;
  title?: any;
  popupText?: any;
  date?: any;
  files?: any;
  type?: any;
  id?: any;
  flagBlue?: boolean;
  status?: any;
}

export interface ISearchObjects {
  index?: string;
  name?: string;
  number?: string;
  _source?: string[];
  SHA1hash?: string;
}

export interface ISearchINN {
  id?: string;
  searchTradeMarks?: boolean;
  searchDomains?: boolean;
  SHA1hash?: string;
}

export interface IMainData {
  hits?: IHitsData;
  max_score?: any;
  total?: {};
  timed_out?: any;
  took?: any;
  _shards?: {};
}

export interface IHitsData {
  hits?: any[];
}

export interface IMainSearchData {
  _id?: string;
  _index?: any;
  _score?: number;
  _source: IAllPossibleFields;
  _type?: any;
}

export interface IAllPossibleFields {
  _index?: any;
  registrationString?: any;
  applicationString?: any;
  imageText?: any;
  expiryDate?: any;
  TIN?: any;
  PSRN?: any;
  goodsServicesClassNumber?: any;
  goodsServices?: any;
  applicantName?: any;
  markImageFileName?: any;
  inventionName?: any;
  patentStartingDate?: any;
  patentGrantPublishDate?: any;
  patentGrantPublishNumber?: any;
  programName?: any;
  applicationNumber?: any;
  registrationNumber?: any;
  registrationDate?: any;
  authors?: any;
  patentHolders?: any;
  actual?: any;
  dbName?: any;
  domain?: any;
  zone?: any;
  fullDomain?: any;
  domainRegistrator?: any;
  imgUrl?: any;
  rightHolders?: string;
  checked?: boolean;
  payDate?: string;
}

export interface IRutBasicResponse {
  _index?: any;
  registrationString?: any;
  applicationString?: any;
  imageText?: any;
  TIN?: any;
  PSRN?: any;
  goodsServicesClassNumber?: any;
  markImageFileName?: any;
}

export interface IPatentResponse {
  inventionName?: any;
  patentStartingDate?: any;
  patentGrantPublishDate?: any;
  patentGrantPublishNumber?: any;
  applicationNumber?: any;
  registrationNumber?: any;
  registrationDate?: any;
  authors?: any;
  patentHolders?: any;
}

export interface IProgramsResponse {
  programName?: any;
  applicationNumber?: any;
  registrationNumber?: any;
  registrationDate?: any;
  authors?: any;
  patentHolders?: any;
  actual?: any;
}

export interface IRudbResponse {
  dbName?: any;
  applicationNumber?: any;
  registrationNumber?: any;
  registrationDate?: any;
  authors?: any;
  patentHolders?: any;
  actual?: any;
}

export interface IDomainResponse {
  domain?: any;
  zone?: any;
  fullDomain?: any;
  TIN?: any;
  PSRN?: any;
}

export interface ITrademarks {
  name: string;
  id: string;
  type: string;
  imgUrl: string;
  author: string;
  rightHolder: string;
  validUntil: string;
}

export interface IContracts {
  assignee?: IAssignee;
  assignor?: IAssignor;
  contractDate?: string;
  contractNumber?: string;
  contractTerms?: string;
  contractType?: string;
  dateOfValidity?: string;
  trademarks?: ITrademarks[];
  valid?: boolean;
  _id?: string;
  _options?: string[];
}

export interface IAssignee {
  PSRN?: string;
  TIN?: string;
  address?: string;
  applicantStatus?: number;
  countryCode?: string;
  name?: string;
  statusOfMatching?: string;
}

export interface IAssignor {
  PSRN?: string;
  TIN?: string;
  address?: string;
  applicantStatus?: number;
  countryCode?: string;
  name?: string;
  statusOfMatching?: string;
}

export interface ITrademarks {
  goodsServices?: IGoods[];
}

export interface IGoods {
  imageText?: string;
  index?: string;
  registrationDate?: string;
  registrationString?: string;
}

export interface IOrganisation {
  name?: string;
  id?: string;
  TIN?: string;
  Icases?: IActiveCases[];
  PSRN?: string;
}
export interface IActiveCases {
  number?: string;
  stage?: string;
  type?: string;
}

export interface ITasks {
  displayMode?: number;
  items: ITasksItems[];
}

export interface ITasksItems {
  ID?: string;
  OKVED?: any;
  active?: true;
  designationType?: string;
  designationValue?: string;
  details?: { title?: string, description: string, progress: number };
  director?: string;
  employeeSelectClasses?: boolean;
  info?: ITasksInfo[];
  inn?: string;
  lastStage?: number;
  legalAdress?: string;
  number?: string;
  opened?: boolean;
  status?: string;
  tariff?: string;
  toAgreement?: number;
  type?: string;
  warn?: boolean;
}

export interface ITasksInfo {
  title?: string;
  text?: string;
}

export interface IOrgDefault {
  error?: any;
  success?: any;
}

export interface IWidget {
  totalSum: number;
  caseCount: number;
  regionCount?: number;
  mainDebt?: number;
  penalty?: number;
  courtExpenses?: number;
  claims?: number;
  legalProceedings?: number;
  awarded?: number;
}

export interface IWidgets {
  total: IWidget;
  inProcess: IWidget;
  exact: IWidget;
}

export interface IArchiveFiles {
  link: string;
  error: boolean;
}

export type ICompanySearchParam = { TIN: string, PSRN?: string } | { PSRN: string, TIN?: string };
