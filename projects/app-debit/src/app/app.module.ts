import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { SystemPageModule } from './pages/system-page.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthPageModule } from './pages/auth/auth-page.module';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Ng2TelInputModule } from 'ng2-tel-input';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { BnNgIdleService } from 'bn-ng-idle';
import { HttpIntercept } from 'projects/app-ipid/src/app/shared/interceptors/http-interceptor';
import { SharedAuthModule } from '../../../shared/components/auth/shared-auth.module';
import { BsDatepickerModule, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { NgxsModule } from '@ngxs/store';
import { NgxsRouterPluginModule } from '@ngxs/router-plugin';
import { WebModule } from '@web/web.module';
import { environment } from '../environments/environment';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SystemPageModule,
    AuthPageModule,
    HttpClientModule,
    BrowserAnimationsModule,
    Ng2TelInputModule,
    SharedAuthModule,
    BsDatepickerModule.forRoot(),
    WebModule.forRoot({
      production: environment.production,
      BASE_URL_API: environment.BASE_URL_API,
      BASE_ELASTIC_URL_API: environment.BASE_ELASTIC_URL_API,
      isDebit: true,
    }),
    NgxsModule.forRoot([], {
      developmentMode: true,
    }),
    NgxsRouterPluginModule.forRoot(),
    NgxsReduxDevtoolsPluginModule.forRoot(),
  ],
  providers: [
    BnNgIdleService,
    { provide: HTTP_INTERCEPTORS, useClass: HttpIntercept, multi: true },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
  constructor(private bsLocaleService: BsLocaleService) {
    this.bsLocaleService.use('ru');
  }
}
