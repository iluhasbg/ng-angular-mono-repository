export interface SectionControlDef {
  id: string;
  style: SectionControlStyle;
}

export enum SectionControlStyle {
  Attention = 'ВШапке',
  AttentionFooter = 'ВПодвале',
  Recommendation = 'РекомендацияШапка',
  RecommendationFooter = 'РекомендацияПодвал'
}
