import { moduleMetadata, Story, Meta } from '@storybook/angular';

import { CommonModule } from '@angular/common';
import { SectionControlComponent } from './section-control.component';
import { TitleControlComponent } from '../title-control/title-control.component';
import { TextControlComponent } from '../text-control/text-control.component';
import { SectionControlStyle } from './section-control.interface';

export default {
  component: SectionControlComponent,
  decorators: [
    moduleMetadata({
      declarations: [SectionControlComponent, TitleControlComponent, TextControlComponent],
      imports: [CommonModule],
    }),
  ],
  title: 'dynamic-form/section-control',
} as Meta;

const Template: Story<SectionControlComponent> = args => ({
  props: args,
  template: `
    <app-section-control [def]="def">
      <app-title-control [def]="{ text: 'Важно', style: 'РазделВШапке' }"></app-title-control>
      <app-text-control [def]="{ text: 'В любой момент должник может произвести оплату полностью или частично. Если это произошло, пожалуйста сообщите нам для внесения изменений в порядок претензионного производства и увеличения эффективности предпринимаемых усилий.' }"></app-text-control>
    </app-section-control>
    `,
});

export const Default = Template.bind({ });
Default.args = {
  def: {

  }
};

export const Attention = Template.bind({ });
Attention.args = {
  def: {
    style: SectionControlStyle.Attention,
  }
};

export const AttentionFooter = Template.bind({ });
AttentionFooter.args = {
  def: {
    style: SectionControlStyle.AttentionFooter,
  }
};
