import { Component, Input } from '@angular/core';
import { SectionControlDef, SectionControlStyle } from './section-control.interface';

@Component({
  selector: 'app-section-control',
  templateUrl: './section-control.component.html',
  styleUrls: ['./section-control.component.scss']
})
export class SectionControlComponent {
  @Input() id!: string;
  @Input() def!: SectionControlDef;

  private readonly classNamesMap = {
    [SectionControlStyle.Attention]: 'attention',
    [SectionControlStyle.AttentionFooter]: 'footer',
  };

  get className() {
    return this.classNamesMap[this.def?.style] ?? '';
  }

}
