import { moduleMetadata, Story, Meta } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { InputFieldControlComponent } from './input-field-control.component';
import { InputFieldControlStyle } from './input-filed-control.interface';
import { InputTextControlComponent } from '../input-text-control/input-text-control.component';
import { InputNumberControlComponent } from '../input-number-control/input-number-control.component';
import { ReactiveFormsModule } from '@angular/forms';

export default {
  component: InputFieldControlComponent,
  decorators: [
    moduleMetadata({
      declarations: [InputFieldControlComponent, InputTextControlComponent, InputNumberControlComponent],
      imports: [CommonModule, ReactiveFormsModule],
    }),
  ],
  title: 'dynamic-form/input-field-control',
} as Meta;

const Template: Story<InputFieldControlComponent> = args => ({
  props: args
});


export const TextField = Template.bind({ });
TextField.args = {
  def: {
    id: '1',
    placeholder: 'Текст',
    required: false,
    action: {} as any,
    style: InputFieldControlStyle.TextField
  }
};

export const NumberField = Template.bind({ });
NumberField.args = {
  def: {
    placeholder: 'Пожалуйста уточните сумму',
    style: InputFieldControlStyle.NumberField
  }
};
