import { DynamicFormAction } from '../../dynamic-form/interfaces/dynamic-form-action.interface';
import { DynamicFormValueChangeEvent } from '../../dynamic-form/interfaces/dynamic-form-value-change-event.interface';

export interface InputFieldControlDef {
  /**
   * https://cf.9958258.ru/pages/viewpage.action?pageId=114691559
   */
  id: string;
  placeholder: string; // Текст подсказки поля ввода.
  required: boolean; // Признак обязательности ввода значения в это поля.
  style: InputFieldControlStyle; // Стиль кнопки
  action: DynamicFormValueChangeEvent; // Действие
}

export enum InputFieldControlStyle {
  TextField = 'Текст',
  StringField = 'Строка',
  PhoneField = 'ТипТел',
  CheckboxField = 'ЧекБокс',
  SumField = 'Сумма',
  IntField = 'ЦелоеЧисло',
  DateField = 'Дата',
  DateTimeField = 'ДатаИВремя',
}
