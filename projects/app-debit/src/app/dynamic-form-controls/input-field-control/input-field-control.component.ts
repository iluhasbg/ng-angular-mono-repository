import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {InputFieldControlDef, InputFieldControlStyle} from './input-filed-control.interface';
import {DynamicFormService} from '../../dynamic-form/services/dynamic-form.service';
import {DynamicFormState} from '../../dynamic-form/interfaces/dynamic-form-state.interface';

@Component({
  selector: 'app-input-field-control',
  templateUrl: './input-field-control.component.html',
  styleUrls: ['./input-field-control.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InputFieldControlComponent implements OnInit, OnDestroy {
  @Input() id: string;
  @Input() def: InputFieldControlDef;
  @Input() formState: DynamicFormState;
  @Input() savedFormState: boolean;
  @Input() filesLength: boolean;

  @Output() valueChange = new EventEmitter<string>();

  value = '';

  constructor(
    readonly dynamicFormService: DynamicFormService
  ) {
  }

  private readonly inputTypesMap = {
    [InputFieldControlStyle.TextField]: 'text',
    [InputFieldControlStyle.PhoneField]: 'tel',
    [InputFieldControlStyle.CheckboxField]: 'checkbox',
    [InputFieldControlStyle.SumField]: 'number',
    [InputFieldControlStyle.IntField]: 'number',
    [InputFieldControlStyle.StringField]: 'string',
    [InputFieldControlStyle.DateField]: 'date',
    [InputFieldControlStyle.DateTimeField]: 'dateTime'
  };

  ngOnInit(): void {
    if (this.def) {
      Object.keys(this.dynamicFormService.state.forms[this.dynamicFormService.state.currentForm].values).find(item => {
        if (item === this.id) {
          this.value = this.dynamicFormService.state.forms[this.dynamicFormService.state.currentForm].values[this.id];
        }
      });
    }
  }

  get inputType() {
    return this.inputTypesMap[this.def?.style || InputFieldControlStyle.TextField];
  }

  onValueChange(value: any) {
    this.dynamicFormService.noDocs = value;
    this.valueChange.emit(value);
  }

  ngOnDestroy(): void {
    this.valueChange.emit(undefined);
  }
}
