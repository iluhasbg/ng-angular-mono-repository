/**
 * https://cf.9958258.ru/pages/viewpage.action?pageId=115671430
 */
import { DynamicFormAction } from '../../dynamic-form/interfaces/dynamic-form-action.interface';

export interface ChoiceDialogControlDef {
  title: string; // Заголовок диалога выбора.
  style: ChoiceDialogStyle;
  styleItem: ChoiceDialogItemStyle;
  items: ChoiceDialogItem[];
  buttonText?: string;
}

export enum ChoiceDialogStyle {}

export interface ChoiceDialogItem {
  id: string;
  style: ChoiceDialogItemStyle;
  name: string; // Надпись на кнопке выбора.
  action: DynamicFormAction;
}

export enum ChoiceDialogItemStyle {}
