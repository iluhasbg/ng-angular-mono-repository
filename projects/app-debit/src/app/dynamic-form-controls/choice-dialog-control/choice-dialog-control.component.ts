import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {ChoiceDialogControlDef, ChoiceDialogItem } from './choiсe-dialog-control.interface';
import {FormControl, FormGroup} from '@angular/forms';
import {DynamicFormActionEvent} from '../../dynamic-form/interfaces/dynamic-form-action-event.interface';
import {Subscription} from 'rxjs';
import {DynamicFormService} from '../../dynamic-form/services/dynamic-form.service';
import {DynamicFormState} from '../../dynamic-form/interfaces/dynamic-form-state.interface';

@Component({
  selector: 'app-choice-dialog-control',
  templateUrl: './choice-dialog-control.component.html',
  styleUrls: ['./choice-dialog-control.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChoiceDialogControlComponent implements OnInit, OnDestroy {
  @Input() id!: string;
  @Input() def!: ChoiceDialogControlDef;
  @Input() formState: DynamicFormState;
  @Input() savedFormState: boolean;
  @Output() action = new EventEmitter<DynamicFormActionEvent>();
  @Output() valueChange = new EventEmitter<string>();

  private idPrefix = `choice-dialog-control-${Math.random()}`;
  private formValuesSubscription: Subscription;
  valueFromState = '';
  selectedOption;

  form = new FormGroup({
    option: new FormControl(this.valueFromState ? this.valueFromState : '')
  });

  constructor(
    readonly dynamicFormService: DynamicFormService,
  ) { }

  ngOnInit(): void {
    this.formValuesSubscription = this.form.valueChanges.subscribe(nextValue => {
      this.valueChange.emit(nextValue.option);
    });
    if (this.def && this.savedFormState) {
      this.def.items.forEach(el => Object.values(this.dynamicFormService.state.forms[this.dynamicFormService.state.currentForm].values).find(item => {
        if (item === el.id) {
          this.valueFromState = el.id;
          this.form.get('option').setValue(this.valueFromState);
        }
      }));
      this.def.items = this.def.items.filter(el => el.id === this.valueFromState);
    }
  }

  setValue() {
    this.selectedOption = this.def.items.find(i => i.id === this.form.getRawValue().option);
  }

  ngOnDestroy(): void {
    this.valueChange.emit(undefined);
    this.formValuesSubscription.unsubscribe();
  }

  itemId(item: ChoiceDialogItem) {
    return `${this.idPrefix}-${item.id}`;
  }

  onSubmit() {
    this.action.emit({
      controlId: this.id,
      action: this.selectedOption.action
    });
  }

}
