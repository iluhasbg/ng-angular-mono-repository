import { moduleMetadata, Story, Meta } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { ChoiceDialogControlComponent } from './choice-dialog-control.component';
import { ReactiveFormsModule } from '@angular/forms';

export default {
  component: ChoiceDialogControlComponent,
  decorators: [
    moduleMetadata({
      declarations: [ChoiceDialogControlComponent],
      imports: [CommonModule, ReactiveFormsModule],
    }),
  ],
  title: 'dynamic-form/choice-control',
} as Meta;

const Template: Story<ChoiceDialogControlComponent> = args => ({
  props: args
});

export const Default = Template.bind({});
Default.args = {
  id: '11',
  def: {
    title: 'Если вы соглашаетесь с решением суда, то мы можем приступить к взысканию сразу после получения исполнительного листа.',
    style: '',
    items: [
      {id: '1', name: 'Согласиться с решением суда', action: {} as any, style: ''},
      {id: '2', name: 'Обжаловать решение суда', action: {} as any, style: ''}
    ]
  }
};
