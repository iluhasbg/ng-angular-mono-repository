import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {DynamicFormState} from '../../dynamic-form/interfaces/dynamic-form-state.interface';
import {DynamicFormService} from '../../dynamic-form/services/dynamic-form.service';
import {InputNumberType} from './input-number-control-interface';

@Component({
  selector: 'app-input-number-control',
  templateUrl: './input-number-control.component.html',
  styleUrls: ['./input-number-control.component.scss']
})
export class InputNumberControlComponent implements OnInit {
  @Input() placeholder;
  @Input() stateValue;
  @Input() formState: DynamicFormState;
  @Input() savedFormState: boolean;
  @Input() type: string;
  @Output() valueChange = new EventEmitter<string>();
  value = '';

  constructor(
    readonly dynamicFormService: DynamicFormService
  ) {
  }

  private readonly TypeNumberMap = {
    [InputNumberType.IntType]: 'int',
    [InputNumberType.SumType]: 'sum',
  };

  ngOnInit(): void {
  }

  get TypeNumber() {
    return this.TypeNumberMap[this.type];
  }

  enterNumbers(event: any) {
    if (this.TypeNumber === 'int') {
      if (event.keyCode > 31 && (event.keyCode < 48 || event.keyCode > 57)) {
        return false;
      } else {
        this.onValueChange(event);
      }
    } else {
      if (event.keyCode > 31 && ((event.keyCode < 48 && event.keyCode !== 44 && event.keyCode !== 46) || event.keyCode > 57)) {
        return false;
      } else {
        this.onValueChange(event);
      }
    }
  }

  onValueChange(event: any): void {
    this.value = event.target.value;
    this.valueChange.emit(event.target.value);
  }
}
