import { Component, Input, OnInit } from '@angular/core';
import { AttachmentsControlDef } from './attachments-control.interface';

@Component({
  selector: 'app-attachments-control',
  templateUrl: './attachments-control.component.html',
  styleUrls: ['./attachments-control.component.scss']
})
export class AttachmentsControlComponent implements OnInit {
  @Input() id!: string;
  @Input() def!: AttachmentsControlDef;

  constructor() { }

  ngOnInit(): void {
  }

  get files() {
    return this.def;
  }

}
