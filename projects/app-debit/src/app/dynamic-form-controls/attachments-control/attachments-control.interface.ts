import { FileControlDef } from '../file-control/file-control.interface';

export type AttachmentsControlDef = FileControlDef[];
