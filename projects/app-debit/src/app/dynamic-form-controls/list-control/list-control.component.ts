import { Component, Input, OnInit } from '@angular/core';
import { ListControlDef } from './list-control.interface';

@Component({
  selector: 'app-list-control',
  templateUrl: './list-control.component.html',
  styleUrls: ['./list-control.component.scss']
})
export class ListControlComponent implements OnInit {
  @Input() id!: string;
  @Input() def!: ListControlDef;
  toggle = false;

  constructor() { }

  ngOnInit(): void {
  }

  getLabel() {
    switch (this.toggle) {
      case true:
        if (this.def.title) {
          return 'Скрыть ' + this.def.title;
        } else {
          return 'Скрыть состав услуг';
        }
      case false:
        if (this.def.title) {
          return 'Посмотреть ' + this.def.title;
        } else {
          return 'Посмотреть состав услуг';
        }

    }
  }

}
