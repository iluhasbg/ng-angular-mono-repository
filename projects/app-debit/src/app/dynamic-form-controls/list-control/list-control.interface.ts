export interface ListControlDef {
  id: string;
  /**
   * https://cf.9958258.ru/pages/viewpage.action?pageId=115671557
   */
  title: string; // Заголовок списка
  style: ListControlStyle; // Идентификатор стиля списка
  items: string[];
  styleItem: string; // Идентификатор стиля элементов
  collapsible: boolean; // Признак сворачиваемости списка
}

export enum ListControlStyle {
  Default = '',
}
