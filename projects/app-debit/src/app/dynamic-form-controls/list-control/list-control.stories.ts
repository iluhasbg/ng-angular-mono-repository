import { moduleMetadata, Meta, Story } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { ListControlComponent } from './list-control.component';
import { ListControlStyle } from './list-control.interface';

export default {
  component: ListControlComponent,
  decorators: [
    moduleMetadata({
      declarations: [ListControlComponent],
      imports: [CommonModule],
    }),
  ],
  title: 'dynamic-form/list-control',
} as Meta;

const Template: Story<ListControlComponent> = args => ({
  props: args
});

export const Default = Template.bind({ });
Default.args = {
  def: {
    title: 'Заголовок списка',
    style: ListControlStyle.Default,
    items: [
      'Первый элемент списка',
      'Второй элемент списка',
      'Третий элемент списка',
      'Четвертый элемент списка'
    ],
  }
};
