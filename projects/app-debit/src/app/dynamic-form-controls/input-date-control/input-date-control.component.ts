import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DynamicFormState} from '../../dynamic-form/interfaces/dynamic-form-state.interface';
import {DynamicFormService} from '../../dynamic-form/services/dynamic-form.service';

@Component({
  selector: 'app-input-date-control',
  templateUrl: './input-date-control.component.html',
  styleUrls: ['./input-date-control.component.scss']
})
export class InputDateControlComponent implements OnInit {
  @Input() placeholder;
  @Input() stateValue;
  @Input() formState: DynamicFormState;
  @Input() savedFormState: boolean;
  @Output() valueChange = new EventEmitter<string>();
  value = '';

  constructor(
    readonly dynamicFormService: DynamicFormService
  ) { }

  ngOnInit(): void {
  }

  onValueChange(event: any) {
    this.value = event.target.value;
    this.valueChange.emit(event.target.value);
  }

}
