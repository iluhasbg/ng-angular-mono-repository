import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import { ITextControlDef } from './text-control.interface';

@Component({
  selector: 'app-text-control',
  templateUrl: './text-control.component.html',
  styleUrls: ['./text-control.component.scss'],
})
export class TextControlComponent implements OnInit {
  @Input() id!: string;
  @Input() def!: ITextControlDef;

  constructor() { }

  ngOnInit(): void {
  }

  getText(entities, text) {
    if (entities?.length > 0) {
      entities.sort((a, b) => a.offset < b.offset ? 1 : -1);
      const titleLetters = text.split('');
      let substr = ``;
      entities.forEach(item => {
        titleLetters.splice(item.offset, item.length);
        switch (item.type) {
          case 'text_link':
            substr = `<a class="form-link" href="${item.url}" target="_blank">${text.substr(item.offset, item.length)}</a>`;
            titleLetters.splice(item.offset, 0, substr);
            break;
          case 'bold':
            substr = `<span class="bold-text">${text.substr(item.offset, item.length)}</span>`;
            titleLetters.splice(item.offset, 0, substr);
            break;
          case 'italic':
            substr = `<span class="italic-text">${text.substr(item.offset, item.length)}</span>`;
            titleLetters.splice(item.offset, 0, substr);
            break;
          case 'strikethrough':
            substr = `<span class="strikethrough-text">${text.substr(item.offset, item.length)}</span>`;
            titleLetters.splice(item.offset, 0, substr);
            break;
          case 'underline':
            substr = `<span class="underline-text">${text.substr(item.offset, item.length)}</span>`;
            titleLetters.splice(item.offset, 0, substr);
            break;
        }
        text = titleLetters.join('');
      });
    }
    return `${text}`;
  }
}
