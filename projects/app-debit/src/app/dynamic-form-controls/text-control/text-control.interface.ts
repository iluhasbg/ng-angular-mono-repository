export interface ITextControlDef {
  id: string;
  /**
   * https://cf.9958258.ru/pages/viewpage.action?pageId=114691521
   */
  text: string; // Произвольный текст.
  entities: object[]; // Форматирование текста
}
