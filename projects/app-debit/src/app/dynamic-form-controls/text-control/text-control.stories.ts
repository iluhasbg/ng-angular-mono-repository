import { moduleMetadata, Story, Meta } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { TextControlComponent } from './text-control.component';
import { TextControlStyle } from './text-control.interface';

export default {
  component: TextControlComponent,
  decorators: [
    moduleMetadata({
      declarations: [TextControlComponent],
      imports: [CommonModule],
    }),
  ],
  title: 'dynamic-form/text-control',
} as Meta;

const Template: Story<TextControlComponent> = args => ({
  props: args
});

export const Default = Template.bind({});
Default.args = {
  def: {
    text: 'Обычный текст',
    style: TextControlStyle.Default,
    ref: null
  }
};

export const Html = Template.bind({});
Html.args = {
  def: {
    text: '<strong>Вставляем html</strong>',
    style: TextControlStyle.Html,
    ref: null
  },
};

export const Ref = Template.bind({});
Ref.args = {
  def: {
    text: 'Ссылка',
    style: TextControlStyle.Ref,
    ref: '#',
  },
};
