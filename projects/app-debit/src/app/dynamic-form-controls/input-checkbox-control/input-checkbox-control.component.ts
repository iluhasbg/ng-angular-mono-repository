import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges
} from '@angular/core';
import {DynamicFormState} from '../../dynamic-form/interfaces/dynamic-form-state.interface';
import {DynamicFormService} from '../../dynamic-form/services/dynamic-form.service';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-input-checkbox-control',
  templateUrl: './input-checkbox-control.component.html',
  styleUrls: ['./input-checkbox-control.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InputCheckboxControlComponent implements OnInit, OnChanges {
  @Input() placeholder;
  @Input() stateValue;
  @Input() formState: DynamicFormState;
  @Input() savedFormState: boolean;
  @Input() filesLength: boolean;
  @Output() valueChange = new EventEmitter<string>();
  @Input() def;
  @Input() id;
  value = '';

  form = new FormGroup({
    option: new FormControl(false)
  });

  constructor(
    readonly dynamicFormService: DynamicFormService
  ) { }

  ngOnInit(): void {
    if (this.dynamicFormService.state.forms[this.dynamicFormService.state.currentForm].values) {
      this.setForm();
    }
  }

  setForm() {
    this.form = new FormGroup({
      option: new FormControl(this.dynamicFormService.state.forms[this.dynamicFormService.state.currentForm].values[this.id])
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.filesLength) {
      delete this.dynamicFormService.currentFormState.values[this.id];
    }
  }

  onValueChange() {
    this.valueChange.emit(this.form.value.option);
  }

}
