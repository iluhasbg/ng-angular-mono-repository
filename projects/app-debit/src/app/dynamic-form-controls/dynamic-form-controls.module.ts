import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TitleControlComponent } from './title-control/title-control.component';
import { TextControlComponent } from './text-control/text-control.component';
import { SectionControlComponent } from './section-control/section-control.component';
import { FileControlComponent } from './file-control/file-control.component';
import { AttachmentsControlComponent } from './attachments-control/attachments-control.component';
import { ButtonControlComponent } from './button-control/button-control.component';
import { ListControlComponent } from './list-control/list-control.component';
import { FormColumnsControlComponent } from './form-columns-control/form-columns-control.component';
import { TableControlComponent } from './table-control/table-control.component';
import { ChoiceDialogControlComponent } from './choice-dialog-control/choice-dialog-control.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InputFieldControlComponent } from './input-field-control/input-field-control.component';
import { InputNumberControlComponent } from './input-number-control/input-number-control.component';
import { FileUploadControlComponent } from './file-upload-control/file-upload-control.component';
import { HttpClientModule } from '@angular/common/http';
import {SharedModule} from '../../../../app-ipid/src/app/shared/shared.module';
import {TextareaControlComponent} from './textarea-control/textarea-control.component';
import { InputTextControlComponent } from './input-text-control/input-text-control.component';
import { InputDateControlComponent } from './input-date-control/input-date-control.component';
import { InputDatetimeControlComponent } from './input-datetime-control/input-datetime-control.component';
import { InputCheckboxControlComponent } from './input-checkbox-control/input-checkbox-control.component';
import {BsDatepickerModule} from "ngx-bootstrap/datepicker";


@NgModule({
  declarations: [
    TitleControlComponent,
    TextControlComponent,
    SectionControlComponent,
    FileControlComponent,
    AttachmentsControlComponent,
    ButtonControlComponent,
    ListControlComponent,
    FormColumnsControlComponent,
    TableControlComponent,
    ChoiceDialogControlComponent,
    InputFieldControlComponent,
    TextareaControlComponent,
    InputNumberControlComponent,
    FileUploadControlComponent,
    InputTextControlComponent,
    InputDateControlComponent,
    InputDatetimeControlComponent,
    InputCheckboxControlComponent
  ],
  exports: [
    TitleControlComponent,
    SectionControlComponent,
    TextControlComponent,
    AttachmentsControlComponent,
    ButtonControlComponent,
    ListControlComponent,
    FormColumnsControlComponent,
    TableControlComponent,
    ChoiceDialogControlComponent,
    InputFieldControlComponent,
    FileUploadControlComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    SharedModule,
    BsDatepickerModule
  ]
})
export class DynamicFormControlsModule { }
