export interface TitleControlDef {
  id: string;
  /**
   * https://cf.9958258.ru/pages/viewpage.action?pageId=114691447
   **/
  style: TitleControlStyle;

  // Произвольный текст заголовка.
  text: string;
}

export enum TitleControlStyle {
  Default = '',
  headerTitle = 'РазделВШапке',
  footerTitle = 'ВПодвале',
  warningTitle = 'WarningTitle', // (! в оранжевом треугольнике)
  warningTitleBlue = 'Заголовок, требующий действия (! в синем треугольнике)', // (! в синем треугольнике)
  formColumns = 'formColumns' //  серый шрифт - обозначение шага
}
