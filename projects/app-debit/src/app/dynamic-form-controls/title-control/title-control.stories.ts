import { moduleMetadata, Story, Meta } from '@storybook/angular';

import { CommonModule } from '@angular/common';
import { TitleControlComponent } from './title-control.component';
import { TitleControlStyle } from './title-control.interface';

export default {
  component: TitleControlComponent,
  decorators: [
    moduleMetadata({
      declarations: [TitleControlComponent],
      imports: [CommonModule],
    }),
  ],
  title: 'dynamic-form/title-control',
} as Meta;

const Template: Story<TitleControlComponent> = args => ({
  props: args
});

export const Default = Template.bind({});
Default.args = {
  def: {
    text: 'Обычный заголовок'
  }
};

export const HeaderTitle = Template.bind({});
HeaderTitle.args = {
  def: {
    text: 'Заголовок в шапке',
    style: TitleControlStyle.headerTitle
  },
};

export const FooterTitle = Template.bind({});
FooterTitle.args = {
  def: {
    text: 'В подвале',
    style: TitleControlStyle.footerTitle
  },
};

export const FormColumn = Template.bind({});
FormColumn.args = {
  def: {
    text: 'Заголовок колонки',
    style: TitleControlStyle.formColumns
  },
};


export const WarningTitle = Template.bind({});
WarningTitle.args = {
  def: {
    text: 'Заголовок в шапке с warning иконкой',
    style: TitleControlStyle.warningTitle
  },
};
