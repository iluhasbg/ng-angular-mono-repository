import { Component, Input } from '@angular/core';
import { TitleControlDef, TitleControlStyle } from './title-control.interface';
import { findKey } from 'lodash';

@Component({
  selector: 'app-title-control',
  templateUrl: './title-control.component.html',
  styleUrls: ['./title-control.component.scss']
})
export class TitleControlComponent {
  @Input() id!: string;
  @Input() def!: TitleControlDef;

  constructor() { }

  get styleName() {
    return findKey(TitleControlStyle, i => i === this.def.style) ?? 'default';
  }

  get hasWarningIcon() {
    return [TitleControlStyle.warningTitleBlue, TitleControlStyle.warningTitle].includes(this.def.style);
  }

}
