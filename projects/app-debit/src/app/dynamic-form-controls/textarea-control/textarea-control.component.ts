import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DynamicFormState} from '../../dynamic-form/interfaces/dynamic-form-state.interface';
import {DynamicFormService} from '../../dynamic-form/services/dynamic-form.service';

@Component({
  selector: 'app-textarea-control',
  templateUrl: './textarea-control.component.html',
  styleUrls: ['./textarea-control.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TextareaControlComponent implements OnInit {
  @Input() placeholder;
  @Input() stateValue;
  @Input() formState: DynamicFormState;
  @Input() savedFormState: boolean;
  @Output() valueChange = new EventEmitter<string>();
  value = '';

  constructor(
    readonly dynamicFormService: DynamicFormService
  ) {
  }

  ngOnInit(): void {
  }

  onValueChange(event: any) {
    this.value = event.target.value;
    this.valueChange.emit(event.target.value);
  }
}
