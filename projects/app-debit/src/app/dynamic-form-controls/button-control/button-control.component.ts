import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input, OnChanges,
  OnInit,
  Output, SimpleChanges
} from '@angular/core';
import { ButtonControlStyle, ButtonControlDef } from './button-control.interface';
import { DynamicFormActionEvent } from '../../dynamic-form/interfaces/dynamic-form-action-event.interface';
import {DynamicFormService} from '../../dynamic-form/services/dynamic-form.service';

@Component({
  selector: 'app-button-control',
  templateUrl: './button-control.component.html',
  styleUrls: ['./button-control.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonControlComponent implements OnInit, OnChanges{
  @Input() id!: string;
  @Input() def!: ButtonControlDef;
  @Input() savedFormState: boolean;
  @Input() isSending: boolean;
  @Input() filesLength: boolean;
  @Input() noDocs: boolean;
  @Output() action = new EventEmitter<DynamicFormActionEvent>();
  uploadDocButton = false;

  private readonly classNamesMap = {
    [ButtonControlStyle.Default]: 'btn-blue',
    [ButtonControlStyle.Border]: 'btn-blue-attr',
  };

  constructor(
    readonly dynamicFormService: DynamicFormService
  ) {
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if ((this.filesLength || this.noDocs === true) && (this.def.fileUpload || this.dynamicFormService.currentFormId === '000004404')) {
      this.uploadDocButton = false;
    } else if ((!this.filesLength || this.noDocs === false) && (this.def.fileUpload || this.dynamicFormService.currentFormId === '000004404')) {
      this.uploadDocButton = true;
    }
  }

  get className() {
    return this.classNamesMap[this.def?.style || ButtonControlStyle.Default] ?? '';
  }

  onClick() {
    this.action.emit({
      controlId: this.id,
      action: this.def.action
    });
  }

}
