import { DynamicFormAction } from '../../dynamic-form/interfaces/dynamic-form-action.interface';

export interface ButtonControlDef {
  /**
   * https://cf.9958258.ru/pages/viewpage.action?pageId=114691559
   */
  name: string; // Текст кнопки
  style: ButtonControlStyle; // Стиль кнопки
  action: DynamicFormAction; // Действие
  fileUpload: boolean; // Признак Диалога загрузки
}

export enum ButtonControlStyle {
  Default = 'Цветная',
  Border = 'СБордером'
}
