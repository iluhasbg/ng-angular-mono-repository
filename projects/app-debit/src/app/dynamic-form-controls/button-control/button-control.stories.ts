import { moduleMetadata, Story, Meta } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { ButtonControlComponent } from './button-control.component';
import { ButtonControlStyle } from './button-control.interface';

export default {
  component: ButtonControlComponent,
  decorators: [
    moduleMetadata({
      declarations: [ButtonControlComponent],
      imports: [CommonModule],
    }),
  ],
  title: 'dynamic-form/button-control',
} as Meta;

const Template: Story<ButtonControlComponent> = args => ({
  props: args
});

export const Default = Template.bind({ });
Default.args = {
  id: '1',
  def: {
    name: 'Я просто кнопка',
    style: ButtonControlStyle.Default
  }
};

export const Border = Template.bind({ });
Border.args = {
  id: '1',
  def: {
    name: 'Я кнопка с обводкой',
    style: ButtonControlStyle.Border
  }
};
