import { moduleMetadata, Story, Meta } from '@storybook/angular';

import { CommonModule } from '@angular/common';
import { FileControlComponent } from './file-control.component';
import { FileControlStyle } from './file-control.interface';

export default {
  component: FileControlComponent,
  decorators: [
    moduleMetadata({
      declarations: [FileControlComponent],
      imports: [CommonModule],
    }),
  ],
  title: 'dynamic-form/file-control',
} as Meta;

const Template: Story<FileControlComponent> = args => ({
  props: args
});

export const Default = Template.bind({});
Default.args = {
  def: {
    id: "000000040",
    link: "https://9497cefe-ad3a-473a-90c1-0e54081cc5e7.mock.pstmn.io/templates",
    name: "doverennost.pdf",
    representation: "ДОВЕРЕННОСТЬ",
  }
};

export const ImportantIcon = Template.bind({});
ImportantIcon.args = {
  def: {
    ...Default.args.def,
    style: FileControlStyle.Important
  }
};
