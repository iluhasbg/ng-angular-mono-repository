import { Component, Input, OnInit } from '@angular/core';
import { FileControlDef, FileControlStyle } from './file-control.interface';
import {DownloadFilesService} from '../../../../../shared/services/download-files.service';

@Component({
  selector: 'app-file-control',
  templateUrl: './file-control.component.html',
  styleUrls: ['./file-control.component.scss']
})
export class FileControlComponent implements OnInit {
  @Input() def: FileControlDef;

  constructor(
    readonly downloadFiles: DownloadFilesService
  ) { }

  ngOnInit(): void {
  }

  get isDefaultStyle() {
    return !this.def.style || this.def.style === FileControlStyle.Default;
  }

  get isImportantStyle() {
    return this.def.style === FileControlStyle.Important;
  }

  trimString(str, count) {
    return str?.length > count ? str.slice(0, count) + '...' : str;
  }

}
