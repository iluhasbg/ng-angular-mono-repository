export interface IFilesByCategory {
  files?: FileControlDef[];
  name: string;
  subcategories?: ISubCategories[];
}

export interface ISubCategories {
  files?: FileControlDef[];
  name: string;
}

export interface FileControlDef {
  path: string;
  fileName: string;
  representation: string;
  size: number;
  style?: FileControlStyle;
}

export enum FileControlStyle {
  Default = 'default',
  Important = 'important'
}
