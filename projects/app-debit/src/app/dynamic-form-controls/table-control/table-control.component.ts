import {Component, Input, OnInit} from '@angular/core';
import { TableControlDef, TableControlStyle } from './table-control.interface';

@Component({
  selector: 'app-table-control',
  templateUrl: './table-control.component.html',
  styleUrls: ['./table-control.component.scss']
})
export class TableControlComponent implements OnInit{
  @Input() id!: string;
  @Input() def!: TableControlDef;

  constructor() {
  }

  ngOnInit() {
  }

  get className() {
    return {
      [TableControlStyle.Default]: 'default',
      [TableControlStyle.WithFooter]: 'with-footer',
    }[this.def.style || TableControlStyle.Default];
  }
}
