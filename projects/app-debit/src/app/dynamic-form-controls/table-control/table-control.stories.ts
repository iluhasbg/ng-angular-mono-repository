import { moduleMetadata, Meta, Story } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { TableControlComponent } from './table-control.component';
import {
  TableControlColumnStyle,
  TableControlRowStyle,
  TableControlStyle
} from './table-control.interface';

export default {
  component: TableControlComponent,
  decorators: [
    moduleMetadata({
      declarations: [TableControlComponent],
      imports: [CommonModule],
    }),
  ],
  title: 'dynamic-form/table-control',
} as Meta;

const Template: Story<TableControlComponent> = args => ({
  props: args
});


export const Default = Template.bind({});
Default.args = {
  def: {
    style: TableControlStyle.Default,
    columns: [
      {
        name: 'Состав требований',
        style: TableControlColumnStyle.Default
      },
      {
        name: 'Сумма требований',
        style: TableControlColumnStyle.Default
      },
      {
        name: 'Присуждено судом',
        style: TableControlColumnStyle.Default
      },
      {
        name: 'Разница',
        style: TableControlColumnStyle.Default
      }
    ],
    rows: [
      {
        style: TableControlRowStyle.Default,
        cells: [
          'Основной долг',
          '80 000 ₽',
          '80 000 ₽',
          '0 ₽',
        ]
      },
      {
        style: TableControlRowStyle.Default,
        cells: [
          'Неустойка',
          '20 000 ₽',
          '5 000 ₽',
          {
            value: '-15 000 ₽',
            help: ' Суд посчитал предоставленные доказательства недостаточными для удовлетворения основного долга в полном объеме. '
          }
        ]
      },
      {
        style: TableControlRowStyle.Default,
        cells: [
          'Судебные расходы',
          '19 890 ₽',
          '14 240 ₽',
          {
            value: '-5 240 ₽',
            help: 'Комментарий к сумме -5 240 ₽'
          }
        ]
      },
      {
        style: TableControlRowStyle.Default,
        cells: [
          'Итого',
          '119 890 ₽',
          '85 100 ₽',
          '-20 240 ₽'
        ]
      },
    ]
  }
};

export const WithFooter = Template.bind({});

WithFooter.args = {
  def: {
    ...Default.args.def,
    style: TableControlStyle.WithFooter,
  }
}
