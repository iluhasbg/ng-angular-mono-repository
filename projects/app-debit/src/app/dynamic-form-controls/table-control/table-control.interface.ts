export interface TableControlDef {
  id: string;
  /**
   * https://cf.9958258.ru/pages/viewpage.action?pageId=115671588
   */
  style: TableControlStyle; // Идентификатор стиля в котором заложены свойства таблицы, такие как: Расположение колонок; Формат шапки; Формат подвала; Формат представления значения колонки; и т.п.
  columns: TableControlColumn[]; // Колонки таблицы.
  rows: TableControlRow[]; // Строки таблицы.
}

export interface TableControlColumn {
  name: string; // Отображаемое имя колонки в шапке.
  style: TableControlColumnStyle; // Идентификатор стиля/формата колонки. Имеет приоритет перед стилем таблицы.
}

export interface TableControlRow {
  style: TableControlRowStyle; // Идентификатор стиля/формата ряда. Имеет приоритет перед стилем таблицы.
  cells: TableControlRowItem; // Содержит значения ряда.
}

export interface TableControlRowItem {
  value: string;
  help: string; // Текст выводимой подсказки.
}

export enum TableControlStyle {
  Default = '',
  WithFooter = 'ТаблицаСПодвалом'
}

export enum TableControlColumnStyle {
  Default = '',
}

export enum TableControlRowStyle {
  Default = '',
}

