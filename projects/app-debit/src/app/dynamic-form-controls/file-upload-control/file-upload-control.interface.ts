import { DynamicFormAction } from '../../dynamic-form/interfaces/dynamic-form-action.interface';

export interface FileUploadControlInterface {
  id: string;
  help: string;
  action: DynamicFormAction;
  buttonText: string;
  comment: string;
  title: string;
  files: object[];
}
