import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { DynamicFormActionEvent } from '../../dynamic-form/interfaces/dynamic-form-action-event.interface';
import { FileUploadControlInterface } from './file-upload-control.interface';
import { Subscription } from 'rxjs';
import {HttpService} from '../../../../../app-ipid/src/app/shared/services/http.service';
import {DynamicFormService} from '../../dynamic-form/services/dynamic-form.service';
import {DynamicFormState} from '../../dynamic-form/interfaces/dynamic-form-state.interface';
import allowedTypes from '../../../../../shared/types/allowed-types';
import * as _ from 'lodash';
import {DownloadFilesService} from '../../../../../shared/services/download-files.service';


@Component({
  selector: 'app-file-upload-control',
  templateUrl: './file-upload-control.component.html',
  styleUrls: ['./file-upload-control.component.scss']
})
export class FileUploadControlComponent implements OnInit {
  uploadSub: Subscription;
  uploadProgress: number;
  fileError = ''; // ошибка загрузки файла
  isUploadingFile = false;


  form = new FormGroup({
    file: new FormControl()
  });

  @Input() def!: FileUploadControlInterface;
  @Input() id: string;
  @Input() formState: DynamicFormState;
  @Input() savedFormState: boolean;
  @Input() noDocs: boolean;
  @Output() action = new EventEmitter<DynamicFormActionEvent>();
  @Output() valueChange = new EventEmitter<object>();

  @ViewChild('fileInput') fileInputRef: ElementRef;

  constructor(
    private httpService: HttpService,
    readonly dynamicFormService: DynamicFormService,
    readonly downloadFiles: DownloadFilesService
  ) {}

  ngOnInit(): void {
    this.parseFile();
  }

  parseFile() {
    if (Object.keys(this.dynamicFormService.state.forms[this.dynamicFormService.state.currentForm].values).length !== 0) {
      if (Array.isArray(this.dynamicFormService.state.forms[this.dynamicFormService.state.currentForm].values[this.id])) {
        this.dynamicFormService.files = this.dynamicFormService.state.forms[this.dynamicFormService.state.currentForm].values[this.id];
      }
    }
  }

  /** Событие загрузки файла
   */
  onFileUpload(fileInput: any) {
    if (this.fileError?.length) {
      this.fileError = '';
    }
    if (fileInput.target.files?.length) {
      let files = [...fileInput.target.files];
      files = files.filter(el => {
        const maxSize = 12000000;
        if (el.size <= maxSize && _.includes(allowedTypes, el.type)) {
          return el;
        }
        if (el.size > maxSize) {
          this.fileError = 'Максимальный размер файла ' + maxSize / 1000000 + 'Mb';
        }
        if (!_.includes(allowedTypes, el.type)) {
          this.fileError = 'Недопустимый тип файла';
        }
      });
      if (files.length) {
        files.forEach(el => {
          this.isUploadingFile = true;
          const formData: FormData = new FormData();
          formData.append('file', el, el.name);
          this.httpService.post({path: 'request/file/add', body: formData, isFile: true})
            .subscribe((data: any) => {
              if (data) {
                this.isUploadingFile = false;
                this.dynamicFormService.files.push({
                  fileName: el.name,
                  file: el,
                  path: data.path,
                  size: el.size
                });
                this.valueChange.emit(this.dynamicFormService.files);
              }
            });
        });
      }
    }
  }
  trimString(str, count) {
    return str?.length > count ? str.slice(0, count) + '...' : str;
  }

  deleteFile(index): void {
    this.dynamicFormService.files.splice(index, 1);
    this.valueChange.emit(this.dynamicFormService.files);
  }

  onSubmit(): void {
    this.action.emit({
        controlId: this.id,
        action: this.def.action
      }
    );
  }
}
