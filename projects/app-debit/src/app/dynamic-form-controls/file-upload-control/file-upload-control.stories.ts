import { moduleMetadata, Story, Meta, componentWrapperDecorator } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { FileUploadControlComponent } from './file-upload-control.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

export default {
  component: FileUploadControlComponent,
  decorators: [
    moduleMetadata({
      declarations: [FileUploadControlComponent],
      imports: [CommonModule, ReactiveFormsModule, HttpClientModule],
    }),
    componentWrapperDecorator((story) => `<form style="margin: 40px 20px">${story}</form>`)
  ],
  title: 'dynamic-form/file-upload-control',
} as Meta;

const Template: Story<FileUploadControlComponent> = args => ({
  props: args
});

export const Default = Template.bind({});
Default.args = {
  def: {
    id: '0002',
    help: 'Тут могла бы быть ваша реклама',
    action: {} as any
  }
};
