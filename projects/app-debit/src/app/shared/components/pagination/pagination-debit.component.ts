import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-pagination-debit',
  templateUrl: './pagination-debit.component.html',
  styleUrls: ['./pagination-debit.component.scss']
})

export class PaginationDebitComponent implements OnInit, OnChanges {

  mobile = true;

  private _countPages: number;
  @Input() set countPages(count: number) {
    this._countPages = Math.ceil(count);
    this.pages = [];
    for (let i = 1; i <= this.countPages; i++) {
      this.pages.push(i)
    }
    this.subPages = this.pages.slice(0, 5);
  };

  get countPages() {
    return this._countPages;
  }
  @Input() allItems;

  @Input() page: number = 1;

  @Output() eventChangePage = new EventEmitter<any>();

  pages = [];
  subPages = [];
  // new
  @Input() itemPerPage: number;
  @Input() totalCount: number;
  @Input() currentPageNumber: number;

  @Output() changePageNumber = new EventEmitter<number>();

  pageNumbersList: number[];
  maxPaginationBtns = 7;
  isNextSet = false;
  isPrevSet = true;

  pageCount: number;
  // end


  constructor() {
  }

  ngOnInit() {
    this.mobile = window.innerWidth < 768;

    window.addEventListener('resize', () => {
      this.mobile = window.innerWidth < 768;
    });
  }

  changePage(pageNumber) {
    this.eventChangePage.emit(pageNumber);
    this.page = pageNumber;
    if (this.countPages > 5) {
      if (pageNumber < 3) {
        this.subPages = this.pages.slice(0, 5)
      } else if (pageNumber > (this.countPages - 2)) {
        this.subPages = this.pages.slice(this.countPages - 5, this.countPages)
      } else {
        this.subPages = this.pages.slice(pageNumber - 3, pageNumber + 2);
      }
    }
  }

  backPage() {
    this.changePage(this.page - 1)
  }

  nextPage() {
    this.changePage(this.page + 1)
  }
// new

  onChangePageNumber(pageNumber: number) {
    this.changePageNumber.emit(pageNumber);
    this.eventChangePage.emit(pageNumber);

  }

  ngOnChanges(changes: SimpleChanges): void {
    this.isNextSet = false;
    this.isPrevSet = false;

    this.pageCount = Math.ceil(this.totalCount / this.itemPerPage);

    let numberBtns = this.maxPaginationBtns;
    if (this.pageCount > this.maxPaginationBtns) {
      if (this.currentPageNumber > 4) {
        this.isPrevSet = true;
        numberBtns -= 2;
      }
      if ((this.pageCount + 1 - this.currentPageNumber) > 4) {
        this.isNextSet = true;
        numberBtns -= 2;
      }
    } else {
      numberBtns = this.pageCount;
    }

    let startPageBtn: number;
    if (!this.isPrevSet) {
      startPageBtn = 1;
    } else if (!this.isNextSet) {
      startPageBtn = this.pageCount + 1 - numberBtns;
    } else {
      startPageBtn = this.currentPageNumber - 1;
    }

    this.pageNumbersList = [];
    for (let i = 0; i < numberBtns; i++) {
      this.pageNumbersList.push(startPageBtn + i);
    }
  }
}
