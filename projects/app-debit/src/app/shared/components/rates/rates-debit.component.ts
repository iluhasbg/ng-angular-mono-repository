import {
  AfterContentChecked,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import {SurchargeRequestService} from '../../../pages/index/index-item/surcharge-request/services/surcharge-request.service';

declare var window: any;

@Component({
  selector: 'app-rates-debit',
  templateUrl: './rates-debit.component.html',
  styleUrls: ['./rates-debit.component.scss']
})
export class RatesDebitComponent implements OnInit, OnDestroy, AfterContentChecked {

  @Input() rates;
  @Input() activeTariff;
  @Input() countRates;
  @Input() requestType;
  @Output() selectingTariff = new EventEmitter();
  max2min: boolean;
  max2opty: boolean;
  max2max: boolean;

  max3min: boolean;
  max3opty: boolean;
  max3max: boolean;
  constructor(
    readonly surchargeRequestService: SurchargeRequestService
  ) {}

  ngOnInit(): void {
    if (window.innerWidth < 768) {
      this.max2min = false;
      this.max2opty = false;
      this.max2max = false;
      this.max3min = false;
      this.max3opty = false;
      this.max3max = false;
    } else {
      this.max2min = true;
      this.max2opty = true;
      this.max2max = true;
      this.max3min = true;
      this.max3opty = true;
      this.max3max = true;
    }

    window.addEventListener('resize', () => {
      if (window.innerWidth < 768) {
      this.max2min = false;
      this.max2opty = false;
      this.max2max = false;
      this.max3min = false;
      this.max3opty = false;
      this.max3max = false;
    } else {
      this.max2min = true;
      this.max2opty = true;
      this.max2max = true;
      this.max3min = true;
      this.max3opty = true;
      this.max3max = true;
    }
  });
  }

  ngAfterContentChecked() {
    if (this.countRates && this.countRates !== 1 && window.innerWidth < 768) {
      this.setTariff();
    }
  }

  selectTariff(event) {
    if (event.id) {
      switch (event.id) {
        case '000000001':
          window.dataLayer.push({event: 'clickMin'});
          break;
        case '000000002':
          window.dataLayer.push({event: 'clickOpt'});
          break;
        case '000000003':
          window.dataLayer.push({event: 'clickMax'});
          break;
        default:
          console.warn(`Для тарифа id: ${event.id} отсутствует dataLayer событие`);
      }
    }
    this.selectingTariff.emit(event);
  }

  ngOnDestroy(): void {
  }

  toCheckMax2Min() {
    if (this.max2min) {
      this.max2opty = false;
      this.max2max = false;
    }
  }

  toCheckMax2Opty() {
    if (this.max2opty) {
      this.max2min = false;
      this.max2max = false;
    }
  }

  toCheckMax2Max() {
    if (this.max2max) {
      this.max2min = false;
      this.max2opty = false;
    }
  }

  toCheckMax3Min() {
    if (this.max3min) {
      this.max3opty = false;
      this.max3max = false;
    }
  }

  toCheckMax3Opty() {
    if (this.max3opty) {
      this.max3min = false;
      this.max3max = false;
    }
  }

  toCheckMax3Max() {
    if (this.max3max) {
      this.max3min = false;
      this.max3opty = false;
    }
  }

  setTariff() {
    if (this.activeTariff?.length) {
      for (const item in this.rates) {
        if (this.rates.hasOwnProperty(item)) {
          if (this.rates[item] && this.rates[item].id === this.activeTariff) {
            switch (this.countRates) {
              case 3:
                switch (item) {
                  case 'min':
                    this.max3min = true;
                    this.max3opty = false;
                    this.max3max = false;
                    break;
                  case 'max':
                    this.max3min = false;
                    this.max3opty = false;
                    this.max3max = true;
                    break;
                  case 'opty':
                    this.max3min = false;
                    this.max3opty = true;
                    this.max3max = false;
                    break;
                }
                break;
              case 2:
                switch (item) {
                  case 'min':
                    this.max2min = true;
                    this.max2opty = false;
                    this.max2max = false;
                    break;
                  case 'max':
                    this.max2min = false;
                    this.max2opty = false;
                    this.max2max = true;
                    break;
                  case 'opty':
                    this.max2min = false;
                    this.max2opty = true;
                    this.max2max = false;
                    break;
                }
                break;
            }
          }
        }
      }
    } else {
      switch (this.countRates) {
        case 3:
          this.max3min = true;
          this.max3opty = false;
          this.max3max = false;
          break;
        case 2:
          this.max2min = true;
          this.max2opty = false;
          this.max2max = false;
          break;
      }
    }
  }
}
