export const nextTick: (fn: Function) => any = (function(window, prefixes, i, p, fnc, to) {
  while (!fnc && i < prefixes.length) {
    fnc = window[prefixes[i++] + 'equestAnimationFrame'];
  }
  return (fnc && fnc.bind(window)) || (window as any).setImmediate || function(fnc) {window.setTimeout(fnc, 0);};
})(window, 'r webkitR mozR msR oR'.split(' '), 0);
