import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {

  private previousBlockedUrl: string;  // (0) Стремненькая, но понятная реализация

  constructor(
              private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    // TODO: uncomment and fix
    // if (!this._authTokenStoreService.actualToken) {
    //   this.previousBlockedUrl = state.url; // (1) Если заблочили ссылку то сохраняем ее
    //   this.router.navigate(['/login']);
    //   return false;
    // }

    // (2) Если мы авторизовались
    if (this.previousBlockedUrl) {     // (3) И у нас была заблоченая ссылка
      this.router.navigate([this.previousBlockedUrl]);  // (4) Переходим по ней
      this.previousBlockedUrl = null;  // (5) Сбрасываем ее
      return false;                     // (6) И блокируем текущий роут (НЕ запомненный)
    }

    return true;
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    return this.canActivate(route, state);
  }
}
