import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { IntellectualPropertyComponent } from './intellectual-property.component';

describe('IntellectualPropertyComponent', () => {
  let component: IntellectualPropertyComponent;
  let fixture: ComponentFixture<IntellectualPropertyComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ IntellectualPropertyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntellectualPropertyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
