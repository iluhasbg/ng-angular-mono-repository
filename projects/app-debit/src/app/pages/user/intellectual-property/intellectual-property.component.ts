import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-intellectual-property',
  templateUrl: './intellectual-property.component.html',
  styleUrls: ['./intellectual-property.component.scss']
})
export class IntellectualPropertyComponent implements OnInit {
  //Временные данные

  intellectuals = [
    {
      title: 'Товарные знаки',
      fields: [
        {
          input: {
            enable: true,
            disabled: false,
            confirmation: true,
          },
          name: 'Поиск и просмотр',
        },
        {
          input: {
            enable: false,
            disabled: false,
            confirmation: true,
          },
          name: 'Подача заявок',
        },
        {
          input: {
            enable: false,
            disabled: false,
            confirmation: true,
          },
          name: 'Согласование делопроизводства по заявке',
        },
        {
          input: {
            enable: false,
            disabled: false,
            confirmation: true,
          },
          name: 'Лицензионные договоры и коммерческая концессия',
        },
        {
          input: {
            enable: false,
            disabled: false,
            confirmation: true,
          },
          name: 'Отчуждение и отказ от прав',
        }
      ]
    },
    {
      title: 'Патенты',
      fields: [
        {
          input: {
            enable: true,
            disabled: false,
            confirmation: true,
          },
          name: 'Поиск и просмотр',
        },
        {
          input: {
            enable: true,
            disabled: false,
            confirmation: true,
          },
          name: 'Подача заявок',
        },
        {
          input: {
            enable: true,
            disabled: false,
            confirmation: false,
          },
          name: 'Согласование делопроизводства по заявке',
        },
        {
          input: {
            enable: true,
            disabled: false,
            confirmation: true,
          },
          name: 'Лицензионные договоры и коммерческая концессия',
        },
        {
          input: {
            enable: false,
            disabled: false,
            confirmation: true,
          },
          name: 'Отчуждение и отказ от прав',
        }
      ]
    },
    {
      title: 'Программы и БД',
      fields: [
        {
          input: {
            enable: true,
            disabled: false,
            confirmation: true,
          },
          name: 'Поиск и просмотр',
        },
        {
          input: {
            enable: false,
            disabled: false,
            confirmation: true,
          },
          name: 'Подача заявок',
        },
        {
          input: {
            enable: false,
            disabled: false,
            confirmation: true,
          },
          name: 'Согласование делопроизводства по заявке',
        },
        {
          input: {
            enable: false,
            disabled: false,
            confirmation: true,
          },
          name: 'Лицензионные договоры и коммерческая концессия',
        },
        {
          input: {
            enable: false,
            disabled: false,
            confirmation: true,
          },
          name: 'Отчуждение и отказ от прав',
        }
      ]
    },
    {
      title: 'Суды и споры',
      fields: [
        {
          input: {
            enable: true,
            disabled: false,
            confirmation: true,
          },
          name: 'Поиск и просмотр',
        },
        {
          input: {
            enable: true,
            disabled: false,
            confirmation: false,
          },
          name: 'Подача заявок',
        },
        {
          input: {
            enable: true,
            disabled: false,
            confirmation: false,
          },
          name: 'Согласование делопроизводства по заявке',
        },
        {
          input: {
            enable: true,
            disabled: false,
            confirmation: true,
          },
          name: 'Лицензионные договоры и коммерческая концессия',
        },
        {
          input: {
            enable: true,
            disabled: false,
            confirmation: true,
          },
          name: 'Отчуждение и отказ от прав',
        }
      ]
    },
    {
      title: 'Авторские права',
      fields: [
        {
          input: {
            enable: false,
            disabled: false,
            confirmation: true,
          },
          name: 'Поиск и просмотр',
        },
        {
          input: {
            enable: false,
            disabled: false,
            confirmation: true,
          },
          name: 'Подача заявок',
        },
        {
          input: {
            enable: false,
            disabled: false,
            confirmation: true,
          },
          name: 'Согласование делопроизводства по заявке',
        },
        {
          input: {
            enable: false,
            disabled: false,
            confirmation: true,
          },
          name: 'Лицензионные договоры и коммерческая концессия',
        },
        {
          input: {
            enable: false,
            disabled: false,
            confirmation: true,
          },
          name: 'Отчуждение и отказ от прав',
        }
      ]
    },
    {
      title: 'Финансовая информация',
      fields: [
        {
          input: {
            enable: false,
            disabled: false,
            confirmation: true,
          },
          name: 'Виджет взаиморасчетов и истории платежей',
        },
        {
          input: {
            enable: false,
            disabled: false,
            confirmation: true,
          },
          name: 'Пополнение баланса',
        },
        {
          input: {
            enable: false,
            disabled: false,
            confirmation: true,
          },
          name: 'Списание с депозита',
        },
        {
          input: {
            enable: false,
            disabled: false,
            confirmation: true,
          },
          name: 'Заказ актов и оригиналов документов',
        }
      ]
    },
  ];

  @Input() userBlock;

  constructor() { }

  ngOnInit(): void {
  }

}
