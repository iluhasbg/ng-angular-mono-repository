import { Component, EventEmitter, HostListener, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-modal-add-user',
  templateUrl: './modal-add-user.component.html',
  styleUrls: ['./modal-add-user.component.scss']
})
export class ModalAddUserComponent implements OnInit {
  usersForm: FormGroup;

  //Форм-группа для выбора всех юзеров
  selectAllGroup: FormGroup;

  subscription: Subscription;

  //Изначально не выбран ни один юзер,
  //флаг для кнопки, меняется в зависимости выбран юзер или никто не выбран - не реализовано нормально :(
  selected;

  //Временные данные
  users = [
    {
      id: '1',
      photo: 'https://via.placeholder.com/48x48',
      name: 'Владислав Носов',
      position: 'Юрист',
    },
    {
      id: '2',
      photo: 'https://via.placeholder.com/48x48',
      name: 'Александра Шестакова',
      position: 'Менеджер',
    },
    {
      id: '3',
      photo: 'https://via.placeholder.com/48x48',
      name: 'Елизавета Рулонова',
      position: 'Секретарь',
    },
    {
      id: '4',
      photo: 'https://via.placeholder.com/48x48',
      name: 'Владислав Носов',
      position: 'Юрист',
    },
    {
      id: '5',
      photo: 'https://via.placeholder.com/48x48',
      name: 'Владислав Носов',
      position: 'Юрист',
    },
    {
      id: '6',
      photo: 'https://via.placeholder.com/48x48',
      name: 'Александра Шестакова',
      position: 'Менеджер',
    },
    {
      id: '7',
      photo: 'https://via.placeholder.com/48x48',
      name: 'Елизавета Рулонова',
      position: 'Секретарь',
    },
    {
      id: '8',
      photo: 'https://via.placeholder.com/48x48',
      name: 'Владислав Носов',
      position: 'Юрист',
    }
  ];

  constructor(private fb: FormBuilder) {
  }

  @Output() modalEvent = new EventEmitter();

  ngOnInit() {
    this.selectAllGroup = this.fb.group({
      selectAll: [false, []]
    });
    this.usersForm = this.fb.group({
      user: this.fb.array(this.users.map(x => false)),
    });

    const checkboxControl = (this.usersForm.controls.users as FormArray);

    this.subscription = checkboxControl.valueChanges.subscribe(checkbox => {
      checkboxControl.setValue(
        checkboxControl.value.map((value, i) => value ? this.users[i].id : false),
        {emitEvent: false}
      );
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  //Кнопка выбрать все
  selectAll() {
    let checkboxControl = (this.usersForm.controls.users);
    if (this.selectAllGroup.controls.selectAll.value) {
      checkboxControl.setValue(this.users.map(item => false));
    } else {
      checkboxControl.setValue(this.users);
    }
    this.selectAllGroup.controls.selectAll.setValue(!this.selectAllGroup.controls.selectAll.value);

    this.selected = !this.selected;
  }

  closeModal() {
    this.modalEvent.emit();
  }

  @HostListener('document:keyup', ['$event']) // 27=esc
  keyup(event: KeyboardEvent): void {
    if (event.keyCode === 27) {
      this.closeModal();
    }
  }

  onChanged($event){
    this.selected = $event && $event.target && $event.target.checked;
  }
}
