import { Component, OnDestroy, OnInit } from '@angular/core';
import { IOrganisation, IUser } from '../../../models/main.interfaces';
import { Subject } from 'rxjs';
import {UserProfileService} from '../../../../../../shared/services/user-profile.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, OnDestroy {
  // Начальный таб
  userTab = 'organization';
  user: IUser;
  organisations: IOrganisation;
  cases;
  private readonly onDestroy = new Subject<void>();

  // Кнопки в секции с экспертом
  btns = ['Консультация по претензии или иску', 'Вопрос по текущему делопроизводству',
    'Консультация по услугам', 'Запрос первичных документов', 'Другое'];

  // Временные данные
  organizations = [
    {
      id: '1',
      name: 'ИП Константинопольский Константин Андреевич',
      ogrnip: 'ОГРНИП 317774600010541',
      inn: 'ИНН 501209738853',
      city: 'Москва',
      balance: '17 325,93',
      bonus: '3 900,00',
      checked: false,
      cases: [
        {
          name: 'Регистрация товарного знака #55467',
          response: 'Ждем ответ от роспатента',
        },
        {
          name: 'Ведение дела в арбитражном суде #44345',
          response: 'Взыскание',
        },
        {
          name: 'Претензионная работа #53578',
          response: 'Претензия, ожидание ответа',
        }
      ],
      user: [
        {
          name: 'Гудреева Алена'
        },
        {
          name: 'Пчелкин Валентин',
        },
        {
          name: 'Козлов Владимир',
        },
        {
          name: 'Гудреева Алена',
        },
        {
          name: 'Пчелкин Валентин',
        }
      ]
    },
    {
      id: '2',
      name: 'ООО Ромашка',
      ogrnip: 'ОГРНИП 317774600010541',
      inn: 'ИНН 501209738853',
      city: 'Москва',
      balance: '17 325,93',
      bonus: '3 900,00',
      checked: false,
      cases: [],
      user: [
        {
          name: 'Гудреева Алена'
        },
        {
          name: 'Пчелкин Валентин',
        }
      ]
    }
  ];

  paymentHistory = [
    {
      id: '1',
      name: 'Регистрация товарного знака по тарифу «Оптимальный»',
      date: '21.10.2020',
      organization: 'ИП Константинопольский К.А.',
      case: 'Дело #54688',
      sum: '65 900,00',
      bonus: '3 900,00',
      minus: true,
      method: 'Онлайн',
    },
    {
      id: '2',
      name: 'Ведение дела в арбитражном суде по тарифу «Максимальный»',
      date: '17.10.2020',
      organization: 'ООО Ромашка',
      case: 'Дело #12533',
      sum: '19 890,00',
      bonus: null,
      minus: true,
      method: 'Счет',
    },
    {
      id: '3',
      name: 'Пополнение депозита',
      date: '12.10.2020',
      organization: 'ИП Константинопольский К.А.',
      case: null,
      sum: '4 890,00',
      bonus: null,
      minus: false,
      method: 'Онлайн',
    },
    {
      id: '4',
      name: 'Ведение дела в арбитражном суде по тарифу «Взыскание»',
      date: '12.10.2020',
      organization: 'ООО Ромашка',
      case: 'Дело #54688',
      sum: '4 890,00',
      bonus: null,
      minus: false,
      method: 'Депозит',
    }
  ];

  users = [
    {
      name: 'Ирина Иванова',
      photo: null,
      position: 'Менеджер',
      email: 'ivanova.i@companyname.ru',
      active: true,
      company: [
        {
          name: 'ИП Константинопольский Константин Андреевич',
          status: 'Активен',
        },
        {
          name: 'ООО Ромашка',
          status: 'Нет доверенности',
        },
        {
          name: 'ООО Системные технологии',
          status: 'Нет доступа',
        },
      ]
    },
    {
      name: 'Петр Николаев',
      photo: 'https://via.placeholder.com/100x100',
      position: 'Ведущий юрист',
      email: 'ivanova.i@companyname.ru',
      active: true,
      company: [
        {
          name: 'ИП Константинопольский Константин Андреевич',
          status: 'Активен',
        },
        {
          name: 'ООО Ромашка',
          status: 'Нет доверенности',
        },
        {
          name: 'ООО Системные технологии',
          status: 'Нет доступа',
        },
      ]
    },
    {
      name: 'Владислав Носов',
      photo: 'https://via.placeholder.com/100x100',
      position: 'Ведущий юрист',
      email: 'ivanova.i@companyname.ru',
      active: true,
      company: [
        {
          name: 'ИП Константинопольский Константин Андреевич',
          status: 'Активен',
        },
        {
          name: 'ООО Ромашка',
          status: 'Нет доверенности',
        },
        {
          name: 'ООО Системные технологии',
          status: 'Нет доступа',
        },
      ]
    },
    {
      name: 'Александра Шестакова',
      photo: 'https://via.placeholder.com/100x100',
      position: 'Менеджер',
      email: 'ivanova.i@companyname.ru',
      active: false,
      company: [
        {
          name: 'ИП Константинопольский Константин Андреевич',
          status: 'Активен',
        },
        {
          name: 'ООО Ромашка',
          status: 'Нет доверенности',
        },
        {
          name: 'ООО Системные технологии',
          status: 'Нет доступа',
        },
      ]
    },
  ];

  attorney = [
    {
      file: 'doverennost_0011.pdf',
      from: 'ИП Константинопольский Константин Андреевич',
      to: 'ООО Гардиум',
      status: 'На проверке',
      upload: '01.01.2019',
      validFrom: '01.01.2020',
      validUntil: '01.01.2021'
    },
    {
      file: 'doverennost_0011.pdf',
      from: 'ООО Ромашка',
      to: 'Александра Шестакова',
      status: 'Действует',
      upload: '01.01.2019',
      validFrom: '01.01.2020',
      validUntil: '01.01.2021'
    },
    {
      file: 'doverennost_0011.pdf',
      from: 'ООО Ромашка',
      to: 'Александра Шестакова',
      status: 'Не действует',
      upload: '01.01.2019',
      validFrom: '01.01.2020',
      validUntil: '01.01.2021'
    },
    {
      file: 'doverennost_0011.pdf',
      from: 'ООО Ромашка',
      to: 'Александра Шестакова',
      status: 'Не действует',
      upload: '01.01.2019',
      validFrom: '01.01.2020',
      validUntil: '01.01.2021'
    }
  ];

  // Временный флаг для переключения администратор/юзер без прав
  administrator = true;

  // Временный флаг для моно-аккаунта - у моно-аккаунта может быть только 1 организация и
  // как я поняла 1 пользователь, но это не точно
  monoAccount = false;

  // Временный флаг для переключения на личный аккаунт с ожиданием подверждения прав от админа
  waitingAccount = false;

  constructor(
    private profileApi: UserProfileService
  ) {
  }

  ngOnInit(): void {
    this.getOrganisations();
    this.user = JSON.parse(localStorage.getItem('currentUserData'));
    console.log(this.user);
  }

  // Выбор табов
  switchUserTab(type: string): void {
    if (type === this.userTab) {
      return;
    }
    this.userTab = type;
  }

  // Временная кнопка для переключения на администратора; изначально - юзер без прав
  changeAdministratorRights(): void {
    this.administrator = !this.administrator;
    this.monoAccount = false;
    this.waitingAccount = false;
  }

  // Временная кнопка для моно-аккаунта
  changeMonoAccount(): void {
    this.monoAccount = !this.monoAccount;
    this.administrator = false;
    this.waitingAccount = false;
  }

  // Временная кнопка для моно-аккаунта
  changeWaitingAccount(): void {
    this.waitingAccount = !this.waitingAccount;
    this.monoAccount = false;
    this.administrator = false;
  }

  getOrganisations() {
    this.profileApi.getOrganizations().subscribe(data => {
      if (data) {
        // this.organisations = data;
        console.log(this.organisations, 'parent');
      }
    });
  }

  ngOnDestroy() {
    this.onDestroy.next();
  }
}
