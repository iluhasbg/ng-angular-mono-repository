import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-tab-payment-history',
  templateUrl: './tab-payment-history.component.html',
  styleUrls: ['./tab-payment-history.component.scss']
})
export class TabPaymentHistoryComponent implements OnInit {
  //Выпадающее меню Организация
  showOrganization = false;

  //Выпадающее меню Тип платежа - radio
  showPaymentType = false;
  radioSelected: any;

  //Выпадающее меню Тип услуги
  showServiceType = false;

  //Выпадающее меню Платеж от
  showPaymentFrom = false;

  //Выпадающее меню Платеж до
  showPaymentTo = false;

  //Выбранные чекбоксы в селекте Организация
  checkedListOrganization = [];
  currentSelectedOrganization: {};

  //Выбранные чекбоксы в селекте Тип услуги
  checkedListService = [];
  currentSelectedService: {};

  //Модальное окно с чеком оплаты
  showModalPay = false;

  //Выбранный платеж в таблице для вызова модалки
  selectedPay;

  //Форма поиска на мобилке
  form: FormGroup;

  //Тип платежа
  payments = [
    {
      id: '1',
      name: 'Пополнение',
      checked: false,
    },
    {
      id: '2',
      name: 'Списание',
      checked: false,
    }
  ];

  //Тип услуги
  services = [
    {
      id: '1',
      name: 'Регистрация товарного знака',
      checked: false,
    },
    {
      id: '2',
      name: 'Претензия',
      checked: false,
    },
    {
      id: '3',
      name: 'Представительство в Арбитражном суде',
      checked: false,
    },
    {
      id: '4',
      name: 'Взыскание',
      checked: false,
    }
  ];

  //Сколько показывать в истории платежей
  showPayment = 10;

  @Input() organizations;
  @Input() paymentHistory;

  constructor(private fb: FormBuilder) {
  }

  ngOnInit(): void {
    this.initForm();
  }

  //Форма поиска на мобилке
  initForm(): void {
    this.form = this.fb.group({
      search: ['', [Validators.required]],
    });
  }

  //Выпадлающее меню селекта Организация
  getSelectedValueOrganization(status: Boolean, value: String) {
    if (status) {
      this.checkedListOrganization.push(value);
    } else {
      const index = this.checkedListOrganization.indexOf(value);
      this.checkedListOrganization.splice(index, 1);
    }
    this.currentSelectedOrganization = {checked: status, name: value};
  }

  //Выпадлающее меню селекта Тип услуги
  getSelectedValueService(status: Boolean, value: String) {
    if (status) {
      this.checkedListService.push(value);
    } else {
      const index = this.checkedListService.indexOf(value);
      this.checkedListService.splice(index, 1);
    }
    this.currentSelectedService = {checked: status, name: value};
  }

  //Показ всей истории платежей
  paymentShow(): void {
    this.showPayment += 10;
  }

  //Открыть модальное окно добавления представителя
  openModalPay(id): void {
    this.showModalPay = true;
    const bodyStyles = document.body.style;
    bodyStyles.setProperty('overflow', 'hidden');
    this.selectedPay = this.paymentHistory.find((pay) => pay.id === id);
    console.log(this.selectedPay);
  }

  //Закрыть модальное окно добавления представителя
  closeModal(): void {
    this.showModalPay = false;
    const bodyStyles = document.body.style;
    bodyStyles.setProperty('overflow', 'visible');
  }
}
