import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TabPaymentHistoryComponent } from './tab-payment-history.component';

describe('TabPaymentHistoryComponent', () => {
  let component: TabPaymentHistoryComponent;
  let fixture: ComponentFixture<TabPaymentHistoryComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TabPaymentHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabPaymentHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
