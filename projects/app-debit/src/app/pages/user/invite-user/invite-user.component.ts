import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-invite-user',
  templateUrl: './invite-user.component.html',
  styleUrls: ['./invite-user.component.scss']
})
export class InviteUserComponent implements OnInit {
  //Временные данные
  organizations = [
    {
      id: '1',
      name: 'ИП Константинопольский Константин Андреевич',
      ogrnip: 'ОГРНИП 317774600010541',
      inn: 'ИНН 501209738853',
      city: 'Москва',
      balance: '17 325,93',
      bonus: '3 900,00',
      checked: false,
      cases: [
        {
          name: 'Регистрация товарного знака #55467',
          response: 'Ждем ответ от роспатента',
        },
        {
          name: 'Ведение дела в арбитражном суде #44345',
          response: 'Взыскание',
        },
        {
          name: 'Претензионная работа #53578',
          response: 'Претензия, ожидание ответа',
        }
      ],
      user: [
        {
          name: 'Гудреева Алена'
        },
        {
          name: 'Пчелкин Валентин',
        },
        {
          name: 'Козлов Владимир',
        },
        {
          name: 'Гудреева Алена',
        },
        {
          name: 'Пчелкин Валентин',
        }
      ]
    },
    {
      id: '2',
      name: 'ООО Ромашка',
      ogrnip: 'ОГРНИП 317774600010541',
      inn: 'ИНН 501209738853',
      city: 'Москва',
      balance: '17 325,93',
      bonus: '3 900,00',
      checked: false,
      cases: [],
      user: [
        {
          name: 'Гудреева Алена'
        },
        {
          name: 'Пчелкин Валентин',
        }
      ]
    }
  ];

  form: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(): void {
    this.form = this.fb.group({
      name: ['', [Validators.required]],
      surname: ['', [Validators.required]],
      position: ['', [Validators.required]],
      email: ['', [Validators.required]],
      organizations: this.fb.array(this.organizations.map(x => false)),
    });
  }
}
