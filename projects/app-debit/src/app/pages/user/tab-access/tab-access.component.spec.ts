import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TabAccessComponent } from './tab-access.component';

describe('TabAccessComponent', () => {
  let component: TabAccessComponent;
  let fixture: ComponentFixture<TabAccessComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TabAccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabAccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
