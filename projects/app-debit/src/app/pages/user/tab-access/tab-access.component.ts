import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-tab-access',
  templateUrl: './tab-access.component.html',
  styleUrls: ['./tab-access.component.scss']
})
export class TabAccessComponent implements OnInit {
  //Выпадающее меню с выбором организации
  showDropdown = false;
  selected;

  @Input() organizations;

  constructor() {
  }

  ngOnInit(): void {
    this.selected = this.organizations[0].name;
  }
}
