import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OrganizationComponent } from './organization/organization.component';
import { ProfileComponent } from './profile/profile.component';
import { ChangeBalanceComponent } from './change-balance/change-balance.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { AuthGuard } from '../../shared/guardes/auth-guard.service';
import { SystemComponent } from '../system.component';
import { UserCardSettingsComponent } from './user-card-settings/user-card-settings.component';
import { InviteUserComponent } from './invite-user/invite-user.component';
import { AddOrganizationComponent } from './add-organization/add-organization.component';
// Душан import { ChangeProfileComponent } from '../../../../../shared/components/user/change-profile/change-profile.component';

const systemRoutes: Routes = [
  {
    path: '', component: SystemComponent, canActivate: [AuthGuard], children: [
      {path: '', redirectTo: 'index', pathMatch: 'full'},
      // { Ганин закоментил
      //   path: 'user/change-profile',
      //   component: ChangeProfileComponent,
      // },
      // { Душан старый код
      //   path: 'user/change-profile',
      //   component: ChangeProfileComponent,
      //   data : {isDebit: true}
      // },
      {
        path: 'user/change-password',
        component: ChangePasswordComponent,
      },
      {
        path: 'user/change-balance',
        component: ChangeBalanceComponent,
      },
      {
        path: 'user/profile',
        component: ProfileComponent,
      },
      {
        path: 'user/organization',
        component: OrganizationComponent,
      },
      {
        path: 'user/settings',
        component: UserCardSettingsComponent,
      },
      {
        path: 'user/invite',
        component: InviteUserComponent,
      },
      {
        path: 'user/add-organization',
        component: AddOrganizationComponent,
      },
    ]
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(systemRoutes),
  ], exports: [
    RouterModule,
  ],
})
export class UserRoutingModule {
}
