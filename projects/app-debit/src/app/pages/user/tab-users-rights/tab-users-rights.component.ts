import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-tab-users-rights',
  templateUrl: './tab-users-rights.component.html',
  styleUrls: ['./tab-users-rights.component.scss']
})
export class TabUsersRightsComponent implements OnInit {
  //Массив для вабранных чекбоксов в селекте
  checkedList = [];

  //Объект из массива выбранных чекбоксов в селекте
  currentSelected: {};

  //Изначально выпадающее меню селекта скрыто
  showOrganization = false;

  constructor() {
  }

  @Input() organizations;
  @Input() users;

  ngOnInit(): void {
  }

  //Выпадлающее меню селекта - Организация
  getSelectedValue(status: Boolean, value: String) {
    if (status) {
      this.checkedList.push(value);
    } else {
      const index = this.checkedList.indexOf(value);
      this.checkedList.splice(index, 1);
    }
    this.currentSelected = {checked: status, name: value};
  }
}
