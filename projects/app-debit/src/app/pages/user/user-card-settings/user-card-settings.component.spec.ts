import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { UserCardSettingsComponent } from './user-card-settings.component';

describe('UserCardSettingsComponent', () => {
  let component: UserCardSettingsComponent;
  let fixture: ComponentFixture<UserCardSettingsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ UserCardSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserCardSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
