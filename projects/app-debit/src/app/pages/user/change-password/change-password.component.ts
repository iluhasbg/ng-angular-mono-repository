import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit, OnDestroy {
  userPassForm: FormGroup;
  passwordPattern = /^(?=.*\d)(?=.*[a-zа-я])(?=.*[A-ZА-Я]).{6,}$/;


  constructor(
    private fb: FormBuilder
  ) {
  }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(): void {
    const PassPattern = this.passwordPattern;

    this.userPassForm = this.fb.group({
      password: ['', [Validators.pattern(PassPattern)]],
      newPassword: ['', [Validators.pattern(PassPattern)]],
      confirmPassword: ['', [Validators.pattern(PassPattern)]],
    });
  }

  changePassword() {
    const body = {
      organizations: [
        {
          password: this.userPassForm.value.password,
          newPassword: this.userPassForm.value.newPassword,
          confirmPassword: this.userPassForm.value.confirmPassword,
        },
      ]
    };
  }

  ngOnDestroy() {
  }
}
