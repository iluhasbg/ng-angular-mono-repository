import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-confidants',
  templateUrl: './confidants.component.html',
  styleUrls: ['./confidants.component.scss']
})
export class ConfidantsComponent implements OnInit {
  //Временные данные
  confidants = [
    {
      name: 'doverennost_irina_99324.pdf',
      status: 'На проверке',
      date: '15.10.2020',
      from: '15.10.2020',
      to: '15.04.2021',
    },
    {
      name: 'doverennost_irina_99324.pdf',
      status: 'Действует',
      date: '15.10.2020',
      from: '15.10.2020',
      to: '15.04.2021',
    },
    {
      name: 'doverennost_irina_99324.pdf',
      status: 'Не действует',
      date: '15.10.2020',
      from: '15.10.2020',
      to: '15.04.2021',
    },
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
