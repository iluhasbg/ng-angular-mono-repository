import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ConfidantsComponent } from './confidants.component';

describe('ConfidantsComponent', () => {
  let component: ConfidantsComponent;
  let fixture: ComponentFixture<ConfidantsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfidantsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfidantsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
