import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tab-organizations-ip',
  templateUrl: './tab-organizations-ip.component.html',
  styleUrls: ['./tab-organizations-ip.component.scss']
})
export class TabOrganizationsIpComponent implements OnInit, OnChanges {
  // Сколько показывать дел
  showCases = 2;

  // Сколько показывать представителей
  showUsers = 3;
  hasData = false;
  cases;
  showAllCases = false;

  @Input() organizations;
  @Input() administrator;
  @Input() waitingAccount;
  @Input() organisations;
  @Input() tasks;
  orgData;

  constructor(private router: Router) {
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes && this.organisations) {
      console.log(this.organisations.organizations, 'child');
      this.orgData = this.organisations.organizations;
      this.cases = this.organisations.organizations.cases;
    }
  }


  // Показ всех дел - не очень реализовано; когда организаций больше, чем 1
  // по кнопке раскрываются сразу все дела во всех организациях
  casesShow(): void {
    this.showCases += 10;
  }

  // Показ всех представителей - тут аналогично с casesShow()
  usersShow(): void {
    this.showUsers += 10;
  }

  showAllCase() {
    this.showAllCases = true;
  }

  addOrg() {
    this.router.navigate(['user/add-organization']);
  }
}
