import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-organization',
  templateUrl: './organization.component.html',
  styleUrls: ['./organization.component.scss']
})
export class OrganizationComponent implements OnInit {
  //Временный флаг для переключения администратор/юзер без прав
  administrator = false;

  //Временный флаг Новая организация
  newOrganization = false;

  //Сколько показывать дел
  showCases = 3;

  //Сколько показывать в истории платежей
  showPayment = 3;

  //Форма поиска
  form: FormGroup;

  //Модальное окно добавления представителя
  showModalAddUser = false;

  //Временные данные
  organization = {
    name: 'ИП Константинопольский Константин Андреевич',
    ogrnip: 'ОГРНИП 317774600010541',
    city: 'Москва',
    tel: '+7 499 883 43 43',
    email: 'mail@mycompany.ru',
    cases: [
      {
        name: 'Регистрация товарного знака #55467',
        response: 'Ждем ответ от роспатента',
      },
      {
        name: 'Ведение дела в арбитражном суде #44345',
        response: 'Взыскание',
      },
      {
        name: 'Претензионная работа #53578',
        response: 'Претензия, ожидание ответа',
      },
    ],
  };

  archive = [
    {
      name: 'Регистрация товарного знака #55467',
      response: 'Завершено',
      date: '01.06.2020'
    },
    {
      name: 'Ведение дела в арбитражном суде #47521',
      response: 'Завершено',
      date: '01.06.2020'
    },
    {
      name: 'Регистрация товарного знака #55467',
      response: 'Завершено',
      date: '01.06.2020'
    },
    {
      name: 'Регистрация товарного знака #55467',
      response: 'Завершено',
      date: '01.06.2020'
    },
  ];

  users = [
    {
      name: 'Гудреева Алена',
      photo: 'https://via.placeholder.com/100x100',
      position: 'Руководитель',
      email: 'ivanova.i@companyname.ru',
      attorney: true,
      rights: [
        {
          trademarks: [
            {
              name: 'Поиск и просмотр',
            },
          ],
          patents: [],
          databases: [],
          disputes: [],
          copyrights: [],
        }
      ]
    },
    {
      name: 'Пчелкин Валентин',
      photo: null,
      position: 'Руководитель',
      email: 'ivanova.i@companyname.ru',
      attorney: true,
      rights: [
        {
          trademarks: [
            {
              name: 'Поиск и просмотр',
            },
          ],
          patents: [
            {
              name: 'Поиск и просмотр',
            },
            {
              name: 'Подача заявок',
            },
            {
              name: 'Согласование делопроизводства по заявке',
            },
            {
              name: 'Лицензионные договоры и коммерческая концессия',
            },
          ],
          databases: [
            {
              name: 'Поиск и просмотр',
            },
          ],
          disputes: [
            {
              name: 'Поиск и просмотр',
            },
            {
              name: 'Подача заявок',
            },
            {
              name: 'Согласование делопроизводства по заявке',
            },
            {
              name: 'Лицензионные договоры и коммерческая концессия',
            },
            {
              name: 'Поиск и просмотр',
            },
          ],
          copyrights: [],
        }
      ]
    },
    {
      name: 'Козлов Владимир',
      photo: 'https://via.placeholder.com/100x100',
      position: 'Руководитель',
      email: 'ivanova.i@companyname.ru',
      attorney: true,
      rights: [
        {
          trademarks: [
            {
              name: 'Поиск и просмотр',
            },
            {
              name: 'Подача заявок',
            },
          ],
          patents: [
            {
              name: 'Поиск и просмотр',
            },
            {
              name: 'Подача заявок',
            },
          ],
          databases: [],
          disputes: [
            {
              name: 'Поиск и просмотр',
            },
            {
              name: 'Подача заявок',
            },
            {
              name: 'Согласование делопроизводства по заявке',
            },
            {
              name: 'Лицензионные договоры и коммерческая концессия',
            },
            {
              name: 'Поиск и просмотр',
            },
          ],
          copyrights: [],
        }
      ]
    },
    {
      name: 'Гудреева Алена',
      photo: 'https://via.placeholder.com/100x100',
      position: 'Руководитель',
      email: 'ivanova.i@companyname.ru',
      attorney: true,
      rights: [
        {
          trademarks: [
            {
              name: 'Поиск и просмотр',
            },
            {
              name: 'Подача заявок',
            },
          ],
          patents: [
            {
              name: 'Поиск и просмотр',
            },
            {
              name: 'Подача заявок',
            },
            {
              name: 'Согласование делопроизводства по заявке',
            },
            {
              name: 'Лицензионные договоры и коммерческая концессия',
            },
          ],
          databases: [
            {
              name: 'Поиск и просмотр',
            },
          ],
          disputes: [],
          copyrights: [],
        }
      ]
    },
    {
      name: 'Пчелкин Валентин',
      photo: null,
      position: 'Руководитель',
      email: 'ivanova.i@companyname.ru',
      attorney: true,
      rights: [
        {
          trademarks: [
            {
              name: 'Поиск и просмотр',
            },
            {
              name: 'Подача заявок',
            },
            {
              name: 'Согласование делопроизводства по заявке',
            },
            {
              name: 'Лицензионные договоры и коммерческая концессия',
            },
          ],
          patents: [
            {
              name: 'Поиск и просмотр',
            },
          ],
          databases: [],
          disputes: [],
          copyrights: [],
        }
      ]
    }
  ];

  userAdmin = {
    name: 'Константинопольский К. А.',
    photo: 'https://via.placeholder.com/100x100',
    position: 'Руководитель',
    email: 'ivanova.i@companyname.ru',
  };

  paymentHistory = [
    {
      name: 'Регистрация товарного знака по тарифу «Оптимальный»',
      date: '21.10.2020',
      organization: 'ИП Константинопольский К.А.',
      case: 'Дело #54688',
      sum: '65 900,00',
      bonus: '3 900,00',
      minus: true,
      method: 'Онлайн',
    },
    {
      name: 'Ведение дела в арбитражном суде по тарифу «Максимальный»',
      date: '17.10.2020',
      organization: 'ООО Ромашка',
      case: 'Дело #12533',
      sum: '19 890,00',
      bonus: null,
      minus: true,
      method: 'Счет',
    },
    {
      name: 'Пополнение депозита',
      date: '12.10.2020',
      organization: 'ИП Константинопольский К.А.',
      case: null,
      sum: '4 890,00',
      bonus: null,
      minus: false,
      method: 'Онлайн',
    },
    {
      name: 'Ведение дела в арбитражном суде по тарифу «Взыскание»',
      date: '12.10.2020',
      organization: 'ООО Ромашка',
      case: 'Дело #54688',
      sum: '4 890,00',
      bonus: null,
      minus: false,
      method: 'Депозит',
    }
  ];

  constructor(private fb: FormBuilder) {
  }

  ngOnInit(): void {
    this.initForm();
  }

  //Показ всех дел
  casesShow(): void {
    this.showCases += 10;
  }

  //Показ всей истории платежей
  paymentShow(): void {
    this.showPayment += 10;
  }

  initForm(): void {
    this.form = this.fb.group({
      search: ['', [Validators.required]],
    });
  }

  //Открыть модальное окно добавления представителя
  openModalAddUser(): void {
    this.showModalAddUser = true;
    const bodyStyles = document.body.style;
    bodyStyles.setProperty('overflow', 'hidden');
  }

  //Закрыть модальное окно добавления представителя
  closeModal(): void {
    this.showModalAddUser = false;
    const bodyStyles = document.body.style;
    bodyStyles.setProperty('overflow', 'visible');
  }

  //Временная кнопка для переключения администратор/юзер без прав
  changeAdministratorRights(): void {
    this.administrator = !this.administrator;
    this.newOrganization = false;
  }

  //Временная кнопка для переключения на новую организацию
  changeNewOrganization(): void {
    this.newOrganization = !this.newOrganization;
    this.administrator = true;
  }
}
