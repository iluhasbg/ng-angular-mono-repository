import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TabAttorneyComponent } from './tab-attorney.component';

describe('TabAttorneyComponent', () => {
  let component: TabAttorneyComponent;
  let fixture: ComponentFixture<TabAttorneyComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TabAttorneyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabAttorneyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
