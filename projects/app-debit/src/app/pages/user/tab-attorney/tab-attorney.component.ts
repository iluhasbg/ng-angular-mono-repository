import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-tab-attorney',
  templateUrl: './tab-attorney.component.html',
  styleUrls: ['./tab-attorney.component.scss']
})
export class TabAttorneyComponent implements OnInit {
  //Выпадающее меню От кого
  showFromWhom = false;

  //Выпадающее меню Кому выдана
  showToWhom = false;

  //Выпадающее меню Статус
  showStatus = false;

  //Выбранные чекбоксы в селекте От кого
  checkedListFromWhom = [];
  currentSelectedFromWhom: {};

  //Выбранные чекбоксы в селекте Кому выдана
  checkedListToWhom = [];
  currentSelectedToWhom: {};

  //Выбранные чекбоксы в селекте Кому выдана
  checkedListStatus = [];
  currentSelectedStatus: {};

  //Сколько показывать доверенностей
  showAttorney = 10;

  //Форма поиска на мобилке
  form: FormGroup;

  //Временные данные кому выдана доверенность
  toWhom = [
    {
      id: '1',
      name: 'ООО Гардиум',
      checked: false,
    },
    {
      id: '2',
      name: 'Петр Николаев',
      checked: false,
    }
  ];

  statuses = [
    {
      id: '1',
      name: 'На проверке',
      checked: false,
    },
    {
      id: '2',
      name: 'Действует',
      checked: false,
    },
    {
      id: '3',
      name: 'Не действует',
      checked: false,
    }
  ]

  @Input() organizations;
  @Input() attorney;

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.initForm();
  }

  //Форма поиска на мобилке
  initForm(): void {
    this.form = this.fb.group({
      search: ['', [Validators.required]],
    });
  }

  //Выпадлающее меню селекта От кого
  getSelectedValueFromWhom(status: Boolean, value: String) {
    if (status) {
      this.checkedListFromWhom.push(value);
    } else {
      const index = this.checkedListFromWhom.indexOf(value);
      this.checkedListFromWhom.splice(index, 1);
    }
    this.currentSelectedFromWhom = {checked: status, name: value};
  }

  //Выпадлающее меню селекта Кому выдана
  getSelectedValueToWhom(status: Boolean, value: String) {
    if (status) {
      this.checkedListToWhom.push(value);
    } else {
      const index = this.checkedListToWhom.indexOf(value);
      this.checkedListToWhom.splice(index, 1);
    }
    this.currentSelectedToWhom = {checked: status, name: value};
  }

  //Выпадлающее меню селекта Кому выдана
  getSelectedValueStatus(status: Boolean, value: String) {
    if (status) {
      this.checkedListStatus.push(value);
    } else {
      const index = this.checkedListStatus.indexOf(value);
      this.checkedListStatus.splice(index, 1);
    }
    this.currentSelectedStatus = {checked: status, name: value};
  }

  //Показ всех доверенностей
  attorneyShow(): void {
    this.showAttorney += 10;
  }
}
