import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';
import {UserProfileService} from '../../../../../../shared/services/user-profile.service';

@Component({
  selector: 'app-add-organization',
  templateUrl: './add-organization.component.html',
  styleUrls: ['./add-organization.component.scss']
})
export class AddOrganizationComponent implements OnInit, OnDestroy {
  addOrg: FormGroup;

  // Организация добавлена
  addOrganization = false;
  private readonly onDestroy = new Subject<void>();


  constructor(
    private fb: FormBuilder,
    private profileApi: UserProfileService,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(): void {
    this.addOrg = this.fb.group({
      OGRN: ['', [Validators.required]],
      INN: ['', [Validators.required]],
      name: ['', [Validators.required]],
    });
  }

  addOrganisation() {
    const body = {
      organizations: [
        {
          name: this.addOrg.value.name,
          TIN: this.addOrg.value.INN,
          PSRN: this.addOrg.value.OGRN
        },
      ]
    };
    this.profileApi.createOrganizations(body).pipe(takeUntil(this.onDestroy)).subscribe(data => {
      console.log(data);
      if (data.error.length) {
        console.log(data.error);
      } else {
        this.router.navigate(['user/profile']);
      }
    });
  }

  // Временная кнопка для моно-аккаунта
  changeAddOrganization(): void {
    this.addOrganization = !this.addOrganization;
  }

  ngOnDestroy() {
    this.onDestroy.next();
  }
}
