import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable, Subject, timer } from 'rxjs';
import { RegistrationService } from './registration.service';
import { map, take, takeUntil } from 'rxjs/operators';
import { HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';
import { nextTick } from '../../../shared/utils/next-tick';

import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss'],
  providers: [],
})
export class RegistrationComponent implements OnInit, OnDestroy {

  footerText = environment.FOOTER_TEXT;

  registrationForm: FormGroup;
  codeEntered = false;
  codeMatched = false;
  passwordMatch: boolean;
  private readonly onDestroy = new Subject<void>();
  intNumber;
  phoneError = false;
  errorText;
  codeError = false;
  isCodeSuccess = false;
  errorRegister = false;
  counter$: Observable<number>;
  count = 45;
  showTimer = false;
  showRepeat = false;
  passwordPattern = /^(?=.*\d)(?=.*[a-zа-я])(?=.*[A-ZА-Я]).{6,}$/;
  showLoader: boolean;
  phone = '';
  isPhoneValid = false;
  brand = '';
  staticFolder = '';


  constructor(private registerApi: RegistrationService,
              private router: Router,
  ) {
    this.brand = environment.BRAND;
    this.staticFolder = environment.STATIC_FILES;
    console.log('brand: ', this.brand);
    console.log('prod: ', environment.production);
  }

  ngOnInit() {
    this.showLoader = false;
    this.passwordMatch = true;
    this.initForm();

  }

  testTimer() {
    this.showTimer = true;
    this.counter$ = timer(0, 1000).pipe(
      take(this.count),
      map(() => --this.count)
    );
  }

  initForm() {
    const PassPattern = this.passwordPattern;

    this.registrationForm = new FormGroup({
      name: new FormControl(null, Validators.required),
      surname: new FormControl(null, Validators.required),
      phone: new FormControl(null, Validators.required),
      email: new FormControl(null, Validators.required),
      newPassword: new FormControl(null, Validators.pattern(PassPattern)),
      password: new FormControl(null, Validators.pattern(PassPattern)),
      code: new FormControl(null, Validators.required),
    });
  }


  registerUser() {
    this.showLoader = true;
    const data = {
      firstName: this.registrationForm.value.name,
      lastName: this.registrationForm.value.surname,
      phone: this.phone,
      login: this.registrationForm.value.email,
      password: this.registrationForm.value.password,
      validationCode: this.registrationForm.value.code,
    };
    const params = new HttpParams()
      .set('brand', this.brand);
    this.registerApi.registerUser(data, params)
      .pipe(takeUntil(this.onDestroy))
      .subscribe(regData => {
        console.log('success', regData);
        if (regData.answer === 'Успешно') {
          console.log('success');
          this.router.navigate(['/login']);
        } else {
          this.showLoader = false;
          this.errorRegister = true;
          this.errorText = regData.answer;
        }
      });
  }

  initPhoneForm() {
    if (this.phone.length < 1) {
      this.phone = '+7';
    }
    this.codeMatched = false;
    this.codeEntered = false;
    this.showTimer = false;
    this.codeError = false;
    this.phoneError = false;
  }

  codeGetter() {
    this.testTimer();
    const params = new HttpParams()
      .set('phone', this.phone.toString())
      .set('brand', this.brand);
    this.registerApi.getCode(params)
      .pipe(takeUntil(this.onDestroy))
      .subscribe(data => {
        if (data.answer === 'Успешно') {
          console.log('success');
        } else {
          this.codeError = true;
          console.log(this.codeError, 'code');
          this.errorText = data.answer;
        }
      });
    this.codeEntered = true;
  }

  onInputPhone(e) {
    nextTick(() => {
      let phone = this.phone.slice(2).replace(/\D+/, '');
      if (phone.length !== 10) {
        phone = phone.slice(0, 10);
      }
      phone = '+7' + phone;
      console.log('ohine', phone, this.phone);
      if (phone !== this.phone) {
        this.phone = phone;
      }

      this.validatePhone();
    });
  }

  validatePhone() {
    const re = new RegExp('^(\\+7|7|8)?[\\s\\-]?\\(?[489][0-9]{2}\\)?[\\s\\-]?[0-9]{3}[\\s\\-]?[0-9]{2}[\\s\\-]?[0-9]{2}$');
    if (re.test(this.phone)) {
      this.isPhoneValid = true;
      console.log('Valid');
    } else {
      console.log('Invalid');
      this.isPhoneValid = false;
    }
  }

  fetchCode() {
    this.showRepeat = true;
    this.codeGetter();
  }

  fetchCodeRepeat() {
    this.count = 45;
    this.showRepeat = false;
    this.showTimer = true;
    this.codeGetter();
    // this.initForm();
  }

  checkCode(event, maxLength) {
    const body = {
      phone: this.phone,
      validationCode: this.registrationForm.value.code
    };

    this.codeError = false;

    if (maxLength === 4 && this.registrationForm.value.code.length === 4) {
      const params = new HttpParams()
        .set('brand', this.brand);
      this.registerApi.checkCode(body, params)
        .pipe(takeUntil(this.onDestroy))
        .subscribe(data => {
          console.log(data.answer);
          console.log(data);
          if (data.answer === 'Успешно') {
            this.isCodeSuccess = true;
            console.log('success');
          } else {
            this.codeError = true;
            this.errorText = data.answer;
          }
        });
      console.log(this.phoneError, 'phone');
      this.codeMatched = true;
    }
  }

  resetPhone() {
    this.codeMatched = false;
    this.codeEntered = false;
    this.showTimer = false;
    this.phone = '';
    this.codeError = false;
    this.phoneError = false;
  }

  comparePasswords() {
    if (this.registrationForm.value.newPassword === this.registrationForm.value.password) {
      console.log('no match');
      this.passwordMatch = true;
    } else {
      this.passwordMatch = false;
    }
  }

  onCountryChange(event) {
    alert(event);
  }

  tellInputObject(event) {
    alert(event);
  }

  getNumber(event) {
    this.intNumber = event;
  }

  hasError(event) {
    alert(event);
  }

  ngOnDestroy(): void {
    this.onDestroy.next();
    this.showLoader = false;
  }
}
