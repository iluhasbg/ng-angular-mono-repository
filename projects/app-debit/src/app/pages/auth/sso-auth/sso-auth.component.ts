import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DebitSsoService } from './debit-sso.service';
import { environment } from '../../../../../../themes/debit-default/app.environment';

@Component({
  selector: 'app-sso-auth',
  templateUrl: './sso-auth.component.html',
  styleUrls: ['./sso-auth.component.scss']
})
export class SsoAuthComponent implements OnInit {

  xkey;
  showPhoneForm = false;
  code;
  footerText;
  logoPath;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private ssoAlfa: DebitSsoService
  ) {
    this.footerText = environment.FOOTER_TEXT;
    this.logoPath = environment.LOGO_PATH;


  }

  ngOnInit(): void {
    this.route.queryParams.subscribe(param => {
      if (param.xkey) {
        this.xkey = param.xkey;
        this.loginAlfa(this.xkey);
      }
    });
  }

  loginAlfa(xkey) {
    console.log('loginAlfa xkey: ', xkey);
    this.ssoAlfa.login(xkey).subscribe((data) => {
      if (data.validation) {
        console.log('data.validation: ', data);
        this.setUserData(data);
        this.showPhoneForm = true;
      } else {
        console.log('else data.validation: ', data);
        this.loginDefault(data);
      }
    });
  }

  setUserData(data) {
    if (data.token.length) {
      localStorage.setItem('currentUserIPID', (data.token));
    }
    localStorage.setItem('currentUserData', (JSON.stringify(data.user)));
  }

  loginDefault(login) {
    console.log('sso: ', 1);
    if (login.token.length && login.user.phone) {
      console.log('sso: ', 2);
      this.setUserData(login);
      if (login.caseID) {
        console.log('sso: ', 3);
        this.router.navigate([`/index/item`],
          {queryParams: {id: login.caseID}});
      } else {
        console.log('sso: ', 4);
        this.router.navigate(['/index']);
      }
    } else if (login.token.length && !login.user.phone) {
      console.log('sso: ', 5);
      this.setUserData(login);
      this.router.navigate([`/user/change-profile`]);
    }
  }

  loginWIthCode() {
    if (this.code.length === 4) {
      this.ssoAlfa.login(this.xkey, this.code).subscribe(login => {
        this.loginDefault(login);
      });
    }
  }

}
