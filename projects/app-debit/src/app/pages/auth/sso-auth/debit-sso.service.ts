import { Injectable } from '@angular/core';
import { ILogin } from '../../../models/main.interfaces';
import { HttpService } from '../../../services/http.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../../environments/environment';

const baseUrl = environment.BASE_URL_API + '/user/login';

@Injectable({
  providedIn: 'root'
})
export class DebitSsoService {

  constructor(
    private http: HttpService,
    private api: HttpClient
  ) { }

  login(xkey, phone?) {
    console.log('xkey: ', xkey);
    console.log('phone: ', phone);
    console.log('baseUrl: ', baseUrl);
    if (phone) {
      console.log('sso phone');
      const header = new HttpHeaders({'Service-X-Key': xkey, 'validationCode': phone });
      const options = { headers: header };
      console.log('options: ', options);
      return this.api.post<ILogin>(baseUrl, null, options);
    } else {
      console.log('sso no phone');
      const header = new HttpHeaders({'Service-X-Key': xkey });
      const options = { headers: header };
      console.log('options: ', options);
      return this.api.post<ILogin>(baseUrl, null, options);
    }
  }
}
