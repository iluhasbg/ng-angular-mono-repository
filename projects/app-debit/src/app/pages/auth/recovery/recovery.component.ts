import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { RecoveryService } from './recovery.service';
import { HttpParams } from '@angular/common/http';
import { takeUntil } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';


import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-recovery',
  templateUrl: './recovery.component.html',
  styleUrls: ['./recovery.component.scss']
})
export class RecoveryComponent implements OnInit, OnDestroy {
  brand = '';


  constructor(private recoverApi: RecoveryService,
              private route: ActivatedRoute,
  ) {
    this.brand = environment.BRAND;
    console.log('brand: ', this.brand);
    console.log('prod: ', environment.production);
  }

  private readonly onDestroy = new Subject<void>();
  recoveryForm: FormGroup;
  recoverPass: FormGroup;

  footerText = environment.FOOTER_TEXT;


  default = true;
  success = false;
  error = false;
  passwordSC = false;
  passwordMatcher = false;
  recoverError;
  passwordMatch: boolean;
  showLoader: boolean;
  ID;
  restoreSuccess = false;
  passwordPattern = /^(?=.*[0-9])(?=.*[!"#$%&'()*+,.\/:;<=>?@^_`{|}~])[a-zA-Z0-9А-Яа-я!"#$%&'()*+,.\/:;<=>?@^_`{|}~]{6,16}$/;


  ngOnInit(): void {
    this.showLoader = false;
    this.initForm();
    this.passwordMatch = true;

    this.getUrlParam();
    if (this.ID) {
      this.passwordSC = true;
      this.default = false;
    }
  }

  initForm() {
    const PassPattern = this.passwordPattern;

    this.recoveryForm = new FormGroup({
      email: new FormControl('', Validators.required),
    });

    this.recoverPass = new FormGroup({
      password: new FormControl('', [Validators.required, Validators.pattern(PassPattern)]),
      confirmPassword: new FormControl('', [Validators.required, Validators.pattern(PassPattern)]),
    });
  }

  recover() {
    const params = new HttpParams()
      .set('login', this.recoveryForm.value.email.toString())
      .set('brand', this.brand);
    this.recoverApi.recoverAccount(params)
      .pipe(takeUntil(this.onDestroy))
      .subscribe(data => {
        if (data.answer === 'Успешно') {
          this.passwordSC = true;
          this.default = false;
        }
        if (data.answer === 'Указанный пользователь не зарегистрирован.') {
        } else {
          this.success = false;
          this.default = false;
          this.error = true;
        }
      });
  }

  getMail() {
    this.showLoader = true;
    const params = new HttpParams()
      .set('login', this.recoveryForm.value.email.toString())
      .set('brand', this.brand);
    this.recoverApi.recoverAccountMail(params)
      .pipe(takeUntil(this.onDestroy))
      .subscribe(dataRecover => {
        if (dataRecover.answer === 'Успешно') {
          this.passwordSC = false;
          this.default = false;
          this.success = true;
        } else {
          this.showLoader = false;
          this.success = false;
          this.default = false;
          this.error = true;
        }
      });
  }

  getUrlParam() {
    this.route.queryParams.subscribe(params => {
      console.log(params.id);
      if (params.id) {
        this.ID = params.id;
      }
    });
  }

  resetPassword() {
    this.showLoader = true;
    const body = {
      id: this.ID,
      passNew: this.recoverPass.value.password,
      passConfirm: this.recoverPass.value.confirmPassword,
    };
    const params = new HttpParams()
      .set('brand', this.brand);
    this.recoverApi.resetPassword(body, params)
      .pipe(takeUntil(this.onDestroy))
      .subscribe(recoverData => {
        console.log(recoverData);
        if (recoverData.answer === 'Успешно') {
          this.restoreSuccess = true;
          this.success = true;
          this.default = false;
          this.error = false;
          this.passwordSC = false;
        } else {
          this.showLoader = false;
          this.recoverError = recoverData.answer;
          this.error = true;
          this.success = false;
        }
      });
  }

  retryRecovery() {
    this.default = true;
    this.error = false;
    this.success = false;
  }


  ngOnDestroy(): void {
    this.onDestroy.next();
  }

  comparePasswords() {
    if (this.recoverPass.value.password === this.recoverPass.value.confirmPassword) {
      console.log('no match');
      this.passwordMatch = true;
    } else {
      this.passwordMatch = false;
    }
  }
}
