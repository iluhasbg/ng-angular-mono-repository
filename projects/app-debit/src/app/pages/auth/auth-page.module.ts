import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthPageRoutingModule } from './auth-page-routing.module';
import { AuthComponent } from './auth.component';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { RecoveryComponent } from './recovery/recovery.component';
import { Ng2TelInputModule } from 'ng2-tel-input';
import { AuthPageGuard } from './auth-page.guard';
import { SharedAuthModule } from '../../../../../shared/components/auth/shared-auth.module';
import { SystemPageModule } from '../system-page.module';
import { SsoAuthComponent } from './sso-auth/sso-auth.component';

@NgModule({
  imports: [
    CommonModule,
    AuthPageRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    Ng2TelInputModule,
    SharedAuthModule,
    SystemPageModule
  ],
  declarations: [
    AuthComponent,
    LoginComponent,
    RegistrationComponent,
    RecoveryComponent,
    SsoAuthComponent,
  ],
  providers: [
    AuthPageGuard,
  ]
})
export class AuthPageModule {
}
