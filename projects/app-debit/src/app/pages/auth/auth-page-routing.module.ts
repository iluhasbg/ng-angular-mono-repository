import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthComponent } from './auth.component';
import { AuthPageGuard } from './auth-page.guard';
import { LoginComponent } from '../../../../../shared/components/auth/login/login.component';
import { RegistrationComponent } from '../../../../../shared/components/auth/registration/registration.component';
import { RecoveryComponent } from '../../../../../shared/components/auth/recovery/recovery.component';
import { SsoAuthComponent } from './sso-auth/sso-auth.component';
import { ExternalAuthComponent } from '../../../../../shared/components/auth/external-auth/external-auth.component';

const authRoutes: Routes = [
  {
    path: 'alfa',
    component: SsoAuthComponent,
  },
  {
    path: 'external-auth',
    component: ExternalAuthComponent,
  },
  {
    path: '', component: AuthComponent, canActivate: [AuthPageGuard], children: [
      {path: '', redirectTo: 'login', pathMatch: 'full', data : {isDebit: true}},
      {path: 'login', component: LoginComponent, data : {isDebit: true}},
      {path: 'registration', component: RegistrationComponent, data : {isDebit: true}},
      {path: 'recovery', component: RecoveryComponent, data : {isDebit: true}},
    ],
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(authRoutes),
  ], exports: [
    RouterModule,
  ],
})
export class AuthPageRoutingModule {
}
