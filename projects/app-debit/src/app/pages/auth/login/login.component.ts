import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { LoginService } from './login.service';
import { ToastrService } from 'ngx-toastr';
import { map, take, takeUntil } from 'rxjs/operators';
import { Observable, Subject, timer } from 'rxjs';
import { Router } from '@angular/router';
import { HttpParams } from '@angular/common/http';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {

  loginForm: FormGroup;
  incorrectLoginData = false;
  private readonly onDestroy = new Subject<void>();
  emailPattern = /^\S+@\S+?\.[a-zA-Z]{2,3}$/;
  showLoader: boolean;
  brand = '';

  footerText = environment.FOOTER_TEXT;

  constructor(
    private toastr: ToastrService,
    private loginService: LoginService,
    private fb: FormBuilder,
    private router: Router,
  ) {
    this.brand = environment.BRAND;
    console.log('brand: ', this.brand);
    console.log('prod: ', environment.production);
  }

  ngOnInit() {
    this.showLoader = false;
    this._initForm();
  }


  private _initForm() {

    this.loginForm = new FormGroup({
      email: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
    });
  }

  login() {
    this.showLoader = true;
    const authData = {
      login: this.loginForm.value.email,
      pass: this.loginForm.value.password,
    };
    console.log(authData);
    const params = new HttpParams()
      .set('brand', this.brand);
    this.loginService.login(authData, params)
      .pipe(takeUntil(this.onDestroy))
      .subscribe(data => {
        if (data.token) {
          localStorage.setItem('currentUserIPID', (data.token));
          if (data.user) {
            localStorage.setItem('currentUserData', (JSON.stringify(data.user)));
          }
          this.router.navigate(['/index']);
        } else if (data.answer === 'Неверный логин или пароль.') {
          console.log('incorrect pass');
          this.showLoader = false;
          this.incorrectLoginData = true;
        } else {
          console.log('error');
          this.showLoader = false;
        }
      });
  }

  ngOnDestroy(): void {
    this.onDestroy.next();
  }
}
