import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import {HttpService} from '../../../services/http.service';
import {ILogin} from '../../../models/main.interfaces';

const path = 'user';


@Injectable()
export class LoginService {

  constructor(private http: HttpService) {
  }

  login(body, params) {
     return this.http.post<ILogin>({path: `${path}/login`, body: {body}, subjectType: 'login', params});
  }

}
