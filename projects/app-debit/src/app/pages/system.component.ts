import { Component, OnInit } from '@angular/core';
import { SharedService } from '../services/shared.service';

@Component({
  selector: 'app-system',
  templateUrl: './system.component.html',
  styleUrls: ['./system.component.scss'],
})

export class SystemComponent implements OnInit {


  constructor(private shared: SharedService) {
  }

  ngOnInit() {
  }

  closeDropdowns() {
    this.shared.closeModals.next(null);
  }
}
