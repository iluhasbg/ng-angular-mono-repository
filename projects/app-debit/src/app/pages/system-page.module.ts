import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { ClickOutsideModule } from 'ng-click-outside';
import { SystemPageRoutingModule } from './system-page-routing.module';
import { SystemComponent } from './system.component';
import { AuthGuard } from '../shared/guardes/auth-guard.service';
import { MainFooterComponent } from './components/main-footer/main-footer.component';
import { MainMenuComponent } from './components/main-menu/main-menu.component';
import { LoginService } from './auth/login/login.service';
import { MatTooltipModule } from '@angular/material/tooltip';
import { IndexComponent } from './index/index.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { LogoComponent } from './components/logo/logo.component';
import { IndexPaginationComponent } from './index/index-pagination/index-pagination.component';
import { IndexItemComponent } from './index/index-item/index-item.component';
import { IndexPersonComponent } from './index/index-person/index-person.component';
import { PostModalComponent } from './index/post-modal/post-modal.component';
import { IndexTariffComponent } from './index/index-tariff/index-tariff.component';
import { IndexPaymentComponent } from './index/index-payment/index-payment.component';
import { CreatingClaimComponent } from './request-claim/creating-claim/creating-claim.component';
import { ModalEditDocComponent } from './request-claim/modal-edit-doc/modal-edit-doc.component';
import { UserModule } from './user/user.module';
import { RequestPageComponent } from './index/index-item/request-page/request-page.component';
import { ClaimPageComponent } from './index/index-item/claim-page/claim-page.component';
import { CourtPageComponent } from './index/index-item/court-page/court-page.component';
import { PenaltyPageComponent } from './index/index-item/penalty-page/penalty-page.component';
import { FinishedPageComponent } from './index/index-item/finished-page/finished-page.component';
import { DocTabComponent } from './index/index-item/tabs/doc-tab/doc-tab.component';
import { EventsTabComponent } from './index/index-item/tabs/events-tab/events-tab.component';
import { TabsTypesComponent } from './index/index-item/tabs/tabs-types/tabs-types.component';
import { TabHasDocsComponent } from './index/index-item/tabs/tabs-types/tab-has-docs/tab-has-docs.component';
import { TabAddingDocsComponent } from './index/index-item/tabs/tabs-types/tab-adding-docs/tab-adding-docs.component';
import { TabWithTableComponent } from './index/index-item/tabs/tabs-types/tab-with-table/tab-with-table.component';
import { TabCommonInfoComponent } from './index/index-item/tabs/tabs-types/tab-common-info/tab-common-info.component';
import { TabTariffComponent } from './index/index-item/tabs/tabs-types/tab-tariff/tab-tariff.component';
import { TabResultsComponent } from './index/index-item/tabs/tabs-types/tab-results/tab-results.component';
import { TabWrongComponent } from './index/index-item/tabs/tabs-types/tab-wrong/tab-wrong.component';
import { TabDocMixComponent } from './index/index-item/tabs/tabs-types/tab-doc-mix/tab-doc-mix.component';
import { TabWhatWeDidComponent } from './index/index-item/tabs/tabs-types/tab-what-we-did/tab-what-we-did.component';
import { TabComplexFormComponent } from './index/index-item/tabs/tabs-types/tab-complex-form/tab-complex-form.component';
import { WarningWithListComponent } from './index/index-item/tabs/tabs-types/warnings/warning-with-list/warning-with-list.component';
import { WarningPaymentComponent } from './index/index-item/tabs/tabs-types/warnings/warning-payment/warning-payment.component';
import { WarningTextComponent } from './index/index-item/tabs/tabs-types/warnings/warning-text/warning-text.component';
import { TestComponent } from './test/test.component';
import { DynamicFormModule } from '../dynamic-form/dynamic-form.module';
import { DynamicFormControlsModule } from '../dynamic-form-controls/dynamic-form-controls.module';
import { RequestClaimComponent } from './request-claim/request-claim.component';
import { FinishedClaimComponent } from './request-claim/finished-claim/finished-claim.component';
import { PaymentClaimComponent } from './request-claim/payment-claim/payment-claim.component';
import { PaymentModule } from '../../../../shared/components/payment/payment.module';
import { ErrorService } from '../../../../app-ipid/src/app/shared/services/error-service';
import { ButtonModule } from '../../../../shared/components/button/button.module';
import {SharedModule} from '../../../../app-ipid/src/app/shared/shared.module';
import {PaginationDebitComponent} from '../shared/components/pagination/pagination-debit.component';
import {RatesDebitComponent} from '../shared/components/rates/rates-debit.component';
import {IndexWidgetComponent} from './index/index-widget/index-widget.component';
import {UserPhoneModule} from '../../../../shared/components/user-phone/user-phone.module';
import { SurchargeRequestComponent } from './index/index-item/surcharge-request/surcharge-request.component';
import { SurchargePaymentComponent } from './index/index-item/surcharge-request/surcharge-payment/surcharge-payment.component';
import {SurchargeRequestService} from './index/index-item/surcharge-request/services/surcharge-request.service';
import { SurchargeFinishedComponent } from './index/index-item/surcharge-request/surcharge-finished/surcharge-finished.component';
import {DownloadFilesService} from '../../../../shared/services/download-files.service';
import {BsDatepickerModule} from 'ngx-bootstrap/datepicker';
import { InterestService } from '../../../../shared/services/interest.service';
import {ExpertModule} from '../../../../app-ipid/src/app/pages/layout/expert/expert.mdoule';

@NgModule({
    imports: [
        CommonModule,
        SystemPageRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        ToastrModule.forRoot(),
        MatTooltipModule,
        ClickOutsideModule,
        PerfectScrollbarModule,
        FontAwesomeModule,
        UserModule,
        DynamicFormModule,
        DynamicFormControlsModule,
        PaymentModule,
        ButtonModule,
        SharedModule,
        UserPhoneModule,
        BsDatepickerModule,
        ExpertModule
    ],
  declarations: [
    SystemComponent,
    MainMenuComponent,
    MainFooterComponent,
    LogoComponent,
    IndexComponent,
    IndexPaginationComponent,
    IndexItemComponent,
    IndexPersonComponent,
    PostModalComponent,
    IndexTariffComponent,
    IndexPaymentComponent,
    CreatingClaimComponent,
    ModalEditDocComponent,
    RequestPageComponent,
    ClaimPageComponent,
    CourtPageComponent,
    PenaltyPageComponent,
    FinishedPageComponent,
    DocTabComponent,
    EventsTabComponent,
    TabsTypesComponent,
    TabHasDocsComponent,
    TabAddingDocsComponent,
    TabWithTableComponent,
    TabCommonInfoComponent,
    TabTariffComponent,
    TabResultsComponent,
    TabWrongComponent,
    TabDocMixComponent,
    TabWhatWeDidComponent,
    TabComplexFormComponent,
    WarningWithListComponent,
    WarningPaymentComponent,
    WarningTextComponent,
    TestComponent,
    RequestClaimComponent,
    FinishedClaimComponent,
    PaymentClaimComponent,
    PaginationDebitComponent,
    RatesDebitComponent,
    IndexWidgetComponent,
    SurchargeRequestComponent,
    SurchargePaymentComponent,
    SurchargeFinishedComponent
  ],
    exports: [
        LogoComponent,
    ], providers: [
    AuthGuard,
    LoginService,
    ErrorService,
    SurchargeRequestService,
    DownloadFilesService,
    InterestService
  ]
})
export class SystemPageModule {
}
