import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PenaltyPageComponent } from './penalty-page.component';

describe('PenaltyPageComponent', () => {
  let component: PenaltyPageComponent;
  let fixture: ComponentFixture<PenaltyPageComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PenaltyPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PenaltyPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
