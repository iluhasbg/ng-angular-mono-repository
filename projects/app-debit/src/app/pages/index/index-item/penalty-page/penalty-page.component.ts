import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-penalty-page',
  templateUrl: './penalty-page.component.html',
  styleUrls: ['./penalty-page.component.scss']
})
export class PenaltyPageComponent implements OnInit {
  @Input() caseData;
  @Input() caseID;
  @Input() mobile;
  @Input() declOfDaysPassed;
  @Input() declOfDaysLeft;
  typeColor = 'penalty';

  constructor() { }

  ngOnInit(): void {

  }

}
