import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-request-page',
  templateUrl: './request-page.component.html',
  styleUrls: ['./request-page.component.scss']
})
export class RequestPageComponent implements OnInit {
  @Input() caseData;
  @Input() caseID;
  @Input() mobile;
  typeColor = 'request';

  constructor() { }

  ngOnInit(): void {

  }
}
