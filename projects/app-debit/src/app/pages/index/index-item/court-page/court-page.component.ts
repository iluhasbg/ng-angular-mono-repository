import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-court-page',
  templateUrl: './court-page.component.html',
  styleUrls: ['./court-page.component.scss']
})
export class CourtPageComponent implements OnInit {
  @Input() caseData;
  @Input() caseID;
  @Input() mobile;
  @Input() declOfDaysPassed;
  @Input() declOfDaysLeft;
  typeColor = 'court';

  constructor() { }

  ngOnInit(): void {

  }

}
