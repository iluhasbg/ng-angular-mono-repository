import {Injectable} from '@angular/core';
import {SurchargeService} from './surcharge.service';
import {ISurchargePayment} from '../models/surcharge.model';
import {Router} from '@angular/router';
import {HttpService} from '../../../../../services/http.service';
import {finalize} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})

export class SurchargeRequestService {
  step = 0;
  isLoading = false;
  responseID: string;
  caseID: string;
  eventID: string;
  extraPayType: string;
  rates = {min: null, opty: null, max: null}; // тарифы
  countRates; // количество тарифов
  activeTariff: string; // id выбранного тарифа
  duty: any; // пошлины
  sumBeforeDiscount = 0; // общая сумма (тариф + пошлины) до скидки
  paymentSum; // сумма к оплате с учетом скидки
  draft; // черновик
  paymentType: 'online' | 'offline' | 'act' = 'online'; // тип оплаты
  payer; // плательщик

  constructor(
    private surchargeService: SurchargeService,
    private router: Router,
    private httpService: HttpService
  ) {
  }

  createSurcharge() {
    const query = {
      caseID: this.caseID,
      eventID: this.eventID,
      extraPayType: this.extraPayType,
    };
    this.isLoading = true;
    return this.surchargeService.requestSurcharge(query).subscribe((data) => {
      if (data) {
        this.step = 1;
        this.responseID = data.responseID;
        const routeWithResponseID = window.location.protocol + '//' + window.location.host + window.location.pathname + `?id=${this.responseID}`;
        window.history.replaceState({}, '', routeWithResponseID);
        this.router.navigate(['/surcharge-request'], {queryParams: {id: this.responseID}});
        this.getRates();
      }
    });
  }

  /**
   * Запрос на восстановление заявки по id
   * @param responseID: id заявки
   */
  loadRequestIfNeeded(responseID: string): void {
    if (!this.responseID) {
      this.loadRequest(responseID);
    }
  }

  /**
   * Запрос на восстановление заявки по id
   * @param responseID: id заявки
   */
  loadRequest(responseID: string) {
    const params = {
      isDraft: true
    };
    this.isLoading = true;
    this.surchargeService.requestSurchargeGetDetail(params, responseID)
      .subscribe(data => {
          this.draft = data.task;
          this.responseID = data.responseID;
          if (this.draft) {
            this.parseDraft();
          }
      });
  }

  /**
   * Восстановление из черновика
   */
  parseDraft() {
    this.activeTariff = this.draft.tariff;
    this.caseID = this.draft.caseID;
    switch (this.draft.lastStage) {
      case 1:
        this.step = this.draft.lastStage;
        break;
      case 3:
        this.step = 2;
        break;
      case 4:
        this.step = 3;
        break;
    }
    this.getRates();
  }

  /**
   * Получение тарифов и пошлин
   */
  getRates(): void {
    this.isLoading = true;
    this.httpService.get({
      path: 'request/tariff',
      params: {responseID: this.responseID}
    }).subscribe((data: any) => {
      if (data?.tariff && data.duty) {
        this.isLoading = false;
        this.countRates = data.tariff?.length;
        data.tariff.forEach((item, index) => {
          if (index === 0) {
            this.rates.min = item;
          }
          if (index === 1) {
            this.rates.opty = item;
          }
          if (index === 2) {
            this.rates.max = item;
          }
          if (item.default && !this.activeTariff) {
            this.activeTariff = item.id;
            this.paymentSum = +item.sumDiscount + +this.duty?.sumDiscount;
            this.sumBeforeDiscount = +item.sum;
          }
        });
        this.duty = data.duty;
        this.paymentSum = +this.rates?.min?.sumDiscount + +this.duty?.sumDiscount;
        // switch (this.draft?.tariff) {
        //   case '000000002':
        //   case 'Оптимальный':
        //     this.activeTariff = this.rates.opty.id;
        //     this.paymentSum = +this.rates.opty.sumDiscount + +this.duty.sumDiscount;
        //     this.sumBeforeDiscount = +this.rates.opty.sum + +this.duty.sum;
        //     break;
        //   case '000000003':
        //   case 'Максимальный':
        //     this.activeTariff = this.rates.max.id;
        //     this.paymentSum = +this.rates.max.sumDiscount + +this.duty.sumDiscount;
        //     this.sumBeforeDiscount = +this.rates.max.sum + +this.duty.sum;
        //     break;
        // }
        // if (!this.draft) {
        //   this.step = 2;
        // }
      }
    });
  }

  /**
   * Отправка Тарифа
   */
  sendTariff() {
    const params = {
      responseID: this.responseID,
      tariffID: this.activeTariff,
      caseID: this.caseID
    };
    this.isLoading = true;
    return this.surchargeService.requestSurchargeTariff(params)
      .pipe(
        finalize(() => this.isLoading = false)
      ).subscribe(data => {
          this.step = 2;
          this.responseID = data.responseID;
        }
      );
  }

  /**
   * Выбор Тарифа
   */
  selectTariff(event) {
    this.activeTariff = event.id;
    if (this.duty) {
      this.paymentSum = +event.sumDiscount + +this.duty.sumDiscount;
      this.sumBeforeDiscount = +event.sum + +this.duty.sum;
    } else {
      this.paymentSum = +event.sumDiscount;
      this.sumBeforeDiscount = +event.sum;
    }
  }

  /**
   * Выбор типа оплаты
   * @param type: тип оплаты
   */
  setPaymentType(type: 'online' | 'offline' | 'act') {
    this.paymentType = type;
  }

  /**
   * Выбор плательщика
   * @param payerData: плательщик
   */
  setPayer(payerData) {
    this.payer = payerData;
  }

  goSuccessStage(yandexResponse) {
    this.isLoading = true;

    const query: ISurchargePayment = {
      responseId: this.responseID,
      inn: this.payer?.TIN,
      ogrn: this.payer?.PSRN,
      name: this.payer?.name || this.payer?.fullName,
      address: this.payer?.address,
      sum: this.paymentSum,
      type: this.paymentType,
      caseID: this.caseID,
      yandexResponse
    };
    this.step = 3;
    return this.surchargeService.requestSurchargePayment(query)
      .pipe(
        finalize(() => this.isLoading = false)
      )
      .subscribe((data) => {
        localStorage.removeItem('payer');
        this.responseID = data.responseID;
      });
  }

  backToCase() {
    this.router.navigate(['index/item'], {queryParams: {id: this.caseID}});
  }

}
