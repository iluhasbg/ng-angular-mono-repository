import { Component, OnInit } from '@angular/core';
import { baseEnvironmentDebt } from '../../../../../../environments/environment.debt';
import { SurchargeRequestService } from '../services/surcharge-request.service';

@Component({
  selector: 'app-surcharge-finished',
  templateUrl: './surcharge-finished.component.html',
  styleUrls: ['./surcharge-finished.component.scss'],
})
export class SurchargeFinishedComponent implements OnInit {
  logo2path = '';

  constructor(readonly surchargeRequestService: SurchargeRequestService) {
    this.logo2path = baseEnvironmentDebt.LOGO2_PATH;
  }

  ngOnInit(): void {}
}
