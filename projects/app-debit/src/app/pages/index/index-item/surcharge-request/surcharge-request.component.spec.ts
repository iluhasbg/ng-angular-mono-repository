import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SurchargeRequestComponent } from './surcharge-request.component';

describe('ExtrapayRequestComponent', () => {
  let component: SurchargeRequestComponent;
  let fixture: ComponentFixture<SurchargeRequestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SurchargeRequestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SurchargeRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
