import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SurchargePaymentComponent } from './surcharge-payment.component';

describe('SurchagePaymentComponent', () => {
  let component: SurchargePaymentComponent;
  let fixture: ComponentFixture<SurchargePaymentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SurchargePaymentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SurchargePaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
