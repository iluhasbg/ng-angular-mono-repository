export interface ISurchargeResponse {
  responseID: string;
}

export interface ISurchargeRequest {
  caseID: string;
  eventID: string;
  extraPayType: string;
}

/**
 * Модель для отправки Оплаты
 */
export interface ISurchargePayment {
  responseId: string;
  inn?: string;
  ogrn?: string;
  name?: string;
  address?: string;
  sum?: string;
  type: string;
  yandexResponse?: object;
  caseID: string;
}

