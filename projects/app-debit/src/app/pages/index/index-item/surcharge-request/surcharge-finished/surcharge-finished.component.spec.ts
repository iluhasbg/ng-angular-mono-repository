import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SurchargeFinishedComponent } from './surcharge-finished.component';

describe('SurchargeFinishedComponent', () => {
  let component: SurchargeFinishedComponent;
  let fixture: ComponentFixture<SurchargeFinishedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SurchargeFinishedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SurchargeFinishedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
