import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {SurchargeRequestService} from '../services/surcharge-request.service';

@Component({
  selector: 'app-surcharge-payment',
  templateUrl: './surcharge-payment.component.html',
  styleUrls: ['./surcharge-payment.component.scss']
})
export class SurchargePaymentComponent implements OnInit {

  returnAfterPayment = false;
  yaLastPaymentId;
  error;

  @Output() nextStepEvent = new EventEmitter(); // событие перехода на следующий шаг
  @Output() backStepEvent = new EventEmitter(); // событие перехода на предыдущий шаг

  constructor(
    private activatedRoute: ActivatedRoute,
    readonly surchargeRequestService: SurchargeRequestService
  ) { }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe((params) => {
      this.returnAfterPayment = params.returnAfterPayment === 'true';
      this.yaLastPaymentId = params.t;
    });
    const payer = JSON.parse(localStorage.getItem('payer'));
    if (payer) {
      this.selectPayer(payer);
    }
    window.scroll(0, 0);
  }

  nextStep() {
    this.nextStepEvent.emit();
  }

  backStep() {
    this.backStepEvent.emit();
  }

  switchPaymentType(type) {
    this.surchargeRequestService.setPaymentType(type);
  }

  get paymentType() {
    return this.surchargeRequestService.paymentType;
  }

  get idApplication() {
    return this.surchargeRequestService.responseID;
  }

  get paymentSum() {
    return this.surchargeRequestService.paymentSum;
  }

  onlineError(error) {
    this.error = error;
  }

  paymentOnlineSuccessed(data) {
    if (data.status === 'succeeded') {
      this.toSuccessPage(data);
    }
  }

  toSuccessPage(data?) {
    this.surchargeRequestService.goSuccessStage(data);
  }

  selectPayer(data) {
    if (this.paymentType === 'online') {
      localStorage.setItem('payer', JSON.stringify(data));
    }
    this.surchargeRequestService.setPayer(data);
  }

  get payer() {
    return this.surchargeRequestService.payer;
  }


}
