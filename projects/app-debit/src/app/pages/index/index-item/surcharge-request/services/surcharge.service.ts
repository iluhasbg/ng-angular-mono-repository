import {Injectable} from '@angular/core';
import {HttpService} from '../../../../../services/http.service';
import {Observable} from 'rxjs';
import {ISurchargePayment, ISurchargeRequest, ISurchargeResponse} from '../models/surcharge.model';
import {map} from 'rxjs/operators';
import {CasesService} from '../../../../cases/cases.service';

@Injectable({
  providedIn: 'root'
})

export class SurchargeService {

  constructor(
    private httpService: HttpService,
    private casesService: CasesService,
  ) {
  }

  /**
   * Запрос на создание заявки на доплату
   * @param params: модель для отправки
   */
  requestSurcharge(params?: ISurchargeRequest): Observable<ISurchargeResponse> {
    return this.httpService.post({ path: 'request/load', body: { ...params, stage: 1, type: 'extraPay'} });
  }

  /**
   * Запрос на отправку Тарифа
   * @param data: модель для отправки Тарифа
   */
  requestSurchargeTariff(data): Observable<ISurchargeResponse> {
    return this.httpService.post({ path: 'request/load', body: { ...data, stage: 3} });
  }

  /**
   * Запрос на отправку Оплату оффлайн
   * @param data: модель для отправки Оплаты оффлайн
   */
  requestSurchargePayment(data: ISurchargePayment): Observable<ISurchargeResponse> {
    return this.httpService.post({ path: 'request/load', body: { ...data, stage: 4 }  });
  }

  /**
   * Запрос на восстановление заявки из черновика
   * @param responseID: id заявка
   * @param params: дополнительные параметры
   */
  requestSurchargeGetDetail(params, responseID: string) {
    return this.casesService.getCase(params, responseID)
      .pipe(
        map((data: any) => {
          const task = data?.case?.tasks?.items[0];
          return {
            responseID,
            task
          };
        })
      );
  }
}
