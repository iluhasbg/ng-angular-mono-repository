import { Component, OnDestroy, OnInit } from '@angular/core';
import { SurchargeRequestService } from './services/surcharge-request.service';
import { ActivatedRoute } from '@angular/router';
import { baseEnvironmentDebt } from '../../../../../environments/environment.debt';

@Component({
  selector: 'app-surcharge-request',
  templateUrl: './surcharge-request.component.html',
  styleUrls: ['./surcharge-request.component.scss'],
})
export class SurchargeRequestComponent implements OnInit, OnDestroy {
  logo2path = '';
  public userEmail: string = JSON.parse(localStorage.getItem('currentUserData')).userInfo
    ? JSON.parse(localStorage.getItem('currentUserData')).userInfo.email
    : JSON.parse(localStorage.getItem('currentUserData')).email;

  constructor(
    readonly surchargeRequestService: SurchargeRequestService,
    readonly activatedRoute: ActivatedRoute
  ) {
    this.logo2path = baseEnvironmentDebt.LOGO2_PATH;
  }

  ngOnInit(): void {
    const id = this.activatedRoute.snapshot.queryParams.id;
    if (id) {
      this.surchargeRequestService.loadRequestIfNeeded(id);
    } else {
      this.surchargeRequestService.step = 1;
      this.surchargeRequestService.createSurcharge();
    }
  }

  get step() {
    return this.surchargeRequestService.step;
  }

  getStepClass(step): string {
    if (this.step === step) {
      return 'steps__element--active';
    } else if (this.step > step) {
      return 'steps__element--finished';
    } else {
      return '';
    }
  }

  backStep() {
    if (this.surchargeRequestService.step === 1) {
      this.surchargeRequestService.backToCase();
    } else {
      this.surchargeRequestService.step -= 1;
    }
  }

  ngOnDestroy() {}
}
