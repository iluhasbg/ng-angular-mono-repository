import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WarningPaymentComponent } from './warning-payment.component';

describe('WarningPaymentComponent', () => {
  let component: WarningPaymentComponent;
  let fixture: ComponentFixture<WarningPaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WarningPaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WarningPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
