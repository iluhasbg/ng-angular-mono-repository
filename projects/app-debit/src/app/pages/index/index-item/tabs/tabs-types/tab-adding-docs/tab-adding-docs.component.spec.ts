import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabAddingDocsComponent } from './tab-adding-docs.component';

describe('TabAddingDocsComponent', () => {
  let component: TabAddingDocsComponent;
  let fixture: ComponentFixture<TabAddingDocsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabAddingDocsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabAddingDocsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
