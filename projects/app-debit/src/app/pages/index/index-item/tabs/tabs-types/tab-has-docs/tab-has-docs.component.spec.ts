import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabHasDocsComponent } from './tab-has-docs.component';

describe('TabHasDocsComponent', () => {
  let component: TabHasDocsComponent;
  let fixture: ComponentFixture<TabHasDocsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabHasDocsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabHasDocsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
