import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DocTabComponent } from './doc-tab.component';

describe('DocTabComponent', () => {
  let component: DocTabComponent;
  let fixture: ComponentFixture<DocTabComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DocTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
