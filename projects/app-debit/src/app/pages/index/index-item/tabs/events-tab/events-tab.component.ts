import {AfterContentInit, AfterViewInit, Component, DoCheck, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import {DownloadFilesService} from '../../../../../../../../shared/services/download-files.service';

@Component({
  selector: 'app-events-tab',
  templateUrl: './events-tab.component.html',
  styleUrls: ['./events-tab.component.scss']
})
export class EventsTabComponent implements OnInit, OnDestroy {
  @Input() $events;
  @Input() caseID;

  showOptions = false;
  attention = true;
  attentionStep1 = false;
  showLast = true;
  showMore = false;
  // сколько событий показывать без кнопки Показть еще
  eventsLength = 3;

  reverseEvents = [];

  // для мобилки
  docsPreClaim = true;
  docsClaim = true;
  docsCourt = true;
  docsPenalty = true;
  docsResult = true;
  docsTalk = true;

  // для мобилки
  mobile = false;

  constructor(
    readonly downloadFiles: DownloadFilesService
  ) { }

  ngOnInit(): void {
    if (window.innerWidth < 500) {
      this.docsPreClaim = false;
      this.docsClaim = false;
      this.docsCourt = false;
      this.docsPenalty = false;
      this.docsResult = false;
      this.docsTalk = false;
      this.eventsLength = 1;
      this.mobile = true;
    } else {
      this.docsPreClaim = true;
      this.docsClaim = true;
      this.docsCourt = true;
      this.docsPenalty = true;
      this.docsResult = true;
      this.docsTalk = true;
      this.eventsLength = 3;
      this.mobile = false;
    }


    window.addEventListener('resize', () => {
      if (window.innerWidth < 500) {
        this.docsPreClaim = false;
        this.docsClaim = false;
        this.docsCourt = false;
        this.docsPenalty = false;
        this.docsResult = false;
        this.docsTalk = false;
        this.eventsLength = 1;
        this.mobile = true;
      } else {
        this.docsPreClaim = true;
        this.docsClaim = true;
        this.docsCourt = true;
        this.docsPenalty = true;
        this.docsResult = true;
        this.docsTalk = true;
        this.eventsLength = 3;
        this.mobile = false;
      }
    });
    if (this.$events !== undefined) {
      this.reverseEvents = this.$events.reverse();
      this.reverseEvents.forEach((el, i) => {
        el.showForm = false;
        el.showColumnsFooter = i === this.reverseEvents.length - 1 && !el.formState;
      });
      this.showCard(this.reverseEvents[this.reverseEvents.length - 1].eventID);
    }
  }

  ngOnDestroy() {
    this.reverseEvents = this.$events.reverse();
  }

  showCard(id) {
    this.reverseEvents.forEach(el => el.eventID === id ? el.showForm = true : false);
  }

  hideCard(id) {
    this.reverseEvents.forEach(el => el.eventID === id ? el.showForm = false : true);
  }

  getTitle(entities, title) {
    if (entities?.length > 0) {
      entities.sort((a, b) => a.offset < b.offset ? 1 : -1);
      const titleLetters = title.split('');
      let substr = ``;
      entities.forEach(item => {
        titleLetters.splice(item.offset, item.length);
        switch (item.type) {
          case 'bold':
            substr = `<b>${title.substr(item.offset, item.length)}</b>`;
            titleLetters.splice(item.offset, 0, substr);
            break;
          case 'text_link':
            substr = `<a class="events-steps__link" href="${item.url}" (click)="${this.downloadFiles.downloadFile.bind(this, item)}">${title.substr(item.offset, item.length)}</a>`;
            titleLetters.splice(item.offset, 0, substr);
            break;
        }
        title = titleLetters.join('');
      });
    }
    return `${title}`;
  }
}
