import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabTariffComponent } from './tab-tariff.component';

describe('TabTariffComponent', () => {
  let component: TabTariffComponent;
  let fixture: ComponentFixture<TabTariffComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabTariffComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabTariffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
