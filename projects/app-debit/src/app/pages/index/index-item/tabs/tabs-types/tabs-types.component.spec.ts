import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabsTypesComponent } from './tabs-types.component';

describe('TabsTypesComponent', () => {
  let component: TabsTypesComponent;
  let fixture: ComponentFixture<TabsTypesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabsTypesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabsTypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
