import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WarningWithListComponent } from './warning-with-list.component';

describe('WarningComponent', () => {
  let component: WarningWithListComponent;
  let fixture: ComponentFixture<WarningWithListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WarningWithListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WarningWithListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
