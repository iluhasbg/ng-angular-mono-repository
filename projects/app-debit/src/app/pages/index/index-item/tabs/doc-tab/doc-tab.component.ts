import {Component, Input, OnInit} from '@angular/core';
import { FileControlDef, IFilesByCategory } from '../../../../../dynamic-form-controls/file-control/file-control.interface';
import {CasesService} from '../../../../cases/cases.service';
import {DownloadFilesService} from '../../../../../../../../shared/services/download-files.service';

@Component({
  selector: 'app-doc-tab',
  templateUrl: './doc-tab.component.html',
  styleUrls: ['./doc-tab.component.scss']
})
export class DocTabComponent implements OnInit {
  @Input() $docs: FileControlDef[];
  @Input() filesByCategory: IFilesByCategory[];
  @Input() caseID;

  showOptions = false;
  attention = true;
  attentionStep1 = false;
  showServicesList = false;
  toHideForm = false;
  toHideForm1 = true;
  toHideForm2 = true;
  toHideForm3 = true;

  showMore = false;

  cardShown: any;
  // сколько событий показывать без кнопки Показть еще
  eventsLength = 3;

  // для мобилки
  docsPreClaim = true;
  docsClaim = true;
  docsCourt = true;
  docsPenalty = true;
  docsResult = true;
  docsTalk = true;

  // для мобилки
  mobile = false;
  linkArchive;

  constructor(
    private casesAPI: CasesService,
    readonly downloadFiles: DownloadFilesService
  ) { }

  ngOnInit(): void {
    if (window.innerWidth < 500) {
      this.docsPreClaim = false;
      this.docsClaim = false;
      this.docsCourt = false;
      this.docsPenalty = false;
      this.docsResult = false;
      this.docsTalk = false;
      this.eventsLength = 1;
      this.mobile = true;
    } else {
      this.docsPreClaim = true;
      this.docsClaim = true;
      this.docsCourt = true;
      this.docsPenalty = true;
      this.docsResult = true;
      this.docsTalk = true;
      this.eventsLength = 3;
      this.mobile = false;
    }


    window.addEventListener('resize', () => {
      if (window.innerWidth < 500) {
        this.docsPreClaim = false;
        this.docsClaim = false;
        this.docsCourt = false;
        this.docsPenalty = false;
        this.docsResult = false;
        this.docsTalk = false;
        this.eventsLength = 1;
        this.mobile = true;
      } else {
        this.docsPreClaim = true;
        this.docsClaim = true;
        this.docsCourt = true;
        this.docsPenalty = true;
        this.docsResult = true;
        this.docsTalk = true;
        this.eventsLength = 3;
        this.mobile = false;
      }
    });
    this.getArchiveFiles();
  }

  trimString(str, count) {
    return str?.length > count ? str.slice(0, count) + '...' : str;
  }

  showCard(id) {
    this.cardShown = id;
  }

  hideCard() {
    this.cardShown = '';
  }

  getArchiveFiles() {
    const params: any = {
      caseID: this.caseID
    };
    this.casesAPI.getCaseArchives(params).subscribe(data => {
      if (data && !data.error) {
        this.linkArchive = data.link;
      }
    });
  }

}
