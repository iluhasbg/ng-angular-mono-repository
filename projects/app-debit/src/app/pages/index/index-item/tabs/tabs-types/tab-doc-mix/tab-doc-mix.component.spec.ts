import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabDocMixComponent } from './tab-doc-mix.component';

describe('TabDocMixComponent', () => {
  let component: TabDocMixComponent;
  let fixture: ComponentFixture<TabDocMixComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabDocMixComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabDocMixComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
