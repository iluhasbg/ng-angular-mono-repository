import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabCommonInfoComponent } from './tab-common-info.component';

describe('TabCommonInfoComponent', () => {
  let component: TabCommonInfoComponent;
  let fixture: ComponentFixture<TabCommonInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabCommonInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabCommonInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
