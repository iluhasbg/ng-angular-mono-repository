import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {CasesService} from '../../cases/cases.service';

@Component({
  selector: 'app-index-item',
  templateUrl: './index-item.component.html',
  styleUrls: ['./index-item.component.scss']
})
export class IndexItemComponent implements OnInit {
  private readonly onDestroy = new Subject<void>();
  showEvents = true;
  showDocs = false;
  caseData;
  $events;
  $docs;
  filesByCategory;
  $tracks;
  filesCount = 0;
  filesClarificationRequired;
  caseID;
  noObjects = false;
  typeColor;

  btns = ['Консультация по претензии или иску', 'Вопрос по текущему делопроизводству',
    'Консультация по услугам', 'Запрос первичных документов', 'Другое'];
  showMore = false;

  showModalPerson = false;
  showModalPost = false;

  showOptions = false;
  attention = true;
  attentionStep1 = false;
  showServicesList = false;
  toHideForm = false;
  toHideForm1 = true;
  toHideForm2 = true;
  toHideForm3 = true;

  cardShown: any;
  // сколько событий показывать без кнопки Показть еще
  eventsLength = 3;

  // для мобилки
  docsPreClaim = true;
  docsClaim = true;
  docsCourt = true;
  docsPenalty = true;
  docsResult = true;
  docsTalk = true;

  // для мобилки
  mobile = false;
  isLoading = false;
  declOfDaysPassed;
  declOfDaysLeft;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private casesAPI: CasesService,
  ) {
  }

  ngOnInit(): void {
    if (window.innerWidth < 500) {
      this.docsPreClaim = false;
      this.docsClaim = false;
      this.docsCourt = false;
      this.docsPenalty = false;
      this.docsResult = false;
      this.docsTalk = false;
      this.eventsLength = 1;
      this.mobile = true;
    } else {
      this.docsPreClaim = true;
      this.docsClaim = true;
      this.docsCourt = true;
      this.docsPenalty = true;
      this.docsResult = true;
      this.docsTalk = true;
      this.eventsLength = 3;
      this.mobile = false;
    }


    window.addEventListener('resize', () => {
      if (window.innerWidth < 500) {
        this.docsPreClaim = false;
        this.docsClaim = false;
        this.docsCourt = false;
        this.docsPenalty = false;
        this.docsResult = false;
        this.docsTalk = false;
        this.eventsLength = 1;
        this.mobile = true;
      } else {
        this.docsPreClaim = true;
        this.docsClaim = true;
        this.docsCourt = true;
        this.docsPenalty = true;
        this.docsResult = true;
        this.docsTalk = true;
        this.eventsLength = 3;
        this.mobile = false;
      }
    });

    const id = this.activatedRoute.snapshot.queryParams.id;
    this.getCase(id);

  }


  showCard(id) {
    this.cardShown = id;
  }

  hideCard() {
    this.cardShown = '';
  }

  closeModal() {
    this.showModalPerson = false;
    this.showModalPost = false;
    const bodyStyles = document.body.style;
    bodyStyles.setProperty('overflow', 'visible');
  }
  openModalPerson() {
    this.showModalPerson = true;
    const bodyStyles = document.body.style;
    bodyStyles.setProperty('overflow', 'hidden');
  }
  openModalPost() {
    this.showModalPost = true;
    const bodyStyles = document.body.style;
    bodyStyles.setProperty('overflow', 'hidden');
  }

  getCase(id, isDraft?) {
    this.isLoading = true;
    const params: any = {};
    if (isDraft) {
      params.isDraft = isDraft;
    }
    this.casesAPI.getCase(params, id).pipe(takeUntil(this.onDestroy))
      .subscribe((data: any) => {
        this.isLoading = false;
        // cases
        const info = data?.case?.tasks?.items?.filter(t => t.ID === id);
        if (info?.length) {
          this.caseData = info[0];
          this.declOfDaysPassed = this.declOfNum(this.caseData?.daysPassed);
          this.declOfDaysLeft = this.declOfNum(this.caseData?.daysLeft);
        }
        switch (this.caseData.caseStage) {
          case 'Претензия':
            this.typeColor = 'claim';
            break;
          case 'Завершено':
            this.typeColor = 'finished';
            break;
          case 'Суд':
            this.typeColor = 'court';
            break;
          case 'Иск':
            this.typeColor = 'request';
            break;
          case 'Взыскание':
            this.typeColor = 'penalty';
            break;
        }
        // events
        this.caseID = this.caseData.ID;
        this.$events = data?.events;
        this.$events?.map(i => i.last = false);
        this.$events.map(el => el.registrationDate = new Date(el.registrationDate).toLocaleString('ru-RU', {
          year: 'numeric',
          month: 'long',
          day: 'numeric',
          hour: 'numeric',
          minute: 'numeric',
          second: 'numeric',
        }));
        this.$docs = data?.files;
        this.filesByCategory = data?.filesByCategory;

        this.filesByCategory.forEach((category) => {
          if (category?.files?.length) {
            this.filesCount += category.files.length;
          }
          if (category?.subcategories?.length) {
            category.subcategories.forEach((subCategory) => {
              if (subCategory?.files?.length) {
                this.filesCount += subCategory.files.length;
              }
            });
          }
        });

        this.filesClarificationRequired = data.filesClarificationRequired;
        if (this.$events?.length) {
          // files
          const test = [];
          this.$events.forEach(item => {
            if (item.popupFiles.length) {
              test.push(item.popupFiles[0]);
            }
          });
        } else {
          this.noObjects = true;
        }
        // tracks
        this.$tracks = data?.tracks;
      });
  }

  declOfNum(num): string {
    const cases = [2, 0, 1, 1, 1, 2];
    const forms = ['день', 'дня', 'дней'];
    return forms[(num % 100 > 4 && num % 100 < 20) ? 2 : cases[(num % 10 < 5) ? num % 10 : 5]];
  }

}
