import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-claim-page',
  templateUrl: './claim-page.component.html',
  styleUrls: ['./claim-page.component.scss']
})
export class ClaimPageComponent implements OnInit {
  @Input() caseData;
  @Input() caseID;
  @Input() mobile;
  @Input() declOfDaysPassed;
  @Input() declOfDaysLeft;
  typeColor = 'claim';

  constructor() { }

  ngOnInit(): void {

  }
}
