import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-finished-page',
  templateUrl: './finished-page.component.html',
  styleUrls: ['./finished-page.component.scss']
})
export class FinishedPageComponent implements OnInit {
  @Input() caseData;
  @Input() caseID;
  @Input() mobile;
  @Input() declOfDaysPassed;
  typeColor = 'finished';

  constructor() { }

  ngOnInit(): void {

  }
}
