import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { IndexPaginationComponent } from './index-pagination.component';

describe('IndexPaginationComponent', () => {
  let component: IndexPaginationComponent;
  let fixture: ComponentFixture<IndexPaginationComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ IndexPaginationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndexPaginationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
