import { Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-index-pagination',
  templateUrl: './index-pagination.component.html',
  styleUrls: ['./index-pagination.component.scss']
})
export class IndexPaginationComponent implements OnInit, OnChanges {

  @Input() itemPerPage: number;
  @Input() totalCount: number;
  @Input() currentPageNumber: number;

  @Output() changePageNumber = new EventEmitter<number>();

  pageNumbersList: number[];
  maxPaginationBtns = 5;
  isNextSet = false;
  isPrevSet = true;

  pageCount: number;

  constructor() {
  }

  ngOnInit() {
  }

  onChangePageNumber(pageNumber: number) {
    this.changePageNumber.emit(pageNumber);
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.isNextSet = false;
    this.isPrevSet = false;

    this.pageCount = Math.ceil(this.totalCount / this.itemPerPage);

    let numberBtns = this.maxPaginationBtns;
    if (this.pageCount > this.maxPaginationBtns) {
      if (this.currentPageNumber > 4) {
        this.isPrevSet = true;
        numberBtns -= 2;
      }
      if ((this.pageCount + 1 - this.currentPageNumber) > 4) {
        this.isNextSet = true;
        numberBtns -= 2;
      }
    } else {
      numberBtns = this.pageCount;
    }

    let startPageBtn: number;
    if (!this.isPrevSet) {
      startPageBtn = 1;
    } else if (!this.isNextSet) {
      startPageBtn = this.pageCount + 1 - numberBtns;
    } else {
      startPageBtn = this.currentPageNumber - 1;
    }

    this.pageNumbersList = [];
    for (let i = 0; i < numberBtns; i++) {
      this.pageNumbersList.push(startPageBtn + i);
    }
  }

}
