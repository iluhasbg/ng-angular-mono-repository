import { Component, OnInit, OnChanges } from '@angular/core';
import { CasesService } from '../cases/cases.service';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { IWidget } from '../../models/main.interfaces';
import {SharedService} from '../../services/shared.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})

export class IndexComponent implements OnInit, OnChanges {
  tasks;
  totalWidget: IWidget;
  inProcessWidget: IWidget;
  exactWidget: IWidget;

  openFirstChart = true;
  openSecondChart = true;
  openThirdChart = true;

  showMobileControl = true;
  // Индикатор загрузки дел
  isLoadingTasks = false;
  isLoadingWidgets = false;

  pages = 1;
  pagesAll;
  totalCount;
  paginationTotalCount;
  searchQuery = '';


  push = false;
  submit = false;

  noObjects = false;
  soon = false;

  currentPage = 1;

  claims = false;
  courts = false;
  penalties = false;
  all = true;
  finished = false;

  allRequests = true;
  inProcess = true;
  done = true;
  limit = 10;
  offset = 0;
  showDynamicPagination = false;
  filteredCount;
  debtorSelect = [];
  regionsSelect = [];
  debtorsParam = [];
  stagesParam = [];
  regionsParam = [];
  caseStage;

  stagesSelect = [
    {
      stageId: 1,
      name: 'Черновик',
      value: 'draft',
      checked: false
    },
    {
      stageId: 2,
      name: 'Претензия',
      value: 'claims',
      checked: false
    },
    {
      stageId: 3,
      name: 'Иск подготовлен',
      value: 'suit',
      checked: false
    },
    {
      stageId: 4,
      name: 'Суд',
      value: 'tribunal',
      checked: false
    },
    {
      stageId: 5,
      name: 'Взыскание',
      value: 'penalty',
      checked: false
    },
    {
      stageId: 6,
      name: 'Завершено',
      value: 'completed',
      checked: false
    }
  ];
  showMore = false;
  countChecked: any;
  expression: any;
  stages;
  params;
  timeoutID;

  private readonly onDestroy = new Subject<void>();

  constructor(
    private casesApi: CasesService,
    private router: Router,
    private sharedService: SharedService,
  ) {
  }

  ngOnInit(): void {
    this.isLoadingTasks = true;
    this.getTasks(this.caseStage);
    this.getWidgets();
    document.body.style.overflow = 'visible';
    if (window.innerWidth < 500) {
      this.openFirstChart = false;
      this.openSecondChart = false;
      this.openThirdChart = false;
      this.showMobileControl = false;
    } else {
      this.openFirstChart = true;
      this.openSecondChart = true;
      this.openThirdChart = true;
      this.showMobileControl = true;
    }

    window.addEventListener('resize', () => {
      if (window.innerWidth < 500) {
        this.openFirstChart = false;
        this.openSecondChart = false;
        this.openThirdChart = false;
        this.showMobileControl = false;
      } else {
        this.openFirstChart = true;
        this.openSecondChart = true;
        this.openThirdChart = true;
        this.showMobileControl = true;
      }
    });
  }

  ngOnChanges(): void {

    if (window.innerWidth < 500) {
      this.openFirstChart = false;
      this.openSecondChart = false;
      this.openThirdChart = false;
      this.showMobileControl = false;
    } else {
      this.openFirstChart = true;
      this.openSecondChart = true;
      this.openThirdChart = true;
      this.showMobileControl = true;
    }
  }

  makeBodyHidden() {
    if (this.showMobileControl === true) {
      document.body.style.overflow = 'hidden';
    } else {
      document.body.style.overflow = 'visible';
    }
  }

  toCheckAll() {
    if (this.all) {
      this.all = true;
      this.finished = false;
      this.courts = false;
      this.claims = false;
      this.penalties = false;
      this.caseStage = 'all';
    }
    this.searchQuery = '';
    this.regionsParam = [];
    this.debtorsParam = [];
    this.stagesParam = [];
    this.stagesSelect.forEach(el => el.checked = false);
    this.regionsSelect.forEach(el => el.checked = false);
    this.debtorSelect.forEach(el => el.checked = false);
    this.offset = 0;
    this.getTasks('all');
    this.setDynamicPagination();
  }

  toCheckCourts(obj) {
    if (this.courts) {
      this.all = false;
      this.penalties = false;
      this.claims = false;
      this.finished = false;
      this.caseStage = 'tribunal';
    }
    this.searchQuery = '';
    this.regionsParam = [];
    this.debtorsParam = [];
    this.stagesParam = [];
    this.regionsSelect.forEach(el => el.checked = false);
    this.debtorSelect.forEach(el => el.checked = false);
    this.offset = 0;
    this.getTasks(obj);
    this.setDynamicPagination(obj);
  }

  toCheckFinished(obj) {
    if (this.finished) {
      this.claims = false;
      this.all = false;
      this.penalties = false;
      this.courts = false;
      this.caseStage = 'completed';
    }
    this.searchQuery = '';
    this.regionsParam = [];
    this.debtorsParam = [];
    this.stagesParam = [];
    this.regionsSelect.forEach(el => el.checked = false);
    this.debtorSelect.forEach(el => el.checked = false);
    this.offset = 0;
    this.getTasks(obj);
    this.setDynamicPagination(obj);
  }

  toCheckClaims(obj) {
    if (this.claims) {
      this.courts = false;
      this.all = false;
      this.penalties = false;
      this.finished = false;
      this.caseStage = 'claims';
    }
    this.searchQuery = '';
    this.regionsParam = [];
    this.debtorsParam = [];
    this.stagesParam = [];
    this.regionsSelect.forEach(el => el.checked = false);
    this.debtorSelect.forEach(el => el.checked = false);
    this.offset = 0;
    this.getTasks(obj);
    this.setDynamicPagination(obj);
  }

  toCheckPenalties(obj) {
    if (this.penalties) {
      this.claims = false;
      this.all = false;
      this.finished = false;
      this.courts = false;
      this.caseStage = 'penalty';
    }
    this.searchQuery = '';
    this.regionsParam = [];
    this.debtorsParam = [];
    this.stagesParam = [];
    this.regionsSelect.forEach(el => el.checked = false);
    this.debtorSelect.forEach(el => el.checked = false);
    this.offset = 0;
    this.getTasks(obj);
    this.setDynamicPagination(obj);
  }

  filterByText() {
    if (this.timeoutID) {
      window.clearTimeout(this.timeoutID);
    }
    this.timeoutID = window.setTimeout(() => {
      if (this.searchQuery.length >= 3) {
        switch (this.caseStage) {
          case 'claims':
            this.getTasks('claims');
            break;
          case 'all':
          case '':
          case undefined:
            this.getTasks();
            break;
          case 'tribunal':
            this.getTasks('tribunal');
            break;
          case 'penalty':
            this.getTasks('penalty');
            break;
          case 'completed':
            this.getTasks('completed');
            break;
        }
      }
    }, 400);
  }

  filterByDebtor(obj) {
    if (obj.checked) {
      this.debtorsParam.push(obj.value);
    } else {
      this.debtorsParam.splice(this.debtorsParam.indexOf(obj.value), 1);
    }
  }

  filterByStage(obj) {
    if (obj.checked) {
      this.stagesParam.push(obj.value);
    } else {
      this.stagesParam.splice(this.stagesParam.indexOf(obj.value), 1);
    }
  }

  filterByRegion(obj) {
    if (obj.checked) {
      this.regionsParam.push(obj.value);
    } else {
      this.regionsParam.splice(this.regionsParam.indexOf(obj.value), 1);
    }
  }

  getCountCheckedObjects(massive) {
    let counter = 0;
    massive.forEach((obj) => {
      if (obj.checked) {
        counter++;
      }
    });

    return counter;
  }

  cleanCheckedObj(massive) {
    massive.forEach((obj) => {
      obj.checked = false;
    });
  }

  onChangePageNumber(page) {
    this.currentPage = page;
  }

  getWidgets() {
    this.isLoadingWidgets = true;
    this.casesApi.getWidgets()
      .subscribe(data => {
        if (data) {
          this.isLoadingWidgets = false;
          this.totalWidget = data.total;
          this.inProcessWidget = data.inProcess;
          this.exactWidget = data.exact;
        }
      });
  }

  getTasks(stage?) {
    this.params = {
      limit: this.limit,
      offset: this.offset,
      caseStage: stage,
    };
    this.resetCases(stage);
    this.isLoadingTasks = true;
    this.getFilteredCases(this.params);
  }

  setDynamicPagination(obj?) {
    const stages = [
      {
        name: 'claims',
        value: this.stages.claims,

      },
      {
        name: 'tribunal',
        value: this.stages.tribunal
      },
      {
        name: 'penalty',
        value: this.stages.penalty
      },
      {
        name: 'completed',
        value: this.stages.completed
      }
    ];
    if (!obj) {
      this.showDynamicPagination = false;
    } else {
      const stageCount = stages.filter(i => i.name === obj);
      this.filteredCount = stageCount[0].value;
      this.showDynamicPagination = true;
    }
  }

  clearAllFilters() {
    switch (this.caseStage) {
      case 'claims':
        this.claims = true;
        this.toCheckClaims('claims');
        break;
      case 'all':
      case '':
      case undefined:
        this.all = true;
        this.toCheckAll();
        break;
      case 'tribunal':
        this.courts = true;
        this.toCheckCourts('tribunal');
        break;
      case 'penalty':
        this.penalties = true;
        this.toCheckPenalties('penalty');
        break;
      case 'completed':
        this.finished = true;
        this.toCheckFinished('completed');
        break;
    }
    this.pages = 1;
    if (this.searchQuery.length) {
      this.searchQuery = '';
    }
    this.debtorSelect.forEach(el => el.checked = false);
    this.stagesSelect.forEach(el => el.checked = false);
    this.regionsSelect.forEach(el => el.checked = false);
    this.regionsParam = [];
    this.debtorsParam = [];
    this.stagesParam = [];
    this.ngOnInit();
  }

  resetCases(stage) {
    if (stage === 'all' || stage === undefined) {
      delete this.params.caseStage;
      this.offset = 0;
      this.all = true;
      this.pages = 1;
    }
    if (stage === 'disablePagination') {
      delete this.params.caseStage;
      this.offset = 0;
      this.all = true;
    } else {
      this.params.offset = this.offset;
    }
  }

  getFilteredCases(params) {
    if (this.searchQuery?.length) {
      params.searchQuery = this.searchQuery;
    }

    if (this.stagesParam?.length) {
      this.stagesParam.forEach((sp) => {
        if (!params.caseStage) {
          params.caseStage = sp;
        } else {
          params.caseStage += `,${sp}`;
        }
      });
    }

    if (this.debtorsParam?.length) {
      this.debtorsParam.forEach((dp) => {
        if (!params.debtors) {
          params.debtors = dp;
        } else {
          params.debtors += `,${dp}`;
        }
      });
    }

    if (this.regionsParam?.length) {
      this.regionsParam.forEach((rp) => {
        if (!params.regions) {
          params.regions = rp;
        } else {
          params.regions += `,${rp}`;
        }
      });
    }

    this.casesApi.getTasks(params).pipe(takeUntil(this.onDestroy))
      .subscribe((tasksData) => {
        if (tasksData) {
          this.tasks = tasksData;
          this.stages = tasksData.stages;
          this.totalCount = tasksData.totalCount;
          this.paginationTotalCount = tasksData.paginationTotalCount;

          // todo: это вообще юзается?
          this.sharedService.totalCount = tasksData.totalCount;
          this.sharedService.paginationTotalCount = tasksData.paginationTotalCount;

          if (this.tasks?.debtors?.length && !this.debtorSelect?.length) {
            this.debtorSelect = [];
            this.tasks.debtors.forEach(d => {
              this.debtorSelect.push({value: d, checked: false, debtorId: Math.floor(7 + Math.random() * (300 + 1 - 7))});
            });
          }
          if (this.tasks?.regions?.length && !this.regionsSelect?.length) {
            this.regionsSelect = [];
            this.tasks.regions.forEach(r => {
              this.regionsSelect.push({value: r, checked: false, regionId: 'r' + Math.floor(7 + Math.random() * (300 + 1 - 7))});
            });
          }
          this.getMappingTasks();
        }
      });
  }

  // todo перенести фильтр на сервер
  // this.tasks = this.tasks.tasks?.items?.filter(el => el.type === 'claim' || el.details.description === 'Претензионная работа');
  getMappingTasks() {
    this.isLoadingTasks = false;
    this.tasks = this.tasks.tasks?.items;
    this.tasks.sort((a, b) => b.number - a.number);
  }

  sendDraft(id, type) {
    if (type === 0) {
      this.router.navigate(['/request-claim'], {queryParams: {id}});
    } else {
      this.router.navigate(['index/item'], {queryParams: {id}});
    }
  }

  changePage(page: number, stage: string): void {
    this.pages = page;
    this.offset = (this.pages - 1) * 10;
    this.params.offset = this.offset;
    if (stage === 'all' || stage === undefined) {
      this.getTasks('disablePagination');
    } else {
      this.params.caseStage = stage;
      this.getTasks(stage);
    }
  }

  /**
   * склонение слова 'классы'
   * @param n: количество классов
   */
  declOfNum(n: number): string {
    n = Math.abs(n) % 100;
    const n1 = n % 10;
    if (n > 10 && n <= 20) {
      return 'дел';
    }
    if (n1 > 1 && n1 < 5) {
      return 'дела';
    }
    if (n1 === 1) {
      return 'дело';
    }
    return 'дел';
  }

  getLabelDefendant(task) {
    if (task.caseStage === 'Черновик') {
      return task.details?.userData?.defendant === 'Не знаю' ? 'Не знаю' : task.details?.userData?.defendant.nameOrganization;
    } else {
      return task.debtor?.nameOrganization;
    }
  }

  getLabelRegion(task) {
    if (task.caseStage === 'Черновик') {
      return task.details?.userData?.defendant.region;
    } else {
      return task.debtor?.region;
    }
  }

  getLabelReason(task) {
    if (task.caseStage === 'Черновик') {
      return task.details?.userData?.reason?.title;
    } else {
      return task.claimSubject;
    }
  }
}
