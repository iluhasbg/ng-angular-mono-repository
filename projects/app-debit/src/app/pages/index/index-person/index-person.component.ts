import {Component, EventEmitter, HostListener, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-index-person',
  templateUrl: './index-person.component.html',
  styleUrls: ['./index-person.component.scss']
})
export class IndexPersonComponent implements OnInit {
  constructor() { }

  @Output() modalEvent = new EventEmitter();

  ngOnInit(): void {
  }

  closeModal() {
    this.modalEvent.emit();
  }

  @HostListener('document:keyup', ['$event']) // 27=esc
  keyup(event: KeyboardEvent): void {
    if (event.keyCode === 27) {
      this.closeModal();
    }
  }
}
