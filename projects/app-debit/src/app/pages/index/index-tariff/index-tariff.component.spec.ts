import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { IndexTariffComponent } from './index-tariff.component';

describe('IndexTariffComponent', () => {
  let component: IndexTariffComponent;
  let fixture: ComponentFixture<IndexTariffComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ IndexTariffComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndexTariffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
