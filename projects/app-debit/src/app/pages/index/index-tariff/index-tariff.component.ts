import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {RequestClaimService} from '../../request-claim/shared/request-claim.service';


@Component({
  selector: 'app-index-tariff',
  templateUrl: './index-tariff.component.html',
  styleUrls: ['./index-tariff.component.scss']
})
export class IndexTariffComponent implements OnInit {

  tariffCounter;

  tariff;
  paymentSum;
  sumBeforeDiscount;

  @Output() nextStepEvent = new EventEmitter(); // событие перехода на следующий шаг
  @Output() backStepEvent = new EventEmitter(); // событие перехода на предыдущий шаг

  currentTariff: string;

  constructor(
    readonly requestClaimService: RequestClaimService
  ) { }

  ngOnInit(): void {
    if (this.requestClaimService.draft?.tariff) {
      switch (this.requestClaimService.draft?.tariff) {
        case '000000003':
        case 'Максимальный':
          this.tariff = 'max';
          break;
        case '000000002':
        case 'Оптимальный':
          this.tariff = 'opt';
          break;
      }
    } else if (this.requestClaimService.activeTariff && !this.requestClaimService.draft?.tariff) {
      this.tariff = this.requestClaimService.activeTariff === '000000002' ? 'opt' : 'max';
    } else {
      this.tariff = 'max';
    }
    if (this.requestClaimService.rates) {
      this.tariffCounter = Object.keys(this.requestClaimService.rates).length;
    }
  }

  nextStep() {
    this.nextStepEvent.emit();
  }

  backStep() {
    this.backStepEvent.emit();
  }

  getCurrentTariff(tariffType: 'opt' | 'max' | 'all') {
    this.tariff = tariffType;
  }

  get duty() {
    return this.requestClaimService.duty;
  }

  get rates() {
    return this.requestClaimService.rates;
  }

  /**
   * Выбор тарифа
   * @param event: тариф
   */
  selectTariff(event) {
    if (event.id) {
      switch (event.id) {
        case '000000002':
          this.tariff = 'opt';
          this.paymentSum = +this.rates?.opty?.sumDiscount + +this.duty?.sumDiscount;
          this.sumBeforeDiscount = +this.rates?.opty?.sum + +this.duty?.sum;
          this.tariffCounter = 1;
          break;
        case '000000003':
          this.tariff = 'max';
          this.paymentSum = +this.rates?.max?.sumDiscount + +this.duty?.sumDiscount;
          this.sumBeforeDiscount = +this.rates?.max?.sum + +this.duty?.sum;
          this.tariffCounter = 1;
          break;
        default:
          console.warn(`Для тарифа id: ${event.id} отсутствует dataLayer событие`);
      }
    }
    this.requestClaimService.activeTariff = event.id;
    this.requestClaimService.paymentSum = this.paymentSum;
    if (this.requestClaimService.draft?.tariff) {
      delete this.requestClaimService.draft.tariff;
    }
  }
}
