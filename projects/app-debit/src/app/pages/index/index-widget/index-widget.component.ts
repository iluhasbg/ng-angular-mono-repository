import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-index-widget',
  templateUrl: './index-widget.component.html',
  styleUrls: ['../index.component.scss']
})
export class IndexWidgetComponent implements OnInit {
  soon = false;
  inProcess = true;
  openFirstChart = true;
  openSecondChart = true;
  openThirdChart = true;
  noObjects = false;
  allRequests = true;
  done = true;
  showMobileControl = true;
  @Input() totalWidget;
  @Input() inProcessWidget;
  @Input() exactWidget;
  @Input() isLoadingWidgets;

  constructor() { }

  ngOnInit(): void {
  }

  /**
   * склонение слова 'классы'
   * @param n: количество классов
   */
  declOfNum(n: number): string {
    n = Math.abs(n) % 100;
    const n1 = n % 10;
    if (n > 10 && n <= 20) {
      return 'дел';
    }
    if (n1 > 1 && n1 < 5) {
      return 'дела';
    }
    if (n1 === 1) {
      return 'дело';
    }
    return 'дел';
  }

}
