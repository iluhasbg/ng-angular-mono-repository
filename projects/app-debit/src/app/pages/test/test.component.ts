import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import {CasesService} from '../../../../../app-ipid/src/app/pages/cases/cases.service';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {
  currentForm = '';
  schemas: Array<{ name: string, schema: any }>;
  private readonly onDestroy = new Subject<void>();

  constructor(
    private readonly httpClient: HttpClient,
    private readonly activatedRoute: ActivatedRoute,
    private casesAPI: CasesService,
  ) {
  }

  ngOnInit(): void {
    // this.activatedRoute.params.subscribe((next: Params) => {
    //   this.currentForm = next.formId;
    //   this.load();
    // });
    const id = this.activatedRoute.snapshot.queryParams.id;
    this.getCase('000056009');
  }

  // private load() {
  //   this.schemas = null;
  //
  //   if (this.currentForm) {
  //     this.loadOne(this.currentForm)
  //       .subscribe(data => {
  //         this.schemas = [{ name: this.currentForm, schema: data }];
  //         console.log(this.schemas);
  //       })
  //   } else {
  //     this.loadAll()
  //       .subscribe((data) => {
  //         this.schemas = Object.entries(data).map(([name, schema]) => ({ name, schema }))
  //       });
  //   }
  // }
  //
  // private loadAll() {
  //   return this.httpClient.get(`/api-debit-forms`);
  // }
  //
  // private loadOne(formName: string) {
  //   return this.httpClient.get(`/api-debit-forms/${formName}`);
  // }

  getCase(id) {
    const params: any = {
      token: localStorage.getItem('currentUserIPID'),
    };
    this.casesAPI.getCase(params, id).pipe(takeUntil(this.onDestroy))
      .subscribe((data: any) => {
        this.schemas = Object.entries(data.events).map(([name, schema]) => ({ name, schema }));
        this.schemas.forEach(el => el.schema = el.schema?.formDetails);
        console.log(this.schemas);
    });
  }

}
