import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FinishedClaimComponent } from './finished-claim.component';

describe('FinishedComponent', () => {
  let component: FinishedClaimComponent;
  let fixture: ComponentFixture<FinishedClaimComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FinishedClaimComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FinishedClaimComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
