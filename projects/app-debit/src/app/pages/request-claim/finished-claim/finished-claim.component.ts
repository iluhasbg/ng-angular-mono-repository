import { Component, OnInit } from '@angular/core';
import { baseEnvironmentDebt } from '../../../../environments/environment.debt';

@Component({
  selector: 'app-finished-claim',
  templateUrl: './finished-claim.component.html',
  styleUrls: ['./finished-claim.component.scss'],
})
export class FinishedClaimComponent implements OnInit {
  logo2path = '';

  constructor() {
    this.logo2path = baseEnvironmentDebt.LOGO2_PATH;
  }

  ngOnInit(): void {}
}
