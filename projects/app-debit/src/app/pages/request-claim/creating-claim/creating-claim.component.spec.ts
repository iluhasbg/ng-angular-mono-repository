import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { CreatingClaimComponent } from './creating-claim.component';

describe('CreatingClaimComponent', () => {
  let component: CreatingClaimComponent;
  let fixture: ComponentFixture<CreatingClaimComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatingClaimComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatingClaimComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
