import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {RequestClaimService} from '../shared/request-claim.service';

@Component({
  selector: 'app-creating-claim',
  templateUrl: './creating-claim.component.html',
  styleUrls: ['./creating-claim.component.scss']
})
export class CreatingClaimComponent implements OnInit {
  @Input() isDisabled;

  step = 1;

  var1 = true;
  var21 = false;
  var2 = false;
  var3 = false;
  var4 = false;

  bill = false;
  online = true;

  showModalDoc: boolean;
  showModalMoreDocs: boolean;
  showModalConfirmingDoc: boolean;
  showModalPayment: boolean;
  showModalOther: boolean;
  showModalGuessWho: boolean;
  showModalGuessWhat: boolean;
  showModalGuessWhatWant: boolean;
  strWhatWant: string;
  strWho: string;
  claimForm: FormGroup;
  actionOther = {
    id: '',
    title: '',
    text: '',
  };
  comment;

  constructor(
    readonly requestClaimService: RequestClaimService,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.initForm();
  }

  getStepClass(step): string {
    if (this.step === step) {
      return 'steps__element--active';
    } else if (this.step > step) {
      return 'steps__element--finished';
    } else {
      return '';
    }
  }

  initForm() {
    this.claimForm = this.fb.group({
      nameOrganization: new FormControl(''),
      reason: new FormControl(''),
      action: new FormControl(''),
      other: new FormControl(''),
      comment: new FormControl(''),
    });
  }

  getWho(event) {
    this.strWho = event;
  }

  getWhat(event) {
    this.requestClaimService.reasonLabel = event;
  }

  getWhatWant(event) {
    this.strWhatWant = event;
  }

  closeModal() {
    this.showModalMoreDocs = false;
    this.showModalConfirmingDoc = false;
    this.showModalPayment = false;
    this.showModalOther = false;
    this.showModalGuessWho = false;
    this.showModalGuessWhat = false;
    this.showModalGuessWhatWant = false;
    this.showModalDoc = false;
    const bodyStyles = document.body.style;
    bodyStyles.setProperty('overflow', 'visible');
  }

  openModalDoc() {
    this.showModalDoc = true;
    const bodyStyles = document.body.style;
    bodyStyles.setProperty('overflow', 'hidden');
  }

  openModalConfirmingDoc() {
    this.showModalConfirmingDoc = true;
    const bodyStyles = document.body.style;
    bodyStyles.setProperty('overflow', 'hidden');
  }

  openModalPayment() {
    this.showModalPayment = true;
    const bodyStyles = document.body.style;
    bodyStyles.setProperty('overflow', 'hidden');
  }

  openModalOther() {
    this.showModalOther = true;
    const bodyStyles = document.body.style;
    bodyStyles.setProperty('overflow', 'hidden');
  }

  openModalGuessWho() {
    this.showModalGuessWho = true;
    const bodyStyles = document.body.style;
    bodyStyles.setProperty('overflow', 'hidden');
  }

  openModalGuessWhatWant() {
    this.showModalGuessWhatWant = true;
    const bodyStyles = document.body.style;
    bodyStyles.setProperty('overflow', 'hidden');
  }

  openModalGuessWhat() {
    this.showModalGuessWhat = true;
    const bodyStyles = document.body.style;
    bodyStyles.setProperty('overflow', 'hidden');
  }

  openModalMoreDocs() {
    this.showModalMoreDocs = true;
    const bodyStyles = document.body.style;
    bodyStyles.setProperty('overflow', 'hidden');
  }

  sendReason(reason) {
    this.requestClaimService.whatHappenedSelect = false;
    this.requestClaimService.reasonLabel = reason.title;
    this.requestClaimService.sendReason(reason);
  }

  sendAction(action) {
    this.requestClaimService.endOfFirstStep = action.id !== 'a3001';
    if (!this.requestClaimService.endOfFirstStep) {
      action.text = this.claimForm.value.other;
      this.actionOther = action;
      if (this.actionOther.text) {
        this.requestClaimService.endOfFirstStep = true;
      }
    }
    this.requestClaimService.sendAction(action);
    this.requestClaimService.actionLabel = action.text ? `${action.title}: ${action.text}` : action.title;
    this.requestClaimService.actionSelected = true;
    this.requestClaimService.whatHappenedSelect = false;
  }

  setValueDefendant() {
    if (this.requestClaimService.data?._source) {
      this.requestClaimService.dropdown = false;
      this.claimForm.controls.nameOrganization.setValue(this.requestClaimService.data._source.name);
      let regionName = this.requestClaimService.data._source.addressHistory[this.requestClaimService.data._source.addressHistory.length - 1]?.regionName?.toLowerCase();
      regionName = regionName[0].toUpperCase() + regionName.slice(1) + ' ';
      const regionType = this.requestClaimService.data._source.addressHistory[this.requestClaimService.data._source.addressHistory.length - 1]?.regionType?.toLowerCase();
      this.requestClaimService.userData.defendant = {
        nameOrganization: this.requestClaimService.data._source.name,
        inn: this.requestClaimService.data._source.TIN,
        ogrn: this.requestClaimService.data._source.PSRN.toString(),
        region: `${regionName + regionType}`
      };
      this.requestClaimService.defendantLabel = this.claimForm.value.nameOrganization;
    } else {
      this.requestClaimService.defendantLabel = 'Не знаю';
      this.requestClaimService.userData.defendant = 'Не знаю';
    }
    this.requestClaimService.startOfFirstStep = false;
  }

  noDocument(type) {
    switch (type) {
      case 'docs':
        if (this.requestClaimService.docs[0].files?.length) {
          this.openModalDoc();
        } else {
          if (this.requestClaimService.docs[0].disabled === true) {
            this.requestClaimService.docs[0].files = [];
            this.requestClaimService.docs[0].disabled = false;
          } else {
            this.openModalDoc();
          }
        }
        break;
      case 'confirmingDocs':
        if (this.requestClaimService.docs[1].files?.length) {
          this.openModalConfirmingDoc();
        } else {
          if (this.requestClaimService.docs[1].disabled === false) {
            this.openModalConfirmingDoc();
          } else {
            this.requestClaimService.docs[1].files = [];
            this.requestClaimService.docs[1].disabled = false;
          }
        }
        break;
      case 'paymentDocs':
        if (this.requestClaimService.docs[2].files?.length) {
          this.openModalPayment();
        } else {
          if (this.requestClaimService.docs[2].disabled === false) {
            this.openModalPayment();
          } else {
            this.requestClaimService.docs[2].files = [];
            this.requestClaimService.docs[2].disabled = false;
          }
        }
        break;
      case 'otherDocs':
        if (this.requestClaimService.docs[3].files?.length) {
          this.openModalOther();
        } else {
          if (this.requestClaimService.docs[3].disabled === false) {
            this.openModalOther();
          } else {
            this.requestClaimService.docs[3].files = [];
            this.requestClaimService.docs[3].disabled = false;
          }
        }
        break;
    }
  }

  getLabelForDocs(docs) {
    const arrNames = [];
    docs.files.forEach(el => arrNames.push(el.fileName));
    arrNames.push(docs.link);
    return arrNames;
  }

  setComment() {
    this.requestClaimService.userData.comment = this.comment;
  }
}
