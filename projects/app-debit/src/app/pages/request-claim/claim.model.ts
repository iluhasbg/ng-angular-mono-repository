export interface IClaimResponse {
  responseID: string;
  data: ClaimStages;
}

export interface IClaimRequestSecondStep {
  responseID: string;
  tariffID: string;
}

export interface ClaimStages {
  userData: UserDataStage1 | null;
  dataStage002: string;
  dataStage001: ClaimStage1;
}

export interface ClaimStage1 {
  defendant: DefendantStage1;
  reason: ReasonStage1;
  action: ActionStage1;
}

export interface DefendantStage1 {
  title: string;
  items: object[];
}

export interface ReasonStage1 {
  title: string;
  items: object[];
}

export interface ActionStage1 {
  title: string;
  items: object;
}

export interface IClaimRequest{
  stageReadOnly?: boolean;
  userData?: UserDataStage1;
  responseID: string;
  interestData?: object;
}

export interface UserDataStage1{
  defendant: string | IDefendant;
  reason: object;
  action: object;
  comment: string;
  docs: object[];
}

export interface IDefendant {
  nameOrganization: string;
  inn: string;
  ogrn: string;
  region?: string;
}

/**
 * Модель для отправки Оплаты
 */
export interface IClaimPayment {
  responseID: string;
  inn?: string;
  ogrn?: string;
  name?: string;
  address?: string;
  sum?: string;
  type: string;
  yandexResponse?: object;
}

