import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {IClaimPayment, IClaimRequestSecondStep, IClaimResponse} from './claim.model';
import {HttpService} from '../../services/http.service';
import {CasesService} from '../cases/cases.service';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class ClaimService {

  constructor(
    private httpService: HttpService,
    private casesService: CasesService
  ) {
  }

  /**
   * Запрос на получение данных для первого шага
   * @param params: модель для отправки
   */
  getDataClaim(params?): Observable<IClaimResponse> {
    return this.httpService.get({ path: 'request/claim/data', params});
  }

  /**
   * Запрос на отправку данных для первого шага
   * @param data: модель для отправки
   */
  sendDataClaim(data?): Observable<IClaimResponse> {
    return this.httpService.post({ path: 'request/load', body: {...data, stage: 1, type: 'claim'} });
  }

  /**
   * Запрос на отправку Тарифа
   * @param data: модель для отправки Тарифа
   */
  requestClaimSecondStep(data: IClaimRequestSecondStep): Observable<IClaimResponse> {
    return this.httpService.post({ path: 'request/load', body: { ...data, stage: 3, type: 'claim'} });
  }

  /**
   * Запрос на отправку Оплату оффлайн
   * @param data: модель для отправки Оплаты оффлайн
   */
  requestClaimPayment(data: IClaimPayment): Observable<IClaimResponse> {
    return this.httpService.post({ path: 'request/load', body: { ...data, stage: 4 }  });
  }

  /**
   * Запрос на восстановление заявки из черновика
   * @param responseID: id заявка
   * @param params: дополнительные параметры
   */
  requestContractGetDetail(params, responseID: string) {
    return this.casesService.getCase(params, responseID)
      .pipe(
        map((data: any) => {
          const task = data?.case?.tasks?.items[0];
          return {
            responseID,
            task
          };
        })
      );
  }
}
