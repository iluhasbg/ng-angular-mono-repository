import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentClaimComponent } from './payment-claim.component';

describe('PaymentClaimComponent', () => {
  let component: PaymentClaimComponent;
  let fixture: ComponentFixture<PaymentClaimComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaymentClaimComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentClaimComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
