import {Component, OnInit, EventEmitter, Output, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {RequestClaimService} from '../shared/request-claim.service';

@Component({
  selector: 'app-payment-claim',
  templateUrl: './payment-claim.component.html',
  styleUrls: ['./payment-claim.component.scss']
})
export class PaymentClaimComponent implements OnInit, OnDestroy {

  returnAfterPayment = false;
  yaLastPaymentId;
  error;

  @Output() nextStepEvent = new EventEmitter(); // событие перехода на следующий шаг
  @Output() backStepEvent = new EventEmitter(); // событие перехода на предыдущий шаг

  constructor(
    private activatedRoute: ActivatedRoute,
    readonly requestClaimService: RequestClaimService,
  ) { }

  ngOnInit(): void {
    console.log(this.paymentType);
    this.activatedRoute.queryParams.subscribe((params) => {
      this.returnAfterPayment = params.returnAfterPayment === 'true';
      this.yaLastPaymentId = params.t;
    });
    const payer = JSON.parse(localStorage.getItem('payer'));
    if (payer) {
      this.selectPayer(payer);
    }
  }

  nextStep() {
    this.nextStepEvent.emit();
  }

  backStep() {
    this.backStepEvent.emit();
  }

  switchPaymentType(type) {
    this.requestClaimService.setPaymentType(type);
  }

  get paymentType() {
    return this.requestClaimService.paymentType;
  }

  get idApplication() {
    return this.requestClaimService.responseID;
  }

  get paymentSum() {
    return this.requestClaimService.paymentSum;
  }

  onlineError(error) {
    this.error = error;
  }

  paymentOnlineSuccessed(data) {
    if (data.status === 'succeeded') {
      this.toSuccessPage(data);
    }
  }

  toSuccessPage(data?) {
    this.requestClaimService.goSuccessStage(data);
  }

  selectPayer(data) {
    if (this.paymentType === 'online') {
      localStorage.setItem('payer', JSON.stringify(data));
    }
    this.requestClaimService.setPayer(data);
  }

  get payer() {
    return this.requestClaimService.payer;
  }

  ngOnDestroy() {
    this.requestClaimService.paymentType = 'online';
  }

}
