import { Component, OnDestroy, OnInit } from '@angular/core';
import { RequestClaimService } from './shared/request-claim.service';
import { baseEnvironmentDebt } from '../../../environments/environment.debt';
import { ActivatedRoute, Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { UserProfileService } from '../../../../../shared/services/user-profile.service';
import { AUTH_STATE_TOKEN } from '@web/core/states/auth.state';
import { Store } from '@ngxs/store';

@Component({
  selector: 'app-request-claim',
  templateUrl: './request-claim.component.html',
  styleUrls: ['./request-claim.component.scss'],
  providers: [RequestClaimService],
})
export class RequestClaimComponent implements OnInit, OnDestroy {
  private readonly onDestroy = new Subject<void>();

  logo2path = '';
  public userEmail: string = JSON.parse(localStorage.getItem('currentUserData')).userInfo
    ? JSON.parse(localStorage.getItem('currentUserData')).userInfo.email
    : JSON.parse(localStorage.getItem('currentUserData')).email;
  requestType = 'patent';
  noPhone;
  personalData = true;
  userData;

  constructor(
    readonly requestClaimService: RequestClaimService,
    readonly activatedRoute: ActivatedRoute,
    private router: Router,
    private profileApi: UserProfileService,
    private store: Store
  ) {
    this.logo2path = baseEnvironmentDebt.LOGO2_PATH;
  }

  ngOnInit(): void {
    const id = this.activatedRoute.snapshot.queryParams.id;
    if (id && window.location.pathname === '/request-claim') {
      this.requestClaimService.loadRequestIfNeeded(id);
    } else {
      this.requestClaimService.step = 1;
      this.requestClaimService.getData();
    }
    if (this.store.selectSnapshot(AUTH_STATE_TOKEN).user) {
      this.userData = this.store.selectSnapshot(AUTH_STATE_TOKEN).user;
      this.personalData = this.userData.phone;
      this.noPhone = !this.personalData;
    }
  }

  get step() {
    return this.requestClaimService.step;
  }

  getStepClass(step): string {
    if (this.step === step) {
      return 'steps__element--active';
    } else if (this.step > step) {
      return 'steps__element--finished';
    } else {
      return '';
    }
  }

  claimValidation() {
    const userData = this.requestClaimService.userData;
    const docs = this.requestClaimService.docs;
    if (userData) {
      return !(
        userData.reason &&
        userData.action &&
        userData.defendant &&
        docs.every(
          (el) =>
            (!el.disabled && !el.files.length) ||
            (!el.disabled && el.files.length)
        )
      );
    } else {
      return true;
    }
  }

  getSuccessPhoneConfirm(evt: boolean): boolean {
    return (this.noPhone = evt);
  }

  getPersonalDataStatus(evt: boolean) {
    return (this.personalData = evt);
  }

  nextStep() {
    switch (this.step) {
      case 1:
        if (!this.claimValidation()) {
          this.requestClaimService.sendClaim();
        }
        break;
      case 2:
        this.requestClaimService.sendTariff();
        break;
    }
  }

  backStep() {
    if (this.requestClaimService.step === 1) {
      this.router.navigate(['/index']);
    }
    this.requestClaimService.step -= 1;
  }

  ngOnDestroy() {
    localStorage.removeItem('payer');
  }
}
