import {Component, EventEmitter, OnChanges, HostListener, OnInit, Output, Input} from '@angular/core';
import {RequestClaimService} from '../shared/request-claim.service';
import {HttpService} from '../../../services/http.service';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {IDefendant} from '../claim.model';
import allowedTypes from '../../../../../../shared/types/allowed-types';
import * as _ from 'lodash';

@Component({
  selector: 'app-modal-edit-doc',
  templateUrl: './modal-edit-doc.component.html',
  styleUrls: ['./modal-edit-doc.component.scss']
})
export class ModalEditDocComponent implements OnInit, OnChanges {
  @Output() modalEvent = new EventEmitter();
  @Output() whatWant = new EventEmitter<string>();
  @Output() who = new EventEmitter<string>();
  @Output() what = new EventEmitter<string>();
  @Input() type: string;
  modalType: string;
  filesSaved = []; // сохраненные файлы
  contractDocsFiles = []; // временные файлы документов в модальном окне
  confirmingDocsFiles = []; // временные файлы подтверждающих документов в модальном окне
  paymentDocsFiles = []; // временные файлы об оплате в модальном окне
  otherDocsFiles = []; // временные файлы иных документов в модальном окне
  moreDocsFiles = []; // временные файлы прочих документов в модальном окне
  linkContract; // временная ссылка в модальном окне для договора
  linkConfirmingDocs; // временная ссылка в модальном окне для подтверждающих документов
  linkPaymentDocs; // временная ссылка в модальном окне для документов по оплате
  linkOtherDocs; // временная ссылка в модальном окне для прочи
  linkMoreDocs; // временная ссылка в модальном окне
  linkSaved; // сохраненная ссылка
  fileError = ''; // ошибка загрузки файла
  reason; // что случилось?
  action; // что вы хотите от должника?
  defendantForm: FormGroup;
  defendant: IDefendant; // должник

  massiveGuessWhat = this.requestClaimService.reasons;

  constructor(
    readonly requestClaimService: RequestClaimService,
    private httpService: HttpService,
    private fb: FormBuilder,
  ) {
  }

  ngOnInit(): void {
    this.initDefendantForm();
    this.setFiles();
    if (this.requestClaimService.data) {
      for (const prop of Object.getOwnPropertyNames(this.requestClaimService.data)) {
        delete this.requestClaimService.data[prop];
      }
    }
  }

  ngOnChanges(): void {
    if (this.type) {
      this.modalType = this.type;
    }
  }

  initDefendantForm() {
    this.defendantForm = this.fb.group({
      nameOrganization: new FormControl('')
    });
  }

  closeModal() {
    this.modalEvent.emit();
  }

  onRadioChange(label) {
    this.whatWant.emit(label);
  }

  onTextAreaChange(event) {
    this.action.text = event.target.value;
    console.log(this.action);
  }

  onTextChange(event) {
    this.who.emit(event.target.value);
  }

  @HostListener('document:keyup', ['$event']) // 27=esc
  keyup(event: KeyboardEvent): void {
    if (event.keyCode === 27) {
      this.closeModal();
    }
  }

  /** Событие загрузки файла
   */
  fileChangeEvent(fileInput: any, files: any) {
    if (this.fileError?.length) {
      this.fileError = '';
    }
    if (fileInput.target.files && fileInput.target.files[0]) {
      if (this.fileError?.length) {
        this.fileError = '';
      }
      if (fileInput.target.files?.length) {
        let filesUpload = [...fileInput.target.files];
        filesUpload = filesUpload.filter(el => {
          const maxSize = 12000000;
          if (el.size <= maxSize && _.includes(allowedTypes, el.type)) {
            return el;
          }
          if (el.size > maxSize) {
            this.fileError = 'Максимальный размер файла ' + maxSize / 1000000 + 'Mb';
          }
          if (!_.includes(allowedTypes, el.type)) {
            this.fileError = 'Недопустимый тип файла';
          }
        });
        if (filesUpload.length) {
          filesUpload.forEach(el => {
            const formData: FormData = new FormData();
            formData.append('file', el, el.name);
            this.httpService.post({path: 'request/file/add', body: formData, isFile: true})
              .subscribe((data: any) => {
                if (data) {
                  files.push({
                    fileName: el.name,
                    file: el,
                    path: data.path,
                    size: el.size
                  });
                  fileInput.target.value = '';
                }
              });
          });
        }
      }
    }
  }


  setFiles() {
    this.contractDocsFiles = this.requestClaimService.docs[0].files?.length ? this.requestClaimService.docs[0].files : [];
    // this.moreDocsFiles = this.claimService.docs[1].files?.length ? this.claimService.docs[1].files : [];
    this.confirmingDocsFiles = this.requestClaimService.docs[1].files?.length ? this.requestClaimService.docs[2].files : [];
    this.paymentDocsFiles = this.requestClaimService.docs[2].files?.length ? this.requestClaimService.docs[3].files : [];
    this.otherDocsFiles = this.requestClaimService.docs[3].files?.length ? this.requestClaimService.docs[4].files : [];
    this.linkContract = this.requestClaimService.docs[0].link?.length ? this.requestClaimService.docs[0].link : '';
    this.linkConfirmingDocs = this.requestClaimService.docs[1].link?.length ? this.requestClaimService.docs[1].link : '';
    this.linkPaymentDocs = this.requestClaimService.docs[2].link?.length ? this.requestClaimService.docs[2].link : '';
    this.linkOtherDocs = this.requestClaimService.docs[3].link?.length ? this.requestClaimService.docs[3].link : '';
  }

  trimString(str, count) {
    return str?.length > count ? str.slice(0, count) + '...' : str;
  }

  /** Удаление файла
   */
  removeFile(index, array) {
    switch (array) {
      case this.contractDocsFiles:
        this.contractDocsFiles.splice(index, 1);
        this.requestClaimService.docs[0].files = this.contractDocsFiles;
        if (this.requestClaimService.docs[0].files.length === 0) {
          this.requestClaimService.docs[0].disabled = true;
        }
        break;
      // case this.moreDocsFiles:
      //   this.moreDocsFiles.splice(index, 1);
      //   this.claimService.docs[1].files = this.moreDocsFiles;
      //   if (this.claimService.docs[1].files.length === 0) {
      //     this.claimService.docs[1].disabled = true;
      //   }
      //   break;
      case this.confirmingDocsFiles:
        this.confirmingDocsFiles.splice(index, 1);
        this.requestClaimService.docs[1].files = this.confirmingDocsFiles;
        if (this.requestClaimService.docs[1].files.length === 0) {
          this.requestClaimService.docs[1].disabled = false;
        }
        break;
      case this.paymentDocsFiles:
        this.paymentDocsFiles.splice(index, 1);
        this.requestClaimService.docs[2].files = this.paymentDocsFiles;
        if (this.requestClaimService.docs[2].files.length === 0) {
          this.requestClaimService.docs[2].disabled = false;
        }
        break;
      case this.otherDocsFiles:
        this.otherDocsFiles.splice(index, 1);
        this.requestClaimService.docs[3].files = this.otherDocsFiles;
        if (this.requestClaimService.docs[3].files.length === 0) {
          this.requestClaimService.docs[3].disabled = false;
        }
        break;
    }
  }

  /** Сброс изменений и закрытие модального окна
   */
  cancelModal(files) {
    files = [];
    this.closeModal();
  }

  /** Сохранение данных из модального окна только при нажатии на кнопку "Сохранить"
   */
  safeModal(files, type) {
    switch (type) {
      case 'docs':
        if (files.length) {
          this.requestClaimService.docs[0].files = files;
          files.forEach(el => {
            this.requestClaimService.docs[0].filesLinks.push(el.path);
          });
          this.requestClaimService.docs[0].disabled = false;
        } else {
          this.requestClaimService.docs[0].filesLinks = [];
        }
        if (this.linkContract.length) {
          this.requestClaimService.docs[0].link = this.linkContract;
          this.requestClaimService.docs[0].disabled = false;
        } else {
          this.requestClaimService.docs[0].link = '';
        }
        break;
      // case 'moreDocs':
      //   if (files.length) {
      //     this.claimService.docs[1].files = files;
      //     files.forEach(el => {
      //       this.claimService.docs[1].filesLinks.push(el.path);
      //     });
      //     this.claimService.docs[1].disabled = false;
      //   } else {
      //     this.claimService.docs[1].filesLinks = [];
      //   }
      //   break;
      case 'confirmingDocs':
        if (files.length) {
          this.requestClaimService.docs[1].files = files;
          files.forEach(el => {
            this.requestClaimService.docs[1].filesLinks.push(el.path);
          });
          this.requestClaimService.docs[1].disabled = false;
        } else {
          this.requestClaimService.docs[1].filesLinks = [];
        }
        if (this.linkConfirmingDocs.length) {
          this.requestClaimService.docs[1].link = this.linkConfirmingDocs;
          this.requestClaimService.docs[1].disabled = false;
        } else {
          this.requestClaimService.docs[1].link = '';
        }
        break;
      case 'paymentDocs':
        if (files.length) {
          this.requestClaimService.docs[2].files = files;
          files.forEach(el => {
            this.requestClaimService.docs[2].filesLinks.push(el.path);
          });
          this.requestClaimService.docs[2].disabled = false;
        } else {
          this.requestClaimService.docs[2].filesLinks = [];
        }
        if (this.linkPaymentDocs.length) {
          this.requestClaimService.docs[2].link = this.linkPaymentDocs;
          this.requestClaimService.docs[2].disabled = false;
        } else {
          this.requestClaimService.docs[2].link = '';
        }
        break;
      case 'otherDocs':
        if (files.length) {
          this.requestClaimService.docs[3].files = files;
          files.forEach(el => {
            this.requestClaimService.docs[3].filesLinks.push(el.path);
          });
          this.requestClaimService.docs[3].disabled = false;
        } else {
          this.requestClaimService.docs[3].filesLinks = [];
        }
        if (this.linkOtherDocs.length) {
          this.requestClaimService.docs[3].link = this.linkOtherDocs;
          this.requestClaimService.docs[3].disabled = false;
        } else {
          this.requestClaimService.docs[3].link = '';
        }
        break;
    }
    this.closeModal();
  }

  setValueDefendant() {
    if (this.requestClaimService.data?._source) {
      this.requestClaimService.dropdown = false;
      this.defendantForm.controls.nameOrganization.setValue(this.requestClaimService.data._source.name);
      let regionName = this.requestClaimService.data._source.addressHistory[this.requestClaimService.data._source.addressHistory.length - 1]?.regionName?.toLowerCase();
      regionName = regionName[0].toUpperCase() + regionName.slice(1) + ' ';
      const regionType = this.requestClaimService.data._source.addressHistory[this.requestClaimService.data._source.addressHistory.length - 1]?.regionType?.toLowerCase();
      this.defendant = {
        nameOrganization: this.requestClaimService.data._source.name,
        inn: this.requestClaimService.data._source.TIN,
        ogrn: this.requestClaimService.data._source.PSRN.toString(),
        region: `${regionName + regionType}`
      };
    } else {
      this.defendantForm.controls.nameOrganization.setValue('Не знаю');
    }
  }

  saveDefendant() {
    if (this.defendant !== undefined) {
      this.requestClaimService.userData.defendant = this.defendant;
      this.requestClaimService.defendantLabel = this.defendant.nameOrganization;
    } else {
      this.requestClaimService.userData.defendant = 'Не знаю';
      this.requestClaimService.defendantLabel = 'Не знаю';
    }
    this.closeModal();
  }

  editReason(reason) {
    this.requestClaimService.sendReason(reason);
    this.requestClaimService.actionSelected = false;
    this.closeModal();
  }

  editAction(action) {
    if (this.action.id === 'a3001') {
      this.requestClaimService.actionLabel = `${action.title}: ${action.text}`;
    }
    this.requestClaimService.actionSelected = true;
    this.requestClaimService.endOfFirstStep = true;
    this.requestClaimService.sendAction(action);
    this.closeModal();
  }
}

