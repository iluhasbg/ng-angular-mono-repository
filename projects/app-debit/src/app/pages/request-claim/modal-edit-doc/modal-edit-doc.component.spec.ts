import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ModalEditDocComponent } from './modal-edit-doc.component';

describe('ModalEditDocComponent', () => {
  let component: ModalEditDocComponent;
  let fixture: ComponentFixture<ModalEditDocComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalEditDocComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalEditDocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
