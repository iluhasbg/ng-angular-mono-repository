import {Injectable} from '@angular/core';
import {HttpService} from '../../../services/http.service';
import {Observable} from 'rxjs';
import {
  IClaimPayment,
  IClaimRequest,
  UserDataStage1
} from '../claim.model';
import {SearchService} from '../../../../../../app-search/src/app/services/search.service';
import {Router} from '@angular/router';
import {finalize} from 'rxjs/operators';
import {ClaimService} from '../claim.service';
import { InterestService } from '../../../../../../shared/services/interest.service';

@Injectable({
  providedIn: 'root'
})

export class RequestClaimService {

  userData: UserDataStage1 = {
    defendant: '',
    reason: {},
    action: {},
    docs: [],
    comment: ''
  };
  step = 0;
  reasons: object[];
  docs = [
    {
      name: 'docs',
      files: [],
      filesLinks: [],
      link: '',
      disabled: true,
      representation: 'Договор'
    },
    {
      name: 'confirmingDocs',
      files: [],
      filesLinks: [],
      link: '',
      disabled: true,
      representation: 'Подтверждающие документы'
    },
    {
      name: 'paymentDocs',
      files: [],
      filesLinks: [],
      link: '',
      disabled: true,
      representation: 'Факт оплаты'
    },
    {
      name: 'otherDocs',
      files: [],
      filesLinks: [],
      link: '',
      disabled: true,
      representation: 'Иное'
    },
  ];
  startOfFirstStep = true;
  whatHappenedSelect = true;
  endOfFirstStep = false;
  defendant: object;
  actions: object;
  responseID: string;
  reasonTitle: string;
  actionTitle: string;
  filteredActions = [];
  data;
  dropdown: boolean;
  loadingOwner: boolean;
  defendantLabel: string;
  reasonLabel: string;
  actionLabel: string;
  actionSelected = false;
  rates = {min: null, opty: null, max: null}; // тарифы
  countRates; // количество тарифов
  activeTariff: string; // id выбранного тарифа
  duty: any; // пошлины
  sumBeforeDiscount = 0; // общая сумма (тариф + пошлины) до скидки
  paymentSum; // сумма к оплате с учетом скидки
  draft; // черновик
  isLoading = false;
  paymentType: 'online' | 'offline' | 'act' = 'online'; // тип оплаты
  payer; // плательщик

  constructor(
    private httpService: HttpService,
    private searchService: SearchService,
    private readonly router: Router,
    private claimService: ClaimService,
    private interestService: InterestService
  ) {
  }

  getData(responseID?) {
    let query = {};
    if (responseID) {
      query = {
        responseID,
        stageReadOnly: true,
      };
    } else {
      query = {
        stage: 1,
        stageReadOnly: true,
      };
    }
    this.isLoading = true;
    return this.claimService.getDataClaim(query).subscribe((data) => {
      if (data) {
        this.isLoading = false;
        this.responseID = data.responseID;
        this.defendant = data.data?.dataStage001?.defendant;
        this.reasons = data.data?.dataStage001?.reason.items;
        this.reasonTitle = data.data?.dataStage001?.reason.title;
        this.actionTitle = data.data?.dataStage001?.action.title;
        this.actions = data.data?.dataStage001?.action.items;
      }
    });
  }

  sendClaim() {
    this.userData.docs = this.docs;
    const query: IClaimRequest = {
      userData: this.userData,
      stageReadOnly: false,
      responseID: this.responseID,
      interestData: this.interestService.addInterestData(),
    };
    this.isLoading = true;
    return this.claimService.sendDataClaim(query).subscribe(data => {
      this.isLoading = false;
      this.responseID = data.responseID;
      this.getRates();
      this.router.navigate(['/request-claim'], {queryParams: {id: this.responseID}});
      this.step = 2;
    });
  }

  getLegalByName(name: string): Observable<any> {
    return this.searchService.getLegal({id: name});
  }

  sendReason(reason) {
    this.reasonLabel = reason.title;
    this.userData.reason = reason;
    for (const item in this.actions) {
      if (item === reason.id) {
        if (this.actions.hasOwnProperty(item)) {
          this.filteredActions = this.actions[item];
        }
      }
    }
    return this.filteredActions;
  }

  sendAction(action) {
    if (action.id !== 'a3001') {
      this.actionLabel = action.title;
    }
    this.userData.action = action;
  }

  getLegal(text: string) {
    if (text.length > 0) {
      this.dropdown = false;
      this.loadingOwner = true;
      this.getLegalByName(text).subscribe(data => {
        if (data?.hits?.hits?.length && text) {
          this.data = data.hits.hits.shift();
          this.dropdown = true;
          this.loadingOwner = false;
        } else if (!data?.hits?.hits?.length) {
          this.loadingOwner = false;
        }
      });
    } else {
      this.loadingOwner = false;
      this.dropdown = false;
    }
  }

  /**
   * Получение тарифов и пошлин
   */
  getRates(): void {
    this.isLoading = true;
    this.httpService.get({
      path: 'request/tariff',
      params: {responseID: this.responseID}
    }).subscribe((data: any) => {
      this.isLoading = false;
      if (data?.tariff && data.duty) {
        this.countRates = data.tariff?.length;
        data.tariff.forEach((item, index) => {
          if (index === 0) {
            this.rates.min = item;
          }
          if (index === 1) {
            this.rates.opty = item;
          }
          if (index === 2) {
            this.rates.max = item;
          }
          if (item.default && !this.activeTariff) {
            this.activeTariff = item.id;
            this.paymentSum = +item.sumDiscount + +this.duty?.sumDiscount;
            this.sumBeforeDiscount = +item.sum;
          }
        });
        this.duty = data.duty;
        this.paymentSum = +this.rates?.min?.sumDiscount + +this.duty?.sumDiscount;
        switch (this.draft?.tariff) {
          case '000000002':
          case 'Оптимальный':
            this.activeTariff = this.rates.opty.id;
            this.paymentSum = +this.rates.opty.sumDiscount + +this.duty.sumDiscount;
            this.sumBeforeDiscount = +this.rates.opty.sum + +this.duty.sum;
            break;
          case '000000003':
          case 'Максимальный':
            this.activeTariff = this.rates.max.id;
            this.paymentSum = +this.rates.max.sumDiscount + +this.duty.sumDiscount;
            this.sumBeforeDiscount = +this.rates.max.sum + +this.duty.sum;
            break;
        }
        if (!this.draft) {
          this.step = 2;
        }
      }
    });
  }

  /**
   * Отправка Тарифа
   */
  sendTariff() {
    const params = {
      responseID: this.responseID,
      tariffID: this.activeTariff,
    };
    this.isLoading = true;
    return this.claimService.requestClaimSecondStep(params)
      .pipe(
        finalize(() => this.isLoading = false)
      ).subscribe(data => {
          this.step = 3;
          this.responseID = data.responseID;
        }
      );
  }

  /**
   * Выбор типа оплаты
   * @param type: тип оплаты
   */
  setPaymentType(type: 'online' | 'offline' | 'act') {
    this.paymentType = type;
  }

  /**
   * Выбор плательщика
   * @param payerData: плательщик
   */
  setPayer(payerData) {
    this.payer = payerData;
  }

  goSuccessStage(yandexResponse) {
    this.isLoading = true;

    const query: IClaimPayment = {
      responseID: this.responseID,
      inn: this.payer?.TIN,
      ogrn: this.payer?.PSRN,
      name: this.payer?.name || this.payer?.fullName,
      address: this.payer?.address,
      sum: this.paymentSum,
      type: this.paymentType,
      yandexResponse
    };
    this.step = 4;
    return this.claimService.requestClaimPayment(query)
      .pipe(
        finalize(() => this.isLoading = false)
      )
      .subscribe((data) => {
        localStorage.removeItem('payer');
        this.responseID = data.responseID;
      });
  }

  /**
   * Запрос на восстановление заявки по id
   * @param responseID: id заявки
   */
  loadRequestIfNeeded(responseID: string): void {
    if (!this.responseID) {
      this.loadRequest(responseID);
    }
  }
  /**
   * Запрос на восстановление заявки по id
   * @param responseID: id заявки
   */
  loadRequest(responseID: string) {
    const params = {
      isDraft: true
    };
    this.getData(responseID);
    this.isLoading = true;
    this.claimService.requestContractGetDetail(params, responseID)
      .pipe(
        finalize(() => this.isLoading = false)
      )
      .subscribe(data => {
        this.draft = data.task;
        this.responseID = data.responseID;
        if (this.draft) {
          this.parseDraft();
        }
      });
  }

  parseDraft() {
    if (Object.keys(this.draft.details.userData).length > 0) {
      this.userData = this.draft.details.userData;
      this.defendantLabel = this.draft.details.userData.defendant === 'Не знаю' ? 'Не знаю' : this.draft.details.userData.defendant.nameOrganization;
      this.reasonLabel = this.draft.details.userData.reason?.title;
      this.actionLabel = this.draft.details.userData.action?.text ? `${this.draft.details.userData.action?.title}: ${this.draft.details.userData.action?.text}` : this.draft.details.userData.action?.title;
      this.startOfFirstStep = false;
      this.whatHappenedSelect = false;
      this.actionSelected = true;
      this.endOfFirstStep = true;
      this.docs = this.draft.details.userData.docs;
      this.sendReason(this.draft.details.userData.reason);
    }
    this.activeTariff = this.draft.tariff;
    switch (this.draft.lastStage) {
      case 1:
        this.getRates();
        this.step = this.draft.lastStage + 1;
        break;
      case 2:
      case 3:
        this.getRates();
        this.step = this.draft.lastStage;
        break;
      case 4:
        this.step = this.draft.lastStage;
        break;
    }
  }

  selectTariff(event) {
    this.activeTariff = event.id;
    if (this.duty) {
      this.paymentSum = +event.sumDiscount + +this.duty.sumDiscount;
      this.sumBeforeDiscount = +event.sum + +this.duty.sum;
    } else {
      this.paymentSum = +event.sumDiscount;
      this.sumBeforeDiscount = +event.sum;
    }
  }
}
