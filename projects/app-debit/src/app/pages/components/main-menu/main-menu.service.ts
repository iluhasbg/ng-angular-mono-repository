import { Injectable } from '@angular/core';
import {HttpService} from '../../../services/http.service';
import {IResponse} from '../../../models/main.interfaces';

const path = 'user';


@Injectable({
  providedIn: 'root'
})

export class MainMenuService {


  constructor(private http: HttpService) { }

  logOut(params) {
    return this.http.get<IResponse>({path: `${path}/logout`, subjectType: 'logout', params});
  }
}
