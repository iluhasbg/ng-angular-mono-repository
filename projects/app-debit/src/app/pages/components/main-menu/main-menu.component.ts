import { Component, OnDestroy, OnInit } from '@angular/core';
import { MainMenuService } from './main-menu.service';
import { HttpParams } from '@angular/common/http';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { SharedService } from '../../../services/shared.service';

// declare var $: any;

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.scss']
})
export class MainMenuComponent implements OnInit, OnDestroy {

  private readonly onDestroy = new Subject<void>();
  showDropdown = false;
  userName: string;
  showModal = false;
  tasks;
  showMenu = false;
  overlay = false;
  userData;

  constructor(private menuService: MainMenuService,
              private router: Router,
              private route: ActivatedRoute,
              private shared: SharedService,
              readonly activatedRoute: ActivatedRoute,
              readonly sharedService: SharedService,
  ) {
  }

  ngOnInit() {

    if (!localStorage.getItem('currentUserIPID')) {
      this.router.navigate(['/login']);
    }
    if (localStorage.getItem('currentUserData')) {
      this.userName = JSON.parse(localStorage.getItem('currentUserData')).name;
      this.userData = JSON.parse(localStorage.getItem('currentUserData'));
      console.log(this.userData, 'gfgfgfg');
    }

    // // TODO: rewrite me to Angular please
    // $('.js-show-modal').each(function(): void {
    //   const $context = $(this);
    //   const btn = $context.find('.js-show-modal__trigger');
    //   const modal = $context.find('.js-show-modal__modal');
    //   const close = $context.find('.js-show-modal__close');
    //   btn.on('click', (evt): void => {
    //     modal.fadeIn();
    //   });
    //   close.on('click', (): void => {
    //     modal.fadeOut();
    //   });
    // });
    this.shared.closeModals
      .pipe(takeUntil(this.onDestroy))
      .subscribe(() => {
        this.closeLogoutDropdown();
      });

    this.shared.openModal.subscribe(() => {
      console.log('subject');
      this.openModal();
    });

    this.showDropdown = window.innerWidth < 992;

    window.addEventListener('resize', () => {
      this.showDropdown = window.innerWidth < 992;
    });
  }

  toRequestPage() {
    const currentUrl = this.router.url;
    if (currentUrl.includes('/request') || currentUrl === 'undefined') {
      this.router.navigate(
        ['.'],
        { relativeTo: this.route, queryParams: { } }
      );
      setTimeout(() => {
        window.location.reload();
      }, 150);
    } else {
      this.router.navigate(['/request']);
    }
  }

  openModal() {
    this.showModal = true;
  }

  logout() {
    const token = localStorage.getItem('currentUserIPID');
    if (token) {
      const params = new HttpParams()
        .set('logout', 'true')
        .set('token', token);
      this.menuService.logOut(params).pipe(takeUntil(this.onDestroy))
        .subscribe(data => {
          console.log(data);
          localStorage.removeItem('currentUserIPID');
          localStorage.removeItem('currentUserData');
          this.router.navigate(['/login']);
          localStorage.removeItem('currentUserIPID');
        });
    } else {
      this.router.navigate(['/login']);
    }
    localStorage.removeItem('selectCase');
  }

  toggleLogOut() {
    if (window.innerWidth < 992) {
      this.showDropdown = true;
    } else {
      this.showDropdown = !this.showDropdown;
    }

    window.addEventListener('resize', () => {
      this.showDropdown = window.innerWidth < 992;
    });

  }

  makeBodyHidden(): void {
    if (window.innerWidth <= 793) {
      if (this.showMenu === true) {
        document.body.style.overflow = 'hidden';
        document.body.style.position = 'fixed';
        document.body.style.height = '100%';
        document.body.style.width = '100%';

      } else {
        document.body.style.overflow = 'visible';
        document.body.style.position = '';
        document.body.style.height = 'auto';
      }
    }
  }

  closeLogoutDropdown() {
    if (window.innerWidth > 992) {
      this.showDropdown = false;
    }
  }

  toClaim() {
    const id = this.activatedRoute.snapshot.queryParams.id;
    if (this.router.url === '/request-claim') {
      window.location.reload();
    } else if (id && window.location.pathname === '/request-claim') {
      this.router.navigate(['/request-claim'], {queryParams: {}}).then(result => {
        if (result === true) {
          console.log(3);
          window.location.reload();
        }
      });
    } else if (!id || (id && window.location.pathname !== '/request-claim')) {
      this.router.navigate(['/request-claim']);
    }
  }

  ngOnDestroy(): void {
    this.onDestroy.next();
    localStorage.removeItem('currentUserIPID');
  }

}
