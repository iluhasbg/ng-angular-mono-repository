import { Component, OnInit } from '@angular/core';
import { SupportChatService } from '../../../../../../shared/services/support-chat.service'
import { document } from 'ngx-bootstrap/utils';
import { IntercomEventsService } from 'projects/shared/services/intercom-events.service';

@Component({
  selector: 'app-expert',
  templateUrl: './expert.component.html',
  styleUrls: ['./expert.component.scss']
})
export class ExpertComponent implements OnInit {

  btns = ['Вопрос по текущему делопроизводству', 'Консультация по претензии или иску', 'Запрос первичных документов',
    'Консультация по услугам', 'Другое'];

  constructor(
    private readonly supportChatService: SupportChatService,
    private intercomEventsService: IntercomEventsService,
  ) {
  }

  onClickBtn() {
    this.intercomEventsService.push({event: 'clickAskPP'});
    this.supportChatService.open();
  }

  ngOnInit(): void {
  }

}
