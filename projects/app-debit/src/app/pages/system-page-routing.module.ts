import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../shared/guardes/auth-guard.service';
import { SystemComponent } from './system.component';
import { IndexComponent } from './index/index.component';
import { IndexItemComponent } from './index/index-item/index-item.component';
import { IndexTariffComponent } from './index/index-tariff/index-tariff.component';
import { IndexPaymentComponent } from './index/index-payment/index-payment.component';
import { TestComponent } from './test/test.component';
import {RequestClaimComponent} from './request-claim/request-claim.component';
import {SurchargeRequestComponent} from './index/index-item/surcharge-request/surcharge-request.component';
import {ChangeProfileComponent} from './user/change-profile/change-profile.component';

const systemRoutes: Routes = [
  {
    path: '', component: SystemComponent, canActivate: [AuthGuard], children: [
      {path: '', redirectTo: 'index', pathMatch: 'full'},
      {
        path: 'index',
        component: IndexComponent,
      },
      {
        path: 'index/item',
        component: IndexItemComponent,
      },
      {
        path: 'index/tariff',
        component: IndexTariffComponent,
      },
      {
        path: 'index/payment',
        component: IndexPaymentComponent,
      },
      {
        path: 'request-claim',
        component: RequestClaimComponent,
      },
      {
        path: 'test',
        component: TestComponent,
      },
      {
        path: 'test/:formId',
        component: TestComponent,
      },
      {
        path: 'surcharge-request',
        component: SurchargeRequestComponent
      },
      {
        path: 'user/change-profile',
        component: ChangeProfileComponent
      }
    ],
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(systemRoutes),
  ], exports: [
    RouterModule,
  ],
})
export class SystemPageRoutingModule {
}
