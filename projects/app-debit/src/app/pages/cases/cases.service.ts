import { Injectable } from '@angular/core';
import { HttpService } from '../../services/http.service';
import {IArchiveFiles, IResponse, ITasks, IWidgets} from '../../models/main.interfaces';

const path = 'tasks';
const eventsPath = 'events';
const newCasesPath = 'cases';

@Injectable({
  providedIn: 'root'
})
export class CasesService {

  constructor(private http: HttpService) { }

  fetchTasks(params) {
    return this.http.get<IResponse>({path: `${path}`, subjectType: 'tasks', params});
  }

  getTasks(params) {
    return this.http.get<ITasks>({path: `${path}`, subjectType: 'tasks', params});
  }

  getWidgets() {
    return this.http.get<IWidgets>({path: `${path}/debt-widgets`});
  }

  fetchTasksById(params) {
    return this.http.get<IResponse>({path: `${path}`, subjectType: 'tasks', params});
  }

  fetchTracks(params) {
    return this.http.get({path: `${path}/tracks`, subjectType: 'tracks', params});
  }

  fetchEvents(params) {
    return this.http.get({path: `${eventsPath}/events`, subjectType: 'events', params});
  }

  addQuestion(params, body) {
    return this.http.post({path: `${path}/question`, subjectType: 'question', params, body});
  }

  addComment(params, body) {
    return this.http.post({path: `${eventsPath}/comment`, subjectType: 'comment', params, body});
  }

  closeAgreement(params) {
    return this.http.get({path: `${eventsPath}/read`, subjectType: 'events', params});
  }

  eventAgree(params) {
    return this.http.get({path: `${eventsPath}/agree`, subjectType: 'events', params});
  }

  deleteTask(params) {
    return this.http.get<IResponse>({path: `${path}/delete`, subjectType: 'delete task', params});
  }

  getCases() {
    return this.http.get({path: `${newCasesPath}`, subjectType: 'cases'});
  }

  getCaseArchives(params) {
    return this.http.get<IArchiveFiles>({path: `${newCasesPath}/case_archives`, subjectType: 'cases', params});
  }

  getCase(params, id) {
    return this.http.get<ITasks>({path: `${newCasesPath}/${id}`, subjectType: 'cases', params});
  }
}
