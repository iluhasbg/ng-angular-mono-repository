import { Component, OnInit } from '@angular/core';
import { ScriptLoaderService } from '../../../shared/services/script-loader.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'app-ipid';
  constructor(
    private scriptLoaderService: ScriptLoaderService,
  ) {
  }

  ngOnInit() {
    this.scriptLoaderService
      .loadScript(`https://services.wiseadvice.ru/tracker/js/v1/wa.min.js`)
      // .loadScript(`/assets/js/wa.min.js`)
      .subscribe((data) =>
        console.log(
          `console.log('%c Custom scripts is Loaded! ', 'background: #222; color: #bada55');`,
        )
      );
  }
}
