import { setCompodocJson } from "@storybook/addon-docs/angular";
const docJson = require('../documentation.json');

setCompodocJson(docJson);


console.log('compododod', setCompodocJson, docJson);
export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
}
