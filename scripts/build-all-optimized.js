const path = require('path');
const fs = require('fs');
const fse = require('fs-extra');

const args = require('args-parser')(process.argv);

const { getBrandList, getDebitBrands, getIpidBrands } = require('./common');
const { ngBuild } = require('./ng-build');
const {
  compileBrandVariables,
  compileBrandEnvironment,
  compileBrandStyles,
} = require('./lib/build-theme');
const { injectTheme } = require('./lib/inject-theme');

const IS_CI_BUILD = !!process.env.BUILD_FROM_CI && !args.prod;

if (IS_CI_BUILD) {
  console.warn('REMOVING STATIC FILES...');
  fse.removeSync(
    path.join(__dirname, '../../projects/app-ipid/src/assets/static-files')
  );
}

let flag = ['--configuration', 'development'];
if (args.prod) {
  flag = ['--configuration', 'production'];
}

let defaultProj;
let brands;

if (args.debit) {
  defaultProj = 'debit-default';
  brands = getDebitBrands();
} else {
  defaultProj = 'ipid';
  brands = getIpidBrands();
}

console.time('compile');
const outDir = ngBuild(defaultProj, [...flag], args.prod === true);
console.timeEnd('compile');

const BUILD_DIR = path.join(__dirname, '../dist');

(async () => {
  await fse.remove(path.join(outDir, 'assets/static-files'));
  brands = brands.filter((brand) => brand !== defaultProj);

  console.log('copying project files...');
  console.time('copyProjectFiles');
  for (const brand of brands) {
    const srcDir = path.join(BUILD_DIR, `app-${brand}`);
    await fse.remove(srcDir);
    await fse.copy(outDir, srcDir);
  }
  console.timeEnd('copyProjectFiles');

  console.log('injecting themes..');

  console.time('injectThemes');
  for (const brand of brands) {
    const srcDir = path.join(BUILD_DIR, `app-${brand}`);
    const [variablesCss, styles, envSource] = await Promise.all([
      compileBrandVariables(brand),
      compileBrandStyles(brand),
      compileBrandEnvironment(brand),
    ]);

    await injectTheme(srcDir, envSource, variablesCss, styles);
  }
  console.timeEnd('injectThemes');
})().catch((e) => {
  console.error(e);
  process.exit(1);
});
