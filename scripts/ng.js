const spawn = require('cross-spawn');

module.exports.ng = (action, args) => {
  let [ brand, ...customArgs ] = args;

  const cmd = ['ng', action, `app-${brand}`, ...customArgs];

  if (!brand) {
    console.log('not specified brand');
    process.exit(1);
  }

  console.log(`exec: yarn ${cmd.join(' ')}`);
  spawn.sync('yarn', cmd, { stdio: 'inherit' });
};
