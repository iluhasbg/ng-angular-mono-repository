const args = require('args-parser')(process.argv);

const { getBrandList } = require('./common');
const { ngBuild } = require('./ng-build');

const brandList = getBrandList();
let flag = ['--configuration', 'development'];
if (args.prod) {
  flag = ['--configuration', 'production'];
}

for (const brand of brandList) {
  // если у нас сборка всех брендов дебита и этот бренд не дебит - то
  if (args.debit && !brand.startsWith('debit-')) {
    continue;
  }
  // если у нас сборка всех НЕ дебит брендов, а этот бренд Дебит
  if (!args.debit && brand.startsWith('debit-')) {
    continue;
  }

  ngBuild(brand, [...flag], args.prod === true);
}
