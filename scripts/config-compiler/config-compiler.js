const fs = require('fs');
const path = require('path');

const { compileBrandIpidConfig } = require('./compile-brand-ipid-config');
const { getBrandList } = require('../common');
const { compileBrandDebitConfig } = require('./compile-brand-debit-config');

const compileConfigAndSave = (module.exports.compileConfigAndSave = (
  params
) => {
  const angularJSON = compileConfig(params);
  fs.writeFileSync(
    path.join(__dirname, '../../angular.json'),
    JSON.stringify(angularJSON, null, 2)
  );
});

const compileBrandConfig = (brand, port) => {
  if (brand.startsWith('debit')) {
    return compileBrandDebitConfig(brand, port);
  } else {
    return compileBrandIpidConfig(brand, port);
  }
};

const compileConfig = (module.exports.compileConfig = (params) => {
  const brandList = getBrandList();
  const angularJSON = require('../../angular.json');

  const staticAppProjects = ['app-search', 'app-search-ls'];
  // удаляем все аппы из angular-json
  for (const key of Object.keys(angularJSON.projects)) {
    // app-search и app-search-ls нельзя сносить)
    // лучше бы конечно app-search, app-search-ls, app-debit переименовать и убрать app, но пока так
    if (key.startsWith('app-') && !staticAppProjects.includes(key)) {
      delete angularJSON.projects[key];
    }
  }

  let port = 4200;
  for (const brand of brandList) {
    angularJSON.projects = {
      ...angularJSON.projects,
      ...compileBrandConfig(brand, port),
    };
    port++;
  }

  return angularJSON;
});
