module.exports.compileBrandDebitConfig = (brand, port) => {
  const brandPath = `projects/themes/${brand}`;

  return {
    [`app-${brand}`]: {
      projectType: 'application',
      schematics: {
        '@schematics/angular:component': {
          style: 'scss',
        },
      },
      root: 'projects/app-debit',
      sourceRoot: 'projects/app-debit/src',
      prefix: 'app',
      architect: {
        build: {
          builder: '@angular-devkit/build-angular:browser',
          options: {
            outputPath: `dist/app-${brand}`,
            index: 'projects/app-debit/src/index.html',
            main: 'projects/app-debit/src/main.ts',
            polyfills: 'projects/app-debit/src/polyfills.ts',
            tsConfig: 'projects/app-debit/tsconfig.app.json',
            aot: true,
            assets: [
              'projects/app-debit/src/favicon.ico',
              'projects/app-debit/src/assets',
            ],

            stylePreprocessorOptions: {
              includePaths: [brandPath],
            },

            styles: [
              `projects/app-debit/src/styles.scss`,
              `${brandPath}/app.style.scss`,
              './node_modules/intl-tel-input/build/css/intlTelInput.css',
            ],
            scripts: [
              './node_modules/intl-tel-input/build/js/intlTelInput.min.js',
            ],
          },
          configurations: {
            production: {
              fileReplacements: [
                {
                  replace: 'projects/app-debit/src/environments/environment.ts',
                  with: `${brandPath}/app.environment.prod.ts`,
                },
              ],
              optimization: true,
              outputHashing: 'all',
              sourceMap: false,
              extractCss: true,
              namedChunks: false,
              extractLicenses: true,
              vendorChunk: false,
              buildOptimizer: true,
              budgets: [
                {
                  type: 'initial',
                  maximumWarning: '6.5mb',
                  maximumError: '7mb',
                },
                {
                  type: 'anyComponentStyle',
                  maximumWarning: '43kb',
                  maximumError: '45kb',
                },
              ],
            },
            [`loc`]: {
              fileReplacements: [
                {
                  replace: 'projects/app-debit/src/environments/environment.ts',
                  with: `${brandPath}/app.environment.ts`,
                },
              ],
            },
            development: {
              buildOptimizer: false,
              optimization: false,
              vendorChunk: true,
              extractLicenses: false,
              sourceMap: true,
              namedChunks: true,
              fileReplacements: [
                {
                  replace: 'projects/app-debit/src/environments/environment.ts',
                  with: `${brandPath}/app.environment.ts`,
                },
              ],
            },
            defaultConfiguration: 'development',
          },
        },
        serve: {
          builder: '@angular-devkit/build-angular:dev-server',
          options: {
            browserTarget: `app-${brand}:build`,
            port: port,
          },
          configurations: {
            production: {
              browserTarget: `app-${brand}:build:production`,
            },
            loc: {
              browserTarget: `app-${brand}:build:loc`,
            },
            development: {
              browserTarget: `app-${brand}:build:development`,
            },
            defaultConfiguration: 'development',
          },
        },

        'extract-i18n': {
          builder: '@angular-devkit/build-angular:extract-i18n',
          options: {
            browserTarget: `app-${brand}:build`,
          },
        },

        lint: {
          builder: '@angular-devkit/build-angular:tslint',
          options: {
            tsConfig: [
              'projects/app-debit/tsconfig.app.json',
              'projects/app-debit/tsconfig.spec.json',
              'projects/app-debit/e2e/tsconfig.json',
            ],
            exclude: ['**/node_modules/**'],
          },
        },
      },
    },
  };
};
