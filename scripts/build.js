const args = require("args-parser")(process.argv);

const { ngBuild } = require('./ng-build');

let flag = ['-c', 'development'];
if (args.prod) {
  flag = ['--configuration', 'production'];
}

const [brand, ...restArgs] = process.argv.slice(2).filter(i => i !== '--prod');

ngBuild(brand, [...restArgs, ...flag], args.prod === true);
