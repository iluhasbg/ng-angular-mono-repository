const fs = require('fs').promises;
const webpack = require('webpack');
const os = require('os');
const path = require('path');

const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');

module.exports.compileTs = (src, libraryName) => {
  const tmpDir = path.join(os.tmpdir(), 'dist');
  const configFile = path.join(__dirname, '../../tsconfig.app.json');

  return new Promise((resolve, reject) => {
    webpack(
      {
        entry: src,
        cache: false,
        module: {
          rules: [
            {
              test: /\.tsx?$/,
              use: {
                loader: 'ts-loader',
                options: {
                  configFile,
                },
              },
              exclude: /node_modules/,
            },
          ],
        },
        resolve: {
          extensions: ['.tsx', '.ts', '.js'],
          plugins: [
            new TsconfigPathsPlugin({
              configFile,
            }),
          ],
        },
        optimization: {
          minimize: false,
        },
        output: {
          filename: 'bundle.js',
          path: tmpDir,
          library: libraryName,
          libraryTarget: 'window',
          clean: true,
        },
      },
      (err, result) => {
        if (err || result.hasErrors()) {
          console.log(result.toString({ colors: true }));
          return reject(err || result.toJson());
        }

        fs.readFile(path.join(tmpDir, 'bundle.js'), 'utf8')
          .then(resolve)
          .catch(reject);
      }
    );
  });
};
