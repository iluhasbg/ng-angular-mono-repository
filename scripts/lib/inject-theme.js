const fs = require('fs').promises;
const path = require('path');

module.exports.injectTheme = async (buildDir, envScript, variables, styles) => {
  const indexHtmlPath = path.join(buildDir, 'index.html');

  let indexHtml = await fs.readFile(indexHtmlPath, 'utf8');

  indexHtml = indexHtml.replace(
    /<head>/,
    `<head>\n<style>${variables}</style>`
  );
  indexHtml = indexHtml.replace(
    /<\/head>/,
    `<style>${styles}</style><script>${envScript}</script>\n</head>`
  );

  await fs.writeFile(indexHtmlPath, indexHtml);
};
