const fs = require('fs').promises;
const path = require('path');
const sass = require('sass');
const { compileTs } = require('./compile-typescript');

const THEMES_DIR = path.join(__dirname, '../../projects/themes');

module.exports.compileBrandEnvironment = async function compileEnvironment(
  theme,
  prod = false
) {
  const appEnvironmentPath = path.join(
    THEMES_DIR,
    theme,
    `app.environment${prod ? '.prod' : ''}.ts`
  );
  return await compileTs(appEnvironmentPath, 'GardiumEnv');
};

module.exports.compileBrandVariables = async function compileBrandCss(theme) {
  const vars = await getThemeVariables(theme);

  const varsScss = Object.keys(vars).map((name) => `--${name}: ${vars[name]}`);

  return `
:root {
  ${varsScss.join(';\n  ')}
}
  `;
};

module.exports.compileBrandStyles = async function compileBrandStyles(theme) {
  const result = await sass.compileAsync(
    path.join(THEMES_DIR, theme, 'app.style.scss')
  );
  return result.css;
};

async function getThemeVariables(theme) {
  const file = await fs.readFile(
    path.join(THEMES_DIR, theme, 'variables.scss'),
    'utf8'
  );

  const vars = {};
  const result = file.matchAll(/\$(.*?): (.*?);/g);

  for (const [, name, val] of result) {
    vars[name] = val;
  }

  return vars;
}
