const { ng } = require('./ng');
const fse = require('fs-extra');
const path = require('path');

const buildDir = path.join(__dirname, '../build');
const distDir = path.join(__dirname, '../dist');

fse.ensureDirSync(buildDir);

module.exports.ngBuild = (brand, args = [], moveToBuild = false) => {
  ng('build', [brand, ...args]);

  if (moveToBuild) {
    const appDir = `app-${brand}`;

    fse.ensureDirSync(buildDir);
    fse.removeSync(path.join(buildDir, appDir));
    fse.moveSync(path.join(distDir, appDir), path.join(buildDir, appDir));
  }

  return path.join(distDir, `app-${brand}`);
};
