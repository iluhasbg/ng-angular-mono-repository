const path = require('path');
const fs = require('fs');

const THEMES_DIR = (module.exports.THEMES_DIR = path.join(
  __dirname,
  '../projects/themes'
));

module.exports.getIpidBrands = () => {
  return getBrandList().filter((i) => !i.startsWith('debit'));
};

module.exports.getDebitBrands = () => {
  return getBrandList().filter((i) => i.startsWith('debit'));
};

const getBrandList = (module.exports.getBrandList = () => {
  // const disabledItems = ['shared', '.', 'ipid', 'mts', 'tochka'];
  const disabledItems = ['shared', '.', 'mts', 'tochka', 'yookassa', 'tinkoff', 'psb', 'debit', 'debit-default'];

  const brands = fs
    .readdirSync(THEMES_DIR)
    .filter((i) => !disabledItems.includes(i));

  const brandBirthtime = new Map();
  for (const brand of brands) {
    const stat = fs.statSync(path.join(THEMES_DIR, brand));
    brandBirthtime.set(brand, stat.birthtimeMs);
  }

  return brands.sort((a, b) => brandBirthtime.get(a) - brandBirthtime.get(b));
});
