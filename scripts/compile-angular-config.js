const { THEMES_DIR } = require("./common");
const { compileConfigAndSave } = require("./config-compiler/config-compiler");

compileConfigAndSave({ themesDir: THEMES_DIR });
