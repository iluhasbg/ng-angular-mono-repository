const fs = require('fs');
const path = require('path');
const { execSync } = require('child_process');
const args = require("args-parser")(process.argv);

const { compileConfigAndSave } = require("./config-compiler/config-compiler");

const { THEMES_DIR } = require("./common");

if (!args.name || args.name.length < 2) {
  exit('Укажите параметр --name=имя_нового_бренда');
}

if (checkBrandExists(args.name)) {
  exit(`Бренд "${args.name}" уже существует`);
}

// по умолчанию базовый бренд - ipid
const baseBrand = args.base && checkBrandExists(args.base) ? args.base : 'ipid';

const srcBrand = path.join(THEMES_DIR, baseBrand);
const dstBrand = path.join(THEMES_DIR, args.name.toLowerCase());

// Копируем бренд
execSync(`cp -r ${srcBrand} ${dstBrand}`);

// Компилим название
compileConfigAndSave({ themesDir: THEMES_DIR });

function checkBrandExists(brand) {
  return fs.existsSync(path.join(THEMES_DIR, brand));
}

function exit (message) {
  console.error(message);
  process.exit(1);
}
