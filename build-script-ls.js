const fse = require('fs-extra');
const fs = require('fs');
const concat = require('concat');
const { ng } = require('./scripts/ng');
const spawn = require('cross-spawn');

(async function build() {
  const brands = ['legalsupport', 'brandsearch', 'open'];

  for (const brand of brands) {
    await buildBrand(brand);
  }
})();

async function buildBrand(brand) {
  await ng('build', ['search-ls', `-c`, `${brand}-prod`]);

  const files = [
    './dist/app-search-ls/runtime-es5.js',
    './dist/app-search-ls/polyfills-es5.js',
    './dist/app-search-ls/main-es5.js',
  ];

  const destDir = `./elements-ls/${brand}`;

  await fse.remove(destDir);
  await fse.ensureDir(destDir);

  await concat(files, `${destDir}/search.js`);
  await fse.copy('./dist/app-search-ls/assets/', `${destDir}/assets/`);
  await fse.copy('./elements-ls/index-sample.html', `${destDir}/index.html`);

  await fse.copy('./projects/app-search-ls/docs', `${destDir}/docs`);

  spawn.sync('zip', ['./search.zip', '-r', `${destDir}`], { stdio: 'inherit' });
  await fse.move(`./search.zip`, `${destDir}/docs/search.zip`);
}
